简搭(jabdp) 低代码开发平台（开源版）
===============
当前最新版本： 1.2.0（更新时间：20231023）

# 简介
简搭（jabdp）低代码平台，是一款引擎模式的web快速开发平台，采用新颖的在线开发部署模式，配置式，低代码，使开发变得更加简单、纯粹，提高了开发的效率。
> 复杂的业务功能，只需要会基本的sql语句和javascript语法，就能进行快速开发，满足其个性化的业务需求，设计出各种复杂的企业web应用。既能快速提高开发效率，帮助公司节省人力成本，同时又有效解决企业级项目中常遇到的改需求的问题，不失灵活性。

#  项目说明


| 项目名                | 说明                     | 传送门                                                                                                                          |
|--------------------|------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| `jadbp` | 简搭低代码平台V1.2.0 |  [Gitee](https://gitee.com/jabdp/jabdp) |

# 技术文档

- 官方文档：[https://jabdp.7yxx.com/doc](https://jabdp.7yxx.com/doc)
- 官方网站：[https://jabdp.7yxx.com](https://jabdp.7yxx.com)
- 官方案例：[https://jabdp.7yxx.com/html/case.html](https://jabdp.7yxx.com/html/case.html)
- 商业授权：[https://jabdp.7yxx.com/html/vip.html](https://jabdp.7yxx.com/html/vip.html)
- 快速入门：[零基础文档](https://jabdp.7yxx.com/html/tutorial.html) |  [入门视频(西瓜视频)](https://www.ixigua.com/7080700792390615567?id=7078990307265610240) | [入门视频(Bilibili)](https://www.bilibili.com/video/BV1Wi4y1Q7Dr?share_source=copy_web)
- 部署教程：[简搭开源版部署教程](https://www.ixigua.com/7139045759882232350) （持续更新中……）

# 简搭开源版使用手册
> 获取方式
- star本项目
- 今日头条中搜索“简搭jabdp低代码”并关注
- 微信中搜索“简搭jabdp”公众号并关注

完成以上事项后（需要截图），加群[801507856](https://jq.qq.com/?_wv=1027&k=5BK7PIw)，私聊管理员（jabdp运营支持），发送截图获取【简搭开源版使用手册】！

# 加入讨论群

> QQ交流群

[801507856](https://jq.qq.com/?_wv=1027&k=5BK7PIw)

> 微信交流群

![微信交流一群](doc/img/WeChat1.png)

![微信交流二群](doc/img/WeChat2.png)

# 目录结构
- ae是基于activiti explorer的流程设计器。
- jabdp-designer是表单、业务设计器。
- jabdp-jwp是业务模型的数据结构。
- jabdp-platform是应用端。

# 开发环境
- 语言：Java 8
- IDE： IDEA / Eclipse
- 依赖管理：Maven
- 数据库脚本：MySQL5.7+ / Sqlserver2008+
- 服务器：tomcat 8.0+ 

# 部署步骤

1. maven install部署jabdp-jwp到本地repository;
2. maven package打包jabdp-designer为iDesigner.war；
3. maven package打包jabdp-platform为iPlatform.war；
4. 在maven的本地仓库中找到sevenzipjbinding-9.20-2.00beta.jar和sevenzipjbinding-all-platforms-9.20-2.00beta.jar复制到tomcat/lib目录下。P.S.:两个jar包所在目录如下图：
![sevenzipjbinding-9.20-2.00beta.jar的路径](doc/img/jar_url1.png)
![sevenzipjbinding-all-platforms-9.20-2.00beta.jar的路径](doc/img/jar_url2.png)
5. 将ae、iDesigner.war、iPlatform.war解压后放到tomcat8.0下webapps目录。
6. 删除 webapps\iPlatform\upload目录下的所有文件。
7. 新建一个空的数据库，修改webapps\iPlatform工程目录WEB-INF\classes\application.properties文件：修改jdbc.url、jdbc.username、jdbc.password修改为新建数据库的相关属性。
     ![输入图片说明](doc/img/图片1.png)
8. 执行 webapps\iPlatform工程目录WEB-INF\init-db.bat文件，即可进行数据库初始化（包括建表及数据初始化）。
   注意：如果报错提示“不是内部或外部命令”，说明文件乱码了，记事本打开init-db.bat，复制里面的代码到一个新建的记事本，然后另存为时编码选择ANSI，替换原来的init-db.bat文件。
9. 启动tomcat。
10. 启动成功后，打开浏览器，输入127.0.0.1:9090/iDesigner（设计器）和127.0.0.1:9090/iPlatform（业务系统）分别浏览即可。

# Tips
> 简搭（jabdp）分为开源版和商业版。商业版支持一键部署，不需要安装上述开发环境提到的众多工具，也不需要上述繁琐的部署步骤。商用版只需官网下载解压后双击启动服务即可一键部署。

# 浏览器支持

**本地开发**推荐使用`Chrome 最新版`浏览器，**不支持**`Chrome 80`以下版本。

**生产环境**支持现代浏览器。

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="IE / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)<br/>IE / Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)<br/>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)<br/>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)<br/>Safari | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari-ios/safari-ios_48x48.png" alt="iOS Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)<br/>iOS Safari |
| --------- | --------- | --------- | --------- | --------- |
| IE11, Edge| last 2 versions| last 2 versions| last 2 versions| last 2 versions