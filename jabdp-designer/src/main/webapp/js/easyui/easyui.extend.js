/**
 * 回收iframe内存
 */
$.fn.panel.defaults.onBeforeDestroy = function() {
    var frame = $('iframe', this);  
    if (frame.length > 0) {  
        frame[0].contentWindow.document.write('');  
        frame[0].contentWindow.close();  
        frame.remove();  
        if ($.browser.msie) {  
            CollectGarbage();  
        }  
    }  
};

$.extend($.fn.datagrid.defaults.editors, {  
    label: {  
        init: function(container, options){  
            var input = $('<label class="datagrid-editable-input"/>').appendTo(container);
            return input;  
        },  
        getValue: function(target){  
            return $(target).text();  
        },  
        setValue: function(target, value){  
            $(target).text(value);  
        },  
        resize: function(target, width){  
            var input = $(target);  
            if ($.boxModel == true){  
                input.width(width - (input.outerWidth() - input.width()));  
            } else {  
                input.width(width);  
            }  
        }  
    }  
}); 
$.extend($.fn.validatebox.defaults.rules, {  
    equalsField: {  
        validator: function(value,param){  
        //	alert(param[0]+" "+param[1]);
			var checkArray=["ID","VERSION","CREATE_USER","CREATE_TIME","LAST_UPDATE_USER","LAST_UPDATE_TIME","STATUS","SORT_ORDER_NO","ATM_ID","MASTER_ID","FLOW_INS_ID"];
			var fields =$.fn.module.getFormProp(param[0]);
			var field = $.fn.module.getFieldProp(param[0],param[1]);
			var flag=true;
			if(fields){
				var fl = fields.fieldList;
				if(fl){
					for(var i=0;i<fl.length;i++){
						if(fl[i].index != param[1]) {
							checkArray.push(fl[i].key.toUpperCase());
						}
					}
					if($.inArray(value.toUpperCase(), checkArray)>=0){
						flag=false;
					} else {
						flag=true;
					}
				}
			}
			return flag;
        },  
      message: '字段名为关键字或者已经存在'  
    },
    equalsTable: {  
        validator: function(value,param){ 
        	var form = $.fn.module.getModule();
        	var checkArray = [];
        	var flag=true;
        //	alert("检查表key方法已调用");
        	if(value.substr(0,3).toUpperCase()=="SYS"){
				flag=false;
			}else{
	        	if(form){
	        		if(form.dataSource){
	        			for(var i=0;i<form.dataSource.length;i++){
	        				checkArray.push(form.dataSource[i].key.toUpperCase());
	        			}
	        			if($.inArray(value.toUpperCase(), checkArray)>=0){
	        				flag=flase;
	    				}
	        		}
	        	}
			}
            return flag;  
        }, 
        message: '表名为关键字或者已经存在'  
    },
    covCaptionTOKey: {  
    	 validator: function(value,param){  
    				var upColName = $.fn.toPinyin(value).toUpperCase();
    				var newColName="";
    				$.each(upColName.split("_"),function(k,v){
    					newColName +=v; 
    				});
    				if(upColName){
    					alert(newColName);
    					return newColName;
    				}else{
    					return true;
    				}
    				
    	        },  
    	  message: ''  
    }  
});  