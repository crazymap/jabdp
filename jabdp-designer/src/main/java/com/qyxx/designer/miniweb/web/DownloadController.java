package com.qyxx.designer.miniweb.web;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.qyxx.designer.miniweb.service.DownloadFileManager;
import com.qyxx.designer.modules.utils.ZipUtils;

@Controller
@SessionAttributes({"loginUrl","loginName","configPath","passwd"})
@RequestMapping(value="/design")
public class DownloadController {
	
	/**
	 * 下载帐套
	 * @param request
	 * @param configPath 所有帐套路径键值对
	 * @param response 帐套名称id
	 * @param treeId
	 * return null*/
	@RequestMapping(value = "/download")
	@ResponseBody 
	public ModelAndView download(HttpServletRequest request,@ModelAttribute("configPath")  Map<String,String> configPath,
			HttpServletResponse response,@RequestParam("treeId") String treeId,@RequestParam("menuTitle") String menuTitle) throws Exception {
		String path = configPath.get(treeId);
		//String allPath = path+DesignController.CONFIG_FILE_NAME;
		String allPath = path+menuTitle+DesignController.JWP_SUFFIX;
		File jwpFile = new File(allPath);
		ZipUtils.zip(path, allPath);
		File fl = new File(allPath);
		String realName = fl.getName();
		String contentType =  "application/octet-stream";	
		DownloadFileManager.download(request, response, contentType,
				realName,allPath);
		FileUtils.deleteQuietly(jwpFile);
		return null;
	}
}
