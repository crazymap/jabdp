/*
 * @(#)Formula.java
 * 2011-7-23 下午10:45:05
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *  方案，公式节点
 *  
 *  @author gxj
 *  @version 1.0 2012-5-9 上午11:22:17
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Formula {
	
	/**
	 * 固定值
	 */
	public static final Integer TYPE_ITEM = 1;
	
	/**
	 * sql类型
	 */
	public static final Integer TYPE_SQL = 2;
	
	/**
	 * 存过类型
	 */
	public static final Integer TYPE_PROCEDURE = 3;
	
	/**
	 * 表达式
	 */
	public static final Integer TYPE_FORMULA = 4;
	
	/**
	 * 模块
	 */
	public static final Integer TYPE_MODULE = 5;
	
	@XmlAttribute
	private Integer type;
	@XmlElement
	private Sql sql = new Sql();
	@XmlElement(name="item")
	private List<Item> itemList = new ArrayList<Item>();
	@XmlElement
	private String content;
	
	@XmlAttribute
	private String refKey;//编辑数据源
	
	@XmlAttribute
	private String refQsKey;//查询数据源
	
	/**
	 * @return type
	 */
	public Integer getType() {
		return type;
	}
	
	/**
	 * @param type
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	
	/**
	 * @return sql
	 */
	public Sql getSql() {
		return sql;
	}
	
	/**
	 * @param sql
	 */
	public void setSql(Sql sql) {
		this.sql = sql;
	}
	
	/**
	 * @return itemList
	 */
	public List<Item> getItemList() {
		return itemList;
	}
	
	/**
	 * @param itemList
	 */
	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

	
	/**
	 * @return content
	 */
	public String getContent() {
		return content;
	}

	
	/**
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	
	/**
	 * @return refKey
	 */
	public String getRefKey() {
		return refKey;
	}

	
	/**
	 * @param refKey
	 */
	public void setRefKey(String refKey) {
		this.refKey = refKey;
	}

	
	/**
	 * @return refQsKey
	 */
	public String getRefQsKey() {
		return refQsKey;
	}

	
	/**
	 * @param refQsKey
	 */
	public void setRefQsKey(String refQsKey) {
		this.refQsKey = refQsKey;
	}
	
}
