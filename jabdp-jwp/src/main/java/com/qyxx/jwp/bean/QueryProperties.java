/*
 * @(#)QueryProperties.java
 * 2012-5-8 下午06:06:40
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *  字段查询属性
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午06:06:40
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryProperties {
	
	public static final String SEARCH_TYPE_EXACT_QUERY = "LE";//精确查询
	
	public static final String SEARCH_TYPE_LIKE_QUERY_LEFT = "LL";//模糊查询--左匹配
	
	public static final String SEARCH_TYPE_LIKE_QUERY_RIGHT = "LR";//模糊查询--右匹配
	
	public static final String SEARCH_TYPE_LIKE_QUERY_ALL = "LA";//模糊查询--全匹配

	@XmlAttribute
	private String align;
	@XmlAttribute
	private Integer width;
	@XmlAttribute
	private Boolean showInGrid = false;
	@XmlAttribute
	private Boolean showInSearch = false;
	@XmlAttribute
	private String defaultValue;
	
	@XmlAttribute
	private Boolean sortable = false;
	
	//是否支持文本过滤查询
	@XmlAttribute
	private Boolean filterable = false;

	//作为树形查询条件
	@XmlAttribute
	private Boolean showInTreeSearch = false;
	
	//作为tab标签页查询条件
	@XmlAttribute
	private Boolean showInTabSearch = false;
	
	//作为共享用户字段
	@XmlAttribute
	private Boolean useAsShareUser = false;
	
	//作为冻结列字段
	@XmlAttribute
	private Boolean frozenColumn = false;
	
	//合并相同值的单元格
	@XmlAttribute
	private Boolean mergeable = false;
	
	//作为权限控制字段
	@XmlAttribute
	private Boolean useAsAuthField = false;
	
	//查询方式，分为精确查询和模糊查询（左匹配，右匹配，全匹配）
	@XmlAttribute
	private String searchType = SEARCH_TYPE_LIKE_QUERY_ALL;
	
	//作为常用查询字段
	@XmlAttribute
	private Boolean showInCommonSearch = false;
	

	/**
	 * @return align
	 */
	public String getAlign() {
		return align;
	}
	
	/**
	 * @param align
	 */
	public void setAlign(String align) {
		this.align = align;
	}
	
	/**
	 * @return width
	 */
	public Integer getWidth() {
		return width;
	}
	
	/**
	 * @param width
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}
	
	/**
	 * @return showInGrid
	 */
	public Boolean getShowInGrid() {
		return showInGrid;
	}
	
	/**
	 * @param showInGrid
	 */
	public void setShowInGrid(Boolean showInGrid) {
		this.showInGrid = showInGrid;
	}
	
	/**
	 * @return showInSearch
	 */
	public Boolean getShowInSearch() {
		return showInSearch;
	}
	
	/**
	 * @param showInSearch
	 */
	public void setShowInSearch(Boolean showInSearch) {
		this.showInSearch = showInSearch;
	}
	
	/**
	 * @return defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	
	/**
	 * @param defaultValue
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the sortable
	 */
	public Boolean getSortable() {
		return sortable;
	}

	/**
	 * @param sortable the sortable to set
	 */
	public void setSortable(Boolean sortable) {
		this.sortable = sortable;
	}

	/**
	 * @return the filterable
	 */
	public Boolean getFilterable() {
		return filterable;
	}

	/**
	 * @param filterable the filterable to set
	 */
	public void setFilterable(Boolean filterable) {
		this.filterable = filterable;
	}

	
	/**
	 * @return showInTreeSearch
	 */
	public Boolean getShowInTreeSearch() {
		return showInTreeSearch;
	}

	
	/**
	 * @param showInTreeSearch
	 */
	public void setShowInTreeSearch(Boolean showInTreeSearch) {
		this.showInTreeSearch = showInTreeSearch;
	}

	
	/**
	 * @return showInTabSearch
	 */
	public Boolean getShowInTabSearch() {
		return showInTabSearch;
	}

	
	/**
	 * @param showInTabSearch
	 */
	public void setShowInTabSearch(Boolean showInTabSearch) {
		this.showInTabSearch = showInTabSearch;
	}

	
	/**
	 * @return useAsShareUser
	 */
	public Boolean getUseAsShareUser() {
		return useAsShareUser;
	}

	
	/**
	 * @param useAsShareUser
	 */
	public void setUseAsShareUser(Boolean useAsShareUser) {
		this.useAsShareUser = useAsShareUser;
	}

	
	/**
	 * @return frozenColumn
	 */
	public Boolean getFrozenColumn() {
		return frozenColumn;
	}

	
	/**
	 * @param frozenColumn
	 */
	public void setFrozenColumn(Boolean frozenColumn) {
		this.frozenColumn = frozenColumn;
	}

	
	/**
	 * @return mergeable
	 */
	public Boolean getMergeable() {
		return mergeable;
	}

	
	/**
	 * @param mergeable
	 */
	public void setMergeable(Boolean mergeable) {
		this.mergeable = mergeable;
	}

	
	/**
	 * @return useAsAuthField
	 */
	public Boolean getUseAsAuthField() {
		return useAsAuthField;
	}

	
	/**
	 * @param useAsAuthField
	 */
	public void setUseAsAuthField(Boolean useAsAuthField) {
		this.useAsAuthField = useAsAuthField;
	}

	/**
	 * @return the searchType
	 */
	public String getSearchType() {
		return searchType;
	}

	/**
	 * @param searchType the searchType to set
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	
	/**
	 * @return showInCommonSearch
	 */
	public Boolean getShowInCommonSearch() {
		return showInCommonSearch;
	}

	
	/**
	 * @param showInCommonSearch
	 */
	public void setShowInCommonSearch(Boolean showInCommonSearch) {
		this.showInCommonSearch = showInCommonSearch;
	}
	
	
}
