package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class ExportSystemField {
	
	@XmlAttribute
	private String caption;
	
	@XmlAttribute
	private String columnName;
	
	@XmlAttribute
	private boolean enableExport;
	
	@XmlAttribute
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public boolean getEnableExport() {
		return enableExport;
	}

	public void setEnableExport(boolean enableExport) {
		this.enableExport = enableExport;
	}
	
}
