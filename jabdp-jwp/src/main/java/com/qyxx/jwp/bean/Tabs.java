/*
 * @(#)Tabs.java
 * 2012-5-8 下午04:46:13
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 *  Tabs标签页组节点
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午04:46:13
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Tabs {
    
	@XmlAttribute
	private Integer rows;
	
	@XmlElement(name="tab")
	private List<Tab> tabList = new ArrayList<Tab>();

	/**
	 * @return the rows
	 */
	public Integer getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(Integer rows) {
		this.rows = rows;
	}

	/**
	 * @return the tabList
	 */
	public List<Tab> getTabList() {
		return tabList;
	}

	/**
	 * @param tabList the tabList to set
	 */
	public void setTabList(List<Tab> tabList) {
		this.tabList = tabList;
	}

}
