/*
 * @(#)Extension.java
 * 2012-11-6 上午11:11:28
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 *  form扩展属性
 *  @author gxj
 *  @version 1.0 2012-11-6 上午11:11:28
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Extension {
	
	@XmlElement
	private QueryPage queryPage;
	
	@XmlElement
	private EditPage editPage;

	
	/**
	 * @return queryPage
	 */
	public QueryPage getQueryPage() {
		return queryPage;
	}

	
	/**
	 * @param queryPage
	 */
	public void setQueryPage(QueryPage queryPage) {
		this.queryPage = queryPage;
	}

	
	/**
	 * @return editPage
	 */
	public EditPage getEditPage() {
		return editPage;
	}

	
	/**
	 * @param editPage
	 */
	public void setEditPage(EditPage editPage) {
		this.editPage = editPage;
	}
	
	

}
