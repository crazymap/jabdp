/*
 * @(#)GsModule.java
 * 2011-9-24 下午11:38:15
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


/**
 *  GS模块
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-24 下午11:38:15
 */
public class GsModule {

	//模块名
	private String name;
	
	private Map<String, Module> moduleMap = new HashMap<String, Module>();

	
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * @return moduleMap
	 */
	public Map<String, Module> getModuleMap() {
		return moduleMap;
	}

	
	/**
	 * @param moduleMap
	 */
	public void setModuleMap(Map<String, Module> moduleMap) {
		this.moduleMap = moduleMap;
	}
	
}
