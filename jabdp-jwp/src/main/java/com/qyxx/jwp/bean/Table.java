/*
 * @(#)Table.java
 * 2012-11-15 下午08:46:39
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 *  表映射
 *  @author gxj
 *  @version 1.0 2012-11-15 下午08:46:39
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Table {
	
	@XmlAttribute
	private String source;
	
	@XmlAttribute
	private String dest;
	
	@XmlAttribute
	private String sourceModule;
	
	@XmlAttribute
	private String destModule;
	
	@XmlElement(name="column")
	private List<Column> columnList = new ArrayList<Column>();

	
	/**
	 * @return source
	 */
	public String getSource() {
		return source;
	}

	
	/**
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	
	/**
	 * @return dest
	 */
	public String getDest() {
		return dest;
	}

	
	/**
	 * @param dest
	 */
	public void setDest(String dest) {
		this.dest = dest;
	}

	
	
	/**
	 * @return sourceModule
	 */
	public String getSourceModule() {
		return sourceModule;
	}


	
	/**
	 * @param sourceModule
	 */
	public void setSourceModule(String sourceModule) {
		this.sourceModule = sourceModule;
	}


	
	/**
	 * @return destModule
	 */
	public String getDestModule() {
		return destModule;
	}


	
	/**
	 * @param destModule
	 */
	public void setDestModule(String destModule) {
		this.destModule = destModule;
	}


	/**
	 * @return columnList
	 */
	public List<Column> getColumnList() {
		return columnList;
	}

	
	/**
	 * @param columnList
	 */
	public void setColumnList(List<Column> columnList) {
		this.columnList = columnList;
	}
	
	

}
