/*
 * @(#)QuerySql.java
 * 2011-12-4 上午11:36:49
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.qyxx.jwp.bean.Item;
import com.qyxx.jwp.bean.Sql;



/**
 *  查询sql语句
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-12-4 上午11:36:49
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class QuerySql {
	
	/**
	 * 固定值
	 */
	public static final Integer TYPE_ITEM = 1;
	
	/**
	 * sql类型
	 */
	public static final Integer TYPE_SQL = 2;
	
	/**
	 * 模块
	 */
	public static final Integer TYPE_MODULE = 5;
	
	//Sql语句Key
	@XmlAttribute
	private String key;
	
	//Sql语句内容
	@XmlElement
	private Sql sql;
	
	//sql对应字段数据类型
	@XmlAttribute
	private String dataType;
	
	//固定值列表值
	@XmlElement(name="item")
	private List<Item> itemList = new ArrayList<Item>();
	
	//查询数据源类型，默认sql
	@XmlAttribute
	private Integer querySourceType = TYPE_SQL;

	
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}


	/**
	 * @return the sql
	 */
	public Sql getSql() {
		return sql;
	}


	/**
	 * @param sql the sql to set
	 */
	public void setSql(Sql sql) {
		this.sql = sql;
	}


	
	/**
	 * @return dataType
	 */
	public String getDataType() {
		return dataType;
	}

	
	/**
	 * @param dataType
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	
	/**
	 * @return itemList
	 */
	public List<Item> getItemList() {
		return itemList;
	}

	
	/**
	 * @param itemList
	 */
	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

	
	/**
	 * @return querySourceType
	 */
	public Integer getQuerySourceType() {
		return querySourceType;
	}

	
	/**
	 * @param querySourceType
	 */
	public void setQuerySourceType(Integer querySourceType) {
		this.querySourceType = querySourceType;
	}
	
}
