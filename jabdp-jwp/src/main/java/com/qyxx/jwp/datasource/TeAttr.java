/*
 * @(#)TeAttr.java
 * 2011-9-28 下午10:34:50
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.datasource;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

import com.qyxx.jwp.bean.DataProperties;
import com.qyxx.jwp.bean.QueryProperties;


/**
 *  实体属性
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-28 下午10:34:50
 */
public class TeAttr {
	
	//属性名称
	private String name;
	
	//中文名称
	private String caption;
	
	//字段名称
	private String columnName;
	
	//属性类型
	private String type;
	
	//字段类型
	private String columnType;
	
	//字段长度
	private Integer size;
	
	//小数点后长度
	private Integer scale;
	
	//是否允许为空
	private Boolean isNotNull;
	
	//是否主键
	private Boolean isPrimaryKey;
	
	//是否唯一
	private Boolean isUnique;
	
	//属性所属实体
	private String entityName;
	
	//是否系统属性，由系统自动生成
	private Boolean isSystemAttr = false;
	
	/**
	 * 同步实体，需要将字段值同步至对应的实体下
	 */
	private String syncEntityName;
	
	/**
	 * 同步属性，需要将值更新至同表不同记录对应的属性下，用于拆分、合并记录
	 */
	private String syncKey;
	
	/**
	 * 属性源自实体名，表示该属性原先属于哪个实体
	 */
	private String fromEntityName = "";

	/**
	 * 开启导入
	 */
	private Boolean enableImport = false;
	
	/**
	 * 开启导出
	 */
	private Boolean enableExport = false;
	
	/**
	 * 导入时是否检查重复性，允许多个字段联合唯一
	 */
	private Boolean checkImportUnique = false;
	
	//作为共享用户字段
	private Boolean useAsShareUser = false;
	
	//自动编号规则
	private String autoIdRule;
	
	//查询数据源sqlkey，用于导入导出时字段实际值解析使用
	private String queryDsSqlKey = null;
	
	//编辑数据源sqlkey，用于字段数据权限控制时使用
	private String queryEditSqlKey = null;
	
	//作为权限控制字段
	private Boolean useAsAuthField = false;
	
	//显示格式
	private String dtFormat;
	
	//列表显示
	private Boolean showInGrid;

	//作为查询字段
	private Boolean showInSearch;

	//作为排序字段
	private Boolean sortable;
	
	//作为文本过滤查询字段
	private Boolean filterable;
	
	//查询方式，分为精确查询和模糊查询（左匹配，右匹配，全匹配）
	private String searchType = QueryProperties.SEARCH_TYPE_LIKE_QUERY_ALL;
	
	private String editType;
	
	private Boolean hasCaptionField = false; //是否存在显示值字段
	
	private Boolean autoGenPinYin = false;//自动生成拼音
	
	private String expRule;//公式规则
	
	private String defaultVal;//默认值
	
	private String helpMsg;//帮组信息
	
	private String fieldIndex;//字段索引/序号
	
	private Boolean visible = true;//可见

	
	/**
	 * @return name
	 */
	@XmlAttribute
	public String getName() {
		return name;
	}

	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * @return columnName
	 */
	@XmlAttribute
	public String getColumnName() {
		return columnName;
	}

	
	/**
	 * @param columnName
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	@XmlAttribute
	public String getCaption() {
		return caption;
	}
	
	public void setCaption(String caption) {
		this.caption = caption;
	}


	/**
	 * @return type
	 */
	@XmlAttribute
	public String getType() {
		return type;
	}

	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	
	/**
	 * @return columnType
	 */
	@XmlAttribute
	public String getColumnType() {
		return columnType;
	}

	
	/**
	 * @param columnType
	 */
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	
	/**
	 * @return size
	 */
	@XmlAttribute
	public Integer getSize() {
		return size;
	}

	
	/**
	 * @param size
	 */
	public void setSize(Integer size) {
		this.size = size;
	}

	
	/**
	 * @return isNotNull
	 */
	@XmlAttribute
	public Boolean getIsNotNull() {
		return isNotNull;
	}

	
	/**
	 * @param isNotNull
	 */
	public void setIsNotNull(Boolean isNotNull) {
		this.isNotNull = isNotNull;
	}

	
	/**
	 * @return isPrimaryKey
	 */
	@XmlAttribute
	public Boolean getIsPrimaryKey() {
		return isPrimaryKey;
	}

	
	/**
	 * @param isPrimaryKey
	 */
	public void setIsPrimaryKey(Boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	
	/**
	 * @return isUnique
	 */
	@XmlAttribute
	public Boolean getIsUnique() {
		return isUnique;
	}

	
	/**
	 * @param isUnique
	 */
	public void setIsUnique(Boolean isUnique) {
		this.isUnique = isUnique;
	}


	/**
	 * @return scale
	 */
	@XmlAttribute
	public Integer getScale() {
		return scale;
	}

	
	/**
	 * @param scale
	 */
	public void setScale(Integer scale) {
		this.scale = scale;
	}


	/**
	 * @return entityName
	 */
	@XmlAttribute
	public String getEntityName() {
		return entityName;
	}


	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}


	
	/**
	 * @return isSystemAttr
	 */
	@XmlAttribute
	public Boolean getIsSystemAttr() {
		return isSystemAttr;
	}


	
	/**
	 * @param isSystemAttr
	 */
	public void setIsSystemAttr(Boolean isSystemAttr) {
		this.isSystemAttr = isSystemAttr;
	}


	
	/**
	 * @return syncEntityName
	 */
	@XmlAttribute
	public String getSyncEntityName() {
		return syncEntityName;
	}


	
	/**
	 * @param syncEntityName
	 */
	public void setSyncEntityName(String syncEntityName) {
		this.syncEntityName = syncEntityName;
	}


	
	/**
	 * @return syncKey
	 */
	@XmlAttribute
	public String getSyncKey() {
		return syncKey;
	}


	
	/**
	 * @param syncKey
	 */
	public void setSyncKey(String syncKey) {
		this.syncKey = syncKey;
	}


	/**
	 * @return fromEntityName
	 */
	@XmlAttribute
	public String getFromEntityName() {
		return fromEntityName;
	}

	/**
	 * @param fromEntityName
	 */
	public void setFromEntityName(String fromEntityName) {
		this.fromEntityName = fromEntityName;
	}

	
	/**
	 * @return enableImport
	 */
	@XmlAttribute
	public Boolean getEnableImport() {
		return enableImport;
	}


	/**
	 * @param enableImport
	 */
	public void setEnableImport(Boolean enableImport) {
		this.enableImport = enableImport;
	}

	
	/**
	 * @return enableExport
	 */
	@XmlAttribute
	public Boolean getEnableExport() {
		return enableExport;
	}


	
	/**
	 * @param enableExport
	 */
	public void setEnableExport(Boolean enableExport) {
		this.enableExport = enableExport;
	}


	
	/**
	 * @return checkImportUnique
	 */
	@XmlAttribute
	public Boolean getCheckImportUnique() {
		return checkImportUnique;
	}


	
	/**
	 * @param checkImportUnique
	 */
	public void setCheckImportUnique(Boolean checkImportUnique) {
		this.checkImportUnique = checkImportUnique;
	}


	
	/**
	 * @return useAsShareUser
	 */
	@XmlAttribute
	public Boolean getUseAsShareUser() {
		return useAsShareUser;
	}


	
	/**
	 * @param useAsShareUser
	 */
	public void setUseAsShareUser(Boolean useAsShareUser) {
		this.useAsShareUser = useAsShareUser;
	}


	
	/**
	 * @return autoIdRule
	 */
	@XmlAttribute
	public String getAutoIdRule() {
		return autoIdRule;
	}

	
	/**
	 * @param autoIdRule
	 */
	public void setAutoIdRule(String autoIdRule) {
		this.autoIdRule = autoIdRule;
	}

	
	/**
	 * @return queryDsSqlKey
	 */
	@XmlAttribute
	public String getQueryDsSqlKey() {
		return queryDsSqlKey;
	}


	/**
	 * @param queryDsSqlKey
	 */
	public void setQueryDsSqlKey(String queryDsSqlKey) {
		this.queryDsSqlKey = queryDsSqlKey;
	}


	
	/**
	 * @return useAsAuthField
	 */
	@XmlAttribute
	public Boolean getUseAsAuthField() {
		return useAsAuthField;
	}


	
	/**
	 * @param useAsAuthField
	 */
	public void setUseAsAuthField(Boolean useAsAuthField) {
		this.useAsAuthField = useAsAuthField;
	}
	
	
	/**
	 * @return dtFormat
	 */
	@XmlAttribute
	public String getDtFormat() {
		return dtFormat;
	}


	
	/**
	 * @param dtFormat
	 */
	public void setDtFormat(String dtFormat) {
		this.dtFormat = dtFormat;
	}

	/**
	 * @return queryEditSqlKey
	 */
	@XmlAttribute
	public String getQueryEditSqlKey() {
		return queryEditSqlKey;
	}


	
	/**
	 * @param queryEditSqlKey
	 */
	public void setQueryEditSqlKey(String queryEditSqlKey) {
		this.queryEditSqlKey = queryEditSqlKey;
	}
	
	


	/**
	 * @return the showInGrid
	 */
	@XmlAttribute
	public Boolean getShowInGrid() {
		return showInGrid;
	}


	/**
	 * @param showInGrid the showInGrid to set
	 */
	public void setShowInGrid(Boolean showInGrid) {
		this.showInGrid = showInGrid;
	}


	/**
	 * @return the showInSearch
	 */
	@XmlAttribute
	public Boolean getShowInSearch() {
		return showInSearch;
	}


	/**
	 * @param showInSearch the showInSearch to set
	 */
	public void setShowInSearch(Boolean showInSearch) {
		this.showInSearch = showInSearch;
	}


	/**
	 * @return the sortable
	 */
	@XmlAttribute
	public Boolean getSortable() {
		return sortable;
	}


	/**
	 * @param sortable the sortable to set
	 */
	public void setSortable(Boolean sortable) {
		this.sortable = sortable;
	}


	/**
	 * @return the filterable
	 */
	@XmlAttribute
	public Boolean getFilterable() {
		return filterable;
	}


	/**
	 * @param filterable the filterable to set
	 */
	public void setFilterable(Boolean filterable) {
		this.filterable = filterable;
	}


	/**
	 * @return the searchType
	 */
	@XmlAttribute
	public String getSearchType() {
		return searchType;
	}


	/**
	 * @param searchType the searchType to set
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	/**
	 * @return the editType
	 */
	@XmlAttribute
	public String getEditType() {
		return editType;
	}


	/**
	 * @param editType the editType to set
	 */
	public void setEditType(String editType) {
		this.editType = editType;
	}


	/**
	 * @return the hasCaptionField
	 */
	@XmlAttribute
	public Boolean getHasCaptionField() {
		return hasCaptionField;
	}


	/**
	 * @param hasCaptionField the hasCaptionField to set
	 */
	public void setHasCaptionField(Boolean hasCaptionField) {
		this.hasCaptionField = hasCaptionField;
	}

	
	/**
	 * @return autoGenPinYin
	 */
	@XmlAttribute
	public Boolean getAutoGenPinYin() {
		return autoGenPinYin;
	}

	
	/**
	 * @param autoGenPinYin
	 */
	public void setAutoGenPinYin(Boolean autoGenPinYin) {
		this.autoGenPinYin = autoGenPinYin;
	}


	/**
	 * @return expRule
	 */
	@XmlAttribute
	public String getExpRule() {
		return expRule;
	}


	/**
	 * @param expRule
	 */
	public void setExpRule(String expRule) {
		this.expRule = expRule;
	}


	
	/**
	 * @return defaultVal
	 */
	@XmlAttribute
	public String getDefaultVal() {
		return defaultVal;
	}


	
	/**
	 * @param defaultVal
	 */
	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}


	
	/**
	 * @return helpMsg
	 */
	@XmlAttribute
	public String getHelpMsg() {
		return helpMsg;
	}


	
	/**
	 * @param helpMsg
	 */
	public void setHelpMsg(String helpMsg) {
		this.helpMsg = helpMsg;
	}

	
	/**
	 * @return fieldIndex
	 */
	@XmlAttribute
	public String getFieldIndex() {
		return fieldIndex;
	}


	
	/**
	 * @param fieldIndex
	 */
	public void setFieldIndex(String fieldIndex) {
		this.fieldIndex = fieldIndex;
	}
	

	/**
	 * @return visible
	 */
	@XmlAttribute
	public Boolean getVisible() {
		return visible;
	}


	
	/**
	 * @param visible
	 */
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}


	/**
	 * 根据字段类型获取类型简称
	 * 
	 * @return
	 */
	@XmlTransient
	public String getShortTypeByType() {
		String st = "S";
		if(DataProperties.DATA_TYPE_LONG.equals(type)) {
			st = "L";
		} else if(DataProperties.DATA_TYPE_DOUBLE.equals(type)) {
			st = "C";
		} else if(DataProperties.DATA_TYPE_DATETIME.equals(type)) {
			st = "D";
		} else if(DataProperties.DATA_TYPE_INT.equals(type)) {
			st = "I";
		}
		return st;
	}

	/**
	 * 获取查询字符串
	 * 
	 * @return
	 */
	public String convertSearchStr(String search) {
		String st = "%" + search + "%";
		if(QueryProperties.SEARCH_TYPE_EXACT_QUERY.equals(this.searchType)) {
			st = search;
		} else if(QueryProperties.SEARCH_TYPE_LIKE_QUERY_LEFT.equals(this.searchType)) {
			st = search + "%";
		} else if(QueryProperties.SEARCH_TYPE_LIKE_QUERY_RIGHT.equals(this.searchType)) {
			st = "%" + search;
		}
		return st;
	}
	
	/**
	 * 获取查询匹配符
	 * 
	 * @return
	 */
	@XmlTransient
	public String getSearchMethod() {
		String st = "LIKE";
		if(QueryProperties.SEARCH_TYPE_EXACT_QUERY.equals(this.searchType)) {
			st = "LIKEE";
		} else if(QueryProperties.SEARCH_TYPE_LIKE_QUERY_LEFT.equals(this.searchType)) {
			st = "LIKEL";
		} else if(QueryProperties.SEARCH_TYPE_LIKE_QUERY_RIGHT.equals(this.searchType)) {
			st = "LIKER";
		}
		return st;
	}

}
