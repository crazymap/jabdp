/*
 * @(#)TableEntitys.java
 * 2011-9-28 下午10:34:09
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *  表实体结构
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-28 下午10:34:09
 */
@XmlRootElement(name="Root")
public class TableEntitys {
	
	private String name;
	
	private String displayName;
	
	private String moduleType;
	
	private List<TableEntity> tableEntity = new ArrayList<TableEntity>();
	
	private List<QuerySql> querySql = new ArrayList<QuerySql>();
	
	/**
	 * @return name
	 */
	@XmlAttribute
	public String getName() {
		return name;
	}

	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return displayName
	 */
	@XmlAttribute
	public String getDisplayName() {
		return displayName;
	}


	/**
	 * @param displayName
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	/**
	 * @return tableEntity
	 */
	@XmlElement
	public List<TableEntity> getTableEntity() {
		return tableEntity;
	}

	
	/**
	 * @param tableEntity
	 */
	public void setTableEntity(List<TableEntity> tableEntity) {
		this.tableEntity = tableEntity;
	}

	
	/**
	 * @return querySql
	 */
	@XmlElement
	public List<QuerySql> getQuerySql() {
		return querySql;
	}

	
	/**
	 * @param querySql
	 */
	public void setQuerySql(List<QuerySql> querySql) {
		this.querySql = querySql;
	}


	@XmlElement
	public String getModuleType() {
		return moduleType;
	}


	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

}
