/*
 * @(#)Definition.java
 * 2011-5-24 下午09:12:51
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.definition;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.qyxx.platform.common.utils.encode.JsonBinder;

import java.util.Collections;


/**
 *  数据字典定义
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-24 下午09:12:51
 */
public class Definition {
	
	private String keyWord;
	
	private Set<DefinitionEntity> set;
	
	private List<DefinitionEntity> list;
	
	private String jsonString;
	
	private static JsonBinder jsonBinder = JsonBinder.getAlwaysMapper();
	
	
	/**
	 * @return keyWord
	 */
	public String getKeyWord() {
		return keyWord;
	}

	
	/**
	 * @return set
	 */
	public Set<DefinitionEntity> getSet() {
		return set;
	}

	
	/**
	 * @return list
	 */
	public List<DefinitionEntity> getList() {
		return list;
	}

	
	
	/**
	 * @return jsonString
	 */
	public String getJsonString() {
		return jsonString;
	}
	
	public Definition(DefinitionSource definitionSource,String keyWord){
		list = new ArrayList<DefinitionEntity>();
		set = new LinkedHashSet<DefinitionEntity>();
		List<DefinitionEntity> source = definitionSource.getDefinitions(keyWord);
		list.addAll(source);
		set.addAll(source);
		list = Collections.unmodifiableList(list);
		set = Collections.unmodifiableSet(set);
		jsonString = toJsonString();
	}
	
	public Definition(DefinitionSource definitionSource, String keyWord, String locale){
		list = new ArrayList<DefinitionEntity>();
		set = new LinkedHashSet<DefinitionEntity>();
		List<DefinitionEntity> source = definitionSource.getDefinitions(keyWord, locale);
		list.addAll(source);
		set.addAll(source);
		list = Collections.unmodifiableList(list);
		set = Collections.unmodifiableSet(set);
		jsonString = toJsonString();
	}
	
	private String toJsonString() {
		Map<String, DefinitionEntity> jsonMap = new LinkedHashMap<String,DefinitionEntity>();
		for(DefinitionEntity definitionEntity:set){
			jsonMap.put(definitionEntity.value, definitionEntity);
		}
		return jsonBinder.toJson(jsonMap);
	}
	
	
	
	/* （非 Javadoc）
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj==null){
			return false;
		}
		if(!(obj instanceof Definition)){
			return false;
		}
		Definition target = (Definition) obj;
		return this.keyWord==null?target.keyWord==null:this.keyWord.equals(target.keyWord);
	}


	/* （非 Javadoc）
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return keyWord==null?0:keyWord.hashCode();
	}

	public static class DefinitionEntity{
		
		private String displayName;
		
		private String value;
		
		private String type;
		
		private String resId;
		
		private Map<String,Object> externValue;

		/**
		 * @param displayName
		 * @param value
		 * @param type
		 * @param resId
		 * @param externValue
		 */
		public DefinitionEntity(String displayName, String value,
				String type, String resId, Map<String,Object> externValue) {
			super();
			this.displayName = displayName;
			this.value = value;
			this.type = type;
			this.resId = resId;
			this.externValue = externValue;
		}
		
		
		/**
		 * @return resId
		 */
		public String getResId() {
			return resId;
		}
		
		
		/**
		 * @return externValue
		 */
		public Map<String, Object> getExternValue() {
			return externValue;
		}


		/**
		 * @return displayName
		 */
		public String getDisplayName() {
			return displayName;
		}

		
		/**
		 * @return value
		 */
		public String getValue() {
			return value;
		}

		
		/**
		 * @return type
		 */
		public String getType() {
			return type;
		}

		/* （非 Javadoc）
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if(obj==null){
				return false;
			}
			if(!(obj instanceof DefinitionEntity)){
				return false;
			}
			DefinitionEntity target = (DefinitionEntity) obj;
			return this.value==null?target.value==null:this.value.equals(target.value);
		}

		/* （非 Javadoc）
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return value==null?0:value.hashCode();
		}
		
		
	}
}
