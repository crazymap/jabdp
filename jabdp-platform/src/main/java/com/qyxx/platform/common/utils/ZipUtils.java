/*
 * @(#)ZipUtils.java
 * 2012-6-28 下午12:03:21
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Expand;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

import com.qyxx.platform.common.utils.sevenziputils.UnzipBigFile;
import com.qyxx.platform.common.utils.sevenziputils.SevenZipServer;

/**
 *  解压缩文件工具类
 *  
 *  @author gxj
 *  @version 1.0 2012-6-28 下午12:03:21
 *  @since jdk1.6
 */
public class ZipUtils {
	
private static SevenZipServer sevenZip = new SevenZipServer();
	
	/**	
	 * 压缩文件
	 * @param source
	 * @param dest 
	 * 
	 */
    public static void zip(String source, String dest){
    	sevenZip.compressZIP7(source,dest);
		
	}
	
    
    /**
	 * 解压文件
	 * 
	 * @param zipFile
	 * @param dest
	 * @throws IOException
	 */
	public static void unzip(String zipFile, String dest){
		//sevenZip.extractZIP7(zipFile, dest);
		//7zip解压大文件方法
		UnzipBigFile.extractile(zipFile, dest);
	}

	
	/**
	 * 压缩文件旧方法
	 * 
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	public static void zipOld(String source, String dest) throws IOException {
		OutputStream os = new FileOutputStream(dest);
		BufferedOutputStream bos = new BufferedOutputStream(os);
		ZipOutputStream zos = new ZipOutputStream(bos);

		// 支持中文，但有缺陷！这是硬编码！
		//zos.setEncoding("GBK");

		File file = new File(source);

		String basePath = null;
		if (file.isDirectory()) {
			//basePath = file.getPath();
			basePath = file.getParent();
		} else {
			basePath = file.getParent();
		}

		zipFile(file, basePath, zos);

		zos.closeEntry();
		zos.close();
	}

	/**
	 * 解压文件
	 * 
	 * @param zipFile
	 * @param dest
	 * @throws IOException
	 */
	public static void unzip1(String zipFile, String dest) throws IOException {
		Project p = new Project();

		Expand e = new Expand();

		e.setProject(p);

		e.setSrc(new File(zipFile));

		e.setOverwrite(false);

		e.setDest(new File(dest));

		/*
		 * ant下的zip工具默认压缩编码为UTF-8编码，
		 * 
		 * 而winRAR软件压缩是用的windows默认的GBK或者GB2312编码
		 * 
		 * 所以解压缩时要制定编码格式
		 */

		e.setEncoding("UTF-8"); // 根据linux系统的实际编码设置
		e.execute(); 
	}
	
	/**
	 * 解压文件旧方法
	 * 
	 * @param zipFile
	 * @param dest
	 * @throws IOException
	 */
	public static void unzipOld(String zipFile, String dest) throws IOException {
		ZipFile zip = new ZipFile(zipFile);
		Enumeration<ZipEntry> en = zip.getEntries();
		ZipEntry entry = null;
		byte[] buffer = new byte[1024];
		int length = -1;
		InputStream input = null;
		BufferedOutputStream bos = null;
		File file = null;

		while (en.hasMoreElements()) {
			entry = (ZipEntry) en.nextElement();
			if (entry.isDirectory()) {
				file = new File(dest, entry.getName());
				if (!file.exists()) {
					file.mkdir();
				}
				continue;
			}

			input = zip.getInputStream(entry);
			file = new File(dest, entry.getName());
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			bos = new BufferedOutputStream(new FileOutputStream(file));

			while (true) {
				length = input.read(buffer);
				if (length == -1)
					break;
				bos.write(buffer, 0, length);
			}
			bos.close();
			input.close();
		}
		zip.close();
	}	
	

	private static void zipFile(File source, String basePath,
			ZipOutputStream zos) throws IOException {
		File[] files = new File[0];

		if (source.isDirectory()) {
			files = source.listFiles();
		} else {
			files = new File[1];
			files[0] = source;
		}

		String pathName;
		byte[] buf = new byte[1024];
		int length = 0;
		for (File file : files) {
			if (file.isDirectory()) {
				pathName = file.getPath().substring(basePath.length() + 1)
						+ "/";
				zos.putNextEntry(new ZipEntry(pathName));
				zipFile(file, basePath, zos);
			} else {
				pathName = file.getPath().substring(basePath.length() + 1);
				InputStream is = new FileInputStream(file);
				BufferedInputStream bis = new BufferedInputStream(is);
				zos.putNextEntry(new ZipEntry(pathName));
				while ((length = bis.read(buf)) > 0) {
					zos.write(buf, 0, length);
				}
				is.close();
			}
		}
	}
	
	
	public static void main(String[] args) throws Exception {
	    String jwpPath = "H:/tools/jabdp-v1.2.1/webapps/iPlatform/upload/gs-package/20180710105934922/config.jwp";
	    String filePath = "H:/tools/jabdp-v1.2.1/webapps/iPlatform/upload/gs-package/20180710105934922/";
	    System.out.println("---------------------------开始解压");
		unzip(jwpPath, filePath);
		System.out.println("---------------------------结束解压");
	}

}
