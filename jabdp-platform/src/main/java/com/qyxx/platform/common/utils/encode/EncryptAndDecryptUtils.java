package com.qyxx.platform.common.utils.encode;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.support.StaticApplicationContext;

import com.qyxx.jwp.menu.Menu;



/**
 * 加解密工具类<br>
 *
 * @Author:xw
 * 
 *
 */
public class EncryptAndDecryptUtils {
	//默认密码
	private static final String DEFAULT_KEY="123";
	//加密标识
	private static final String XML_ENCRYPT_SUFFIX = "jabdp";
	
	/**
	 * AES加密，并转化为16进制
	 *
	 * 
	 *
	 * @param value
	 * 					待加密内容
	 * @param key
	 * 					秘钥，如果为空则使用默认密钥
	 * @return          
	 *                  密文，并且加上加密标识
	 */
	public static String aesEncrypt(String value,String key ){
		if(StringUtils.isBlank(key)){
			return value;
		}else{
			byte[] encrypt;
			String result = null;
			try {
				if(value != null && !"".equals(value.trim())){		     //value is not null
					encrypt = AESUtils.encrypt(value,key);               //加密
					result = EncodeUtils.parseByte2HexStr(encrypt);          //2进制转换为16进制
					result += XML_ENCRYPT_SUFFIX;                        //给密文加上已经加密过的标识
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}

	}
	
	/**
	 * 先转换为2进制，然后AES解密
	 * 
	 * 
	 *
	 * @param value
	 * 				待解密内容
	 * @param key
	 * 				秘钥，如果为空则使用默认密钥
	 * @return
	 *              解密后内容 ，如果没有加密标识，则返回原文
	 */
	public static String aesDecrypt(String value , String key){
		if(value.endsWith(XML_ENCRYPT_SUFFIX) == true){                    //判断有没有加密标识
			String envalue =  value.split("jabdp")[0];                     //去掉加密标识
			key = key == null ? DEFAULT_KEY: key;                          
			byte[] twoStrValue= null;
			byte[] decrypt = null;
			String result = null;
			try {
				if(value != null && !"".equals(value.trim())){	
					twoStrValue      = EncodeUtils.parseHexStr2Byte(envalue);         //16进制转换成2进制
					decrypt = AESUtils.decrypt(twoStrValue,key);                 //解密              
					 result = new String(decrypt,"UTF-8");	                     //转字符串
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			return result;
		}else{
			
			return value;
		}
	}
	
	public static void main(String[] args) {
		JaxbBinder jb = new JaxbBinder(Menu.class);
		String menuFileName = "H:/tools/jabdp-v1.2.1-mis/webapps/iPlatform/upload/gs-package/default/config/module/JiChuXinXi/ChanPinXinXiapp.xml";
		File file = new File(menuFileName);
		String xml = null;
		try {
			xml = FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		xml = EncryptAndDecryptUtils.aesDecrypt(xml, "");
		System.out.println(xml);
//		Menu menu = jb.fromXml(xml);
//		JsonBinder jsonBinder = JsonBinder.getAlwaysMapper();
//		String s = jsonBinder.toJson(menu);
//		System.out.println(s);
	}
	
}
