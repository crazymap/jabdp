/*
 * @(#)MailAuthenticator.java
 * 2014-1-7 下午11:04:11
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import org.apache.commons.lang3.StringUtils;

/**
 * 邮箱安全认证
 * @author gxj
 * @version 1.0 2014-1-7 下午11:04:11
 * @since jdk1.6
 */

public class MailAuthenticator extends Authenticator {

	/**
	 * 用户名（登录邮箱）
	 */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	
	/**
	 * 域名
	 */
	private String domainUser;

	/**
	 * 初始化邮箱和密码
	 * 
	 * @param username
	 *            邮箱
	 * @param password
	 *            密码
	 */
	public MailAuthenticator(String username, String password, String domainUser) {
		this.username = username;
		this.password = password;
		this.domainUser = domainUser;
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		if(StringUtils.isNotBlank(domainUser)) {
			return new PasswordAuthentication(domainUser, password);
		} else {
			return new PasswordAuthentication(username, password);
		}
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	/**
	 * @return domainUser
	 */
	public String getDomainUser() {
		return domainUser;
	}

	
	/**
	 * @param domainUser
	 */
	public void setDomainUser(String domainUser) {
		this.domainUser = domainUser;
	}
	
	

}
