/*
 * @(#)DefaultFilenameFilter.java
 * 2011-8-11 下午11:33:19
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsc.parse;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;


/**
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-8-11 下午11:33:19
 */

public class DefaultFilenameFilter implements FilenameFilter {
    private boolean ignoreDir = false; //是否忽略文件夹,默认不忽略
    private List<String> extList = new ArrayList<String>();
    
    public DefaultFilenameFilter(final String ext){
        this.extList.add("."+ext);
    }
    public DefaultFilenameFilter(final String... exts){
        if((exts != null) && (exts.length > 0)){
            for(String ext : exts){
                this.extList.add("."+ext);
            }
        }
        
    }
    
    public DefaultFilenameFilter(final String ext, boolean ignoreDir){
        this.ignoreDir = ignoreDir;
        this.extList.add("."+ext);
    }
    
    public void addExt(final String ext){
        this.extList.add("."+ext);
    }
    
    public boolean accept(File dir, String name) {
        if(ignoreDir){
            if(new File(dir+File.separator+name).isDirectory()){
                return false;
            }else{
                return endsWithExt(name);
            }
        }else{
            return endsWithExt(name)||new File(dir+File.separator+name).isDirectory();
        }//if
    }//accept
    
    protected boolean endsWithExt(String name){
        if(!AssertUtil.notEmpty(extList)){
            return true;
        }
        for(String ext : extList){
            if(name.endsWith(ext)){
                return true;
            }
        }
        
        return false;
    }

}
