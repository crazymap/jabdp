package com.qyxx.platform.gsmng.common.dao;


import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.gsmng.common.entity.TaskRun;

/**
 * 流程运行任务管理
 */
@Component
public class TaskRunDao  extends HibernateDao<TaskRun, Long> {

}
