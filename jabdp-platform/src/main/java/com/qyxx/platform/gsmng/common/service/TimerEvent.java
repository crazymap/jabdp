package com.qyxx.platform.gsmng.common.service;

import java.util.Date;
import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.apache.commons.lang.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.accountmng.dao.UserDao;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.notice.dao.NoticeDao;
import com.qyxx.platform.sysmng.notice.dao.NoticeToUserDao;
import com.qyxx.platform.sysmng.notice.entity.Notice;
import com.qyxx.platform.sysmng.notice.entity.NoticeToUser;
/**
 *定时提醒处理类
 */
@Service
@Transactional
public class TimerEvent extends HibernateDao<Object, Long> {

	protected TaskService taskService;

	protected RuntimeService runtimeService;
	
	protected IdentityService identityService;

	private NoticeDao noticeDao;
	
	private NoticeToUserDao noticeToUserDao;
	
	private UserDao userDao;
	
	private HistoryService historyService;
	
	@Autowired
	public void setHistoryService(HistoryService historyService) {
		this.historyService = historyService;
	}
	@Autowired
	public void setNoticeToUserDao(NoticeToUserDao noticeToUserDao) {
		this.noticeToUserDao = noticeToUserDao;
	}

	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Autowired
	public void setNoticeDao(NoticeDao noticeDao) {
		this.noticeDao = noticeDao;
	}

	@Autowired
	public void setIdentityService(IdentityService identityService) {
		this.identityService = identityService;
	}

	@Autowired
	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}

	@Autowired
	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public void sendTimerMessage(ActivityExecution execution,String title,String noticeStyle) {
		// 启动人---owner 审批人---assignee 2个都要---both
		//另一个参数表示以什么方式来发送消息，短信，邮件，内部通知
		String processInstanceId = execution.getProcessInstanceId();
		Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult();
		String userLoginName = task.getAssignee();
		String taskId = task.getId();
		if (StringUtils.isNotBlank(userLoginName)) {//已签收，未办理
			noticeStyle(processInstanceId,noticeStyle,title,userLoginName);
		} else {//未签收，未办理
			List<IdentityLink> list = taskService.getIdentityLinksForTask(taskId);
			for(IdentityLink identityLink :list){
				userLoginName = identityLink.getUserId();
				if (StringUtils.isNotBlank(userLoginName)) {
					noticeStyle(processInstanceId,noticeStyle,title,userLoginName);
				}
			}
		}
	}

	public void noticeStyle(String processInstanceId, String style,
			String title, String userLoginName) {
		Task task = taskService.createTaskQuery()
				.processInstanceId(processInstanceId).singleResult();
		HistoricProcessInstance hpi = historyService
				.createHistoricProcessInstanceQuery()
				.processInstanceId(processInstanceId).singleResult();
		String loginName = hpi.getStartUserId();
		if (style == "owner") {
			Notice notice = new Notice();
			notice.setKeyworlds("提醒");
			notice.setTitle(title);
			notice.setStatus(Notice.STATUS_1);
			notice.setStyle(Notice.STYLE_05);
			notice.setCreateUser(1L);
			notice.setCreateTime(new Date());
			notice.setContent(title);
			notice.setModuleDataId(processInstanceId);
			noticeDao.save(notice);
			NoticeToUser noticeToUser = new NoticeToUser();
			noticeToUser.setNotice(notice);
			noticeToUser.setStatus(NoticeToUser.STATUS_0);
			User user = userDao.findUniqueBy("loginName", loginName);
			noticeToUser.setUserId(user.getId());
			noticeToUserDao.save(noticeToUser);
		} else if (style == "assignee") {
			Notice notice = new Notice();
			notice.setKeyworlds("待办提醒");
			notice.setTitle(title);
			notice.setStatus(Notice.STATUS_1);
			notice.setStyle(Notice.STYLE_03);
			notice.setCreateUser(1L);
			notice.setCreateTime(new Date());
			notice.setContent(title);
			notice.setModuleDataId(task.getId());
			notice.setModuleKey(task.getName());
			noticeDao.save(notice);
			NoticeToUser noticeToUser = new NoticeToUser();
			noticeToUser.setNotice(notice);
			noticeToUser.setStatus(NoticeToUser.STATUS_0);
			User user = userDao.findUniqueBy("loginName",
					userLoginName);
			noticeToUser.setUserId(user.getId());
			noticeToUserDao.save(noticeToUser);
		} else if (style == "both") {
			Notice notice = new Notice();
			notice.setKeyworlds("提醒");
			notice.setTitle(title);
			notice.setStatus(Notice.STATUS_1);
			notice.setStyle(Notice.STYLE_05);
			notice.setCreateUser(1L);
			notice.setCreateTime(new Date());
			notice.setContent(title);
			notice.setModuleDataId(processInstanceId);
			noticeDao.save(notice);
			NoticeToUser noticeToUser = new NoticeToUser();
			noticeToUser.setNotice(notice);
			noticeToUser.setStatus(NoticeToUser.STATUS_0);
			User user = userDao.findUniqueBy("loginName", loginName);
			noticeToUser.setUserId(user.getId());
			noticeToUserDao.save(noticeToUser);

			Notice notice2 = new Notice();
			notice.setKeyworlds("待办提醒");
			notice.setTitle(title);
			notice.setStatus(Notice.STATUS_1);
			notice.setStyle(Notice.STYLE_03);
			notice.setCreateUser(1L);
			notice.setCreateTime(new Date());
			notice.setContent(title);
			notice.setModuleDataId(task.getId());
			notice.setModuleKey(task.getName());
			noticeDao.save(notice2);
			NoticeToUser noticeToUser2 = new NoticeToUser();
			noticeToUser.setNotice(notice2);
			noticeToUser.setStatus(NoticeToUser.STATUS_0);
			User user2 = userDao.findUniqueBy("loginName",
					userLoginName);
			noticeToUser.setUserId(user2.getId());
			noticeToUserDao.save(noticeToUser2);
		}
	}
}