package com.qyxx.platform.gsmng.common.service;

import java.util.List;
import java.util.Set;
import org.activiti.engine.RepositoryService;


import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;

import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.bpmn.behavior.MultiInstanceActivityBehavior;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.task.TaskDefinition;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.sysmng.accountmng.dao.RoleDao;
import com.qyxx.platform.sysmng.accountmng.entity.Role;
import com.qyxx.platform.sysmng.accountmng.entity.User;
/**
 *设置审批用户类
 */
@Service
@Transactional
public class SetCandidateUserTaks implements TaskListener {

	private RepositoryService repositoryService;


	private RoleDao roleDao;

	@Autowired
	public void setRepositoryService(
			RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}

	@Autowired
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	@Override
	public void notify(DelegateTask delegateTask) {
		String roles = "";
		ProcessDefinitionEntity def = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
				.getDeployedProcessDefinition(delegateTask
						.getProcessDefinitionId());
		List<ActivityImpl> activitiList = def.getActivities();
		String activitiId = delegateTask.getExecution()
				.getCurrentActivityId();// 当前执行的ID
		for (ActivityImpl activityImpl : activitiList) {
			if (activitiId.equals(activityImpl.getId())) {// activityImpl//当前的执行活动
				ActivityBehavior activityBehavior = activityImpl
						.getActivityBehavior();
				if (activityBehavior instanceof UserTaskActivityBehavior) {// 非多实例
					TaskDefinition taskDefinition = null;
					if (activityBehavior instanceof UserTaskActivityBehavior) {
						UserTaskActivityBehavior userTaskActivityBehavior = (UserTaskActivityBehavior) activityBehavior;
						taskDefinition = userTaskActivityBehavior
								.getTaskDefinition();
					} else if (activityBehavior instanceof MultiInstanceActivityBehavior) {
						MultiInstanceActivityBehavior multiInstanceActivityBehavior = (MultiInstanceActivityBehavior) activityBehavior;

						taskDefinition = multiInstanceActivityBehavior
								.getTaskDefinition();
					}
					Set<Expression> candidateGroupIdExpressions = taskDefinition
							.getCandidateGroupIdExpressions();
					for (Expression expression : candidateGroupIdExpressions) {
						String expressionText = expression
								.getExpressionText();
						roles = expressionText;
					}
					String role []=roles.split(",");
					for(String roleName : role ){
					delegateTask.deleteCandidateGroup(roleName);
					Role r = roleDao.findUniqueBy("roleName",
							roleName);
					List<User> uList = r.getUserList();
					for (User u : uList) {
						System.out.println(u.getLoginName());
						delegateTask.addCandidateUser(u.getLoginName());
					}
					
				}
			}
		}
	}
	}
}