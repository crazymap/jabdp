/*
 * @(#)ExceptionInterceptor.java
 * 2011-5-28 下午03:58:31
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.exception;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.service.ServiceException;
import com.qyxx.platform.common.module.web.FormatMessage;
import com.qyxx.platform.common.module.web.Messages;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 *  异常拦截器
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-28 下午03:58:31
 */
public class ExceptionInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -8676813686842529174L;
	private static Logger logger = LoggerFactory.getLogger(ExceptionInterceptor.class);

	/**
	 * @see com.opensymphony.xwork2.interceptor.AbstractInterceptor#intercept(com.opensymphony.xwork2.ActionInvocation)
	 */
	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		boolean jsonOutput = false;
		Object action = actionInvocation.getAction();
		try{
			String methodName = actionInvocation.getProxy().getMethod();
			Method method = action.getClass().getMethod(methodName);
			jsonOutput = method.isAnnotationPresent(JsonOutput.class);
		}catch(Exception e){
			logger.warn("判断是否为json输出时出现异常",e);
		}
		FormatMessage fm = null;
		GsException ex = null;
		String result = null;
		try{
			//logger.info("执行异常拦截1" + jsonOutput);
			result = actionInvocation.invoke();
			//logger.info("执行异常拦截2：" + result);
		} catch(ServiceException e) {
			//业务逻辑异常
			logger.error(Messages.LOGIC_ERROR_MESSAGE, e);
			if(jsonOutput) {
				fm = new FormatMessage();
				fm.setFlag(Messages.FAIL);
				fm.setMsg(e.getMessage());
			} else {
				ex = new GsException(Messages.LOGIC_ERROR_MESSAGE, e);
				ex.setFlag(Messages.FAIL);
			}
		} catch(NullPointerException e) {
			//空指针异常，需要判断用户session是否已过期
			Object obj = actionInvocation.getInvocationContext().getSession().get(Constants.USER_SESSION_ID);
			if(null!=obj) {
				//空指针报错
				logger.error(Messages.NULLPOINT_ERROR_MESSAGE, e);
				if(jsonOutput){
					//Json输出
					fm = new FormatMessage();
					fm.setFlag(Messages.ERROR);
					fm.setMsg(Messages.NULLPOINT_ERROR_MESSAGE);
				} else {
					ex = new GsException(Messages.NULLPOINT_ERROR_MESSAGE, e);
					ex.setFlag(Messages.ERROR);
				}
			} else {
				//用户session过期，会话超时报错
				logger.error(Messages.SESSION_TIMEOUT_ERROR_MESSAGE, e);
				if(jsonOutput){
					//Json输出
					fm = new FormatMessage();
					fm.setFlag(Messages.SESSION_TIMEOUT);
					fm.setMsg(Messages.SESSION_TIMEOUT_ERROR_MESSAGE);
				} else {
					ex = new GsException(Messages.SESSION_TIMEOUT_ERROR_MESSAGE, e);
					ex.setFlag(Messages.SESSION_TIMEOUT);
				}
			}
		} catch(Throwable t) {
			//系统异常
			logger.error(Messages.SYSTEM_ERROR_MESSAGE, t);
			if(jsonOutput){
				//Json输出
				fm = new FormatMessage();
				fm.setFlag(Messages.ERROR);
				fm.setMsg(t.getMessage());
			} else {
				ex = new GsException(Messages.SYSTEM_ERROR_MESSAGE, t);
				ex.setFlag(Messages.ERROR);
			}
		}
		if(jsonOutput) {
			//输出json消息
			if(null != fm) {
				Struts2Utils.renderJson(fm);
			}
		} else {
			//输出html消息
			if(null != ex) {
				throw ex;
			}
		}
		return result;
	}
}
