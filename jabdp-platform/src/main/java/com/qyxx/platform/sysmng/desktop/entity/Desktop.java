/*
 * @(#)deskTop.java
 * 2012-9-5 下午05:38:26
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.desktop.entity;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.sysmng.accountmng.entity.Role;

/**
 *  
 *  @author ly
 *  @version 1.0 2012-9-5 下午05:38:26
 *  @since jdk1.6 
 */

/**
 * 系统个人桌面表
 */

@Entity
@Table(name = "SYS_DESKTOP")
// 默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Desktop extends BaseEntity {
   
   /* 
    type:
    textList-------文字列表类型；
    picTextList------图文类型；
	picList-------图片类型；
	datagrid------datagrid列表类型；
	statistic1-----统计分析类型1；
	statistic2-----统计分析类型2；
	weather------天气预报类型；
	datetime-----日期时间类型；*/
	public static final long DESKTOPTOUSER_LEVEL1=1;
	public static final long DESKTOPTOUSER_LEVEL2=2;
	public static final long DESKTOPTOUSER_LEVEL3=3;
	private Long id;
	private String title;
	private String keyworlds;
	private String type;
	private String layoutType;//如：30%;40%;30%（3列） 50%;50% （2列）   98% （1列） 
	private String layoutVal;//p1,p2,p3;p4,p5;p6,p7
	private Long level;//1--1级  2--2级  3--3级
	private Long displayNum;
	private String moduleUrl;
	private String dataUrl;
	private String sourceType;//fixed--固定值（使用json数据标识）sql--sql语句	proc--存储过程java--java代码
	private String source;
	private Long height;
	private Long fontNum;
	private Long parentId;
	private List<Role> roleList = Lists.newArrayList();
	private List<Map<String, Object>> list=Lists.newArrayList();
	private List<Long> roleIds = Lists.newArrayList();
	private String systemPath;
	//private String roleListIds;
	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_DESKTOP_ID")
	@SequenceGenerator(name = "SEQ_SYS_DESKTOP_ID", sequenceName = "SEQ_SYS_DESKTOP_ID")
	public Long getId() {
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return title
	 */
	@Column(name = "TITLE", length = 200)
	public String getTitle() {
		return title;
	}
	
	/**
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @return keyworlds
	 */
	@Column(name = "KEY_VAL", length = 100)
	public String getKeyworlds() {
		return keyworlds;
	}
	
	/**
	 * @param keyworlds
	 */
	public void setKeyworlds(String keyworlds) {
		this.keyworlds = keyworlds;
	}
	
	/**
	 * @return type
	 */
	@Column(name = "TYPE", length = 100)
	public String getType() {
		return type;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return layoutType
	 */
	@Column(name = "LAYOUT_TYPE", length = 100)
	public String getLayoutType() {
		return layoutType;
	}
	
	/**
	 * @param layoutType
	 */
	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}
	
	/**
	 * @return layoutVal
	 */
	@Column(name = "LAYOUT_VAL", length = 2000)
	public String getLayoutVal() {
		return layoutVal;
	}
	
	/**
	 * @param layoutVal
	 */
	public void setLayoutVal(String layoutVal) {
		this.layoutVal = layoutVal;
	}
	
	/**
	 * @return level
	 */
	@Column(name = "LEVEL_VAL")
	public Long getLevel() {
		return level;
	}
	
	/**
	 * @param level
	 */
	public void setLevel(Long level) {
		this.level = level;
	}
	
	/**
	 * @return displayNum
	 */
	@Column(name = "DISPLAY_NUM")
	public Long getDisplayNum() {
		return displayNum;
	}
	
	/**
	 * @param displayNum
	 */
	public void setDisplayNum(Long displayNum) {
		this.displayNum = displayNum;
	}
	
	/**
	 * @return moduleUrl
	 */
	@Column(name = "MODULE_URL", length = 2000)
	public String getModuleUrl() {
		return moduleUrl;
	}
	
	/**
	 * @param moduleUrl
	 */
	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}
	
	/**
	 * @return dataUrl
	 */
	@Column(name = "DATA_URL", length = 2000)
	public String getDataUrl() {
		return dataUrl;
	}
	
	/**
	 * @param dataUrl
	 */
	public void setDataUrl(String dataUrl) {
		this.dataUrl = dataUrl;
	}
	
	/**
	 * @return sourceType
	 */
	@Column(name = "SOURCE_TYPE", length = 100)
	public String getSourceType() {
		return sourceType;
	}
	
	/**
	 * @param sourceType
	 */
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
	/**
	 * @return source
	 */
	@Lob
	@Column(name = "SOURCE", columnDefinition = "ntext")
	public String getSource() {
		return source;
	}
	
	/**
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
	/**
	 * @return height
	 */
	@Column(name = "HEIGHT")
	public Long getHeight() {
		return height;
	}
	
	/**
	 * @param height
	 */
	public void setHeight(Long height) {
		this.height = height;
	}
	
	/**
	 * @return fontNum
	 */
	@Column(name = "FONT_NUM")
	public Long getFontNum() {
		return fontNum;
	}
	
	/**
	 * @param fontNum
	 */
	public void setFontNum(Long fontNum) {
		this.fontNum = fontNum;
	}
	
	/**
	 * @return parentId
	 */
	@Column(name = "PARENT_ID")
	public Long getParentId() {
		return parentId;
	}
	
	/**
	 * @param parentId
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	/**
	 * @return roleLis
	 */
	// 多对多定义
	@ManyToMany
	// 中间表定义,表名采用默认命名规则
	@JoinTable(name = "SYS_DESKTOP_TO_ROLE", joinColumns = { @JoinColumn(name = "DESKTOP_ID") }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID") })
	// Fecth策略定义
	@Fetch(FetchMode.SUBSELECT)
	// 集合按id排序.
	@OrderBy("id")
	// 集合中对象id的缓存.
	//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<Role> getRoleList() {
		return roleList;
	}
	
	/**
	 * @param roleLis
	 */
	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	/**
	 * @return the roleIds
	 */
	@Transient
	public List<Long> getRoleIds() {
		return roleIds;
	}

	/**
	 * @param roleIds the roleIds to set
	 */
	public void setRoleIds(List<Long> roleIds) {
		this.roleIds = roleIds;
	}


	/**
	 * @return the list
	 */
	@Transient
	public List<Map<String, Object>> getList() {
		return list;
	}


	/**
	 * @param list the list to set
	 */
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
	@Transient
	public String getSystemPath() {
		return systemPath;
	}


	public void setSystemPath(String systemPath) {
		this.systemPath = systemPath;
	}
}
