/*
 * @(#)UserToTheme.java
 * 2011-5-29 下午03:54:28
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  用户主题配置表
 *  @author YB
 *  @version 1.0
 *  @since 1.6 2011-5-29 下午03:54:28
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_USER_THEME")
//默认的缓存策略.
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserToTheme extends BaseEntity implements Serializable {

	
	/**
	 * long
	 */
	private static final long serialVersionUID = 7777833845546350315L;
	private Long id;
	private User user;
	private Theme theme;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_USER_THEME_ID")
	@SequenceGenerator(name="SEQ_SYS_USER_THEME_ID", sequenceName="SEQ_SYS_USER_THEME_ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
//	@JoinColumn(name = "USER_ID")
//	public User getUser() {
//		return user;
//	}
//	
//	public void setUser(User user) {
//		this.user = user;
//	}
//	@JoinColumn(name = "THEME_ID")
//	public Theme getTheme() {
//		return theme;
//	}
//	
//	public void setTheme(Theme theme) {
//		this.theme = theme;
//	}
	

}
