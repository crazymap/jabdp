/*
 * @(#)LogInfoThreadPool.java
 * 2011-5-28 下午03:20:34
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.web;

import java.io.Serializable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.qyxx.platform.sysmng.logmng.entity.Log;
import com.qyxx.platform.sysmng.logmng.service.LogManager;

/**
 *  日志处理线程池
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-28 下午03:20:34
 */
@Component
@Lazy(false)
@Configuration
public class LogInfoThreadPool implements Serializable {
	
	private static final long serialVersionUID = -4355235863576006796L;

	private static final Logger logger = LoggerFactory.getLogger(LogInfoThreadPool.class);
	
	private ThreadPoolExecutor threadPool;
	
	private static LogManager logManager;
	
	@Value("${log.corePoolSize}")
	private String corePoolSize;
	
	@Value("${log.maxPoolSize}")
	private String maxPoolSize;
	
	@Value("${log.keepAliveTime}")
	private String keepAliveTime;
	
	/**
	 * @param logManager
	 */
	@Autowired
	public void setLogManager(LogManager logManager) {
		LogInfoThreadPool.logManager = logManager;
	}

	@PostConstruct
	public void init() {
		//logger.info(corePoolSize + ";" + maxPoolSize + ";" + keepAliveTime);
		threadPool = new ThreadPoolExecutor(NumberUtils.toInt(corePoolSize, 1), 
				NumberUtils.toInt(maxPoolSize, 1), NumberUtils.toLong(keepAliveTime, 30L), TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(3));
	}
	
	@PreDestroy 
	public void destory() { 
		if(null != threadPool) {
			threadPool.shutdown();
			threadPool = null;
		}
	}
	
	/**
	 * 增加日志记录，交由线程池处理
	 * 
	 * @param log
	 */
	public void addLog(Log log) {
		threadPool.execute(new ThreadPoolTask(log));
	}
	
	/**
	 *  处理任务类，保存日志
	 *  
	 *  @author gxj
	 *  @version 1.0
	 *  @since 1.6 2011-5-28 下午08:15:30
	 */
	private static class ThreadPoolTask implements Runnable, Serializable {

		private static final long serialVersionUID = 3438123821675008087L;
		
		private Log log;

		public ThreadPoolTask(Log log) {
			this.log = log;
		}

		public void run() {
			try {
				logManager.saveLog(log);
			} catch(Exception e) {
				logger.error("添加操作日志失败", e);
			}
		}
	}
	
}
