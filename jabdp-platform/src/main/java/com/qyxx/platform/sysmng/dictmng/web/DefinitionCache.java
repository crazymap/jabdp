/*
 * @(#)DefinitionCache.java
 * 2011-5-24 下午09:19:16
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.dictmng.web;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qyxx.platform.common.definition.Definition;
import com.qyxx.platform.common.definition.DefinitionSource;
import com.qyxx.platform.common.definition.Definition.DefinitionEntity;
import com.qyxx.platform.common.utils.spring.SpringContextHolder;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  数据字典缓存
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-24 下午09:19:16
 */
public class DefinitionCache {
	
	private static Logger logger = LoggerFactory.getLogger(DefinitionCache.class);
	
	private static Lock lock = new ReentrantLock(); 

	private static Map<String,Definition> map = new HashMap<String,Definition>();
	
	public static String getJsonString(String keyWord){
		Definition definition = getDefinition(keyWord);
		return definition.getJsonString();
	}
	
	public static String getJsonString(String keyWord, String locale){
		Definition definition = getDefinition(keyWord, locale);
		return definition.getJsonString();
	}
	
	public static DefinitionEntity getEntityByValue(String keyWord,String value){
		Definition definition = getDefinition(keyWord);
		for(DefinitionEntity definitionEntity:definition.getSet()){
			if(definitionEntity.getValue().equals(value)){
				return definitionEntity;
			}
		}
		return null;
	}
	
	public static DefinitionEntity getEntityByValue(String keyWord,String value, String locale){
		Definition definition = getDefinition(keyWord, locale);
		for(DefinitionEntity definitionEntity:definition.getSet()){
			if(definitionEntity.getValue().equals(value)){
				return definitionEntity;
			}
		}
		return null;
	}
	
	/**
	 * 根据字典类型与字典key获取值
	 * 
	 * @param keyWord
	 * @param value
	 * @return
	 */
	public static String getValueByKey(String keyWord,String value) {
		DefinitionEntity de = getEntityByValue(keyWord, value);
		if(null!=de) {
			return de.getDisplayName();
		} else {
			return null;
		}
	}
	
	/**
	 * 根据字典类型与字典key及语言获取值
	 * 
	 * @param keyWord
	 * @param value
	 * @param locale
	 * @return
	 */
	public static String getValueByKey(String keyWord,String value, String locale) {
		DefinitionEntity de = getEntityByValue(keyWord, value, locale);
		if(null!=de) {
			return de.getDisplayName();
		} else {
			return null;
		}
	}
	
	public static Set<DefinitionEntity> getSet(String keyWord){
		Definition definition = getDefinition(keyWord);
		return definition.getSet();
	}
	
	public static Set<DefinitionEntity> getSet(String keyWord, String locale){
		Definition definition = getDefinition(keyWord, locale);
		return definition.getSet();
	}
	
	private static Definition getDefinition(String keyWord) {
		Definition result = null;
		lock.lock();
		try {
			result = map.get(keyWord);
			if (result == null) {
				DefinitionSource dictManager = SpringContextHolder.getBean("dictManager");
				result = new Definition(dictManager, keyWord);
				map.put(keyWord, result);
			}
		} finally {
			lock.unlock();
		}
		return result;
	}
	
	private static Definition getDefinition(String keyWord, String locale) {
		Definition result = null;
		String key = keyWord + Constants.UNDERLINE + locale;
		lock.lock();
		try {
			result = map.get(key);
			if (result == null) {
				try {
					DefinitionSource dictManager = SpringContextHolder.getBean("dictManager");
					result = new Definition(dictManager, keyWord, locale);
					map.put(key, result);
				} catch(Exception e) {
					logger.error("", e);
				}
			}
		} finally {
			lock.unlock();
		}
		return result;
	}
	
	public static Definition removeDefinition(String keyWord){
		lock.lock();
		try {
			return map.remove(keyWord);
		} finally {
			lock.unlock();
		}
	}
	
	public static Definition removeDefinition(String keyWord, String locale){
		lock.lock();
		try {
			return map.remove(keyWord+Constants.UNDERLINE+locale);
		} finally {
			lock.unlock();
		}
	}
	
}
