/*
 * @(#)GsSecurityMetadataSource.java
 * 2011-5-5 下午10:54:10
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.AntUrlPathMatcher;
import org.springframework.security.web.util.UrlMatcher;


/**
 *  安全元数据
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-5 下午10:54:10
 */
public class GsSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(GsSecurityMetadataSource.class);
	
	private UrlMatcher urlMatcher = new AntUrlPathMatcher();
	
	private ResourceCacheService resourceCacheService;

	
	public ResourceCacheService getResourceCacheService() {
		return resourceCacheService;
	}
	
	@Autowired
	public void setResourceCacheService(
			ResourceCacheService resourceCacheService) {
		this.resourceCacheService = resourceCacheService;
	}
	// According to a URL, Find out permission configuration of this URL.
	//@Transactional(readOnly = true)
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
		/*if(!isLoad) {
			loadResourceDefine();
		}*/
		if (logger.isDebugEnabled()) {
			logger.debug("getAttributes(Object) - start"); //$NON-NLS-1$
		}
		// guess object is a URL.
		String url = ((FilterInvocation) object).getRequestUrl();
		//logger.info("当前url为：" + url);
		Map<String, Collection<ConfigAttribute>> resourceMap = this.resourceCacheService.getResourceCache();
		Iterator<String> ite = resourceMap.keySet().iterator();
		while (ite.hasNext()) {
			String resURL = ite.next();
			if (StringUtils.isNotBlank(resURL) && urlMatcher.pathMatchesUrl(resURL+"*", url)) {
				Collection<ConfigAttribute> returnCollection = resourceMap.get(resURL);
				if (logger.isDebugEnabled()) {
					logger.debug("getAttributes(Object) - end"); //$NON-NLS-1$
				}
				//logger.info("获取成功" + resURL);
				return returnCollection;
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getAttributes(Object) - end"); //$NON-NLS-1$
		}
		return null;
	}
	public boolean supports(Class<?> clazz) {
		return true;
	}
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		return null;
	}
	
	public static void main(String[] args) {
		UrlMatcher urlMatcher = new AntUrlPathMatcher();
		System.out.println(urlMatcher.pathMatchesUrl("/gs/gs-mng!del.action?entityName=test", "/gs/gs-mng!del.action?entityName=test?ddd"));
	}
	
}
