/*
 * @(#)RoleJson.java
 * 2012-7-16 下午03:22:03
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;


/**
 *  
 *  @author xk
 *  @version 1.0 2012-7-16 下午03:22:03
 *  @since jdk1.6 
 */
@JsonAutoDetect(getterVisibility=Visibility.ANY)
public class RoleJson implements Serializable{


	private static final long serialVersionUID = -8281127245492907610L;
	
	private String name;
    private Boolean checked;
    private Boolean nocheck;
    private String id;
	private String pId;
	private Boolean open;	
	private Long idNum;
    private Long orgId;
    private String iconSkin;
    
	public String getIconSkin() {
		return iconSkin;
	}
	
	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}
	public RoleJson(){
    }
    public RoleJson(Long id,String name,Long pId) {
		super();
		this.id = "r_"+id;
		this.name = name;
		this.pId = "org_" + pId;
		this.orgId = pId;
		this.idNum = id;
		this.iconSkin = "iconRole";
	}
    public RoleJson(User user) {
    	super();
		this.id = user.getLoginName();
		this.pId = "org_" + user.getOrganizationId();
		this.name = user.getRealName() + "(" + user.getLoginName() + ")";
		this.idNum = user.getId();
		this.iconSkin = "iconUser";
	}
    public RoleJson(User user,Long rid) {
		super();
		this.id = user.getLoginName();
		this.pId = "r_" + rid;
		this.name = user.getRealName() + "(" + user.getLoginName() + ")";
		this.orgId = rid;
		this.idNum = user.getId();
		this.iconSkin = "iconUser";
	}
    
	public RoleJson(Long id,String name,Organization organization) {
		super();
		this.id = "org_"+id;
		if(null!=organization.getOrganization()) {//用来解决该记录的没有pid时的情况
		this.pId = "org_" + organization.getOrganization().getId();
		}
		this.name = name;
		/*if(null!=organization.getOrganization()) {
			Long parentId = organization.getOrganization().getId();
			this.orgId = parentId;
		}*/
		this.orgId = id;
		this.open = true;
		this.idNum = id;
		this.iconSkin = "iconGroup";
	}
	

	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getChecked() {
		return checked;
	}
	
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	
	public Boolean getNocheck() {
		return nocheck;
	}
	
	public void setNocheck(Boolean nocheck) {
		this.nocheck = nocheck;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getpId() {
		return pId;
	}
	
	public void setpId(String pId) {
		this.pId = pId;
	}
	
	public Boolean getOpen() {
		return open;
	}
	
	public void setOpen(Boolean open) {
		this.open = open;
	}
	
	public Long getIdNum() {
		return idNum;
	}
	
	public void setIdNum(Long idNum) {
		this.idNum = idNum;
	}
	
	public Long getOrgId() {
		return orgId;
	}
	
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

    
}