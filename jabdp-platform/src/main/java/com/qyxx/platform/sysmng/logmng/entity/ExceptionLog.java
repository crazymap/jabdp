/*
 * @(#)ExceptionLog.java
 * 2011-5-31 下午09:13:32
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *  异常日志Log
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-31 下午09:13:32
 */
@Entity
@Table(name = "SYS_EXCEPTION_LOG")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ExceptionLog implements Serializable {

	private static final long serialVersionUID = 4060338835714526303L;
	
	private Long id;
	
	private String threadName;
	
	private String loggerName;
	
	private String level;
	
	private String message;
	
	private Date logTime;
	
	private String methodName;
	
	private String className;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_EXP_LOG_ID")
	@SequenceGenerator(name="SEQ_SYS_EXP_LOG_ID", sequenceName="SEQ_SYS_EXP_LOG_ID")
	public Long getId() {
		return id;
	}

	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return threadName
	 */
	public String getThreadName() {
		return threadName;
	}

	
	/**
	 * @param threadName
	 */
	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

	
	/**
	 * @return loggerName
	 */
	public String getLoggerName() {
		return loggerName;
	}

	
	/**
	 * @param loggerName
	 */
	public void setLoggerName(String loggerName) {
		this.loggerName = loggerName;
	}

	
	/**
	 * @return level
	 */
	public String getLevel() {
		return level;
	}

	
	/**
	 * @param level
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	
	/**
	 * @return message
	 */
	@Lob
	@Column(columnDefinition = "ntext")
	public String getMessage() {
		return message;
	}

	
	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	
	/**
	 * @return logTime
	 */
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getLogTime() {
		return logTime;
	}

	
	/**
	 * @param logTime
	 */
	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	
	/**
	 * @return methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	
	/**
	 * @param methodName
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	
	/**
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	
	/**
	 * @param className
	 */
	public void setClassName(String className) {
		this.className = className;
	}

}
