/*
 * @(#)Task.java
 * 2013-2-25 下午01:54:38
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  任务实体类
 *  @author gxj
 *  @version 1.0 2013-2-25 下午01:54:38
 *  @since jdk1.6
 */
@Entity
@Table(name = "SYS_TASK")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Task extends BaseEntity implements Serializable {
	
	/**
	 * long
	 */
	private static final long serialVersionUID = -1646639495716936010L;

	/**
	 * 任务类型--普通任务
	 */
	public static final String TASK_TYPE_NORMAL = "1";
	
	/**
	 * 任务类型--预警通知任务
	 */
	public static final String TASK_TYPE_PREWARNING = "2";
	
	/**
	 * 任务类型--GPS更新问题
	 */
	public static final String TASK_TYPE_UPDATE_GPS = "3";
	
	private Long id;
	
	private String name;
	
	private String remark;
	
	private String taskType;
	
	private String taskContent;

	
	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_TASK_ID")
	@SequenceGenerator(name="SEQ_SYS_TASK_ID", sequenceName="SEQ_SYS_TASK_ID")
	public Long getId() {
		return id;
	}

	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return name
	 */
	@Column(name="NAME",length=200)
	public String getName() {
		return name;
	}

	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * @return remark
	 */
	@Column(name="REMARK",length=500)
	public String getRemark() {
		return remark;
	}

	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	/**
	 * @return taskType
	 */
	@Column(name="TASK_TYPE",length=50)
	public String getTaskType() {
		return taskType;
	}

	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	
	/**
	 * @return taskContent
	 */
	@Lob
	@Column(name = "TASK_CONTENT", columnDefinition = "ntext")
	public String getTaskContent() {
		return taskContent;
	}

	
	/**
	 * @param taskContent
	 */
	public void setTaskContent(String taskContent) {
		this.taskContent = taskContent;
	}
	
}
