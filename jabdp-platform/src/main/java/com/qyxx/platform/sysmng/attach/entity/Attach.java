package com.qyxx.platform.sysmng.attach.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.qyxx.platform.common.module.entity.BaseEntity;

/**
 * 附件主表
 */
@Entity
@Table(name = "SYS_ATTACH")
// 默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Attach extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	private Long id;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_ATTACH_ID")
	@SequenceGenerator(name="SEQ_SYS_ATTACH_ID", sequenceName="SEQ_SYS_ATTACH_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
