/*
 * @(#)BsFieldInsDao.java
 * 2016年10月19日 下午4:41:11
 * 
 * Copyright (c) 2016 QiYunInfoTech - All Rights Reserved.
 * ====================================================================
 * The QiYunInfoTech License, Version 1.0
 *
 * This software is the confidential and proprietary information of QiYunInfoTech.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with QiYunInfoTech.
 */
package com.qyxx.platform.sysmng.userconfig.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.userconfig.entity.BsFieldIns;

/**
 *  业务字段实例表
 *  @author bobgao
 *  @version 1.0 2016年10月19日 下午4:41:11
 *  @since jdk1.7 
 */
@Component
public class BsFieldInsDao extends HibernateDao<BsFieldIns, Long>{

}
