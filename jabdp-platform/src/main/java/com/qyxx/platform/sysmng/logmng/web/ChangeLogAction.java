/*
 * @(#)ExpLogAction.java
 * 2011-5-31 下午09:34:04
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.web;

import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.ServletUtils;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.logmng.entity.ChangeLog;
import com.qyxx.platform.sysmng.logmng.service.ChangeLogManager;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  变更日志Action
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-31 下午09:34:04
 */
@Namespace("/sys/log")
public class ChangeLogAction extends CrudActionSupport<ChangeLog> {
	
	private static final long serialVersionUID = 6974441046917587058L;

	private ChangeLogManager changeLogManager;
	
	//-- 页面属性 --//
	private Long id;
	private ChangeLog entity;
	
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @param changeLogManager
	 */
	@Autowired
	public void setChangeLogManagerManager(
			ChangeLogManager changeLogManager) {
		this.changeLogManager = changeLogManager;
	}

	@Override
	public ChangeLog getModel() {
		return entity;
	}

	@Override
	public String list() throws Exception {
		return SUCCESS;
	}
	
	@JsonOutput
	public String queryList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(Struts2Utils.getRequest());
		pager = changeLogManager.searchLog(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 保存变更日志
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#save()
	 */
	@Override
	public String save() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		entity.setOperIp(ServletUtils.getIpAddr(Struts2Utils.getRequest()));
		entity.setOperTime(new Date());
		entity.setLoginName(user.getLoginName());
		entity.setRealName(user.getRealName());
		entity.setEmployeeName(user.getNickname());
		entity.setOrgName(user.getOrganizationName());
		changeLogManager.saveLog(entity);
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@JsonOutput
	@Override
	public String view() throws Exception {
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		try {
			if (id != null) {
				entity = changeLogManager.getLog(id);
			} else {
				entity = new ChangeLog();
			}	
		} catch(Exception e) {
			logger.error("", e);
		}
	}

}
