/*
 * @(#)UpgradeAction.java
 * 2011-9-24 下午09:36:30
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.attach.web;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Namespace;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.gsc.utils.SystemParam;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.attach.service.AttachManager;
import com.qyxx.platform.sysmng.utils.Constants;
/**
 * 附件上传处理类
 */

@Namespace("/sys/ckeditor")
public class CkeditorUploadAction extends CrudActionSupport<File> {
	private SystemParam systemParam;
	
	private File upload;
	
	private String uploadFileName;
	
	
	public File getUpload() {
		return upload;
	}


	public void setUpload(File upload) {
		this.upload = upload;
	}


	public String getUploadFileName() {
		return uploadFileName;
	}


	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}


	public void setSystemParam(SystemParam systemParam) {
		this.systemParam = systemParam;
	}


	@Override
	public String list() throws Exception {
		return SUCCESS;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonOutput
	@Override
	public String save() throws Exception {
		return null;
	}

	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub

	}
	
	 @JsonOutput
	  public String saveImage() throws Exception {
			return null;
		}
	 
	 
	/**
	 * ckeditor上传图片
	 * @return
	 * @throws Exception
	 */
	 public String ckUploadImage() throws Exception{
		 String ckeditorFuncNum = Struts2Utils.getParameter("CKEditorFuncNum");  
         String alt_msg = "";  
         String fileForderName = DateFormatUtils.format(new Date(), "yyyyMMdd");
			String filePackage = systemParam.getUploadPicPath()
					+ com.qyxx.platform.gsc.utils.Constants.UPLOADFILE_PATH;
			String filePath=com.qyxx.platform.gsc.utils.Constants.UPLOADFILE_IMAGE
				+ fileForderName +com.qyxx.platform.gsc.utils.Constants.PATH_SEPARATOR+DateFormatUtils.format(System.currentTimeMillis(),
							"yyyyMMddHHmmssSSS")
					+ uploadFileName.substring(uploadFileName.lastIndexOf("."),
							uploadFileName.length());
			File destFile = new File(filePackage + filePath);
			
	      FileUtils.copyFile(upload, destFile);
         Struts2Utils.renderHtml("<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction("  
                 + ckeditorFuncNum  
                 + ", '"  
                 +systemParam.getVirtualPicPath()+ filePath  
                 + "' , '"  
                 + alt_msg  
                 + "');</script>");  

    
		 return null;
	 }
}
