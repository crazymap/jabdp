/*
 * @(#)Constants.java
 * 2011-5-15 下午01:04:17
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.utils;


/**
 *  系统管理常量类
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-15 下午01:04:17
 */

public class Constants {
	
	/**
	 * 用户session名称
	 */
	public static final String USER_SESSION_ID = "USER";
	
	/**
	 * 语言session名称
	 */
	public static final String LOCALE_SESSION_ID = "LOCALE";
	
	/**
	 * 风格session名称
	 */
	public static final String THEME_STYLE_SESSION_ID = "THEME_STYLE";
	
	/**
	 * 颜色session名词
	 */
	public static final String THEME_COLOR_SESSION_ID = "THEME_COLOR";
	
	/**
	 * 请求参数中语言参数名称
	 */
	public static final String REQUEST_LOCALE = "request_locale";
	
	/**
	 * 默认系统风格
	 */
	public static final String DEFAULT_THEME_STYLE = "themes/default";
	
	/**
	 * 默认系统色调
	 */
	public static final String DEFAULT_THEME_COLOR = "blue";
	
	/**
	 * 下划线
	 */
	public static final String UNDERLINE = "_";
	
	/**
	 * 启用-1
	 */
	public static final String ENABLED = "1";
	
	/**
	 * 禁用-0
	 */
	public static final String DISABLED = "0";
	
	/**
	 * 字典描述
	 */
	public static final String DESC = "desc";
	
	/**
	 * 字典语言类别
	 */
	public static final String LOCALE = "locale";
	
	/**
	 * 登录消息名称
	 */
	public static final String LOGIN_MESSAGE = "系统登录";
	
	/**
	 * 注销名称
	 */
	public static final String LOGOUT_MESSAGE = "系统注销";
	
	/**
	 * 超时注销名称
	 */
	public static final String LOGOUT_TIMEOUT_MESSAGE = "系统超时退出";
	
	/**
	 * 顶级部门ID
	 */
	public static final Long USER_SUPER_ORGANIZATION = 1l;
	/**
	 * 性别男
	 */
	public static final String  USER_SEX_MAN = "1";
	/**
	 * 性别女
	 */
	public static final String  USER_SEX_WOMAN = "0";
	/**
	 * 超级管理员
	 */
	public static final String  IS_SUPER_USERR = "1";
	/**
	 * 非超级管理员
	 */
	public static final String  IS_NOT_SUPER_USERR = "0";
	/**
	 * 无效用户
	 */
	public static final String  NO_USING = "0";
	/**
	 * 有效用户
	 */
	public static final String  IS_USING = "1";
	/**
	 * 已删除状态
	 */
	public static final String  IS_DELETE = "2";
	/**
	 * 允许远程登录
	 */
	public static final String  CAN_REMOTELOGIN = "0";
	/**
	 * 不允许远程登录
	 */
	public static final String  NO_REMOTELOGIN = "1";

	/**
	 * 默认密码
	 */
	public static final String DEFAULT_PASSWORD = "123456";
	
	/**
	 * 资源类型-菜单
	 */
	public static final String RESOURCE_TYPE_MENU = "0";
	
	/**
	 * 资源类型-按钮
	 */
	public static final String RESOURCE_TYPE_BUTTON = "1";
	
	/**
	 * 资源类型-字段
	 */
	public static final String RESOURCE_TYPE_FIELD = "2";
	
	/**
	 * 资源类型-数据
	 */
	public static final String RESOURCE_TYPE_DATA = "3";
	
	/**
	 * 资源等级-0级
	 */
	public static final String RESOURCE_LEVEL_0 = "0";
	
	/**
	 * 资源等级-1级
	 */
	public static final String RESOURCE_LEVEL_1 = "1";
	
	/**
	 * 资源等级-2级
	 */
	public static final String RESOURCE_LEVEL_2 = "2";
	
	/**
	 * 资源等级-3级
	 */
	public static final String RESOURCE_LEVEL_3 = "3";
	
	/**
	 * 资源等级-4级
	 */
	public static final String RESOURCE_LEVEL_4 = "4";
	
	/**
	 * 资源等级-5级
	 */
	public static final String RESOURCE_LEVEL_5 = "5";
	
	/**
	 * 用户名Cookie名称
	 */
	public static final String USER_NAME_COOKIE = "username";
	
	/**
	 * 用户名Cookie有效期，默认7天
	 */
	public static final int USER_NAME_COOKIE_MAX_AGE = 7*24*60*60;
	
	/**
	 * 默认编码UTF-8
	 */
	public static final String DEFAULT_ENCODE = "UTF-8";
	
	/**
	 * 资源相关属性 查询页面url、url数组、按钮key、按钮名称
	 */
	public static final String RESOURCE_QUERY_URL = "/gs/gs-mng.action?entityName=";
	
	public static final String[] RESOURCE_BT_URL = new String[]{"/gs/gs-mng!add.action?entityName=",
															"/gs/gs-mng!modify.action?entityName=",
															"/gs/gs-mng!save.action?entityName=",
															"/gs/gs-mng!view.action?entityName=",
															"/gs/gs-mng!edit.action?entityName=",
															"/gs/gs-mng!delete.action?entityName=",
															"/sys/attach/attach.action?entityName=",
															"/sys/report/report.action?entityName=",
															"/gs/process!startApprove.action?entityName=",
															"/gs/process!cancelApprove.action?entityName=",
															"/gs/process!invalidProcess.action?entityName=",
															"/gs/gs-mng!edit.action?isChange=1&entityName=",
															"/gs/gs-mng!revise.action?entityName=",
															"/sys/log/revise-log.action?mn=",
															"/gs/data-share-config!save.action?entityName=",
															"/gs/gs-mng!importModuleExcel.action?entityName=",
															"/gs/gs-mng!exportModuleExcel.action?entityName="
														};
	
	public static final String[] RESOURCE_BT_KEY = new String[]{"ADD","EDIT","SAVE","VIEW","COPY","DEL","ATTACH","REPORT",
														"START_APPROVE","CANCEL_APPROVE","INVALID_DATA","CHANGE","REVISE",
														"REVISE_VIEW","DATA_SHARE","IMPORT_EXCEL","EXPORT_EXCEL"};
	
	public static final String[] RESOURCE_BT_NAME = new String[]{"新增","修改","保存","查看","复制","删除","附件","报表",
														"审核通过/启动流程","取消审核/取消审批","作废","变更","修订","查看修订日志",
														"数据共享","导入Excel","导出Excel"};
	
	public static final String[] RESOURCE_BT_RULE = new String[]{"新增记录","修改记录（ID为{id}）","保存记录（ID为{id}）","查看记录（ID为{id}）",
														"复制记录（ID为{id}）", "删除记录（ID为{ids}）","查看附件列表（ID为{bid}）",
														"查看报表（ID为{bid}）","审核通过/启动流程（ID为{ids}）","取消审核/取消审批（ID为{ids}）",
														"作废记录（ID为{id}）","变更记录（ID为{id}）","修订记录（ID为{id}）","查看修订日志（记录ID为{id}）",
														"数据共享（记录ID为{ids}）","导入Excel", "导出Excel（ID为{value}）"};
	
	/**
	 * 按钮URL前缀
	 */
	public static final String RESOURCE_BUTTON_URL = "button";
	
	/**
	 * 数据权限-组织内可见，默认是个人可见
	 */
	public static final String RESOURCE_DATA_IN_ORG = "visibleInOrg";
	
	public static final String RESOURCE_ALL_DATA_VISIBLE = "allDataVisible";
	
	public static final String RESOURCE_USER_VISIBLE = "selUserVisible";//选择用户可见
	
	public static final String RESOURCE_SHARE_USER_VISIBLE = "shareUserVisible";//共享用户数据可见
	
	public static final String RESOURCE_FIELD_COND_VISIBLE = "fieldCondVisible";//满足字段条件可见
	
	public static final String[] RESOURCE_DATA_URL = new String[]{RESOURCE_DATA_IN_ORG,RESOURCE_ALL_DATA_VISIBLE,
						RESOURCE_USER_VISIBLE,RESOURCE_SHARE_USER_VISIBLE, RESOURCE_FIELD_COND_VISIBLE};
	
	public static final String[] RESOURCE_DATA_KEY = new String[]{"DATA_IN_ORG","DATA_ALL_VISIBLE",
						"SEL_USER_VISIBLE","SHARE_USER_VISIBLE","FIELD_COND_VISIBLE"};
	
	public static final String[] RESOURCE_DATA_NAME = new String[]{"组织内所有数据可见","所有数据可见",
						"选择可见用户","共享用户数据可见","满足属性条件可见"};
	
	//字段权限URL前缀
	public static final String RESOURCE_FIELD_URL = "field";
	//表单权限URL前缀
	public static final String RESOURCE_FORM_URL = "form";
	//字段权限KEY前缀
	public static final String RESOURCE_FIELD_KEY = "FD";
	//字段权限--只读
	public static final String RESOURCE_FIELD_READONLY = "readonly";
	//字段权限--可见
	public static final String RESOURCE_FIELD_VISIBLE = "visible";
	//字段权限--只读
	public static final String RESOURCE_FIELD_READONLY_CAPTION = "（只读）";
	//字段权限--可见
	public static final String RESOURCE_FIELD_VISIBLE_CAPTION = "（可见）";
	
	/**
	 * 连接符
	 */
	public static final String MINUS = "-";
	
	public static final String LOCALE_ZH_CN = "zh_CN";
	public static final String LOCALE_ZH_TW = "zh_TW";
	public static final String LOCALE_EN = "en";
	
	public static final String COMMA = ",";
	
	/**
	 * 超级管理员角色ID
	 */
	public static final Long SUPER_ADMIN_ROLE_ID = 1L;
	
	/**
	 * 超级管理员用户ID
	 */
	public static final Long SUPER_ADMIN_USER_ID = 1L;
	
	// 状态：初始化
	public static final String STATUS_INIT = "00";
	// 状态：草稿
	public static final String STATUS_DRAFT = "10";
	// 状态：审批中
	public static final String STATUS_APPROVAL = "20";
	// 状态：审批结束
	public static final String STATUS_FINISH = "31";
	// 状态：审批通过
	public static final String STATUS_PASS = "31";
	// 状态：审批不通过
	public static final String STATUS_NO_PASS  = "32";
	// 状态：审核通过
	public static final String STATUS_OK = "35";
	// 状态：作废
	public static final String STATUS_CANCEL = "40";
	// 布尔值：是
	public static final String BOOL_YES = "1";
	// 布尔值：否
	public static final String BOOL_NO = "0";
}
