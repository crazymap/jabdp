package com.qyxx.platform.inf.adapter;

/**
 *  适配器异常信息
 *  
 *  @author gxj
 *  @version 1.0 2014-4-16 下午02:21:22
 *  @since jdk1.6
 */
public class AdapterException extends Exception {

	/**
	 * 序列版本号
	 */
	private static final long serialVersionUID = 6734835826041388601L;

	/**
	 * 默认构造函数
	 */
	public AdapterException() {
		super();
	}
	
	/**
	 * 根据message信息构造异常对象
	 * @param message
	 */
	public AdapterException(String message) {
		super(message);
	}
	
	/**
	 * 根据message信息，cause异常原因构造异常对象
	 * @param message
	 * @param cause
	 */
	public AdapterException(String message, Throwable cause) {
		super(message, cause);
	}
	
	/**
	 * 根据cause异常原因构造异常对象
	 * @param cause
	 */
	public AdapterException(Throwable cause) {
		super(cause);
	}
}
