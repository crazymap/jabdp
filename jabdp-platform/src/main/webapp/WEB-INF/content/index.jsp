<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="com.qyxx.platform.sysmng.accountmng.entity.User" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
SystemParam systemParam = SpringContextHolder.getBean("systemParam");
String portalUrl = "";
String systemPortalUrl = systemParam.getSystemPortalUrl();
if(StringUtils.isNotBlank(systemPortalUrl)) {
	portalUrl = systemPortalUrl;
}
User user = (User)session.getAttribute("USER");
if(null!=user) {
	String rolePortalUrl = user.getPortalUrl();
	if(StringUtils.isNotBlank(rolePortalUrl)) {
		portalUrl = rolePortalUrl;
	}
} else {
	response.sendRedirect(request.getContextPath() + "/login.action");
}
if(StringUtils.isNotBlank(portalUrl)) {
	portalUrl = request.getContextPath() + "/" + portalUrl;
}
request.setAttribute("portalUrl", portalUrl);
session.setAttribute("THEME_VERSION","easyui");
%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title><%=systemParam.getSystemTitle()%></title>
	<%@ include file="/common/meta-css.jsp" %>
	<style type="text/css">
		#main_tabs_div>.tabs-panels>.panel>.panel-body {
			overflow:hidden;
		}
		.pwdcheck {
			/*margin:50px auto;
			width:700px;
			height:50px;*/
		}
			.pwdcheck p.pwdInput input.pwd{
				width:150px;
				height:20px;
			}
			.pwdcheck p.pwdInput label {
				font-size:12px;
			}
			.pwdcheck p.pwdInput label.tips{
				color:red;
			}
			.pwdcheck p.pwdInput label span {
				color:#f00;
				padding:0 5px;
			}
			.pwdcheck p.pwdColor {
				width:150px;
				height:5px;
				margin-top:6px;
				margin-left:6px;
			}
			.pwdcheck p.pwdColor span {
				display:inline-block;
				width:40px;
				height:4px;
				float:left;
				margin-right:10px;
				background:#ccc;
			}
			
			.pwdcheck p.pwdText {
				margin-left:6px;
			}
			.pwdcheck p.pwdText span{
				margin-right:20px;
				margin-left:10px;
				font-size:12px;
				color:#ddd;
			}

			/* 密码强度改变的样式 */

			.pwdcheck p.pwdColor span.co1 {
				background:#f00;
			}
			.pwdcheck p.pwdColor span.co2 {
				background:#00ff66;
			}

			.pwdcheck p.pwdColor span.co3 {
				background:#0033cc;
			}		
	</style>
</head>
<body class="easyui-layout">
		<div region="north" <%-- iconCls="icon-home" title="<s:text name="system.index.title"/>" split="true" --%> style="height:46px;padding:0px;overflow:hidden;" border="false">
			<div class="index_top_bg">
				<div style="float:left;padding-left:5px;" class="index_top_system">
					<div style="float:left;"><img src="${imgPath}/<%=systemParam.getSystemLogo()%>" style="height:40px;"/></div>
					<div style="float:left;padding-left:10px;padding-top:15px;"><span class="index_top_system"><%=systemParam.getSystemTitle()%></span></div>
					<div style="clear:both;"></div>
				</div>
				<div style="float:left;padding-top:25px;"><span style="font-size:20px;color:red;"><%-- V1.0.0 --%></span></div>
				<div style="float:right;padding:2px 20px;text-align:right;">
					<div style="float:left;">
						<div style="padding:10px 20px;" class="bs">
							<!-- <a class="styleswitch a1" style="CURSOR: pointer" title="橘黄色" rel="orange"></a>
							<a class="styleswitch a2" style="CURSOR: pointer" title="竹绿色" rel="green"></a>  -->
							<a class="styleswitch a3" style="CURSOR: pointer" title="<s:text name='system.top.blue.title'/>" rel="blue"></a>	
							<a class="styleswitch a4" style="CURSOR: pointer" title="<s:text name='system.top.gray.title'/>" rel="gray"></a>	
							<!-- <a class="styleswitch a5" style="CURSOR: pointer" title="艳红色" rel="pink"></a>		 -->
						</div>
					</div>
					<div style="float:right;">
						<div>
							<s:if test='#session.USER.isSuperAdmin=="1"'>
							<div style="float:left;">
								<a href="javascript:doRefreshCache();" class="easyui-linkbutton" plain="true" iconCls="icon-reload" title="<s:text name="system.top.synchronization.title"/>"></a>
							</div>
							</s:if>
							<div style="float:right;padding-top:3px;" class="index_top_button">
								<div>
								    <a href="#" id="bt_pad" title="切换至新主题"><span style="vertical-align:middle;"><img src="${imgPath}/index.png" /></span>&nbsp;<s:text name="新主题"/></a>
									<a href="#" id="bt_homepage"><span style="vertical-align:middle;"><img src="${imgPath}/index.png" /></span>&nbsp;<s:text name="system.top.homepage.title"/></a>
									<a href="#" id="bt_favorite"><span style="vertical-align:middle;"><img src="${imgPath}/c.png" /></span>&nbsp;<s:text name="system.top.favorite.title"/></a>
									<a href="#" id="bt_help"><span style="vertical-align:middle;"><img src="${imgPath}/help.png" /></span>&nbsp;<s:text name="system.top.help.title"/></a>
									<a href="#" id="bt_fullscreen"><span style="vertical-align:middle;"><img src="${imgPath}/s.png" /></span>&nbsp;<s:text name="system.top.window.title"/></a>
									<a href="#" id="bt_cancelfullscreen" style="display:none;"><span style="vertical-align:middle;"><img src="${imgPath}/s.png" /></span>&nbsp;<s:text name="system.top.cancelwindow.title"/></a>
									<a href="#" id="bt_update"><span style="vertical-align:middle;"><img src="${imgPath}/o.png" /></span>&nbsp;<s:text name="system.login.userInfo.title"/></a>
									<a href="#" id="bt_logout"><span style="vertical-align:middle;"><img src="${imgPath}/o.png" /></span>&nbsp;<s:text name="system.logout.title"/></a>
								</div>
							</div>
							<div style="clear:both;"></div>
						</div>
						<div style="font-size:14px;">
							<%-- 
							<s:text name="system.index.welcome.title">
								<s:param><s:property value="#session.USER.realName" /></s:param>
								<s:param><s:property value="#session.USER.organizationName" /></s:param>
							</s:text>--%>
							<s:text name="system.sysmng.user.name.title"/>：<s:property value="#session.USER.realName" />&nbsp;&nbsp;&nbsp;&nbsp;
							<s:if test="%{#session.USER.nickname != null && #session.USER.nickname != ''}"><s:text name="system.sysmng.user.nickname.title"/>：<s:property value="#session.USER.nickname" />&nbsp;&nbsp;&nbsp;&nbsp;</s:if>
							<s:text name="system.sysmng.role.organization.title"/>：<s:property value="#session.USER.organizationName" />
						</div>
					</div>
					<div style="clear:both;"></div>
				</div>
				<div style="float:right;">
					<div style="height:25px;padding-top:2px;">
					<%-- <marquee behavior="scroll" scrollamount="1" align="absmiddle" onMouseOut="this.start()" onMouseOver="this.stop()">
						<a href="javascript:parent.addTab('系统升级通知', '${ctx}/sys/notice/news!viewNews.action?id=203');" style="color:red;">New系统将于今晚进行性能优化升级</a>
					</marquee> --%>
					</div>
					<div>
						<div style="float:left;">
							<a href="javascript:void(0)" onclick="parent.addTab('备忘录管理', '${ctx}/sys/memo/memo.action');" title="备忘录"><span style="vertical-align:middle;"><img src="${imgPath}/reminder.png" /></span></a>
						</div>
						<div style="float:left;">
						当前时间：<span id="_span_time_"></span>
						</div>
						<div style="clear:both;"></div>
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
		<div region="center" style="overflow:hidden;" border="false">
			<div id="main_tabs_div" class="easyui-tabs" fit="true" border="false">
				<div title="<s:text name="system.index.workspace.title"/>" closable="false" style="padding:0px;">
					<div id="layout_menu" class="easyui-layout" fit="true">
						<div region="west" iconCls="icon-menu" split="true" title="<s:text name="system.index.menu.title"/>" style="width:200px;padding:1px;" border="false">
							<div id="dv_menu" class="easyui-accordion" fit="true" border="false">
							</div>
						</div>
						<div region="center" title="" split="true" border="false" style="overflow:hidden;">
                        	<iframe  id="ifr_portal" scrolling="auto" frameborder="0"
                        	         src=""  style="width:100%;height:100%;overflow:hidden;visibility:visible;"></iframe>
                       	</div>
					</div>
				</div>
			</div>
		</div>
		<div style="display:none;">
		<div id="aboutWin" class="easyui-window" closed="true" minimizable="false" modal="true" title="About V1.0.0" style="width:450px;height:100px;">
			<div style="text-align:center;padding:10px 10px;"><s:text name="system.index.copyright.title"/></div>
			<%--<div style="text-align:center;padding:2px 2px;"><a href="javascript:void(0);" onclick="$('#authWin').window('open');"><s:text name="system.index.license.info.title"/></a></div>--%>
		</div>
		<%--<div id="authWin" class="easyui-window" closed="true" minimizable="false" href="${ctx}/index!authInfo.action"
			modal="true" title="<s:text name="system.index.license.info.title"/>" style="width:400px;height:210px;padding:10px 10px;font-size:18px;">
		</div>--%>
		<div id="tab_mm" class="easyui-menu" style="width:150px;">
	        <div id="mm-tabclose"><s:text name="system.button.close.title" /></div>
	        <div id="mm-tabcloseall"><s:text name="system.top.tabCloseAll.title" /></div>
	        <div id="mm-tabcloseother"><s:text name="system.top.tabCloseOther.title" /></div>
	        <div class="menu-sep"></div>
	        <div id="mm-tabcloseright"><s:text name="system.top.tabCloseRight.title" /></div>
	        <div id="mm-tabcloseleft"><s:text name="system.top.tabCloseLeft.title" /></div>
		</div>
		<div id="aboutPass" class="easyui-window" closed="true" minimizable="false" modal="true" title="<s:text name="system.login.userInfo.title"/>" style="width:380px;height:320px;">
				<div style="text-align:center;padding:10px 10px;" class="easyui-layout" fit="true">
					<div region="center" border="false">
						<div id="userInfo" class="easyui-tabs"  fit="true">
							<div title="<s:text name="system.login.userInfo.title" />"  style="padding:5px;">
							<form action="" name="userInfoForm" id="userInfoForm" method="post" style="margin: 0px;">
								  <input type="hidden" name="id" id="_u_id" value="<s:property value="#session.USER.id" />" />
								<table align="center" border="0" cellpadding="0" cellspacing="1"
									   class="table_form">
								  <tbody>
								  	<tr>						  		
								  		<th><label for="realName"><s:text name="system.sysmng.user.name.title" />:</label></th>
										<td align="left"><input class="easyui-validatebox Itext"  required="true" type="text" name="realName" id="realName" value="<s:property value="#session.USER.realName" />" ></input>&nbsp;<font color="red">*</font></td>
									</tr>
									<tr>
								  		<th><label for="nickname"><s:text name="system.sysmng.user.nickname.title" />:</label></th>
										<td align="left"><input  type="text" class="Itext" name="nickname" id="nickname" value="<s:property value="#session.USER.nickname" />" ></input></td>
									</tr>
									<tr>
								  		<th><label for="signature"><s:text name="英文名" />:</label></th>
										<td align="left"><input  type="text" class="Itext" name="signature" id="signature" value="<s:property value="#session.USER.signature" />" ></input></td>
									</tr>
									<tr>
								  		<th><label for="mobilePhone"><s:text name="system.sysmng.user.mobilephone.title" />:</label></th>
										<td align="left"><input class="easyui-validatebox Itext" required="true" value="<s:property value="#session.USER.mobilePhone" />" type="text" name="mobilePhone" id="mobilePhone" validType="mobile" invalidMessage="请输入正确的11位手机号码.格式:13120002221"></input>&nbsp;<font color="red">*</font></td>
									</tr>
									<tr>
								  		<th><label for="fax"><s:text name="固定电话" />:</label></th>
										<td align="left"><input class="easyui-validatebox Itext" value="<s:property value="#session.USER.fax" />" type="text" name="fax" id="fax"></input></td>
									</tr>
									<tr>
								  		<th><label for="email"><s:text name="system.sysmng.user.email.title" />:</label></th>
										<td align="left"><input class="easyui-validatebox Itext" type="text" value="<s:property value="#session.USER.email" />" name="email" id="email"  validType="email"></input></td>
									</tr>							
								</tbody>
							   </table>
								</form> 
								</div>
									<div title="<s:text name="system.login.changepasswords.title" />"  style="padding:5px;">
							<form action="" name="passWordForm" id="passWordForm" method="post" style="margin: 0px;">
								<table align="center" border="0" cellpadding="0" cellspacing="1" class="table_form">
								  <tbody>								 				
									<tr>
										<th><label for="oldPass"><s:text name="system.login.oldpassword.title" /></label></th>
										<td align="left"><input type="password" name="oldPass" id="oldPass" class="easyui-validatebox Itext" required="true" validtype="length[6,18]" invalidMessage="<s:text name="system.login.passwordLength.title"/>"/>&nbsp;<font color="red">*</font></td>
									  </tr>
									  <tr>
										<th valign="top"><label for="newPass"><s:text name="system.login.newpassword.title"/></label></th>
										<td align="left">
											<div class="pwdcheck">
												<p class="pwdInput">
												<input type="password" class="pwd easyui-validatebox Itext" id="newPass" name="newPass" required="true" maxlength="18">&nbsp;<font color="red">*</font>
												<label class="tips">由字母（区分大小写）、数字、符号组成6-18位</label>
												</p>
												<p class="pwdColor">
													<span class="c1"></span>
													<span class="c2"></span>
													<span class="c3"></span>
												</p>
												<p class="pwdText">
													<span>弱</span>
													<span>中</span>
													<span>强</span>
												</p>
											</div>
										<!--  <input type="password" name="newPass" id="newPass" class="easyui-validatebox" required="true" validtype="length[6,18]" invalidMessage="<s:text name="system.login.passwordLength.title"/>"/>&nbsp;<font color="red">*</font>-->
										</td>
									  </tr>
									  <tr>
										<th><label for="confPass"><s:text name="system.login.confirmpassword.title" /></label></th>
										<td align="left"><input type="password" name="rePass" id="rePass" class="easyui-validatebox Itext" required="true" validType="equalTo['#newPass']" invalidMessage="<s:text name="system.login.passwordError.title"/>"/>&nbsp;<font color="red">*</font></td>
									  </tr>
								</tbody>
							   </table>
								</form> 
								</div>
								<div title="<s:text name="拥有角色" />"  style="padding:5px;">
									<table align="center" border="0" cellpadding="0" cellspacing="1" class="table_form">
									  <tbody>
									  	<s:iterator value="#session.USER.roleList" id="array">
								  			<tr>
								  		   	<td><s:property value="roleName"/></td>
									  		</tr>
								  		</s:iterator>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
						<div region="south" border="false" style="text-align: right; ">
							 <a id="mm" class="easyui-linkbutton" iconCls="icon-ok"
								href="javascript:void(0);" onclick="doUpdate();"><s:text
								name="system.button.ok.title" /> </a> 
							 <a class="easyui-linkbutton"
								iconCls="icon-cancel" href="javascript:void(0);"
								onclick="doCanel();"><s:text
								name="system.button.cancel.title" /> </a>
					  	</div>
				</div>
			</div>
			<div id="aboutDoAction"  closed="true" minimizable="false" modal="true" resizable="false" title="<s:text name='system.top.message.title'/>" style="width:600px;height:400px;">
				<div class="easyui-layout" fit="true">
				<div region="north" title="" border="false" style="height: 37px; padding: 1px;">
					<form onsubmit="return false;" name="toDoForm" id="toDoForm">
						<table border="0" cellpadding="0" cellspacing="1" class="table_form">
							<tbody>
								<tr>
									<th><label for="keyWords"><s:text name="关键字" />:</label></th>
									<td><input type="text" name="keyWords" id="keyWords" class="Itext" placeholder="请输入关键字搜索"></input></td>						
									<td><button type="submit" id="_bt_query_"><s:text name="system.search.button.title" /></button>
									&nbsp;&nbsp;
									<button type="button" id="_bt_reset_"><s:text name="system.search.reset.title" /></button></td>								
								</tr>
							</tbody>
						</table>
					</form>
				</div>
				<div region="center"  title="" border="false" style="padding: 1px; overflow: hidden;">
					<div id="tnotice" fit="true" border="false" tools="#about_do_tab-tools">
						<div selected="true"
							title="<s:text name="system.sysmng.notice.todoSomething.title"/>">
							<table id="toDoList" border="false"></table>
						</div>
						<div title="<s:text name="system.sysmng.notice.list.title"/>">
							<table id="noReadNoticeDetail" border="false"></table>
						</div>
					</div>
				</div>
				<div region="south" border="false" align="right" style="padding:1px;height:25px;overflow:hidden;">
					自动弹出通知间隔时间设置
					<select id="aboutDoActionConfig" style="padding:1px;width:60px">
	 					<option>不弹出</option>
	 					<option>1</option>
	 					<option>2</option>
	 					<option selected="selected">5</option>
	 					<option>10</option>
	 					<option>30</option>
	 					<option>60</option>
					</select>
					分钟
					<input type="button" value="设置" style="height:22px" onClick="aboutDoActionConfig()"></input>
				</div>
			</div>
			</div>
			<div id="myNotice"  style="width:120px;height:0px;">
			</div>
			<div id="about_do_tab-tools" style="border-width:0 0 1px 0;">
			    <a id="_batch_agree_" href="#" class="easyui-linkbutton" plain="true" iconCls="icon-ok" onclick="doBatchView();" title="批量查看">批量查看</a>
				<a id="_batch_read_" href="#" class="easyui-linkbutton" plain="true" iconCls="icon-contacts-ico" onclick="doReadNotice();" title="批量阅读">批量阅读</a>
				<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-scheduled_Tasks" onclick="addTab('待办历史','${ctx}/sys/workflow/process-history.action');$('#aboutDoAction').window('close');" title="待办历史">待办历史</a>	
				<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-chat-ico" onclick="addTab('通知历史','${ctx}/sys/notice/notice!noticeToUserList.action');$('#aboutDoAction').window('close');" title="通知历史">通知历史</a>		
			</div>
		</div>
		<div id="memoComet" style="visibility:visible;"></div>
	<%@ include file="/common/meta-js.jsp" %>
	<!-- comet -->
	<script type="text/javascript" src="${ctx}/dwr/engine.js"></script>
	<script type="text/javascript" src="${ctx}/dwr/util.js"></script>
	<script type="text/javascript" src="${ctx}/dwr/interface/comet.js"></script>
	<!-- comet end -->
	<script type="text/javascript">
	    dwr.engine.setErrorHandler(function(message, ex) {
	    	dwr.engine._debug("Error: " + ex.name + ", " + ex.message, true);
	    	if (message == null || message == "") {} //alert("A server error has occurred.");
	    	else if (message.indexOf("0x80040111") != -1) dwr.engine._debug(message);
	    	else alert(message);
	    });

	    var aboutDoActionConfigID = null;
		var myNoticeID = null;
		/**
		 * 注销
		 */
		function doLogout() {
			window.location.replace("${ctx}/logout.action");
		}

		/**
		 * 登录
		 */
		function doLogin() {
			window.location.replace("${ctx}/login.action?timeout=true");
		}
	
		function doCollapseMenu() {
			$("#layout_menu").layout("collapse", "west");
		}

		/**
		 * 获取[min,max]之间的随机数
		 */
		function getRandomNum(min,max) {
			var range = max - min;   
			var rand = Math.random();
			return(min + Math.round(rand * range)); 
		}
	
		function stateChangeIE(_frame, raId, title) {
			//state: loading, interactive, complete 
	        if(_frame.readyState == "complete") {
	            /*var loader = document.getElementById("loadDiv_" + raId);
	            //loader.innerHTML = "加载中";
	            loader.style.display = "none";
	            _frame.style.height = "100%";
	            _frame.style.visibility = "visible";*/
	            //_frame.style.display = "block";
	            //_frame.style.width = "100%";
	            //_frame.style.height = "100%";
	            //setTimeout(function() {$('#main_tabs_div').tabs('select',title);}, 100);
	            //$('#main_tabs_div').tabs('select',title);
	        	NProgress.done();
	        }
	    }
	    function stateChangeFirefox(_frame, raId) {
	    	/*var loader = document.getElementById("loadDiv_" + raId);
	        //loader.innerHTML = "加载中";
	        loader.style.display = "none";
	        //_frame.style.width = "100%";
            _frame.style.height = "100%";
	        _frame.style.visibility = "visible";
	        //_frame.style.display = "block";*/
	    	NProgress.done();
	    }
	
		function addTab(title, url) {
			if ($('#main_tabs_div').tabs('exists',title)){
				$('#main_tabs_div').tabs('select', title);
			} else {
				var raId = getRandomNum(1, 1000);
				var content = [];
				content.push("<iframe scrolling='auto' frameborder='0'  src='");
				content.push(url);
				content.push("' style='width:100%;height:100%;'");
				content.push(" onreadystatechange='stateChangeIE(this,");
				content.push(raId);
				content.push(",\"");
				content.push(title);
				content.push("\")'  onload='stateChangeFirefox(this,");
				content.push(raId);
				content.push(")'");
				content.push(" ></iframe>");
				NProgress.start();
				$("#main_tabs_div").tabs("add",{
					title: title,
					content:content.join(""),
					closable:true,
					cache:false
				});
				/*$("#main_tabs_div").tabs("add",{
					id:"tab_" + raId,
					title: title,
					href: url,
					closable:true,
					cache:true,
					loadingMessage:"正在加载页面..."
				});*/
			}	
		}

		function closeTab(title) {
			if ($('#main_tabs_div').tabs('exists',title)){
				$('#main_tabs_div').tabs('close', title);
			} 
		}
		function closeCurrentTab() {
			var tab = $("#main_tabs_div").tabs('getSelected');
			if(tab) {
				var tabIndex = $("#main_tabs_div").tabs("getTabIndex", tab);
				if(tabIndex > 0) {
					$("#main_tabs_div").tabs('close', tabIndex);
				}
			}
		}
		function getCurrentTabTitle() {
			var tab = $("#main_tabs_div").tabs('getSelected');
			var title = "";
			if(tab) {
				title = tab.panel("options").title;
			}
			return title;
		}
		function doModifyTabTitle(title) {
			var tab = $("#main_tabs_div").tabs('getSelected');
			tab.panel("options").title = title;
			$("#main_tabs_div").tabs("update", {
				tab:tab,
				type:"header"
			});
			//tab.panel("setTitle", title);
			//$("#main_tabs_div").find("li.tabs-selected span.tabs-title").text(title);
		}
		function refreshTabData(title) {
			var tb = $('#main_tabs_div').tabs('getTab',title);
			if(tb) {
				var tbby = tb.panel("body");
				if(tbby) {
					var tb_ifr = tbby.find("iframe");
					if(tb_ifr) {
						var win = tb_ifr[0].contentWindow.window;
						if(win && win.doRefreshDataGrid) {
							win.doRefreshDataGrid();
						}
					}
				}
			}
		}
		//根据标题获取tab页窗口对象
		function getTabDataWin(title) {
			var obj = null;
			var tb = $('#main_tabs_div').tabs('getTab',title);
			if(tb) {
				var tbby = tb.panel("body");
				if(tbby) {
					var tb_ifr = tbby.find("iframe");
					if(tb_ifr) {
						var win = tb_ifr[0].contentWindow.window;
						obj = win;
					}
				}
			}
			return obj;
		}
		
		var setting = {
				data: {
					key: {
						children: "childs"
					}
				},
				callback:{
					onClick:zTreeOnclick
				}
			};
		//给节点添加单击事件
		function zTreeOnclick(event,treeId,treeNode){
			if(treeNode && treeNode.resourceUrl) {
				if(treeNode.resourceUrl.indexOf("http") >= 0) {
					addTab(treeNode.name, treeNode.resourceUrl);
				} else {
					var url="${ctx}"+treeNode.resourceUrl;
					addTab(treeNode.name, url);
				}
			}
		}
		function initAccordion(menus) {
			for(var i=0;i<menus.length;i++) {
				var menu = menus[i];
				var content = [];
				if(menu.childs) {
					var ct = "<ul id=menu"+i+" class='ztree' style='width:150px;'></ul>";
					content.push(ct);
				}
				var icon = menu.iconSkin;
				if(!icon) {
					icon = "icon-folder-default";
				}
				var selStatus = false;
				if(i==0) {
					selStatus = true;
				}
				$('#dv_menu').accordion('add',{
					title:menu.name,
					content:content.join("\n"),
					iconCls:icon,
					selected:selStatus
				});
				if(menu.childs){
					var zNodes= menu.childs;
					var treeObj = $.fn.zTree.init($("#menu"+i), setting, zNodes);
					//treeObj.expandAll(true);
				}
				/* 默认全部收缩 */
				//$("#dv_menu .panel-tool a:visible:first").triggerHandler("click");
			}
// 			$("#dv_menu div.index_menu a").click(function() {
// 				addTab($(this).attr("title"), $(this).attr("url"));
// 			});
		}

		//绑定tab右键菜单事件
		function bindTabMenuEvent() {
			var tm = "#tab_mm";
			var tabDiv = "#main_tabs_div";
			//关闭当前
			$('#mm-tabclose').click(function() {
				var currtab_title = $(tm).data("cur_tab");
				if ($(tabDiv + " .tabs-inner:contains('" + currtab_title + "')").next().is('.tabs-close')) {
					$(tabDiv).tabs('close', currtab_title);
				}
			});
			//全部关闭
			$('#mm-tabcloseall').click(function() {
				$(tabDiv + ' .tabs-inner span').each(function(i, n) {
					if ($(this).parent().next().is('.tabs-close')) {
						var t = $(n).text();
						$(tabDiv).tabs('close', t);
					}
				});
			});
			//关闭除当前之外的TAB
			$('#mm-tabcloseother').click(function() {
				var currtab_title = $(tm).data("cur_tab");
				$(tabDiv + ' .tabs-inner span').each(function(i, n) {
					if ($(this).parent().next().is('.tabs-close')) {
						var t = $(n).text();
						if (t != currtab_title)
							$(tabDiv).tabs('close', t);
					}
				});
			});
			//关闭当前右侧的TAB
			$('#mm-tabcloseright').click(function() {
				var currtab_title = $(tm).data("cur_tab");
				var nextall = $(tabDiv + " .tabs-inner:contains('" + currtab_title + "')").parent().nextAll();
				if (nextall.length == 0) {
					//alert('已经是最后一个了');
					return false;
				}
				nextall.each(function(i, n) {
					if ($('a.tabs-close', $(n)).length > 0) {
						var t = $('a:eq(0) span', $(n)).text();
						$(tabDiv).tabs('close', t);
					}
				});
				return false;
			});
			//关闭当前左侧的TAB
			$('#mm-tabcloseleft').click(function() {
				var currtab_title = $(tm).data("cur_tab");
				var prevall = $(tabDiv + " .tabs-inner:contains('" + currtab_title + "')").parent().prevAll();
				if (prevall.length == 1) {
					//alert('已经是第一个了');
					return false;
				}
				prevall.each(function(i, n) {
					if ($('a.tabs-close', $(n)).length > 0) {
						var t = $('a:eq(0) span', $(n)).text();
						$(tabDiv).tabs('close', t);
					}
				});
				return false;
			});
			jQuery(document).bind('keydown', 'Ctrl+Q',function (evt){evt.preventDefault(); closeCurrentTab(); return false; });//关闭
		}
		/**
		 * 初始化菜单
		 **/
		function initMenu() {
			var options = {
					url:'${ctx}/index!view.action',
					success:function(data) {
						if(data.msg) {
							initAccordion(data.msg);
						}
					}
			};
			fnFormAjaxWithJson(options, true);			
		}		

		function bindFullScreenEvent() {
			if (window.fullScreenApi.supportsFullScreen) {
				$("#bt_fullscreen").bind('click', function() {
					window.fullScreenApi.requestFullScreen(document.documentElement);
				});
				$("#bt_cancelfullscreen").bind('click', function() {
					window.fullScreenApi.cancelFullScreen();
				});
				document.addEventListener(fullScreenApi.fullScreenEventName, function() {
					if (fullScreenApi.isFullScreen()) {
						$("#bt_fullscreen").hide();
						$("#bt_cancelfullscreen").show();
					} else {
						$("#bt_fullscreen").show();
						$("#bt_cancelfullscreen").hide();
					}
				}, true);
			} else {
				if($.browser.msie) {
						$("#bt_fullscreen").bind('click', function() {
							try {
								var WshShell = new ActiveXObject('WScript.Shell');
								WshShell.SendKeys('{F11}');
								$("#bt_fullscreen").hide();
								$("#bt_cancelfullscreen").show();
							} catch(e) {
								$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>','<s:text name="system.top.windowInfo.title"/>','info');
							}	
						});
						$("#bt_cancelfullscreen").bind('click', function() {
							try {
								var WshShell = new ActiveXObject('WScript.Shell');
								WshShell.SendKeys('{F11}');
								$("#bt_fullscreen").show();
								$("#bt_cancelfullscreen").hide();
							} catch(e) {
								$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>','<s:text name="system.top.windowInfo.title"/>','info');
							}
						});
					
				}
			}
		}

		// 刷新缓存
		function doRefreshCache() {
			var options = {
					url:'${ctx}/index!refreshCacheData.action',
					success:function(data) {
						$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '<s:text name="system.top.synchronizationData.title"/>');
					}
			};
			fnFormAjaxWithJson(options);
		}
		
		function getUserLoginNameList(ulist) {
			var uMap = {};
			$.each(ulist, function(k,uObj) {
				var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
				uMap[uObj.loginName] = uObj.realName + "[" + uObj.loginName + "]" + nickName;
			});
			return uMap;
		}
	
		var closeOrRefresh = "close";
		
		function loadmemo() { // 加载备忘录推送
			var url = "${ctx}/sys/memo/memo-comet.action";
			$("#memoComet").load(url);
		}
		
		function chengeNoticeNum(num) { // 修改通知数量
			var totalNum = 0;
			if(num) {
				totalNum = (num.approveNum || 0) + (num.noticeNum || 0);
			}
			var str = '<s:text name="system.top.message.title"/>(<span style="color:red;">' + totalNum + '</span>)';
			$("#myNotice").window("setTitle", str);
		}
		
		function initCometPush() { // 初始化服务器推送
			dwr.engine.setActiveReverseAjax(true);
			comet.setUserIdAttribute(); // 保存连接信息
			/*$(window).bind('beforeunload', function() { // 当窗口关闭之前关闭服务器连接
				comet.invalidateScriptSession();
			});*/
		}
		
		$(document).ready(function() {
			$("#_bt_query_").click(queryDoList);
			$("#_bt_reset_").click(function() {
				$("#toDoForm")[0].reset();
				$("#noReadNoticeDetail").datagrid("options")["queryParams"] = {};
				getNotices();
			}); 
			//_userList = findAllUser(); 
			//_userLoginNameList = getUserLoginNameList(_userList);
			$("#bt_logout").click(function() {
				$.messager.confirm('<s:text name="system.logout.title"/>', '<s:text name="system.logout.confirm.title"/>', function(r){
					if (r){
						//$("#logoutForm").submit();
						//window.location.replace("${ctx}/logout.action");
						doLogout();
					}
				});
			});
			<%-- 
			$("#bt_change_theme").click(function() {
				/*
				$.post("${ctx}/sys/account/user!view.action", function(data) {
					$.messager.alert('My Title', data[0]);
				}, "json");*/
				var options = {
						url:'${ctx}/gs/gs-mng!input.action',
						success:function(data) {
							$.messager.alert('重载成功', data.msg + "O(∩_∩)O哈哈~");
						}
				};
				fnFormAjaxWithJson(options);
				
			});
			--%>
			$('.styleswitch').click(function() {
				switchStylestyle(this.getAttribute("rel"));
				return false;
			});
			
			$("#bt_homepage").click(function() {
				window.location.href = "${ctx}/";
			});
			
			$("#bt_pad").click(function() {
				window.location.href = "${ctx}/index-v2.action";<%--"${ctx}/index-qq.action";--%>
			});
			
			$("#bt_favorite").click(function() {
				var ctrl = (navigator.userAgent.toLowerCase()).indexOf('mac') != -1 ? 'Command/Cmd': 'CTRL';
				var url = window.location.href;
				if (document.all) {
					window.external.addFavorite(url, '<s:text name="system.index.title"/>');
				} else if (window.sidebar) {
					window.sidebar.addPanel('<s:text name="system.index.title"/>', url, "");
				} else {
					alert('您可以尝试通过快捷键' + ctrl + ' + D 加入到收藏夹~');
				}
			});

			$("#bt_help").click(function() {
				$("#aboutWin").window("open");
			});
			
			//修改当前用户密码
			$("#bt_update").click(function(){
				$("#aboutPass").window("open");
				$("#realName").focus();
				
			});

			
			$('#main_tabs_div').tabs({
				onDblClick:function(e, title) {
					var ta = $('#main_tabs_div').tabs('getTab', title);
					if(ta.panel('options').closable) {
						$('#main_tabs_div').tabs('close', title);
					}
				},
				onContextMenu:function(e, title) {
					e.preventDefault();
					$("#tab_mm").data("cur_tab", title);
					$('#tab_mm').menu('show', {
						left: e.pageX,
						top: e.pageY
					});
				},
				onBeforeClose: function(title,index){
					var tabWin = getTabDataWin(title);
					if(tabWin && tabWin.checkIsSaveBeforeClose) {
						return tabWin.checkIsSaveBeforeClose();
					} else {
						return true;
					}
				 },
				 onSelect: function(title,index) {
					var tabWin = getTabDataWin(title);
					if(tabWin && tabWin.doResizeDataGrid) {
						tabWin.doResizeDataGrid();
					}
				 }
			});
			initMenu();
			bindTabMenuEvent();
			bindFullScreenEvent();
			$("#ifr_portal").attr("src","${portalUrl}");
			windowInit();
			
			$("#tnotice").tabs({
				onSelect:function(title,index) {
					if(title=="待办事宜") {
						$("#_batch_agree_").show();
						$("#_batch_read_").hide();
					} else {
						$("#_batch_agree_").hide();
						$("#_batch_read_").show();
					}
				}
			});
			
			var aboutDoActionConfigTime = readCookie("aboutDoActionConfigTime");
			if(aboutDoActionConfigTime == null) {
				getNotices();
				aboutDoActionConfigID = setInterval(getNotices,300000);//5分钟
				$("#aboutDoActionConfig").combobox("setText",5);
			}
			else{
				if(!isNaN(aboutDoActionConfigTime)&&parseInt(aboutDoActionConfigTime)!==0){
					aboutDoActionConfigID = setInterval(getNotices,parseInt(aboutDoActionConfigTime)*60000);
					$("#aboutDoActionConfig").combobox("setText",aboutDoActionConfigTime);
				}
				else{
					$("#aboutDoActionConfig").combobox("setText","不弹出");
				}
				$("#myNotice").window("open");
			}
			
			setCurrentTime();
			setInterval(setCurrentTime, 5000);//5秒
			
			initCometPush();
			loadmemo();//加载备忘录推送
		});
		function setCurrentTime() {
			$("#_span_time_").text(new XDate().toString("yyyy年MM月dd日  HH:mm"));
		}
		/*
        window.onbeforeunload = function() {
	        if(confirm("您要退出系统吗（如果要退出系统，请点击确定【如果不点，下次登录时会登录不了】，如果是其他操作，请点击取消继续操作）?")) {
	        	closeOrRefresh = "close";
		    } else {
		    	closeOrRefresh = "refresh";
			}
	        if(closeOrRefresh == "close") {
		    	$.ajax({
					  url: "${ctx}/j_spring_security_logout",
					  async: false
				});
			}
	    };*/
	    function doCanel(){
			$("#aboutPass").window("close");
		}
		
		function doUpdate(){
			var tab = $('#userInfo').tabs('getSelected');
			var tl = tab.panel('options').title;
			if(tl=='<s:text name="system.login.userInfo.title" />'){
				var options = {
						url:'${ctx}/sys/account/user!updateUserInfo.action',  
					    success:function(data){  
					    	doCanel();
					    	window.location.replace("${ctx}/index.action");
					    }  
					};
				fnAjaxSubmitWithJson("userInfoForm",options);						
			}else{
				if(checkpwd($("#newPass"))<2) {
					alert("密码必须设置为中级等级及以上！");
					return false;
				}
				var options = {
						url:'${ctx}/sys/account/user!updatePassWord.action',  
					    success:function(data){  
					    	$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '密码已修改成功，请重新登录', "info");
					    	setTimeout(function(){
					  			window.location.replace("${ctx}/logout.action");
					  		},1000);
					    	
					    }  
					};
				fnAjaxSubmitWithJson("passWordForm",options);							
			}				
		}
	/* 通知消息提示 */
	function showMyNotice(data){
		var jsonMore=[{"href":"${ctx}/sys/notice/notice!noticeToUserList.action","title":"<s:text name='system.sysmng.notice.query.title'/>"}];
			var jsonData=data.msg.length;
			if(jsonData>0){
				 $.messager.show({
					    width:200,  
						height:100,  
						title:'<s:text name="system.sysmng.notice.my.title"/>',
						msg: "<a href='javascript:void(0);' onclick='openImageNews(\""+jsonMore[0].href+"\",\""+jsonMore[0].title+"\")'>您有"+jsonData+"条通知请查收!</a>",
						timeout:60000,
						//showType:'slide'
						showType:'fade'
					}); 
			}
		} 
	 
		function getNotices(flag) {
			var notices = getNoticesNum();
			var process = getToDoNum();
			if (notices && !process) {
				$("#aboutDoAction").window("open");
				$("#tnotice").tabs('select',
						'<s:text name="system.sysmng.notice.list.title"/>');
			} else if (notices || process || flag) {
				$("#aboutDoAction").window("open");
			} else if(!notices && !process) {
				$("#myNotice").window("open");
			}
			$("#keyWords").val("");
			//$("#aboutDoAction").window("open");
		}

		function aboutDoActionConfig() {
			var value = $("#aboutDoActionConfig").combobox("getText");
			var zhengzhengshu = /^[1-9]\d*$/;//正整数
			if (zhengzhengshu.test(value) && parseInt(value) !== 0) {
				var time = parseInt(value) * 60000;
				//console.log(value);
				createCookie('aboutDoActionConfigTime', value, 365);
				//console.log(document.cookie);
				if (aboutDoActionConfigID != null) {
					clearInterval(aboutDoActionConfigID);
				}
				aboutDoActionConfigID = setInterval(getNotices, time);
				alert("成功设置弹出时间间隔为" + value + "分钟");
			} else if (value == "不弹出" || value == '0') {
				createCookie('aboutDoActionConfigTime', 0, 365);
				if (aboutDoActionConfigID != null) {
					clearInterval(aboutDoActionConfigID);
				}
				$("#aboutDoActionConfig").combobox("setText", "不弹出");
				alert("成功设置为不自动弹出");
			} else {
				$("#aboutDoActionConfig").combobox("setText", "");
				alert("请输入整数或从下拉列表中选值");
			}
		}
		function windowInit() {
			//打开待办事宜窗口时，初始化化消息和待办事宜列表
			$("#aboutDoActionConfig").combobox({
				/* onChange:function(newValue,oldValue){
				 //alert(newValue);
				 //egg(newValue);
				}, */
				/* onHidePanel:function() {
				 egg();
				}, */
				panelHeight : "150"
			});
			$("#aboutDoAction")
					.window(
							{
								minimizable : false,
								collapsible : false,
								onClose : function() {
									var notices = getNoticesNum();
									var process = getToDoNum();
									var no = 0;
									var po = 0;
									if (notices) {
										no = parseInt(notices);
									}
									if (process) {
										po = parseInt(process);
									}
									var total = no + po;
									var str = '<s:text name="system.top.message.title"/>(<span style="color:red;">'
											+ total + '</span>)';
									$("#myNotice").window("setTitle", str);
									$("#myNotice").window("open");
								},
								onOpen : function() {
									$("#myNotice").window("close");
									$("#toDoList").datagrid(getToDoList());
									$("#noReadNoticeDetail").datagrid(
											getCurrentUserNotices());
								}
							});
			$("#myNotice")
					.window(
							{
								title : '<s:text name="system.top.message.title"/>',
								collapsible : false,
								minimizable : false,
								maximizable : true,
								closable : false,
								closed : true,
								resizable : false,
								inline : false,
								onOpen : function() {
									var notices = getNoticesNum();
									var process = getToDoNum();
									var no = 0;
									var po = 0;
									if (notices) {
										no = parseInt(notices);
									}
									if (process) {
										po = parseInt(process);
									}
									var total = no + po;
									var str = '<s:text name="system.top.message.title"/>(<span style="color:red;">'
											+ total + '</span>)';
									$("#myNotice").window("setTitle", str);
									/*myNoticeID = setInterval(function(){
										var  notices = getNoticesNum();
										var process = getToDoNum(); 
										var no = 0;
										var po = 0;
										if(notices){
											no = parseInt(notices);
										}
										if(process){
											po = parseInt(process);
										}
										var total = no+po;
										var str = '<s:text name="system.top.message.title"/>(<span style="color:red;">'+total+'</span>)';
										$("#myNotice").window("setTitle",str);
									},5000);*/
									$(this).window("move", {
										top : 5,
										left : 300
									});
								},
								onMaximize : function() {
									$("#myNotice").window("restore");
									$("#myNotice").window("close");
									/*if(myNoticeID!=null){
									 clearInterval(myNoticeID);
									}*/
									getNotices(true);
								}
							});
		}
		function openImageNews(url, title) {
			parent.addTab(title, url);
		}
		function doSignin(id, name) {
			window.parent.addTab(name + '_' + id,
					'${ctx}/gs/process!getProcess.action?taskId=' + id);
			$("#aboutDoAction").window("close");
		}
		//查询当前用户所有的待办事宜数量		
		function getToDoNum() {
			var flag = false;
			var options = {
				async : false,
				url : '${ctx}/gs/process!toDoNumber.action',
				success : function(data) {
					if (data.msg > 0) {
						flag = data.msg;
					}
				}
			};
			fnFormAjaxWithJson(options, true);
			return flag;
		}
		//查询当前用户所有的待办事宜
		function getToDoList() {
			return {
				width : 'auto',
				height : 'auto',
				url : '${ctx}/gs/process!toDoList.action',
				fitColumns : true,
				nowrap : false,
				fit : true,
				pagination : true,
				rownumbers : true,
				remoteSort : true,
				striped : true,
				pageSize : 10,
				pageList : [ 10, 20, 30, 50, 100 ],
				sortName : 'createTime',
				sortOrder : 'desc',
				/* 	rowStyler :  function(rowIndex,rowData) {
						var date =rowData.createTime;
						var d2 =new Date();
						var d1= new Date(Date.parse(date.replace(/-/g, "/")));  
						var day = d1.dateDiff(d2, 'd');
						day= Math.abs(day);
						if(day>=3){            			
							return 'background:pink';
						}
					}, */
				frozenColumns : [ [ {
					field : 'ck',
					checkbox : true
				} ] ],
				columns : [ [
						{
							field : 'moduleName',
							title : '<s:text name="system.sysmng.process.moduleName.title"/>',
							width : 60,
							sortable : false
						},
						{
							field : 'name',
							title : '<s:text name="system.sysmng.process.taskName.title"/>',
							width : 80,
							sortable : false/*,
							formatter : function(value, rowData, rowIndex) {
								var newName = value;
								if (rowData.field) {
									newName = value + '-' + rowData.field;
								}
								return newName;
							}*/
						},
						{
							field : 'createTime',
							title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
							width : 80,
							sortable : false
						},
						{
							field : 'startUser',
							title : '<s:text name="system.sysmng.process.startUser.title"/>',
							width : 80,
							sortable : false,
							formatter : function(value, rowData, rowIndex) {
								return rowData["startUserCaption"];
								/*var val = _userLoginNameList[value];
								if(val) {
								 return val;
								} else {
								 return value;
								}*/
							}
						},
						{
							field : 'field',
							title : '<s:text name="任务描述"/>',
							width : 180,
							sortable : false
						},
						{
							field : 'oper',
							title : '<s:text name="system.button.oper.title"/>',
							width : 80,
							align : 'left',
							formatter : function(value, rowData, rowIndex) {
								var strArr = [];
								if (rowData.assignee == null) {
									strArr
											.push('<a href="javascript:void(0);" onclick="doSignin(\'');
									strArr.push(rowData.id + '\',\''
											+ rowData.name);
									strArr
											.push('\')"><s:text name="system.sysmng.process.signFor.title"/></a>');
								} else if (rowData.assignee != null) {
									strArr
											.push('<a href="javascript:void(0);" onclick="doSignin(\'');
									strArr.push(rowData.id + '\',\''
											+ rowData.name);
									strArr
											.push('\')"><s:text name="system.sysmng.process.transact.title"/></a>');
								}
								return strArr.join("");
							}
						} ] ]
			};

		}

		//查询当前用户所有通知的数量
		function getNoticesNum() {
			var flag = false;
			var options = {
				async : false,
				url : '${ctx}/sys/notice/notice!queryNoReadNoticeNum.action',
				success : function(data) {
					if (data.msg > 0) {
						flag = data.msg;
					}
				}
			};
			fnFormAjaxWithJson(options, true);
			return flag;
		}
		//查询当前用户所有通知queryNoReadNoticeNum
		/* function getCurrentUserNotices() {
		 var flag = false;
		      	var options = {
		      		async :false,
		            url : '${ctx}/sys/notice/notice!queryNoReadNotice.action',
		            success : function(data) {
		           	 transToDatagridJson(data);
		           	 var dt=data.msg;
		           	if(dt.length){
		           		for(var i=0;i<dt.length;i++){
		           			var style=dt[i][0];
		           			var count=dt[i][1];
		           			alert(style+":"+count);
		           		}
		           	} 
		           	 if(data.msg && data.msg.length>0){
		           		 flag =  true;
						}			
		           	 //showMyNotice(data);
		            }
		       };
		       fnFormAjaxWithJson(options,true);  
		       return flag;
		  } */

		//初始化当前用户未读的消息通知datagrid
		function getCurrentUserNotices() {
			return {
				width : 'auto',
				height : 'auto',
				url : '${ctx}/sys/notice/notice!queryNoReadNotice.action',
				fitColumns : true,
				nowrap : false,
				fit : true,
				pagination : true,
				rownumbers : true,
				pageSize : 10,
				pageList : [ 10, 20, 30, 50, 100 ],
				remoteSort : true,
				striped : true,
				frozenColumns : [ [ {
					field : 'ck',
					checkbox : true
				} ] ],
				columns : [ [
						{
							field : 'title',
							title : '<s:text name="system.sysmng.desktop.modelTitle.title"/>',
							width : 160,
							sortable : false
						},
						{
							field : 'style',
							title : '<s:text name="system.sysmng.notice.style.title"/>',
							width : 80,
							sortable : false,
							sortOrder : 'asc',
							formatter : function(value, rowData, rowIndex) {
								if (value == "01") {
									return "<s:text name='system.sysmng.notice.common.title'/>";
								} else if (value == "02") {
									return "<s:text name='system.sysmng.notice.warning.title'/>";
								} else if (value == "03") {
									return "<s:text name='system.sysmng.notice.todo.title'/>";
								} else if (value == "04") {
									return "<s:text name='业务确认通知'/>";
								} else if (value == "05") {
									return "<s:text name='提醒通知'/>";
								} else if (value == "06") {
									return "<s:text name='业务办理通知'/>";
								}

							},
							styler : function(value, row, index) {
								if (value == "01") {
									return 'color:blue;';
								} else if (value == "02") {
									return 'color:red;';
								} else if (value == "03") {
								}
							}
						},
						{
							field : 'createUser',
							title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
							width : 100,
							sortable : false,
							formatter : function(value, rowData, rowIndex) {
								/*var uObj=_userList[value];
								if(uObj){
									var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
									return uObj.realName + "[" + uObj.loginName + "]" + nickName;
								}else{
								    return value;
								}*/
								return rowData["createUserCaption"];
							}
						},
						{
							field : 'createTime',
							title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
							width : 120,
							sortable : false
						} ] ],
				onClickRow : function(rowIndex, rowData) {
					$("#aboutDoAction").window("close");
					if (rowData.style == "01") {
						doDblView(rowData.id, rowData.ntuId, rowData.style);
					} else if (rowData.style == "03") {//待办通知(办理任务)
						doSignin(rowData.moduleDataId, rowData.moduleKey);
						doRead(rowData.ntuId);
					} else if (rowData.style == "05") {//提醒通知(查看历史)
						parent
								.addTab(
										'<s:text name="system.sysmng.process.dealHistory.title"/>-'
												+ rowData.moduleDataId,
										'${ctx}/gs/process!getProcess.action?processInstanceId='
												+ rowData.moduleDataId
												+ '&rptData='
												+ (rowData.moduleFields ? rowData.moduleFields
														: ""));
						doRead(rowData.ntuId);
					} else if (rowData.style == "06") {
						if (rowData.moduleFields == "true") {
							parent.addTab(
									'<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'
											+ rowData.moduleDataId,
									'${ctx}/gs/gs-mng!view.action?entityName='
											+ rowData.moduleKey + '&id='
											+ rowData.moduleDataId);
						} else if (rowData.moduleFields == "false") {
							parent.addTab(
									'<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'
											+ rowData.moduleDataId,
									'${ctx}/gs/gs-mng!view.action?entityName='
											+ rowData.moduleKey + '&id='
											+ rowData.moduleDataId
											+ '&isEdit=2');
						}
						doRead(rowData.ntuId);
					} else if (rowData.style == "04") {
						var url = '${ctx}/gs/gs-mng!view.action?entityName='
								+ rowData.moduleKey + '&id='
								+ rowData.moduleDataId + '&ntuId='
								+ rowData.ntuId + "&initOperMethod=modify";
						parent.addTab(
								'<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'
										+ rowData.moduleDataId, url);
						if (rowData.moduleFields
								&& rowData.moduleFields.indexOf("autoRead") >= 0) {
							doRead(rowData.ntuId);
						}
					} else if (rowData.style == "02") {//业务预警通知
						if (rowData.moduleKey) {
							var url = '${ctx}/gs/gs-mng!view.action?entityName='
									+ rowData.moduleKey
									+ '&id='
									+ rowData.moduleDataId
									+ '&ntuId='
									+ rowData.ntuId + "&initOperMethod=modify";
							parent.addTab(
									'<s:text name="system.button.view.title"/><s:text name="业务预警通知"/>-'
											+ rowData.moduleDataId, url);
							//doRead(rowData.ntuId);
						} else {
							doDblView(rowData.id, rowData.ntuId, rowData.style);
						}
					}
					$('#queryList').datagrid('unselectRow', rowIndex);
				}
			};

		}
		var _userList = {};
		var _userLoginNameList = {};
		function doDblView(id, ntuId, style) {
			parent.addTab('<s:text name="system.sysmng.notice.view.title"/>-'
					+ id,
					'${ctx}/sys/notice/notice!viewNoticeToUser.action?id=' + id
							+ "&nTouId=" + ntuId + "&style=" + style);
		}
		function doRead(nTouId) {
			var options = {
				url : '${ctx}/sys/notice/notice!doRead.action',
				async : false,
				data : {
					"noticeToUserId" : nTouId
				},
				success : function(data) {
					if (data.msg) {

					}
				}
			};
			fnFormAjaxWithJson(options);
		}
		//快速查询消息或待办事宜
		function queryDoList() {
			var selectPanel = $("#tnotice").tabs("getSelected");
			var selectTitle = selectPanel.panel('options').title;
			var queryValue = $("#keyWords").val();
			if (selectTitle == '<s:text name="system.sysmng.notice.todoSomething.title"/>') {
				$("#toDoList").datagrid("clearSelections");
				var data = $("#toDoList").datagrid("getData");
				var rowData = data.rows;
				var len = rowData.length;
				for (var i = 0; i < len; i++) {
					var taskName = "";
					if (rowData[i].field) {
						taskName = rowData[i].name + '-' + rowData[i].field;
					} else {
						taskName = rowData[i].name;
					}
					var isSelect = taskName.indexOf(queryValue);
					if (isSelect > -1) {
						$("#toDoList").datagrid("selectRow", i);
					}
				}
			} else if (selectTitle == '<s:text name="system.sysmng.notice.list.title"/>') {
				$("#noReadNoticeDetail").datagrid("clearSelections");
				var fp = {
					"filter_LIKES_title" : queryValue
				};
				$("#noReadNoticeDetail").datagrid("load", fp);
				/*var data = $("#noReadNoticeDetail").datagrid("getData");
				var rowData = data.rows;
				 var len=rowData.length;
				 for(var i=0;i<len;i++){
					var taskName = rowData[i].title;	     			
					var isSelect =  taskName.indexOf(queryValue);
				    if(isSelect>-1){
				    		$("#noReadNoticeDetail").datagrid("selectRow",i);
				    }
				 }*/
			}
		}//批量阅读
		function doReadNotice() {
			var ntuList = $("#noReadNoticeDetail").datagrid("getSelections");
			if (ntuList && ntuList.length) {
				for ( var i in ntuList) {
					var ntuObj = ntuList[i];
					doRead(ntuObj.ntuId);
				}
				$("#noReadNoticeDetail").datagrid("load");
				$("#noReadNoticeDetail").datagrid("clearSelections");
			} else {
				alert("请选择一条通知");
				return;
			}
		}
		/**
		 * 批量查看待办事宜
		 */
		function doBatchView() {
			var ntuList = $("#toDoList").datagrid("getSelections");
			if (ntuList && ntuList.length) {
				var tids = [];
				var testObj = null;
				var moduleName = "待办事宜";
				for ( var i in ntuList) {
					var obj = ntuList[i];
					var tmpMn = obj["moduleName"];
					if (testObj === null) {
						testObj = {};
						testObj[tmpMn] = true;
					} else {
						if (testObj[tmpMn] !== true) {
							alert("只能对相同的模块名进行批量查看操作！");
							return;
						}
					}
					tids.push(obj["id"]);
					if (i == 0) {
						moduleName = tmpMn;
					}
				}
				var url = "${ctx}/sys/workflow/batch-view-process.action?taskIds="
						+ tids.join(",")
						+ "&moduleName="
						+ encodeURIComponent(moduleName);
				addTab(moduleName + "-待办事宜-批量查看", url);
				$("#aboutDoAction").window("close");
			} else {
				alert("请选择一条待办事宜");
				return;
			}
		}
		
		$("#newPass").keyup(function(){
			/*var txt=$(this).val(); //获取密码框内容
			var len=txt.length; //获取内容长度
			if(txt=='' || len<6){
				$("p.pwdInput label").show();
				$("p.pwdInput label").addClass("tips");
			}else {
				$("p.pwdInput label").hide();
			}*/
			checkpwd($(this));
		});

		//全部都是灰色的
		function primary(){
			$("p.pwdColor span").removeClass("co1,co2,co3");
		}
		
		//密码强度为弱的时候
		function weak(){
			$("span.c1").addClass("co1");
			$("span.c2").removeClass("co2");
			$("span.c3").removeClass("co3");
		}
		//密码强度为中等的时候
		function middle(){
			$("span.c1").addClass("co1");
			$("span.c2").addClass("co2");
			$("span.c3").removeClass("co3");
		}
		
		//密码强度为强的时候
		function strong(){
			$("span.c1").addClass("co1");
			$("span.c2").addClass("co2");	
			$("span.c3").addClass("co3");
		}

		/**判断密码的强弱规则
		1、如果是单一的字符（全是数字 或 字母 ）长度小于 6  弱
		2、如果是两两混合 (数字+字母（大小） 或 数字+特殊字符  或 特殊字符+字母  长度大于 8  中)
		3、如果是三者组合 (数字 +大写字母+小写字母 或 数字+字母+特殊字符 长度>8  强）)
		**/
		
		//密码强弱判断函数
		function checkpwd(obj){
			var txt = $.trim(obj.val());//输入框内容 trim处理两端空格
			var len = txt.length;
			var num = /\d/.test(txt);//匹配数字
			var small = /[a-z]/.test(txt);//匹配小写字母
			var big = /[A-Z]/.test(txt);//匹配大写字母
			var corps = /\W/.test(txt);//特殊符号
			var val = num + small+big+corps; //四个组合


			if(len<1){
				primary();
			}else if(len<6){
				weak();
			}else if(len>6 && len<=8){
				if(val==1){
					weak();
				}else if(val==2){
					middle();
				}
			}else if(len>8){
				if(val==1){
					weak();
				}else if(val==2){
					middle();
				}else if(val==3){
					strong();
				}
			}
			return val;
		}
	<%--
			function doBatchAgree() {
				var ntuList = $("#toDoList").datagrid("getSelections");
				if(ntuList && ntuList.length) {
					var tids = [];
					for(var i in ntuList) {
						var obj = ntuList[i];
						tids.push(obj["id"]);
					}
					jwpf.doBatchAgreeProcess(tids, null, function() {
						$("#toDoList").datagrid("load");
						$("#toDoList").datagrid("clearSelections");
					});
				} else {
					alert("请选择一条待办事宜");
					return;
				}
			}--%>
	</script>
</body>
</html>