<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<link href="${ctx}/js/picNews/style/css.css" rel="stylesheet" type="text/css" />
<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
	function getJsonData(){
		return jsonData;
	}

	//定义一个全局变量保存操作状态
	var status = "";
	var _userList = {};
	var _style ="${param.style}";
	var jsonData;
	var _nTouId = "${param.nTouId}";
	function doInitForm(data,callFun) {
		if (data.msg) {
			jsonData = data.msg;
			$("#showTitle").html(jsonData["title"]);
			$("#showContent").html(jsonData["content"]);
			var userName = jsonData["createUserCaption"];
			var noticeStyle = "";
			/*var uId = jsonData["createUser"];
			var uObj = _userList[uId];
			if (uObj) {
				userName = uObj.nickName || uObj.realName;
			} else {
				userName = "未知";
			}*/

			if(_style=="01"){
				noticeStyle = "<s:text name='system.sysmng.notice.common.title'/>";
			}else if(_style=="02"){
				noticeStyle =  "<s:text name='system.sysmng.notice.warning.title'/>";
			}else if(_style=="03"){
				noticeStyle =  "<s:text name='system.sysmng.notice.todo.title'/>";
			}else if(_style=="04"){
				noticeStyle =  "<s:text name='业务通知'/>";
			}

			var dtTime="";
			if(jsonData["lastUpdateTime"]){
				dtTime=jsonData["lastUpdateTime"];
			}else{
				dtTime=jsonData["createTime"];
			}
			var lut = [];
			lut.push("发布时间："+dtTime);
			lut.push("通知类型："+noticeStyle);
			lut.push("发布人："+userName);
			if(jsonData["readStatus"]=="1") { //已阅
				doDisableOper();
				lut.push("阅读时间："+jsonData["readTime"]);
			}
			$("#lastUpdateTime").html(lut.join("&nbsp;&nbsp;"));
		}
		if(callFun){
			callFun();
		}
		laodAttach(data.msg.atmId);
	}
	
	function doDisableOper() {
		$("#bt_read").linkbutton("disable");
		$("#bt_read").hide();
		$("#layout_body").layout("remove","south");
		//jwpf.disableLinkButton($("a","#showContent"));
	}


	$(function(){
		//_userList = findAllUser();
		doModify("${param.id}");
	});

	//页面数据初始化
	function doModify(id) {
		var options = {
			url : '${ctx}/sys/notice/notice!updateNoticeState.action',
			data : {
				"id" : id
			},
			success : function(data) {
				doInitForm(data);
			}
		};
		fnFormAjaxWithJson(options);
	}
	
	//加载附件列表
	function laodAttach(attachId) {
		if(attachId!=null){
			var url = "${ctx}/sys/attach/attach-notice.action?attachId="+attachId;
			$("#attachment").load(url);
		}
	}

	function doRead(nTouId){
		var options = {
				url : '${ctx}/sys/notice/notice!doRead.action',
				data : {
					"noticeToUserId" : nTouId
				},
				success : function(data) {
					if(data.msg){
						/*var name = '<s:text name="system.sysmng.notice.view.title"/>';
						var id = "${param.id}";
						window.parent.closeTab(name+'-'+id);*/
						top.closeCurrentTab();
					}
				}
		};
		fnFormAjaxWithJson(options); 
			
	}
	
	function doModifyData(){
		var p =	$("<div/>").appendTo("body");
		p.window({
			id:'dataWin',
			title:'回填数据',
			href:'${ctx}/sys/notice/notice-moduledata.action',
			width:500,
			height:400,
			closable:true,
			collapsible:true,
			maximizable:true,
			modal: true,
			shadow: false,
			cache:false,
			closed:true,
			onClose:function() {
				$("#dataWin").window("destroy");
			}
		});
		$("#dataWin").window("open");
	}
</script>
</head>
 <body id="layout_body" class="easyui-layout" fit="true">
 <div region="center" title="" border="false">
    <div id="wrapper" style="padding-top:5px;">
		<!--滚动看图-->
		<div id="picSlideWrap" class="clearfix">
		         <div><h3 class="titleh3" id="showTitle" style="font-size:20px;"></h3></div>
		         <div><h4 class="titleh4" id="lastUpdateTime"></h4></div> 
	             <div id="content"><p id="showContent"></p></div>
	             <!--附件展示-->
	             <div id="attachment" style="padding:2px;" class="attbg" ui-type="attCon"></div>
        </div>
    </div><!--end滚动看图-->
 </div>
 <div region="south" title="" border="false">
 	<c:if test="${param.style!='04'}">
	<div style="overflow:hidden; text-align: center; padding:2px;">
		<a id="bt_read" class="easyui-linkbutton" iconCls="icon-ok" 
					href="javascript:void(0)" onclick="doRead(${param.nTouId});"> <s:text name="已阅读"/></a> 
	</div>	
	</c:if>
	<c:if test="${param.style=='04'}">
  	<div style="overflow:hidden; text-align: center; padding:2px;">
		<a id="bt_read" class="easyui-linkbutton" iconCls="icon-ok" 
					href="javascript:void(0)" onclick="doModifyData();"> <s:text name="已阅读(回填数据)"/></a> 
	</div>	
	</c:if>
 </div>	
 <div style="display:none;">
    <form method="post" name="hidFrm" id="hidFrm">
		<input type="hidden" id="id" name="id" value="${param.id}"/>
	</form>
 </div>
</body>
</html>
