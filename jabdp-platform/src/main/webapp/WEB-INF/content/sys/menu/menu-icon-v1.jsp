<%@page import="java.util.Random"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/meta-min.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/bootstrap/easyui.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/icon.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/portal.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-add.css"/>
<script type="text/javascript" src="${ctx}/js/easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.3.3/locale/easyui-lang-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.3.3/jquery.portal.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/locale/data-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/common.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/json2.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/gscommon.js"></script>
<style type="text/css">
ul,li {padding:0; margin:0; list-style:none; padding-top:10px;}
ul li {padding-top:25px;}
ul li span.imgIcon{
	border : 0 ;
	height:	60px;
	width: 150px;
	text-align:center; 
	background-position: center;
}
ul li div.imgContainer{
	text-align:center; 
	margin:0 auto;
}
ul li .fontName{

}

ul li a {
	color:#000;
}

/*.panel-body {background-color: #E0ECFF;}*/

#imglist li { float:left; text-align:center; line-height:30px; margin:0 0 0 10px; width:150px; white-space:nowrap; overflow:hidden; display:inline; }
#imglist li span { display:block; text-align:center; }
</style>
<script type="text/javascript">
$(function(){
	initMenus();
});
function initMenus() {
	var options = {
			url:'${ctx}/index!view.action',
			success:function(data) {
				if(data.msg) {			
					//initTabs(data.msg);
					initPanelsMenu(data.msg);
				}
			}
	};
	fnFormAjaxWithJson(options,true);			
}	
function initModuleMenu(menuChilds, content) {
	if(menuChilds) {
		for(var j=0;j<menuChilds.length;j++) {
			var cmenu = menuChilds[j];
			var icon = cmenu.iconSkin;
			var name = cmenu.name;
			var url = cmenu.resourceUrl;
			if(!icon) {
				icon = "icon-default-big";
			}else{
				icon = icon +"-big";
			}
			if(url) {
				var ulli = "<li><a href='javascript:void(0);' url='${ctx}"+url+"' title='"+name+"'><div class='imgContainer'><span class='imgIcon "+icon+"'></span></div><span class ='fontName'>"+name+"</span></a></li>";
				content.push(ulli);
			} else {
				initModuleMenu(cmenu.childs, content);
			}
		}
	}
}
//初始化菜单图标
function initPanelsMenu(menus) {
	var content = [];
	for(var i=0;i<menus.length;i++) {
		var menu = menus[i];
		initModuleMenu(menu.childs, content);
	}
	var src = content.join("");
	var tab_ct ="<div id='imglist'><ul type='none'>"+src+"</ul></div>";
	$('#menuTabs').html(tab_ct);
	$("#imglist li a").click(function() {
		window.top.addTab($(this).attr("title"), $(this).attr("url"));
	});
}
function initTabs(menus) {
	for(var i=0;i<menus.length;i++) {
		var menu = menus[i];
		var id = menu.id;
		var titleName = menu.resourceName;
		//var tab = "<div id='m_"+id+"' class='easyui-tabs' data-options='plain:true' ></div>";
		var m_tab = $("<div />").attr('id','m_'+id).appendTo('#menuTabs');
		var tabIcon = menu.resourceIcon;
		if(!tabIcon) {
			tabIcon = "icon-folder-default";
		}
		m_tab.tabs({plain:true});
	 	
		var content = [];
		if(menu.childs) {
			for(var j=0;j<menu.childs.length;j++) {
				var cmenu = menu.childs[j];
				var icon = cmenu.resourceIcon;
				var name = cmenu.resourceName;
				var url = cmenu.resourceUrl;
				if(!icon) {
					icon = "icon-default-big";
				}else{
					icon = icon +"-big";
				}
				//var ulli = "";
				var ulli = "<li><a href='javascript:void(0);' url='${ctx}"+url+"' title='"+name+"'><div class='imgContainer'><span class='imgIcon "+icon+"'></span></div><span class ='fontName'>"+name+"</span></a></li>";
			
				content.push(ulli);
			}
		}
		var src = content.join("");
		var tab_ct ="<div id='imglist'><ul type='none'>"+src+"</ul></div>";		
		m_tab.tabs('add',{  
		    title:titleName,  
		    content:tab_ct,
		    iconCls:tabIcon
		});	
	}
	$("#imglist li a").click(function() {
		window.top.addTab($(this).attr("title"), $(this).attr("url"));
	});
}
</script>
</head>
<body class="easyui-layout" >
	<div id="menuTabs" data-options="region:'center'"  style="overflow:auto;" border="false">
	</div>
</body>
</html>