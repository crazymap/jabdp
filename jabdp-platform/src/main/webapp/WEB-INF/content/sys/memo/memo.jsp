<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
	<%-- <link href="${ctx}/js/easyui/${themeColor}/panel.css" rel="stylesheet" type="text/css"/>--%>
    <script type="text/javascript" src="${ctx}/js/easyui-1.4/easyui.xpPanel.js"></script>
</head>
<body class="easyui-layout" fit="true">
 <div region="west" title="<s:text name='system.search.title'/>"  border="false" 
  split="true" style="width:215px;padding:0px;" 
  iconCls="icon-search" tools="#pl_tt">
  <form action="" name="queryForm" id="queryForm">
   <div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
	<div class="xpstyle" title="<s:text name="主题"/>" collapsible="true" collapsed="true">
	 <input type="text" name="filter_LIKES_title" id="title" class="Itext"/>
	</div>
	<div class="xpstyle" title="<s:text name="提醒日期"/>" collapsible="true" collapsed="true">
	 <input name="filter_GED_createTime" id="remindDateStart" class="Idate Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></input>
	 <br/>--<br/>
	 <input name="filter_LED_createTime" id="remindDateEnd" class="Idate Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></input>
	</div>
	<div class="xpstyle" title="<s:text name="状态"/>" collapsible="true" collapsed="true">
	 <select class="easyui-combobox" name="filter_LIKES_status" id="status" style="width:152px;">
	  <option value>全部</option>
	  <option value="0">草稿</option>
	  <option value="1">已发布</option>
	 </select>
	</div>
	</div>
	 <div style="text-align:center;padding:8px 8px;">
	<button type="button" id="bt_query" class="button_small">
	 <s:text name="system.search.button.title"/>
	</button>
	&nbsp;&nbsp;
	<button type="button" id="bt_reset" class="button_small">
	 <s:text name="system.search.reset.title"/>
	</button>
	</div>
   </div>
  </form>
 </div>
 <div region="center" title="" border="false">
  <table id="queryList" border="false"></table>
 </div>
 <div id="mm" class="easyui-menu" style="width:120px;">
  <div onclick="doAdd()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
  <div onclick="doCmDelete()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
  <div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
  <div onclick="doCopy()" iconCls="icon-copy"><s:text name="system.button.copy.title"/></div>
 </div>
</body>
<script type="text/javascript">
var _userList = {}; //系统所有用户
function getOption() { //获取初始化列表的格式
    return {
        width : 'auto',
        height : 'auto',
        nowrap : false,
        striped : true,
        fit : true,
        url :'${ctx}/sys/memo/memo!queryList.action',
        sortName : 'createTime',
        sortOrder : 'desc',
        frozenColumns : [
            [
                {
                    field : 'ck',
                    checkbox : true
                }
            ]
        ],
        columns : [
            [
                {
                     field : 'title',
                     title : '主题',
                     width : 200,
                     sortable : true
                 } ,
                 {
                     field : 'remindType',
                     title : '定时提醒类型',
                     width : 100,
                     sortable : true
                 } ,
                 {
                     field : 'remindTime',
                     title : '提醒时间',
                     width : 100,
                     sortable : true
                 } ,
                 {
                     field : 'remindDate',
                     title : '提醒日期',
                     width : 100,
                     sortable : true,
                     formatter:function(value,rowData,rowIndex) {
                    	 if(value) {
							value = value.replace("00:00:00","");
                    	 }
                     	 return value;
                     }
                 },
                 {
                     field : 'remindWeek',
                     title : '周提醒日',
                     width : 200,
                     sortable : true,
                     formatter:function(value,rowData,rowIndex) {
                    	 if(value) {
	                    	 value = value.replace("1","周一");
	                    	 value = value.replace("2","周二");
	                    	 value = value.replace("3","周三");
	                    	 value = value.replace("4","周四");
	                    	 value = value.replace("5","周五");
	                    	 value = value.replace("6","周六");
	                    	 value = value.replace("7","周日");
                    	 }
                     	 return value;
                     }
                 } ,
                 {
                     field : 'status',
                     title : '状态',
                     width : 80,
                     sortable : true,
                     formatter:function(value, rowData, rowIndex) {
                    	 if(value == 1) {
                    		 return "已发布";
                    	 } else {
                    		 return "草稿";
                    	 }
                     }
                 } ,
				 {
					field : 'createUser',
                    title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
                    width : 100,
                    sortable : true,
                 	formatter:function(value, rowData, rowIndex){
                     	/*var uObj=_userList[value];
                         if(uObj){
                             var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
								return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                         }else{
                             return value;
                         }*/
                         return rowData["createUserCaption"];
                 	}
                 } ,
                 {
                     field : 'createTime',
                     title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
                     width : 200,
                     sortable : true
                 } 
            ]
        ],
        onDblClickRow:function(rowIndex, rowData){
        	doDblView(rowData.id);
        	$('#queryList').datagrid('unselectRow',rowIndex);
        },
		onRowContextMenu  : function(e, rowIndex, rowData){
			e.preventDefault();
			$("#mm").data("rowIndex", rowIndex);
			$('#mm').data("id", rowData.id);
			$('#mm').menu('show', {
				left: e.pageX,
				top: e.pageY
			});
		},
        pagination : true,
        rownumbers : true,
        toolbar :[
            {
                id : 'bt_add',
                text : '<s:text name="system.button.add.title"/>',
                iconCls : 'icon-add',
                handler : function() {
                    doAdd();
                }
            },
            '-',
            {
                id : 'bt_del',
                text : '<s:text name="system.button.delete.title"/>',
                iconCls : 'icon-remove',
                handler : function() {
                    doDelete();
                }                           
            } ,
            '-',
            {
                id : 'bt_view',
                text : '<s:text name="system.button.view.title"/>',
                iconCls : "icon-search",
                handler : function() {
                    doView();
                }
            },
            '-',
            {
                id : 'bt_copy',
                text : '<s:text name="system.button.copy.title"/>',
                iconCls : "icon-copy",
                handler : function() {
                	doCopyTbar();
                }
            }
        ]
    };
}
function doQuery() { //查询
    var param = $("#queryForm").serializeArrayToParam();
    $("#queryList").datagrid("load", param);
}
function doRefreshDataGrid() { //刷新列表
	$("#queryList").datagrid("load");
}

//重定义尺寸
function doResizeDataGrid() {
	$("#queryList").datagrid("resize");
	$("#queryList").datagrid("fixColumnSize","ck");
}

function doAdd() { //新增
    parent.addTab('新增备忘录', '${ctx}/sys/memo/memo-view.action?operMethod=add');
}
function doView() { //工具栏查看
	var rows = $('#queryList').datagrid('getSelections');
	if(rows.length > 0) {
		for(var i in rows) {
			var id = rows[i].id;
			doDblView(id); 
		}
	} else{
		$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
	}
}
function doCmView() { //右键查看
	var id = $("#mm").data("id");
	doDblView(id);
}
function doDblView(id) { //双击查看
	if(id) {
		parent.addTab('<s:text name="查看备忘录"/>-' + id, '${ctx}/sys/memo/memo-view.action?operMethod=view&id=' + id);
	}
}
function doDelete(id) { //工具栏删除一条记录
    var ids = [];
    if(id) {
    	ids.push(id);
    } else {
    	var rows = $('#queryList').datagrid('getSelections');
        for(var i = 0; i < rows.length; i++) {
            ids.push(rows[i].id);
        }
    }
    if(ids != null && ids.length > 0) {
        $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
            if (r) {
                var options = {
                    url : '${ctx}/sys/memo/memo!delete.action',
                    data : {
                        "memoIds" : ids
                    },
                    success : function(data) {
                        if (data.msg) {
                            $('#queryList').datagrid('clearSelections');
                            doQuery();
                        }
                    },
                    traditional:true
                };
                fnFormAjaxWithJson(options);
            }
        });
    } else {
        $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
    }
}
function doCmDelete() { //右键删除一条记录
	var id = $("#mm").data("id");
	doDelete(id);
}
function doCopyTbar() { //工具条复制一条记录
    var rows = $('#queryList').datagrid('getSelections');
    if(rows.length > 0) {
    	for(var i in rows) {
    		var id = rows[i].id;
    		if(id) {
    			parent.addTab('<s:text name="复制备忘录"/>-' + id, '${ctx}/sys/memo/memo-view.action?operMethod=edit&id=' + id);
    		}
    	}
    } else {
    	$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
	}
}
function doCopy() { //右键复制一条记录 
    var id = $("#mm").data("id");
    if(id) {
    	parent.addTab('<s:text name="复制备忘录"/>-' + id, '${ctx}/sys/memo/memo-view.action?operMethod=edit&id=' + id);
    }else{
    	$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
    }
}
$(function() {
	//_userList = findAllUser();
	$("#queryList").datagrid(getOption());
	$("#bt_query").click(doQuery);
    $("#bt_reset").click(function() {
        $("#queryForm")[0].reset();
        doQuery();
    });
    doQuseryAction("queryForm");
});
</script>
</html>
