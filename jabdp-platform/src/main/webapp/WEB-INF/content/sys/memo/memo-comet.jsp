<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<div id="memo">
 <div id="memoLayout" style="height:300px">
  <div region="south" style="height:30px;padding:2px 5px">
   <span style="color:red">*点击“关闭”按钮后在此时间后再次提醒</span>
   <select id="memoCombobox">
	<option value="5">再过5分钟</option>
	<option value="10">再过10分钟</option>
	<option value="30">再过30分钟</option>
	<option value="60">再过1小时</option>
	<option value="120">再过2小时</option>
	<option value="240">再过4小时</option>
	<option value="480">再过8小时</option>
	<option value="720">再过0.5天</option>
   </select>
  </div>
  <div region="center">
   <table id="memoDataGrid" fit="true"></table>
  </div>
 </div>
</div>
<div id="memoMenu" style="width:120px;">
 <div onclick="doCmViewMemo()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
 <div onclick="doCmDeleteMemo()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
</div>
<script type="text/javascript">
function remindMemo(memo) { // 接收推送来的备忘录
	doRemindMemo(memo);
}
function doRemindMemo(memo) {
	if(memo) {
		$('#memoDataGrid').datagrid("insertRow",{index:0,row:memo});
		$("#memo").window("open");
	}
}
function remindDeleteMemo(memoToUsers) { // 接收备忘录被删除的消息
	doRemindDeleteMemo(memoToUsers);
}
function doRemindDeleteMemo(memoToUsers) {
	if(memoToUsers) {
		for(var i in memoToUsers) {
			var index = $('#memoDataGrid').datagrid("getRowIndex", memoToUsers[i].id);
			if(index > -1) {
				$('#memoDataGrid').datagrid("deleteRow", index);
				$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '"备忘录-' + memoToUsers[i].memo.title + '"被撤销', 'info');
			}
		}
	}
}
function doCmViewMemo() { // 右键查看
	doViewMemo($("#memoMenu").data("rowData"));
}
function doBarViewMemo() { // 工具条查看
	var rowDatas = $('#memoDataGrid').datagrid("getSelections");
	if(rowDatas.length) {
		for(var i in rowDatas) {
			doViewMemo(rowDatas[i]);
		}
	} else {
		$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '请至少选中一项', 'info');
	}
}
function doViewMemo(rowData) { // 查看备忘录
	if(rowData) {
		//$("#memo").window("close");
		parent.addTab('<s:text name="查看备忘录"/>-' + rowData.id, '${ctx}/sys/memo/memo-query-view.action?operMethod=view&id=' + rowData.id
			+ '&mtuId=' + rowData.mtuId);
	}
}
function doCmDeleteMemo() { // 右键删除
	doUpDataMemo($("#memoMenu").data("rowData"));
}
function doBarDeleteMemo() { // 工具条删除
	var rowDatas = $('#memoDataGrid').datagrid("getSelections");
	if(rowDatas.length) {
		for(var i in rowDatas) {
			doUpDataMemo(rowDatas[i]);
		}
	} else {
		$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '请至少选中一项', 'info');
	}
}
function doUpDataMemo(rowData) { // 修改备忘录提醒状态
	if(rowData) {
		$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="清除后今天不会再次提醒，确定要清除吗？"/>', function(r) {
            if(r) {
                var options = {
                    url : '${ctx}/sys/memo/memo!doRead.action',
                    data : {
                        mtuId : rowData.mtuId
                    },
                    success : function(data) {
                        if(data.msg) {
                        	var list = [];
                        	list.push(data.msg);
                        	doRemindDeleteMemo(list);
                        }
                    }
                };
                fnFormAjaxWithJson(options);
            }
        });
	}
}
function openWindowMemo() {
	var rows = $('#memoDataGrid').datagrid("getRows");
	if(rows && rows.length) {
		$("#memo").window("open");
	}
}
function initMemoWindow() { // 初始化提醒窗
	$("#memo").window({ // 初始化easyui窗口
		minimizable : false,
		maximizable : false,
		modal : false,
		resizable : false,
		title : "<s:text name='备忘录提醒'/>",
		width : 450,
		closed : true,
		onOpen:function() {
			$("#memoDataGrid").datagrid("resize");
			$("#memoDataGrid").datagrid("fixColumnSize","ck");
		},
		onClose : function() {
			var time = $('#memoCombobox').combobox("getValue") * 60000;
			setTimeout("openWindowMemo();", time);
		}
	});
	$('#memoCombobox').combobox({
		value : 5,
		width : 150,
		editable : false
	});
	$('#memoLayout').layout(); // 初始化布局控件
	$('#memoDataGrid').datagrid({ // 初始化数据表格
		url : "${ctx}/sys/memo/memo!queryExpireMemo.action",
		striped : true,
		fitColumns : false,
		idField : 'mtuId',
        columns : [[
            {field:"ck", checkbox : true},
			{field:'mtuId',hidden:true},
			{field:'id',hidden:true},
	        {field:'title',title:'主题',width:200},
	        {field:'expireTime',title:'到期时间',width:200,formatter:function(value,rowData,rowIndex) {
	        		if(rowData.remindType == "指定时刻") {
	        			var date = rowData.remindDate;
	        			var remindDateStr = "";
	        			if(typeof date == "string") {
	        				remindDateStr = date.replace(" 00:00:00", "");
	        			} else {
	        				var month = "";
		    				var day = "";
		    				date.getMonth() + 1 < 10 ? month = "0" + (date.getMonth()+1) : month = date.getMonth()+1;
		    				date.getDate() < 10 ? day = "0" + date.getDate() : day = date.getDate();
		    				remindDateStr = date.getFullYear() + "-" + month + "-" + day;
	        			}
	        			return remindDateStr + " " + rowData.remindTime;
	        		} else {
	        			return rowData.remindTime;
	        		}
	        	}
	        }
        ]],
        onDblClickRow : function(rowIndex, rowData) {
        	doViewMemo(rowData);
        },
        onRowContextMenu : function(e, rowIndex, rowData) {
			e.preventDefault();
			$('#memoMenu').data("rowData", rowData);
			$('#memoMenu').menu('show', {
				left: e.pageX,
				top: e.pageY
			});
		},
		onLoadSuccess : function(data) {
			if(data && data.rows && data.rows.length) {
				$("#memo").window("open");
			}
		},
		toolbar : [
	        {
	            id : 'bt_del',
	            text : '<s:text name="清除"/>',
	            iconCls : 'icon-remove',
	            handler : doBarDeleteMemo
	        } ,
	        '-',
	        {
	            id : 'bt_view',
	            text : '<s:text name="system.button.view.title"/>',
	            iconCls : "icon-search",
	            handler : doBarViewMemo
	        }
	    ]
    });
	$('#memoMenu').menu(); // 初始化菜单
}
$(function() {
	initMemoWindow();
});
</script>
