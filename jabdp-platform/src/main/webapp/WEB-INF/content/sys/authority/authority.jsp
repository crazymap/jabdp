<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.sysmng.authority.manager.title"/></title>
<%@ include file="/common/meta-gs.jsp"%>
<script>
	var _isSupportBigData_ = false;//支持大数据
	var setting = {
		data : {
			simpleData : {
				enable : true
			}
		},
		view : {
			dblClickExpand : false
		},
		callback : {
			beforeClick : beforeClick,
			onRightClick : OnRightClick,
			onClick : modifyRole
		}
	};

	var settingZtree = {
		check : {
			enable : true
		},
		data : {
			simpleData : {
				enable : true
			}
		}
	};
	
	var settingAuthority = {
			check : {
				enable : true,
				chkboxType: {"Y": "ps", "N": ""}
			},
			data : {
				simpleData : {
					enable : true
				}
			}
	};
	
	var settingType = {
			data : {
				simpleData : {
					enable : true,
					pIdKey: "pid"
				}
			},
			check: {
				enable: true
				
			}
		};
	function beforeClick(treeId, treeNode) {
		zTree.selectNode(treeNode);
		return treeNode.id.indexOf("org_") == -1;
	}
	//初始化当前用户组织下的角色树
	function initMenu() {
		var options = {
			url : '${ctx}/sys/account/role!roleList.action',
			success : function(data) {
				if (data.msg) {
					$.fn.zTree.init($("#roleList"), setting, data.msg);
					zTree = $.fn.zTree.getZTreeObj("roleList");
					zTree.expandAll(false);
					var rootNodes = zTree.getNodes();
					zTree.expandNode(rootNodes[0], true, false, true);
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	//初始化权限树
	function initAuthorityZtree() {
		var options = {
			//url : '${ctx}/sys/account/authority!list.action',
			url : '${ctx}/sys/account/authority!queryList.action',
			async : false,
			success : function(data) {
				if (data.msg) {
					$.fn.zTree
							.init($("#authorityTree"), settingAuthority, data.msg);
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	//初始化桌面权限树
	function initDesktopMenu() {
		var options = {
				url:'${ctx}/sys/desktop/desktop!desktopList.action',
				success:function(data) {
					if(data.msg) {
					$.fn.zTree.init($("#desktopTree"),settingZtree,data.msg);
					}
				}
		};
		fnFormAjaxWithJson(options);		
	}
	//初始化当前组织下的用户树
	function initUserZtree(org_id) {
		var options = {
			url : '${ctx}/sys/account/authority!userList.action',
			data : {
				"orgId" : 1 //org_id
			},
			success : function(data) {
				if (data.msg) {
					$.fn.zTree.init($("#userTree"), settingZtree, data.msg);
				}
			}
		};

		fnFormAjaxWithJson(options);
	}
	//根据角色ID，初始化相应的权限树
	function getAuthorityZtree(id) {
		var treeObj = $.fn.zTree.getZTreeObj("authorityTree");
		treeObj.checkAllNodes(false);
		if(id) {
				var options = {
					//url : '${ctx}/sys/account/authority!list.action',
					url : "${ctx}/sys/account/authority!getRoleAuthIds.action",
					data : {
						//"id" : id
						"rid":id
					},
					async : false,
					success : function(data) {
						var dataArr = data.msg;
						if (dataArr) {
							for(var i in dataArr) {
								var node = treeObj.getNodeByParam("id", dataArr[i], null);
								if(node) {
									treeObj.checkNode(node, true, false);
								}
							}
						}
					}
				};
				fnFormAjaxWithJson(options);
		}
	}
	//根据角色ID和组织ID，初始化相应的用户树
	function getUserZtree(id, org_id) {
		var options = {
			url : '${ctx}/sys/account/authority!userList.action',
			data : {
				"rid" : id,
				"orgId" : 1 //org_id
			},
			success : function(data) {
				if (data.msg) {
					$.fn.zTree.init($("#userTree"), settingZtree, data.msg);
				}
			}
		};
		fnFormAjaxWithJson(options, true);
	}
	//根据角色ID，初始化相应的桌面树
	  function getDesktopTree(roleId) {			
		  var options = {
					url:'${ctx}/sys/desktop/desktop!desktopList.action',
					data : {
						"roleId" : roleId
					},
					success:function(data) {
						if(data.msg) {
							$.fn.zTree.init($("#desktopTree"),settingZtree,data.msg);
							}
					}
			};
		  fnFormAjaxWithJson(options, true);
		}  
	
		//设置已选中的系统类型
	  function getSystemType(roleId,status) {			
		  var options = {
					url:'${ctx}/sys/common/common-type!getTypeList.action',
					data : {
						"id" : roleId
					},
					success:function(data) {
						if(data.msg) {
							$.fn.zTree.init($("#systemTypeTree"),settingType,data.msg);
						}
					}
			};
		  fnFormAjaxWithJson(options, true);
		} 
		
		//设置已选中的可见用户
		function getSeeUserTree(roleId,status,org_id){
			var options = {
					url:'${ctx}/sys/account/authority!seeUserList.action',
					data : {
						"rid" : roleId,
						"orgId" : 1 //org_id
					},
					success:function(data) {
						
					if (data.msg) {
						$.fn.zTree.init($("#seeUserTree"), settingZtree, data.msg);
					}
				}
			};
			fnFormAjaxWithJson(options, true);
		}
	
	function OnRightClick(event, treeId, treeNode) {
		event.preventDefault();
		zTree.selectNode(treeNode);
		if (treeNode.id.indexOf("org_") != -1) {
			$('#mm').menu('show', {
				left : event.pageX,
				top : event.pageY
			});
		} else {
			$('#mm2').menu('show', {
				left : event.pageX,
				top : event.pageY
			});
		}
	}
//添加角色
	function addRole() {
		var nodes = zTree.getSelectedNodes();
		var org_id = nodes[0].idNum;

		var options = {
			url : '${ctx}/sys/account/role!addInfo.action',
			data : {
				"orgId" : org_id
			},
			success : function(data) {
				if (data.msg) {
					//$("#organizationId").selectTree("enable");
					//initAuthorityZtree();
					getAuthorityZtree();
					initDesktopMenu();
					//initUserZtree(org_id);
					getSystemType("", "modify");
					//getSeeUserTree("", "modify", org_id);
					$('#saveForm')[0].reset();
					$("#id").val("");
					$("#organizationId").selectTree('setValue', org_id);
					$("#win_role_message").window("open");
					$("#b1").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
					$("#b1").linkbutton({iconCls : 'icon-no'});
					$('#status').text('<s:text name="system.button.enabledStatus.title"/>');
					$('#roleStatus').val("1");
				}
			},
			traditional : true
		};
		fnFormAjaxWithJson(options);
	}
	//修改角色
	function modifyRole() {
		var nodes = zTree.getSelectedNodes();
		//var org_id = nodes[0].orgId;
		var r_id = nodes[0].idNum;

		var options = {
			url : '${ctx}/sys/account/role!modifyInfo.action',
			data : {
				"id" : r_id
			},
			success : function(data) {
				if (data.msg) {
					var json2fromdata = data.msg;
					//$("#organizationId").selectTree("enable");
					$("#organizationId").selectTree('setValue',
							data.msg.organizationId);
					$("#saveForm").form('load',json2fromdata);
					 if (json2fromdata.roleStatus == "1") {
							$("#b1").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
							$("#b1").linkbutton({iconCls : 'icon-no'});
							$('#status').text('<s:text name="system.button.enabledStatus.title"/>');
						} else {
							$("#b1").linkbutton({text : '<s:text name="system.button.enabled.title"/>'});
							$("#b1").linkbutton({iconCls : 'icon-ok'});
							$('#status').text('<s:text name="system.button.disableStatus.title"/>');
						}
					getDesktopTree(r_id);
					//getUserZtree(r_id, org_id);
					getSystemType(r_id, "modify");
					if(_isSupportBigData_) {
						$("#userId").dialoguewindow("setValue", json2fromdata.userIds);
						$("#seeUserIds").dialoguewindow("setValue", json2fromdata.seeUserIds);
					} else {
						$("#userId").selectTree("setValue", json2fromdata.userIds);
						$("#seeUserIds").selectTree("setValue", json2fromdata.seeUserIds);
					}
					//getSeeUserTree(r_id, "modify", org_id);
					$("#win_role_message").window("open");
					getAuthorityZtree(r_id);
				}
			}
		};
		fnFormAjaxWithJson(options);

	}
	//删除角色
	function delRole() {
		var nodes = zTree.getSelectedNodes();
		var r_id = nodes[0].idNum;
		$.messager
				.confirm(
						'<s:text name="system.javascript.alertinfo.title"/>',
						'<s:text name="system.javascript.alertinfo.info"/>',
						function(r) {
							if (r) {
								var options = {
									url : '${ctx}/sys/account/role!delete.action',
									data : {
										"id" : r_id
									},
									success : function(data) {
										if (data.msg) {
										/* 	$.messager
													.alert(
															'<s:text name="system.javascript.alertinfo.title"/>',
															'<s:text name="system.javascript.alertinfo.succees"/>',
															'info'); */
											initMenu();
										}
									}
								};
								fnFormAjaxWithJson(options);
							}
						});
	}
	//保存角色
	function saveRole() {
		var flag = getIds();
		if (flag) {
			var options = {
				url : '${ctx}/sys/account/role!save.action?method=saveUserRole',
				success : function(data) {
					if (data.msg) {
						/* $.messager
								.alert(
										'<s:text name="system.javascript.alertinfo.title"/>',
										'<s:text name="system.javascript.alertinfo.succees"/>',
										'info'); */
						$('#win_role_message').window('close');
						$('#saveForm')[0].reset();
						var id = $("#id").val();
						if(!id) initMenu();//新增时需要刷新，修改时不需要
					}
				}
			};
			
			fnAjaxSubmitWithJson('saveForm', options);
		}
	}
	//获取所有树上的选取的节点ID
	function getIds() {
		/*var a_zTree = $.fn.zTree.getZTreeObj("authorityTree");
		var a_nodes = a_zTree.getCheckedNodes(true);		
		var a_nodeIds = [];
		$.each(a_nodes, function(k, v) {
			a_nodeIds.push(v.id);
		});
		$("#authorityId").val(a_nodeIds.join(","));
	
		var u_zTree = $.fn.zTree.getZTreeObj("userTree");
		var u_nodes = u_zTree.getCheckedNodes(true);
		var u_nodeIds = [];
		$.each(u_nodes, function(k, v) {
			u_nodeIds.push(v.userId);
		});
		$("#userId").val(u_nodeIds.join(","));
		
		var d_zTree = $.fn.zTree.getZTreeObj("desktopTree");
		var d_nodes = d_zTree.getCheckedNodes(true);
		var d_nodeIds = [];
		$.each(d_nodes, function(k, v) {
			d_nodeIds.push(v.id);
		});
		$("#desktopId").val(d_nodeIds.join(","));*/
		getIdsByKey('authority');
		//getIdsByKey('user', "userId");
		getIdsByKey('desktop');
		getIdsByKey('systemType');
		//getIdsByKey("seeUser","userId");
		return true;
	}
	
	function getIdsByKey(method, key) {
		var vKey = key || "id"; 
		var zTree = $.fn.zTree.getZTreeObj(method + "Tree");
		var nodes = zTree.getCheckedNodes(true);
		var nodeIds = [];
		$.each(nodes, function(k, v) {
			if(v[vKey]) {
				nodeIds.push(v[vKey]);	
			}
		});
		$("#" + method + "Id").val(nodeIds.join(","));
	}

	function cancelAdd() {
		$('#win_role_message').window('close');
		$('#saveForm')[0].reset();
	}
	//选中或取消选中
	function doCheckNodesOnTree(treeId, param, checkFlag) {
		if(!param) {
			alert("请输入匹配关键字");
			return;
		}
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		var nodes = treeObj.getNodesByParamFuzzy("name", param, null);
		for (var i=0, l=nodes.length; i < l; i++) {
			treeObj.checkNode(nodes[i], checkFlag, true);
		}
	}
	
	function initOrgTree() {
		$("#organizationId").selectTree({
			url:"${ctx}/sys/account/organization!ztreeList.action",
			queryParams:{},
			filterUrl:"${ctx}/sys/account/organization!findOrgList.action",
			filterParam:"filter_INL_id",
			pIdKey:"pId"
		});
	}
	
	function initUserTree() {
		if(_isSupportBigData_) {
			var userOpt = {
		 			title:"用户",
		 			url:"${ctx}/sys/dataFilter/user-select.action",
		 			filterUrl:"${ctx}/sys/account/user!queryUser.action",
		 			filterParam:"filter_INL_id",
		 			keyMap:{"idKey":"id","textKey":"nickName"}
		 	};
			$("#userId").dialoguewindow(userOpt);
			$("#seeUserIds").dialoguewindow($.extend(true, {}, userOpt, {title:"可见用户"}));
		} else {
			var userOpt = {
		 			title:"用户",
		 			pIdKey:"pId",
		 			multiple:true,
		 			url:"${ctx}/sys/account/authority!userTree.action",
		 			queryParams:{
		 				"orgId" : 1
		 			},
		 			filterUrl:"${ctx}/sys/account/user!queryUser.action",
		 			filterParam:"filter_INL_id",
		 			keyMap:{"idKey":"id","textKey":"nickName"},
		 			valMap:{"idKey":"id","textKey":"text"},
		 			idValPrefix:"user_"
		 	};
			$("#userId").selectTree(userOpt);
			$("#seeUserIds").selectTree($.extend(true, {}, userOpt, {title:"可见用户"}));
		}
	}
	
	//选中或取消选中
	function doSelectNodesOnTree(treeId, param) {
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		treeObj.cancelSelectedNode();
		if(!param) {
			return;
		}
		var nodes = treeObj.getNodesByParamFuzzy("name", param, null);
		//for (var i=0, l=nodes.length; i < l; i++) {
		for (var i=nodes.length-1; i >= 0; i--) {	
			treeObj.selectNode(nodes[i],true);
		}
	}
	
	function _doDictTreeQuery_() {
		var val = $("#_dictTree_qd_").val();
		doSelectNodesOnTree("roleList", val);
	}
	
	$(document).ready(function() {
		initMenu();
		initOrgTree();
		initUserTree();
		//初始化角色状态
		$("#b1").toggle(function() {
			$("#b1").linkbutton({text : '<s:text name="system.button.enabled.title"/>'});
			$("#b1").linkbutton({iconCls : 'icon-ok'});
			$('#status').text('<s:text name="system.button.disableStatus.title"/>');
			$('#roleStatus').val("0");
		}, function() {
			$("#b1").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
			$("#b1").linkbutton({iconCls : 'icon-no'});
			$('#status').text('<s:text name="system.button.enabledStatus.title"/>');
			$('#roleStatus').val("1");
		});
		
		$("#_dictTree_qd_").bind('keydown', 'return',function (evt){evt.preventDefault(); _doDictTreeQuery_(); return false; });
		
		initAuthorityZtree();
	});
</script>
</head>
<body class="easyui-layout">

	<div region="west" split="true" title="<s:text name="system.sysmng.role.list.title"/>"
		style="width: 300px; padding: 10px;" border="false">
		<div><input type="text" id="_dictTree_qd_" style="width:130px" class="Itext"/>&nbsp;&nbsp;<button type="button" class="button_small" onclick="_doDictTreeQuery_()">树查询</button></div>
		<ul id="roleList" class="ztree"></ul>

		<div id="mm" class="easyui-menu" style="width: 120px;">
			<div onclick="addRole()" iconCls="icon-add">
				<s:text name="system.sysmng.user.add.title" />
			</div>
		</div>

		<div id="mm2" class="easyui-menu" style="width: 120px;">
			<div onclick="modifyRole()" iconCls="icon-edit">
				<s:text name="system.sysmng.user.modify.title" />
			</div>
			<div onclick="delRole()" iconCls="icon-remove">
				<s:text name="system.sysmng.user.delete.title" />
			</div>
		</div>
	</div>



	<div region="center" title="<s:text name="system.sysmng.authority.manager.title"/>" border="false">
		<!-- 公用一个FORM -->
		<div id="win_role_message" class="easyui-panel" closed="true"
			fit="true" closable="true" modal="true" title="" border="false"
			style="padding: 0px; background: #fafafa;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false"
					style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
					<form action="" name="saveForm" id="saveForm" method="post">
						<table align="center" border="0" cellpadding="0" cellspacing="1"
							class="table_form">
							<tbody>
								<tr>
									<th><label for="roleName"><s:text
												name="system.sysmng.role.name.title" />:</label></th>
									<td><input type="hidden" name="id" id="id" /> <input
										type="hidden" name="version" id="version" /> <input
										class="easyui-validatebox Itext" required="true" style="width:350px"
										validType="length[1,100]" type="text" name="roleName"
										id="roleName"></input></td>
									<th><label for="organizationName"><s:text
												name="system.sysmng.role.organization.title" />:</label></th>
									<td><input id="organizationId"
										<%-- url="${ctx}/sys/account/organization!queryList.action"--%>
										name="organizationId" style="width: 350px"></input></td>
								</tr>
								<tr>
									<th><label for="roleStatus"><s:text
												name="system.sysmng.role.status.title" />:</label></th>
									<td><input type="hidden" name="roleStatus" id="roleStatus" /> <span
											id="status" style="color: red"></span> <a href="#"
											class="easyui-linkbutton" id="b1" ></a>
									</td>
									
									<th><label for="roleDesc"><s:text
												name="system.sysmng.role.desc.title" />:</label></th>
									<td >
									<input
										class="easyui-validatebox Itext"
										validType="length[1,500]" type="text" name="roleDesc"
										id="roleDesc" style="width: 350px; "></input>
										
									</td>
								</tr>
								<tr>
									<th valign="top"><label><s:text name="system.sysmng.role.setAuthority.title"/>:</label></th>
									<td colspan="1" valign="top"><input type="hidden" name="authorityIds"
										id="authorityId" />
										匹配关键字：<input type="text" id="_query_name_" class="Itext"></input>
										<a href="#"
											class="easyui-linkbutton" onclick="doCheckNodesOnTree('authorityTree',$('#_query_name_').val(),true)">选中</a>
										<a href="#"
											class="easyui-linkbutton" onclick="doCheckNodesOnTree('authorityTree',$('#_query_name_').val(),false)">取消选中</a>
										<ul id="authorityTree" class="ztree"></ul>
									</td>
									<th valign="top"><label><s:text name="system.sysmng.authority.setUser.title"/>:</label></th>
									<td colspan="1" valign="top"><input type="text" name="userId"
										id="userId" style="width:350px;"/>
										<!-- <ul id="userTree" class="ztree"></ul> --></td>
								</tr>
								<tr>
					<th valign="top"><label for="desktopName"><s:text name="system.sysmng.role.setDesktop.title"/>:</label>
					</th>
					<td valign="top">
					<input type="hidden" name="desktopIds" id="desktopId" />
						<ul id="desktopTree" class="ztree"></ul></td>				
					<th valign="top"><label ><s:text name="系统分类"/>:</label></th>
								<td valign="top">
									<input type="hidden" name="systemTypeIds" id="systemTypeId"/>
									<ul id="systemTypeTree" class="ztree"></ul>	
								</td>					
						</tr>
					<tr>
						<th valign="top"><label ><s:text name="可见用户"/>:</label></th>
						<td valign="top">
							<input type="text" name="seeUserIds" id="seeUserIds" style="width:350px;"/>
							<!-- <ul id="seeUserTree" class="ztree"></ul>-->
						</td>
						<th valign="top"><label ><s:text name="可见用户属性"/>:</label></th>
						<td valign="top">
							<input
										class="easyui-validatebox Itext"
										validType="length[1,500]" type="text" name="visibleAttr"
										id="visibleAttr" style="width: 350px;"></input>
							<br/><span>如：选择可见用户权限时：INL_kefu_OR_yewuyuan（INL_用户属性名1_OR_用户属性名2）；选择满足属性条件可见权限时：{"INL_kefu":"$curUserId$","INL_userId":"$$sql.key.sqlKey"}</span>
						</td>		
					</tr>	
							</tbody>
						</table>

					</form>
				</div>
				<div region="south" border="false"
					style="text-align: right;">
					<a class="easyui-linkbutton" iconCls="icon-ok"
						href="javascript:void(0)" onclick="saveRole()"><s:text
							name="system.button.save.title" /> </a> <a class="easyui-linkbutton"
						iconCls="icon-cancel" href="javascript:void(0)"
						onclick="cancelAdd();"><s:text
							name="system.button.cancel.title" /> </a>
				</div>
			</div>
		</div>


	</div>
</body>
</html>