<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.sysmng.user.manager.title" /></title>
<%@ include file="/common/meta-gs.jsp"%>
 
<style type="text/css">
<%--
div,a,ul,li,tr,th,td {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	font-weight: inherit;
	font-style: inherit;
	font-size: 100%;
	font-family: inherit;
	vertical-align: baseline;
}

li.title {
	list-style: none;
}

ul.list {
	margin-left: 17px;
}

li {
	list-style: circle;
	font-size: 12px;
}
--%>
ul.ztree {
	margin-top: 10px;
	border: 1px solid #617775;
	background: #f0f6e4;
	width: 220px;
	height: 300px;
	overflow-y: scroll;
	overflow-x: auto;
}
</style>
<script type="text/javascript">
	var _isSupportBigData_ = false;//支持大数据
	function operFormatter(value, rowData, rowIndex){
		var strArr = [];
		strArr.push('<a href="javascript:void(0);" onclick="doView(');
		strArr.push(rowData.id);
		strArr.push(')"><s:text name="system.button.view.title"/></a>&nbsp;<a href="javascript:void(0);" onclick="doModify(');
		strArr.push(rowData.id);
		strArr.push(')"><s:text name="system.button.modify.title"/></a>&nbsp;<a href="javascript:void(0);" onclick="doModify(');
		strArr.push(rowData.id);
		strArr.push(', true)"><s:text name="system.button.copy.title"/></a>&nbsp;<a href="javascript:void(0);" onclick="doPassWord(');
		strArr.push(rowData.id);
		strArr.push(')"><s:text name="system.button.retPassword.title"/></a>');
		strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doChangeStatus(');
		strArr.push(rowData.id);
		strArr.push(',\'');
		strArr.push((rowData["status"]=="0")?"1":"0");
		strArr.push('\')">' + ((rowData["status"]=="0")?'<s:text name="system.button.enabled.title"/>':'<s:text name="system.button.disable.title"/>') + '</a>');
		strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doChangeEmployeeName(');
		strArr.push(rowData.id);
		strArr.push(',\'');
		strArr.push(rowData["nickname"]);
		strArr.push('\')"><s:text name="变更员工姓名"/></a>');
		if(rowData.isSuperAdmin!="1"){
			strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doDelete(');
			strArr.push(rowData.id);
			strArr.push(')"><s:text name="system.button.delete.title"/></a>');
		}
		return strArr.join("");
	}

	var orgId = "${orgId}";
	function getOption() {
		return {
			title : '<s:text name="system.sysmng.user.list.title"/>',
			iconCls : "icon-search",
			width : 700,
			height : 450,
			nowrap : false,
			striped : true,
			fit : true,
			url : '${ctx}/sys/account/user!queryList.action?orgId=' + orgId,
			remoteSort : false,
			sortName : 'id',
			sortOrder : 'desc',
			idField : 'id',
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			}, {
				title : '<s:text name="system.search.id.title"/>',
				field : 'id',
				width : 50,
				sortable : true,
				align : 'right'
			} ] ],
			columns : [ [
					{
						field : 'loginName',
						title : '<s:text name="system.sysmng.user.loginName.title"/>',
						width : 80,
						sortable : true
					},
					{
						field : 'realName',
						title : '<s:text name="system.sysmng.user.name.title"/>',
						width : 80,
						sortable : true
					},
					/*{
						field : 'managerName',
						title : '<s:text name="system.sysmng.user.upmanager.title"/>',
						width : 100,
						sortable : false
					},*/
					{
						field : 'organizationName',
						title : '<s:text name="system.sysmng.organization.name.title"/>',
						width : 100,
						sortable : true
					},
					//{field:'email',title:'<s:text name="system.sysmng.user.email.title"/>',width:140, sortable:true, align:'center'},
					//{field:'sex',title:'<s:text name="system.sysmng.user.sex.title"/>',width:120,align:'center'},
					//{field:'fax',title:'<s:text name="system.sysmng.user.fax.title"/>',width:80, sortable:true},
					{
						field : 'employeeId',
						title : '<s:text name="system.sysmng.user.employee.title"/>',
						width : 80,
						sortable : true
					},
					{
						field : 'nickname',
						title : '<s:text name="system.sysmng.user.nickname.title"/>',
						width : 80,
						sortable : true
					},
					{
						field : 'roleNames',
						title : '<s:text name="拥有角色"/>',
						width : 100,
						sortable : true
					},
					{
						field : 'mobilePhone',
						title : '<s:text name="手机号"/>',
						width : 85,
						sortable : true
					},
					{
						field : 'bindIp',
						title : '<s:text name="绑定IP地址"/>',
						width : 100
					},
					{
						field : 'status',
						title : '<s:text name="system.sysmng.user.status.title"/>',
						width : 80,
						align : 'center',
						sortable : true,
						formatter:function(value,rowData,rowIndex){
							if(value==1){
								return "<s:text name="system.button.enabled.title"/>";
							}else if(value==0){
								return "<s:text name="system.button.disable.title"/>";
							}
							else{
								return "";
							}
						}
					}, {
						field : 'isSuperAdmin',
						title : '<s:text name="system.sysmng.user.superuser.title"/>',
						width : 120,
						align : 'center',
						sortable : true,
						formatter:function(value,rowData,rowIndex){
							if(value==1){
								return "是";
							}else{
								return "否";
							}
						}
					}, {
						field : 'oper',
						title : '<s:text name="system.button.oper.title"/>',
						width : 250,
						align : 'left',
						formatter : operFormatter
					}

			] ],
			pagination : true,
			rownumbers : true,
			toolbar : [ {
				id : 'bt_add',
				text : '<s:text name="system.button.add.title"/>',
				iconCls : 'icon-add',
				handler : function() {
					doAdd(orgId);
				}
			}, '-', {
				id : 'bt_del',
				text : '<s:text name="system.button.delete.title"/>',
				iconCls : 'icon-remove',
				handler : function() {
					doDeleteIds();
				}
			} , '-', {
				id : 'bt_reset',
				text : '<s:text name="重置密码"/>',
				iconCls : 'icon-undo',
				handler : function() {
					doBatchResetPassword();
				}
			}],
			onDblClickRow :function(rowIndex, rowData){
				doView(rowData.id);
			}
		};
	}
	function doQuery() {
		var param = $("#queryForm").serializeArrayToParam();
		$("#queryList").datagrid("load", param);
	}
	
	//焦点聚在第一行-
	 function focusEditor(id) {
		setTimeout(function() {$("#" + id).focus();}, 0);
	} 
	
	function doAdd(orgId) {
	  // $("#saveForm input[name='loginName']").focus();
		//focusEditor("loginName");
		var options = {
			url : '${ctx}/sys/account/user!addInfo.action',
			data : {
				"orgId" : orgId
			},
			success : function(data) {
				if (data.msg) {					
					//addRoleList();
					$("#mm").show();

					doEnabled();
					$("#organizationIds").selectTree('setValue',
							data.msg.parentId);
					//$("#buttons").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
					//$("#buttons").linkbutton({iconCls : 'icon-no'});
					$('#userstatus').text('启用状态');
					//$('#buttons').linkbutton('enable');
					$('#status').val("1");
					$('#sex').combobox('setValue','1');
					$('#remoteLogin').combobox('setValue','0');
					
					$("#win_add_message").window("open");
					focusEditor("loginName");

				}
			},
			traditional : true
		};
		fnFormAjaxWithJson(options);
	}
	function doView(id) {
		//$("#saveForm input[name='loginName']").focus();
		var options = {
			url : '${ctx}/sys/account/user!view.action',
			data : {
				"id" : id
			},
			success : function(data) {
				if (data.msg) {
					var json2fromdata = data.msg;
					 $('#saveForm').form('load',json2fromdata);
					 if (json2fromdata.status=="1") {
							//$("#buttons").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
							//$("#buttons").linkbutton({iconCls : 'icon-no'});
							$('#userstatus').text('启用状态');
							//$('#buttons').linkbutton('disable');
						} else {
							//$("#buttons").linkbutton({text : '<s:text name="system.button.enabled.title"/>'});
							//$("#buttons").linkbutton({iconCls : 'icon-ok'});
							$('#userstatus').text('禁用状态');
							//$('#buttons').linkbutton('disable');
						} if(json2fromdata.isSuperAdmin=="1"){
						 $("#SuperAdmin").attr("checked",true);
						}else{
						$("#SuperAdmin").attr("checked",false);
						}
					 //$('#citySel').val(json2fromdata["roleName"]);
					 $("#organizationIds").selectTree('setValue',
								json2fromdata.organizationId);
					 /*var roleIdsArr = json2fromdata.roleIds;
					 if(roleIdsArr && roleIdsArr.length) {
						$("#roleIds").selectTree('setValue',
									{"id":"r_" + roleIdsArr.join(",r_"), "text":json2fromdata.roleNames});
					 }*/
					 $("#roleIds").selectTree('setValue', json2fromdata.roleIds.join(","));
					 $("#win_add_message").window("setTitle","<s:text name="system.sysmng.user.view.title"/>");
					 $("#mm").hide();
					 $("#win_add_message").window("open");
					 //$("#roleIds").selectTree("options")["queryParams"]["userId"] = id;
					 doDisabled();
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	
	function doDisabled(){
		$("input,select").attr("disabled", "disabled");
		//$("#organizationIds").selectTree("disable");
		//$("#roleIds").selectTree("disable");
	}
	
	function doEnabled(){
		$("input,select").removeAttr("disabled");
		//$("#organizationIds").selectTree("enable");
		//$("#roleIds").selectTree("enable");
	}
	
	 //刷新列表，保持当前页
    function doRefreshDataGrid() {
		$("#queryList").datagrid("reload");
	}
	
	//编辑或复制
	function doModify(id, isCopy) {
		var options = {
			url : '${ctx}/sys/account/user!modifyInfo.action',
			data : {
				"id" : id
			},
			success : function(data) {
				if (data.msg) {
					var json2fromdata = data.msg;
					if(isCopy) {delete json2fromdata["id"];json2fromdata["status"]="1";}
					$('#saveForm').form('load',json2fromdata);
					 if (json2fromdata.status=="1") {
							//$("#buttons").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
							//$("#buttons").linkbutton({iconCls : 'icon-no'});
							$('#userstatus').text('启用状态');
							//$('#buttons').linkbutton('enable');
						} else {
							//$("#buttons").linkbutton({text : '<s:text name="system.button.enabled.title"/>'});
							//$("#buttons").linkbutton({iconCls : 'icon-ok'});
							$('#userstatus').text('禁用状态');
							//$('#buttons').linkbutton('enable');
						}				
					 if(json2fromdata.isSuperAdmin=="1"){
						 $("#SuperAdmin").attr("checked",true);
						}else{
						$("#SuperAdmin").attr("checked",false);
						}
					 
					$("#organizationIds").selectTree('setValue',
							json2fromdata.organizationId);
					/*var roleIdsArr = json2fromdata.roleIds;
					if(roleIdsArr && roleIdsArr.length) {
						$("#roleIds").selectTree('setValue',
								{"id":"r_" + roleIdsArr.join(",r_"), "text":json2fromdata.roleNames});
					}*/
					$("#roleIds").selectTree('setValue', json2fromdata.roleIds.join(","));
					
					$("#win_add_message").window("setTitle","<s:text name="system.sysmng.user.modify.title"/>");
					$("#mm").show();
					$("#win_add_message").window("open");
					
					//$("#roleIds").selectTree("options")["queryParams"]["userId"] = id;
					doEnabled();
					//modifyRoleList(id);
					//不禁用登录名修改
					/* $("#saveForm input[name='loginName']").attr("disabled", "disabled"); */
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	function doDelete(id) {
		$.messager
				.confirm(
						'<s:text name="system.javascript.alertinfo.title"/>',
						'<s:text name="system.javascript.alertinfo.info"/>',
						function(r) {
							if (r) {
								var options = {
									url : '${ctx}/sys/account/user!delete.action',
									data : {
										"id" : id
									},
									success : function(data) {
										if (data.msg) {
											/* $.messager
													.alert(
															'<s:text name="system.javascript.alertinfo.title"/>',
															'<s:text name="system.javascript.alertinfo.succees"/>',
															'info'); */
											$("#queryList").datagrid(
													getOption());
										}
									}
								};
								fnFormAjaxWithJson(options);
							}
						});
	}
	function doDeleteIds() {
		var ids = [];
		var rows = $('#queryList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			if(rows[i].isSuperAdmin=="1"){
			return	$.messager.alert('<s:text name="system.javascript.alertinfo.errorInfo"/>','不能选择超级管理者进行删除','info');
			}else{
				ids.push(rows[i].id);
			}
		}

		if (ids != null && ids.length > 0) {
			$.messager
					.confirm(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.info"/>',
							function(r) {
								if (r) {
									var options = {
										url : '${ctx}/sys/account/user!deleteUsers.action',
										data : {
											"ids" : ids

										},
										success : function(data) {
											if (data.msg) {
												$('#queryList').datagrid(
														'clearSelections');
												doQuery();
												/* $.messager
														.alert(
																'<s:text name="system.javascript.alertinfo.title"/>',
																'<s:text name="system.javascript.alertinfo.succees"/>',
																'info'); */
											}
										},
										traditional : true
									};
									fnFormAjaxWithJson(options,true);
								}
							})

		} else {
			$.messager.alert(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.question"/>',
					'info');
		}
	}
	function saveUser(method) {
		var options = {
			url : '${ctx}/sys/account/user!save.action',
			//resetForm : true,
			success : function(data) {
				if (data.msg) {
					/* $.messager
							.alert(
									'<s:text name="system.javascript.alertinfo.title"/>',
									'<s:text name="system.javascript.alertinfo.succees"/>',
									'info'); */
					$("#queryList").datagrid(getOption());
					if (method == 'add') {
						$('#win_add_message').window('close');
					}
				}
			}
		};

		if (method == 'add') {
			//getIds(method);
			/*var roleIds = $("#roleIds").val();
			if(roleIds) {
				$("#roleIds").val(roleIds.replace(/r_/ig,""));
			}*/
			if($("#SuperAdmin").attr("checked")){
				$("#isSuperAdmin").val("1");
			}else{
				$('#isSuperAdmin').val("0");
			}
			fnAjaxSubmitWithJson('saveForm', options);
			//fnEasyUIFormWithJson("saveForm",options);
		} 

	}
	
	function initOrgTree() {
		$("#organizationIds").selectTree({
			url:"${ctx}/sys/account/organization!ztreeList.action",
			queryParams:{},
			filterUrl:"${ctx}/sys/account/organization!findOrgList.action",
			filterParam:"filter_INL_id",
			pIdKey:"pId"
		});
	}
	
	function initRoleTree() {
		if(_isSupportBigData_) {
			$("#roleIds").selectTree({
				title:"选择角色",
				url:"${ctx}/sys/account/user!selectedList.action",
				queryParams:{},
				filterUrl:"${ctx}/sys/account/user!findRoleList.action",
				filterParam:"filter_INL_id",
				pIdKey:"pId",
				multiple:true,
				idValPrefix:"r_"
			});
		} else {
			$("#roleIds").selectTree({
	 			title:"选择角色",
	 			pIdKey:"pId",
	 			multiple:true,
	 			url:"${ctx}/sys/account/role!roleList.action",
	 			queryParams:{},
	 			filterUrl:"${ctx}/sys/account/user!findRoleList.action",
	 			filterParam:"filter_INL_id",
	 			idValPrefix:"r_"
	 		});
		}
	}
	
	$(document).ready(function() {	
		initOrgTree();
		initRoleTree();
		/*$("#buttons").toggle(function() {
			if(!$('#buttons').linkbutton('options').disabled){
			$("#buttons").linkbutton({text : '<s:text name="system.button.enabled.title"/>'});
			$("#buttons").linkbutton({iconCls : 'icon-ok'});
			$('#userstatus').text('禁用状态');
			$('#status').val("0");
			}
		}, function() {
			if(!$('#buttons').linkbutton('options').disabled){
			$("#buttons").linkbutton({text : '<s:text name="system.button.disable.title"/>'});
			$("#buttons").linkbutton({iconCls : 'icon-no'});
			$('#userstatus').text('启用状态');
			$('#status').val("1");
			}
		});*/

	     $('#queryForm input').bind('keydown', 'return',function (evt){doQuery(); return false; });
		$("#queryList").datagrid(getOption());
		/*$("#logTimeStart").focus(function() {
			WdatePicker({
				maxDate : '#F{$dp.$D(\'logTimeEnd\')}'
			});
		});
		$("#logTimeEnd").focus(function() {
			WdatePicker({
				minDate : '#F{$dp.$D(\'logTimeStart\')}'
			})
		}); */
		$("#bt_query").click(doQuery);
		$("#bt_reset").click(function() {
			//$("#queryForm").form("clear");
			$("#queryForm")[0].reset();
			doQuery();
		});

		//$("#citySel, #menuBtn").click(showMenu);

		$("#win_add_message").window({
			width:650,
			height:420,
			onOpen: function() {
				$(this).window("move", {
					top:($(window).height()-420)*0.5,
					left:($(window).width()-650)*0.5
				});
			},
			onClose:function() {
				//$("#saveForm").form("clear");
				$("#saveForm")[0].reset();
				$("#id").val("");				
			}
		});
		
		$("#changeEmployeeName_message").window({
			width:400,
			height:250,
			onOpen: function() {
				$(this).window("move", {
					top:($(window).height()-250)*0.5,
					left:($(window).width()-400)*0.5
				});
			},
			onClose:function() {
				$("#_user_id").val("");
				$("#oldEmployeeName").val("");	
				$("#newEmployeeName").val("");	
			}
		});
		
		//默认员工号与登录名一致
		/*$("#loginName").blur(function() {
			$("#employeeId").val($(this).val());
		});*/
		
	});
	function cancelAdd() {
		$('#win_add_message').window('close');
		doEnabled();
	}

	<%--var setting = {
		check : {
			enable : true,
			chkboxType : {
				"Y" : "",
				"N" : ""
			}
		},
		view : {
			dblClickExpand : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		callback : {
			beforeClick : beforeClick,
			onCheck : onCheck
		}
	};

	function beforeClick(treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("addroleIds");
		zTree.checkNode(treeNode, !treeNode.checked, null, true);
		return false;
	}

	function onCheck(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("addroleIds"), nodes = zTree
				.getCheckedNodes(true), v = "";
		for ( var i = 0, l = nodes.length; i < l; i++) {
			v += nodes[i].name + ",";
		}
		if (v.length > 0)
			v = v.substring(0, v.length - 1);
		var cityObj = $("#citySel");
		cityObj.attr("value", v);
	}
//显示用户选择角色时的下拉框树
	function showMenu(e) {
		e.preventDefault();
		var cityObj = $("#citySel");
		var cityOffset = cityObj.offset();
		var left = cityOffset.left;
		var top = cityOffset.top;
		//var left = e.pageX;
		//var top = e.pageY;
		
		if(left+$("#menuContent").outerWidth()>$(window).width()+$(document).scrollLeft()){
			left=$(window).width()+$(document).scrollLeft()-$("#menuContent").outerWidth()-5;
		}
		if(top+$("#menuContent").outerHeight()>$(window).height()+$(document).scrollTop()){
			top-=$("#menuContent").outerHeight();
		} else {
			top = top + 20;
		}
		$("#menuContent").css({
			left : left,
			top : top
		}).slideDown("fast");
		$("body").bind("mousedown", onBodyDown);
	}
	function hideMenu() {
		$("#menuContent").fadeOut("fast");
		$("body").unbind("mousedown", onBodyDown);
	}
	function onBodyDown(event) {
		if (!(event.target.id == "menuBtn" || event.target.id == "citySel"
				|| event.target.id == "menuContent" || $(event.target).parents(
				"#menuContent").length > 0)) {
			hideMenu();
		}
	}

	function addRoleList() {
		/*$('#organizationIds').selectTree({
			onChange : function(newValue, oldValue) {
				if(newValue!=oldValue){
				var options = {
						async:false,
						url : '${ctx}/sys/account/user!treeList.action',
						data : {
							"newValue" : newValue
						},
						success : function(data) {
							if (data.msg) {
								$.fn.zTree.init($("#addroleIds"), setting, data.msg);
								$("#citySel").val("");
							}
						}
					};
					fnFormAjaxWithJson(options,true);
				}
			}
		});*/
		var options = {
				async:false,
				url : '${ctx}/sys/account/user!selectedList.action',
				data : {
					"userId" : ""
				},
				success : function(data) {
					if (data.msg) {
						$.fn.zTree.init($("#addroleIds"), setting, data.msg);
						$("#citySel").val("");
					}
				}
			};
			fnFormAjaxWithJson(options,true);
	}

	 function modifyRoleList(id) {
		var options = {
				url : '${ctx}/sys/account/user!selectedList.action',
				data : {
					"userId" : id
				},
				async:false,
				success : function(data) {
					if (data.msg) {
						$.fn.zTree.init($("#addroleIds"), setting, data.msg);
						onCheck();
					}
				}
			};
			fnFormAjaxWithJson(options,true);
	}--%> 
	 
	function getIds(method){
		var zTree = $.fn.zTree.getZTreeObj(""+method+"roleIds");
		var nodes = zTree.getCheckedNodes(true);
		var nodeIds = [];
		$.each(nodes, function(k,v) {
			nodeIds.push(v.idNum);
		});
		if(method == 'add'){
		$("#citySel").val(nodeIds.join(","));
		}
	}
	function doPassWord(id) {
		$.messager.confirm('<s:text name="system.button.retPassword.title"/>', '<s:text name="system.javascript.alertinfo.passwordInfo"/>', function(r) {
			if (r) {
				doResetPassword([id], null);
			}
		});
	}
	//批量重置密码
	function doBatchResetPassword() {
		var rows = $('#queryList').datagrid('getSelections');
		if(rows.length) {
			$.messager.confirm('<s:text name="system.button.retPassword.title"/>', '<s:text name="system.javascript.alertinfo.passwordInfo"/>', function(r) {
			if(r) {
				var ids = [];
				for(var i=0,len=rows.length;i<len;i++) {
					ids.push(rows[i]["id"]);
				}
				doResetPassword(ids,null);
				//$.messager.alert('<s:text name="system.javascript.alertinfo.succees"/>', '密码重置成功','info');
			}});
		} else {
			$.messager.confirm('<s:text name="system.button.retPassword.title"/>', '确定要重置该组织下所有用户的密码吗？', function(r) {
				if(r) {
					doResetPassword([], orgId);
					//$.messager.alert('<s:text name="system.javascript.alertinfo.succees"/>', '密码重置成功','info');
				}
			});
		}
	}
	
	//重置密码
	function doResetPassword(id, orgId) {
		var options = {
				url : '${ctx}/sys/account/user!resetPassword.action',
				data : {
					"ids" : id,
					"orgId" : orgId || ""
				},
				traditional:true,
				success : function(data) {
					if (data.msg) {
						$.messager.alert('<s:text name="system.javascript.alertinfo.succees"/>', data.msg, 'info');
					}
				}
			};
			fnFormAjaxWithJson(options,true);
	}
	
	//变更员工姓名
	function doChangeEmployeeName(id, oldEmployeeName) {
		$("#_user_id").val(id);
		$("#oldEmployeeName").val(oldEmployeeName);
		$("#changeEmployeeName_message").window("open");
	}
	
	function doChangeStatus(userId, status) {
		var options = {
				url : '${ctx}/sys/account/user!changeStatus.action',
				data : {
					"id" : userId,
					"type":status
				},
				success : function(data) {
					if(data.msg) {
						doRefreshDataGrid();
					}
				}
		};
		fnFormAjaxWithJson(options,true);
	}
	
	//保存变更员工姓名
	function doSaveChangeEmployeeName() {
		var empName = $("#newEmployeeName").val();
		if(!empName) {
			alert("请输入变更后员工姓名!");
			return;
		}
		var userId = $("#_user_id").val();
		var options = {
				url : '${ctx}/sys/account/user!changeEmployeeName.action',
				data : {
					"id" : userId,
					"employeeName":empName
				},
				success : function(data) {
					if(data.msg) {
						var opt = {
								"moduleNameCn" : "用户管理",
								"entityName" : "User",
								"entityId" : userId,
								"fieldNameCn" : "<s:text name="system.sysmng.user.nickname.title" />",
								"beforeVal" : $("#oldEmployeeName").val(),
								"afterVal" : empName,
								"changeDesc" : "用户信息员工姓名变更"
						};
						jwpf.doSaveChangeLog(opt);
						$("#changeEmployeeName_message").window("close");
						doRefreshDataGrid();
					}
				}
		};
		fnFormAjaxWithJson(options,true);
	}
</script>
</head>
<body class="easyui-layout">
	<div region="north" title="<s:text name="system.search.title"/>" border="false"
		split="true" style="height: 100px; padding: 0px; overflow: hidden;"
		iconCls="icon-search">
		<form action="" name="queryForm" id="queryForm">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
					<tr>
						<th><label for="loginName1"><s:text
									name="system.sysmng.user.loginName.title" />:</label></th>
						<td><input type="text" name="filter_LIKES_loginName"
							id="loginName1" class="Itext"></input></td>
						<th><label for="realName1"><s:text
									name="system.sysmng.user.name.title" />:</label></th>
						<td><input type="text" name="filter_LIKES_realName" class="Itext"
							id="realName1"></input></td>
						<td></td>
					</tr>
					<tr>
						<th><label for="employeeId1"><s:text
									name="system.sysmng.user.employee.title" />:</label></th>
						<td><input type="text" name="filter_LIKES_employeeId" class="Itext"
							id="employeeId1"></input></td>
						<th><label for="nickname1"><s:text
									name="system.sysmng.user.nickname.title" />:</label></th>
						<td><input type="text" name="filter_LIKES_nickname" class="Itext"
							id="nickname1"></input></td> 
						<td><button type="button" id="bt_query">
								<s:text name="system.search.button.title" />
							</button>&nbsp;&nbsp;
							<button type="button" id="bt_reset">
								<s:text name="system.search.reset.title" />
							</button></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
	<div region="center" title="" border="false"
		style="overflow: text; width: 580px; padding: 1px;">
		<table id="queryList"></table>
	</div>
	

	<!--win弹出框  -->
	<div id="win_add_message" class="easyui-window" closed="true"
		modal="true" title="<s:text name="system.sysmng.user.add.title"/>" 
		iconCls="icon-save"
		style="width: 700px; height: 450px; padding: 0px; background: #fafafa;">
		<div class="easyui-layout" fit="true">
			<div region="center" border="false"
				style="padding: 2px; background: #fff; overflow: hidden; border: 1px solid #ccc;">
				<form action="${ctx}/sys/account/organization!save.action"
					name="saveForm" id="saveForm" method="post">
					<!-- <input type="hidden"  name="id" /> -->
					<table align="center" border="0" cellpadding="0" cellspacing="1"
						class="table_form">
						<tbody>
							<tr>
								<th><label for="loginName"><s:text
											name="system.sysmng.user.loginName.title" />:</label>
								</th>
								<td><input type="hidden" name="id" id="id" /> <input
									type="text" name="loginName" id="loginName" class="easyui-validatebox Itext" required="true"></input>
								</td>
								<th><label for="realName"><s:text
											name="system.sysmng.user.name.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext"  required="true" type="text" name="realName" id="realName"></input>
								</td>
							</tr>
							<tr>
								<th><label for="employeeId"><s:text
											name="system.sysmng.user.employee.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext" type="text" name="employeeId" id="employeeId" placeholder="手动填写或自动生成"></input>
								</td>
								<th><label for="nickname"><s:text
											name="system.sysmng.user.nickname.title" />:</label>
								</th>
								<td><input  type="text" name="nickname" id="nickname" class="Itext"></input>
								</td>
							</tr>
							<tr>
								<th><label for="organizationIds"><s:text
											name="system.sysmng.role.organization.title" />:</label>
								</th>
								<td><input id="organizationIds"
									<%-- url="${ctx}/sys/account/organization!queryList.action"--%>
									name="organizationId" style="width: 152px"></input></td>
								<th><label for="managerName"><s:text
											name="system.sysmng.user.upmanager.title" />:</label>
								</th>
								<td><input type="text" name="managerName" id="managerName" class="Itext"
									readonly="readonly"></input> <input type="hidden"
									name="managerId" id="managerId"></input></td>
							</tr>
							<tr>
								<th><label for="sex"><s:text
											name="system.sysmng.user.sex.title" />:</label>
								</th>
								<td><select class="easyui-combobox" name="sex" id="sex" style="width: 152px;">
										<!-- <option value="">--请选择--</option> -->
										<option value="1">男</option>
										<option value="0">女</option>
								</select></td>
								<th><label for="mobilePhone"><s:text
											name="system.sysmng.user.mobilephone.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext" type="text" name="mobilePhone" id="mobilePhone" validType="mobile" invalidMessage="请输入正确的11位手机号码.格式:13120002221"></input>
								</td>
							</tr>
							<tr>
								<th><label for="fax"><s:text
											name="system.sysmng.user.fax.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext" type="text" name="fax" id="fax"  validType="faxno"></input>
								</td>
								<th><label for="email"><s:text
											name="system.sysmng.user.email.title" />:</label>
								</th>
								<td><input class="easyui-validatebox Itext" type="text" name="email" id="email"  validType="email"></input>
								</td>
							</tr>
							<tr>
								<th><label for="status"><s:text
											name="system.sysmng.user.status.title" />:</label>
								</th>
								<td><input type="hidden" name="status" id="status" value="1" /> <span
											id="userstatus" style="color: red"></span><!-- <a href="#"
											class="easyui-linkbutton" id="buttons" ></a> --></td>
								<th><label for="remoteLogin"><s:text
											name="system.sysmng.user.remotelogin.title" />:</label>
								</th>
								<td><select class="easyui-combobox" name="remoteLogin" id="remoteLogin" style="width: 152px;">
										<!-- <option value="">--请选择--</option> -->
										<option value="0">不允许</option>
										<option value="1">允许</option>
								</select></td>
							</tr>
							<tr>
								<th><label for="isSuperAdmin"><s:text
											name="system.sysmng.user.superuser.title" />:</label>
								</th>
								<td> <input type="hidden" name="isSuperAdmin" id="isSuperAdmin" />
								<input id="SuperAdmin" type="checkbox" /> </td>
								<th><label for="bindIp"><s:text
											name="绑定IP地址" />:</label>
								</th>
								<td><input type="text" id="bindIp" name="bindIp" class="Itext"/></td>
							</tr>
							<tr>
								<th><label for="isSuperAdmin"><s:text
											name="同时在线登录数" />:</label>
								</th>
								<td><input class="easyui-numberbox" type="text" id="alledyerror" name="alledyerror" style="text-align:right;"/></td>
								<th><label for="avatar"><s:text
											name="用户头像" />:</label>
								</th>
								<td><input type="text" id="avatar" name="avatar" class="Itext"/></td>
							</tr>
							<tr>
								<th><label for="signature"><s:text
											name="用户签名" />:</label>
								</th>
								<td colspan="3"><textarea name="signature" class="Itext"
									id="signature" style="width: 400px;"></textarea></td>
							</tr>
							<tr>
								<th><label for="address"><s:text
											name="system.sysmng.user.address.title" />:</label>
								</th>
								<td colspan="3"><input type="text" name="address" class="Itext"
									id="address" style="width: 400px;"></input></td>
							</tr>
							<tr>
								<th><label for="roleName"><s:text name="system.sysmng.user.setRole.title" />:</label>
								</th>
								<td colspan="3">
										<input id="roleIds" name="roleIds" 
										type="text" style="width: 400px;" class="Itext"/>
												<%--&nbsp;<a id="menuBtn" href="#"
												><s:text name="system.button.select.title"/></a> --%>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
			<div region="south" border="false"
				style="text-align: right;">
				 <a id="mm" class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="saveUser('add')"><s:text
						name="system.button.save.title" /> </a> <a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0)"
					onclick="cancelAdd();"><s:text
						name="system.button.cancel.title" /> </a>
              
			</div>
		</div>
	</div>
	<!--win弹出框  -->
	<div id="menuContent" class="menuContent" style="overflow:auto;display: none; position: absolute;z-index:10000;">
		<ul id="addroleIds" class="ztree" style="margin-top: 0; width: 180px; "></ul>
	</div>
	<div id="changeEmployeeName_message" class="easyui-window" closed="true"
		modal="true" title="变更<s:text name="system.sysmng.user.nickname.title" />" 
		iconCls="icon-save"
		style="padding: 0px; background: #fafafa;">
		<div class="easyui-layout" fit="true">
			<div region="center" border="false"
				style="padding: 2px; background: #fff; overflow: hidden; border: 1px solid #ccc;">
				<table align="center" border="0" cellpadding="0" cellspacing="1"
						class="table_form">
						<tbody>
							<tr>
								<th><label for="oldEmployeeName">变更前<s:text
											name="system.sysmng.user.nickname.title" />:</label>
								</th>
								<td><input  type="text" name="oldEmployeeName" id="oldEmployeeName" readonly="readonly" class="Itext"></input>
									<input type="hidden" name="_user_id" id="_user_id"/>
								</td>
							</tr>
							<tr>
								<th><label for="newEmployeeName">变更后<s:text
											name="system.sysmng.user.nickname.title" />:</label>
								</th>
								<td><input  type="text" name="newEmployeeName" id="newEmployeeName" class="Itext"></input>
								</td>
							</tr>
						</tbody>
				</table>
			</div>
			<div region="south" border="false"
				style="text-align: right;">
				 <a class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="doSaveChangeEmployeeName();"><s:text
						name="变更" /> </a><a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0)"
					onclick="$('#changeEmployeeName_message').window('close');"><s:text
						name="system.button.cancel.title" /> </a>
              
			</div>
		</div>
	</div>
</body>
</html>

