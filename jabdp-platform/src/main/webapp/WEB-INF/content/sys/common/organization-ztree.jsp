<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<script type="text/javascript">

	$(document).ready(function() {
		_initOrgZtree_();
	});


	
 function _initOrgZtree_(){
		var options = {
				url : '${ctx}/sys/account/organization!ztreeList.action',
				success : function(data) {
				if (data.msg) {
					var setting = {
							check: {
								enable: true
							},
							data: {
								simpleData: {
									enable: true
								}
							}
						};
					$.fn.zTree.init($("#m_org_"), setting, data.msg);
					zTree = $.fn.zTree.getZTreeObj("m_org_");
					zTree.expandAll(true);
					}
				}
			};
			fnFormAjaxWithJson(options,true);
	} 
	
</script>
<div class="easyui-layout" fit="true">
	
			<form action="" name="sel_orgsForm" id="sel_orgsForm" method="post">
					<table align="center">
						<tbody >
							<tr >
							<th><label ><s:text name="选择组织"/>:</label>
								</th>
								<td >
							<ul  style="text-align: center; width: 230px;height:200px;" id="m_org_" class="ztree"></ul>	
							</td>			
							</tr>
						</tbody>	
					</table>
			</form>	

</div>




