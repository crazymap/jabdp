<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
	<%-- <link href="${ctx}/js/easyui/${themeColor}/panel.css" rel="stylesheet" type="text/css"/>--%>
    <script type="text/javascript" src="${ctx}/js/easyui-1.4/easyui.xpPanel.js"></script>
            <script type="text/javascript">
            var _userList={};
			var _status = {"0":"草稿","1":"待发","2":"已发"};
			var _source = {"0":"手工创建","1":"系统自动"};
            
            function getOption() {
                return {
                    width : 'auto',
                    height : 'auto',
                    nowrap : false,
                    striped : true,
                    fit : true,
                    url :'${ctx}/sys/msg/sms!queryList.action',
                    sortName : 'createTime',
                    sortOrder : 'desc',
                    frozenColumns : [
                        [
                            {
                                field : 'ck',
                                checkbox : true
                            }
                        ]
                    ],
                    columns : [
                        [
											{
											    field : 'targetPhone',
											    title : '<s:text name="收短信人"/>',
											    width : 100,
											    sortable : true
											} ,
	                                        {
	                                            field : 'smsBody',
	                                            title : '<s:text name="短信内容"/>',
	                                            width : 200,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'sourcePhone',
	                                            title : '<s:text name="发短信人"/>',
	                                            width : 120,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'sendTime',
	                                            title : '<s:text name="发送时间"/>',
	                                            width : 130,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'regSendTime',
	                                            title : '<s:text name="定时发送时间"/>',
	                                            width : 130,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'status',
	                                            title : '<s:text name="system.sysmng.role.status.title"/>',
	                                            width : 50,
	                                            sortable : true,
	                                            formatter:function(value,rowData,rowIndex){
	                                            	return _status[value] || value;
	                                            }
	                                        } ,
	                                        {
	                                            field : 'serviceSms',
	                                            title : '<s:text name="服务号"/>',
	                                            width : 100,
	                                            sortable : true
	                                        },
	                                        {
	                                            field : 'source',
	                                            title : '<s:text name="来源"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value,rowData,rowIndex){
	                                            	return _source[value] || "无";
	                                            }
	                                        },
	                                        {
	                                            field : 'createUser',
	                                            title : '<s:text name="system.sysmng.desktop.createUser.title"/> ',
	                                            width : 120,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
	                                            	/*var uObj=_userList[value];
                                                    if(uObj){
                                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
														return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                                                    }else{
                                                        return value;
                                                    }*/
	                                            	return rowData["createUserCaption"];
                                                 }
	                                        } ,
	                                        {
	                                            field : 'createTime',
	                                            title : '<s:text name="system.sysmng.desktop.createTime.title"/> ',
	                                            width : 130,
	                                            sortable : true
	                                        } 
                        ]
                    ],
                    onDblClickRow:function(rowIndex, rowData){
                    	doDblView(rowData.id);
                    	$('#queryList').datagrid('unselectRow',rowIndex);
                    },
                    
    				onRowContextMenu  : function(e, rowIndex, rowData){
    					e.preventDefault();
    					$("#mm").data("rowIndex", rowIndex);
    					$('#mm').data("id", rowData.id);
    					$('#mm').menu('show', {
    						left: e.pageX,
    						top: e.pageY
    					});
    				},
                    pagination : true,
                    rownumbers : true,
                    toolbar :[
                        {
                            id : 'bt_add',
                            text : '<s:text name="system.button.add.title"/>',
                            iconCls : 'icon-add',
                            handler : function() {
                                doAdd();
                            }
                        },
                        '-',
                        {
                            id : 'bt_del',
                            text : '<s:text name="system.button.delete.title"/>',
                            iconCls : 'icon-remove',
                            handler : function() {
                                doDelete(null);
                            }                           
                        } ,
                        '-',
                        {
                            id : 'bt_view',
                            text : '<s:text name="system.button.view.title"/>',
                            iconCls : "icon-search",
                            handler : function() {
                                doView();
                            }
                        },
                        '-',
                        {
                            id : 'bt_copy',
                            text : '<s:text name="system.button.copy.title"/>',
                            iconCls : "icon-copy",
                            handler : function() {
                            	doCopyTbar();
                            }
                        },
                        {
                            id : 'bt_accessory',
                            text : '<s:text name="system.button.accessory.title"/>',
                            iconCls : "icon-attach",
                            handler : function() {
                                doAccessory();
                            }
                        },
                        {
                            id : 'bt_resetSms',
                            text : '<s:text name="重置短信设置"/>',
                            iconCls : "icon-run",
                            handler : function() {
                            	doResetSms();
                            }
                        }   
                    ]
                };
            }
            
            function doResetSms() {
            	var options = {
            			url : '${ctx}/sys/msg/sms!resetSms.action',
                        data : {
                        },
    					async: false,
    					success : function(data) {
    						 if(data.msg) {
        						alert("重置短信设置成功！");
    						} 
    					}
    			};
    			fnFormAjaxWithJson(options);
            }
               
            //右键复制一条记录 
            function doCopy(){
             var id = $("#mm").data("id");
             if(id==null){
            	 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
                  return ;
             }else{
            	  parent.addTab('<s:text name="复制短信"/>-'+id, '${ctx}/sys/msg/sms!view.action?operMethod=edit&id=' + id);
             }
             
          }
           //工具条复制一条记录
           function doCopyTbar(){
        	   var id=0;
             	 var rows = $('#queryList').datagrid('getSelections');
             	 if(rows.length!=1){
             		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
             	     return ;
             	 } else{
             		 id=rows[0].id;
             		 parent.addTab('<s:text name="复制短信"/>-'+id, '${ctx}/sys/msg/sms!view.action?operMethod=edit&id=' + id);
             	 }
           }
           //删除一条记录
            function doDelete() {
                var ids = [];
                    var rows = $('#queryList').datagrid('getSelections');
                    for (var i = 0; i < rows.length; i++) {
                        ids.push(rows[i].id);
                    }

                if (ids != null && ids.length > 0) {
                    $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
                        if (r) {
                            var options = {
                                url : '${ctx}/sys/msg/sms!delete.action',
                                data : {
                                    "ids" : ids
                                },
                                success : function(data) {
                                    if (data.msg) {
                                        $('#queryList').datagrid('clearSelections');
                                        doQuery();
                                    }
                                },
                                traditional:true
                            };
                            fnFormAjaxWithJson(options);
                        }
                    })

                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
                }
            }
            //查询
            function doQuery() {
                var param = $("#queryForm").serializeArrayToParam();
                $("#queryList").datagrid("load", param);
            }

          //重定义尺寸
            function doResizeDataGrid() {
            	$("#queryList").datagrid("resize");
            	$("#queryList").datagrid("fixColumnSize","ck");
            }
          
           //新增
            function doAdd() {
                parent.addTab('<s:text name="新增短信"/>', '${ctx}/sys/msg/sms!view.action?operMethod=add');
            }
            
            function doView() {
            	 var id=0;
            	 var rows = $('#queryList').datagrid('getSelections');
            	  if(rows.length!=1){
            		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
            	     return ;
            	 } else{
            		 id=rows[0].id;
            	 }
            	 doDblView(id); 
            }
           //查看tab
            function doDblView(id) {
            	parent.addTab('<s:text name="查看短信"/>-'+id, '${ctx}/sys/msg/sms!view.action?operMethod=view&id=' + id);
           }
            
            //右键查看
            function doCmView() {
            	var id = $("#mm").data("id");
            	doDblView(id);
            }
            
           //刷新列表
            function doRefreshDataGrid() {
    			$("#queryList").datagrid("load");
    		}
           
            function doViewResult() {
            	var index = $("#mm").data("rowIndex");
            	var rows = $("#queryList").datagrid("getRows");
            	var data = rows[index];
            	if(data) {
            		$("#win_message").window("open");
            		$("#ta_message").text(data.result||"无");
            	}
            }
    		
            $(document).ready(function() {
            	 //_userList=findAllUser();
            	 
               //回车查询
                 doQuseryAction("queryForm");
                $("#queryList").datagrid(getOption());
                $("#bt_query").click(doQuery);
                $("#bt_reset").click(function() {
                    $("#queryForm")[0].reset();
                    doQuery();
                });
                
                $('#win_message').window({
            		width:500,
            		height:400,
            		onOpen:function() {
            			$(this).window("move", {
            				top:($(window).height()-400)*0.5,
            				left:($(window).width()-500)*0.5
            			});
            		},
            		onClose:function() {
            		}
            	});
                
            });
   </script>
</head>            
         <body class="easyui-layout" fit="true">
          <div region="west" title="<s:text name='system.search.title'/>"  border="false"
                 split="true" style="width:215px;padding:0px;"
                 iconCls="icon-search" tools="#pl_tt" >
			             <form action=""
	                      name="queryForm" id="queryForm">
	                      <div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
							 <div class="xpstyle" title="<s:text name="收短信人"/>" collapsible="true" collapsed="true">
								                                                  <input type="text"
								                                                           name="filter_LIKES_targetPhone"
								                                                           id="targetPhone" class="Itext"/>
							 </div>
							 <div class="xpstyle" title="<s:text name="短信内容"/>" collapsible="true" collapsed="true">
								                                                    <input type="text"
								                                                           name="filter_LIKES_smsBody"
								                                                           id="smsBody"  class="Itext"/>
							 </div> 
							 <div class="xpstyle" title="<s:text name="发短信人"/>" collapsible="true" collapsed="true">
								                                                  <input type="text"
								                                                           name="filter_LIKES_sourcePhone"
								                                                           id="sourcePhone"  class="Itext"/>
							 </div>
						 </div>
	                        <div style="text-align:center;padding:8px 8px;">	 
					                              <button type="button" id="bt_query" class="button_small">
								                                    <s:text name="system.search.button.title"/>
								                                </button>
								                                &nbsp;&nbsp;
								                                <button type="button" id="bt_reset" class="button_small">
								                                    <s:text name="system.search.reset.title"/>
								                                </button> 
							</div>	                                
	                    </div>
	                </form>            
      
            </div>
            <div region="center" title="" border="false">
                <table id="queryList" border="false"></table>
            </div>
             <div id="mm" class="easyui-menu" style="width:120px;">
					<div onclick="doAdd()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
					<div onclick="doDelete()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
					<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
					<div onclick="doCopy()" iconCls="icon-copy"><s:text name="system.button.copy.title"/></div>
					 <div onclick="doAccessory()" iconCls = "icon-attach"><s:text name="system.button.accessory.title"/> </div>
					 <div onclick="doViewResult()" iconCls = "icon-search"><s:text name="查看结果"/> </div>  
		    </div>
		    
	<div id="win_message" class="easyui-window" closed="true" modal="true" title="查看结果" style="width:500px;height:400px;padding:5px;background:#fafafa;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
					<div style="width:100%;height:100%;"><pre id="ta_message"></pre></div>
				</div>
			</div>
	</div>		    
</body>
</html>