<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title" /></title>
<%@ include file="/common/meta-gs.jsp"%>
<script type="text/javascript">
	//定义一个全局变量保存操作状态
	var operMethod = "${param.operMethod}" || "view";
	function doInitForm(data, callFunc) {
		if (callFunc) {
			callFunc();
		}
		if (data.msg) {
			var jsonData = data.msg;
			if(operMethod=="edit"){
				$("#showVlaue").html("<s:text name="system.sysmng.news.draft.title"/>");
				jsonData["status"] = "0";
				jsonData["source"] = "0";
				addState();
			} else if(operMethod=="view"){
				viewState();
				if (jsonData["status"] == "1") { //待发
					$("#tedit").hide();
					$("#release").hide();
					$("#showVlaue").html("<s:text name="待发送"/>");
				} else if (jsonData["status"] == "2") { //已发
					$("#tedit").hide();
					$("#release").hide();
					$("#showVlaue").html("<s:text name="已发送"/>");
				}
			}
			$('#viewForm').form('load', jsonData);
		}
	}

	function initRichTextBox() {
		var config = {
			/* toolbar : [
					[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList',
							'-', 'Link', 'Unlink' ], [ 'UIColor', 'Preview' ] ],
			customConfig : 'config_en.js' */
		};
		$('.jquery_ckeditor').ckeditor(config);
	}

	$(document).ready(function() {
		initRichTextBox();
		if (operMethod == "add") {
			//新增 
			addState();
			focusFirstElement();
		} else if (operMethod == "view") {
			//查看
			doModify("${param.id}", doDisabled);
		} else if (operMethod == "edit") {
			//复制
			doModify("${param.id}", doEnable);
		}
	});

	//设置焦点
	function focusFirstElement() {
		focusEditor("subject");
	}

	//将控件设置成可编辑状态
	function setFormToEdit() {
		addState();
		$("#tcancel").show();
		operMethod = "view";
		doEnable();
		focusFirstElement();
	}

	function doCopyMainTab() {
		var id = $("#id").val();
		if (id == null) {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.copy"/>',
							'info');
			return;
		} else {
			parent.addTab(
					'<s:text name="复制短信"/>-' + id,
					'${ctx}/sys/msg/sms!view.action?operMethod=edit&id=' + id);
		}
	}

	//页面数据初始化
	function doModify(id, callFunc) {
		var options = {
			url : '${ctx}/sys/msg/sms!queryView.action',
			data : {
				"id" : id
			},
			success : function(data) {
				//doAfterSave();//修改保存后刷新父页面
				doInitForm(data, callFunc);
				//getOrganization(id,operMethod);
			}
		};
		fnFormAjaxWithJson(options, true);
	}

	function saveObj(st) {
		/*if (st == "1") {
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("0");
		}*/
		if (operMethod == "edit" || operMethod == "add") {
			$("#id").val("");
			//$("#atmId").val("");
			operMethod = "view";
		}
		var options = {
			url : '${ctx}/sys/msg/sms!save.action',
			success : function(data) {
				doModify(data.msg, doAfterSave);
			}

		};
		fnAjaxSubmitWithJson("viewForm", options);
	}

	function doRepeal(st) {
		var id = $("#id").val();
		/*if (st == "1") {
			$("#status").val("1");
		} else if (st == "0") {
			$("#status").val("0");
		}*/
		if (operMethod == "edit" || operMethod == "add") {
			$("#id").val("");
		}
		var options = {
			url : '${ctx}/sys/msg/sms!updateStatus.action',
			data : {
				"id" : id,
				"status" : st
			},
			success : function(data) {
				doModify(id, doAfterSave);
			}
		};
		fnFormAjaxWithJson(options, true);		
	}

	function doAfterSave() {
		if (window.top.refreshTabData) {
			window.top.refreshTabData("<s:text name='短信管理'/>");
		}
		operMethod = "view";
		doDisabled();
	}

	function doEnable() {
		$("input[type!=hidden],textarea,select").removeAttr("disabled");
		$("input.easyui-combobox").combobox('enable');
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(false);
			} catch (e) {
				$(this).ckeditorGet().setReadOnly(false);
			}
		});
	}

	//将页面上的控件置为不可编辑状态
	function doDisabled() {
		$("input[type!=hidden],textarea,select").attr("disabled", "disabled");
		$("input.easyui-combobox").combobox('disable');
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(true);
			} catch (e) {
				$(this).ckeditorGet().setReadOnly(true);
			}
		});
	}

	function formClear() {
		$("#viewForm")[0].reset();
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setData("");
			} catch (e) {
				$(this).ckeditorGet().setData("");
			}
		});
		$("#source").val("0");
	}

	//查看里的新增
	function doAdd() {
		addState();
		operMethod = "add";
		doEnable();
		formClear();
		$("#status").val("0");
		$("#showVlaue").html("<s:text name='system.sysmng.news.draft.title'/>");
	}
	
	function addState(){//新增状态  只显示“关闭”，“保存”
		hideTool();
		showSave();
	}
	function viewState(){//(草稿)的查看状态  
		hideTool();
		hideSave();
	}

	function doCloseCurrentTab() {
		if(window.top.closeCurrentTab) {
			window.top.closeCurrentTab();
		}
    }
	
	function showSave(){
	  $("#tclose").show();
  	  $("#tsave").show();
    }
    
    function hideSave(){
      $("#tclose").show();
      $("#tadd").show();
  	  $('#tedit').show();
  	  $('#tcopy').show();
  	  $("#release").show();
    }
    
    //隐藏工具条
    function hideTool(){
  	  $("div.datagrid-toolbar a", "#toolbar_layout").hide();
    }
	
	//取消修改
	function doCancelEdit() {
		//setViewTitle();
		//viewState();
		var id = jwpf.getId();
		formClear();
		operMethod="view";
		doModify(id, doDisabled);
	}
</script>
</head>
<body class="easyui-layout" fit="true" title="maintable-view">
	<div region="north" style="overflow: hidden;" border="false" id="toolbar_layout">
		<div class="datagrid-toolbar">
			<a id="tclose"
					href="javascript:doCloseCurrentTab();"
					class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/></a>
			<a id="tcancel" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><s:text name="system.button.cancel.title"/></a>		
			<a id="tsave" href="javascript:saveObj('0');"
				class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/><s:text name="system.sysmng.news.draft.title"/></a>
			<a id="tadd" href="javaScript:doAdd();" class="easyui-linkbutton"
				plain="true" iconCls="icon-add"><s:text name="system.button.add.title"/></a> <a id="tedit" href="#"
				onclick="setFormToEdit();" class="easyui-linkbutton" plain="true"
				iconCls="icon-edit"><s:text name="system.button.modify.title"/></a> <a id="tcopy"
				href="javaScript:doCopyMainTab();" class="easyui-linkbutton"
				plain="true" iconCls="icon-copy"><s:text name="system.button.copy.title"/></a> <a id="release"
				href="javaScript:doRepeal('1');" class="easyui-linkbutton"
				plain="true" iconCls="icon-undo"><s:text name="发送短信"/></a>
			<a id="tattach" href="javaScript:doAccessory();"
					class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name='system.button.accessory.title'/></a>	
		</div>
	</div>
	<div region="center" style="position: relative; overflow: auto;" border="false">
		<form action="" method="post" name="viewForm" id="viewForm"
			style="margin: 0px;">
			<input type="hidden" id="id" name="id" />
			<input type="hidden" id="status" name="status" value="0"/>
			<input type="hidden" id="source" name="source" value="0"/>
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tr>
					<th><label><s:text name="收短信人："/></label></th>
					<td colspan="3"><input type="text" name="targetPhone" id="targetPhone"
						class="easyui-validatebox Itext" required="true" style="width: 800px"/><span style="color:red;">（多个手机号码请用分号;隔开）</span>
					</td>
				</tr>
				<tr>
					<th valign="top"><label><s:text name="短信内容："/></label></th>
					<td colspan="3"><textarea
							id="smsBody" name="smsBody" style="height:200px;width:96%"></textarea></td>
				</tr>
				<tr>
					<th><label><s:text name="发短信人："/></label>
					</th>
					<td>
						<input type="text" name="sourcePhone" id="sourcePhone"
						class="easyui-validatebox Itext" />
					</td>
					<th><label><s:text name="服务号"/>：</label>
					</th>
					<td>
						<input type="text" name="serviceSms" id="serviceSms"
						class="easyui-validatebox Itext" />
					</td>
				</tr>
				<tr>
					<th><label><s:text name="定时时间"/>：</label>
					</th>
					<td>
						<input class="Idate Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});" type="text" name="regSendTime" id="regSendTime">
					</td>
					<th><label><s:text name="system.sysmng.notice.status.title"/>：</label>
					</th>
					<td>
						<div id="showVlaue"><s:text name="system.sysmng.news.draft.title"/></div>
					</td>
				</tr>
			</table>
		</form>
	</div>

</body>
</html>
