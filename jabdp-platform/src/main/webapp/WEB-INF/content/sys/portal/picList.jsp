<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GalleryView - Default Demo</title>
</head>

<body>

<%-- <%
    Random rad=new Random();
    int radParam=rad.nextInt();
%> 

<script type="text/javascript">
 
 var imageData=[
               {"src":"${ctx}/js/news/images/bp1.jpg","alt":"Lone Tree Yellowstone","title":"当呜呜","href":"http://qing.weibo.com/1313425490/4e494852330025in.html"},
               {"src":"${ctx}/js/news/images/bp2.jpg","alt":"cc","title":"EASYUI","href":"http://jeasyui.com/documentation/window.php"},
               {"src":"${ctx}/js/news/images/bp4.jpg","alt":"Is He Still There?!","title":"被强制引产孕妇遭辱","href":"http://news.163.com/photoview/00AP0001/24843.html"},
               {"src":"${ctx}/js/news/images/bp7.jpg","alt":"Noni Nectar For Green Gecko","title":"被强制引产孕妇遭辱","href":"http://news.163.com/photoview/00AP0001/24843.html"},
               {"src":"${ctx}/js/news/images/bp14.jpg","alt":"Flight of an Eagle Owl","title":"被强制引产孕妇遭辱","href":"http://news.163.com/photoview/00AP0001/24843.html"},
               {"src":"${ctx}/js/news/images/bp26.jpg","alt":"Winter Lollipops","title":"被强制引产孕妇遭辱","href":"http://news.163.com/photoview/00AP0001/24843.html"},
               {"src":"${ctx}/js/news/images/bp27.jpg","alt":"Day of Youth","title":"被强制引产孕妇遭辱","href":"http://news.163.com/photoview/00AP0001/24843.html"},
               {"src":"${ctx}/js/news/images/bp28.jpg","alt":"Sunbathing Underwater","title":"被强制引产孕妇遭辱","href":"http://news.163.com/photoview/00AP0001/24843.html"},
               {"src":"${ctx}/js/news/images/bp41.jpg","alt":"Untitled","title":"被强制引产孕妇遭辱","href":"http://news.163.com/photoview/00AP0001/24843.html"},
               {"src":"${ctx}/js/news/images/bp49.jpg","alt":"New Orleans Streetcar","title":"被强制引产孕妇遭辱","onclick":"window.open('http://news.163.com/photoview/00AP0001/24843.html')"},
               {"src":"${ctx}/js/news/images/bp52.jpg","alt":"By The Wind of Chance","title":"被强制引产孕妇遭辱","onclick":"window.open('http://news.163.com/photoview/00AP0001/24843.html')"},
               {"src":"${ctx}/js/news/images/bp53.jpg","alt":"Blue Lagoon","title":"被强制引产孕妇遭辱","onclick":"window.open('http://news.163.com/photoview/00AP0001/24843.html')"},
               {"src":"${ctx}/js/news/images/bp54.jpg","alt":"Time","title":"被强制引产孕妇遭辱","onclick":"window.open('http://news.163.com/photoview/00AP0001/24843.html')"},
           ];
 
      
	$(function(){
		  var content=[];
		   for(var i=0;i<imageData.length;i++){
			  var str ="<li><img src='"+imageData[i].src+"' alt='"+imageData[i].alt+"' title='"+imageData[i].title+"' href='"+imageData[i].href+"' ck='openImageNews'></img></li>";
		      content.push(str);
		   }
		    var res=content.join("");
		    $("#myGallery_${param.id}_<%=radParam%>").append(res);
		    
		/* 
		transition_speed	INT	1000年	照片的过渡动画的持续时间（以毫秒为单位）。
		transition_interval	INT	4000	幻灯片方式自动转换之间的延迟时间（以毫秒为单位）。
		缓解	串	“摇摆”	缓解方法中使用的照片和缩略图动画。方法可用于任何自定义宽松。
		show_panels	布尔	真	标志切换显示面板的主要照片。设置为假，以创建一个旋转木马式的画廊。（未完全实现）
		show_panel_nav	布尔	真	标志的下一个和以前在画廊面板的按钮来切换显示。
		enable_overlays	布尔	假	切换显示信息的图像叠加的标志。
		panel_width	INT	800	图像面板的宽度（以像素为单位）。
		panel_height	INT	400	图像面板的高度（以像素为单位）。
		panel_animation	串	“褪色”	的动画方法用于面板图像转换。选项​​是“淡入淡出”，“淡入淡出”，“幻灯片”
		panel_scale	串	“剪裁”	面板的图像缩放方法。选项​​是'作物'，'适合'。
		overlay_position	串	“底”	的定位信息面板覆盖。选项​​'底部'，'顶'
		pan_images	布尔	假	切换拖的标志，超大的面板图像的平移。
		pan_style	串	“拖”	使用淘选法。'拖'=用户点击和拖动图像进行平移，'轨道'=图像自动盘根据鼠标的位置
		start_frame	INT	1	指标的图像显示在画廊负载时
		show_filmstrip	布尔	真	导航幻灯片切换显示的标志。
		show_filmstrip_nav	布尔	真	标志以胶片为基础的导航按钮切换显示。
		enable_slideshow	布尔	真	标志切换播放/暂停按钮和开始/停止幻灯片
		自动播放	布尔	假	标志画廊负载时自动启动幻灯片
		filmstrip_position	串	“底”	胶卷有关的面板的位置。选项​​是'顶'，'左'，'底部'，'右'。
		FRAME_WIDTH	INT	70	胶片帧的宽度，以像素为单位。
		frame_height	INT	40	的胶片帧以像素为单位的高度。
		frame_opacity	浮动	0.4	默认情况下不透明度的电影胶片帧（范围0.0 - 1.0）
		frame_scale	串	“剪裁”	帧图像的缩放方法。选项​​是'作物'，'适合'。
		frame_gap	INT	5	之间的距离（以像素为单位）的帧。
		show_captions	布尔	假	标志切换帧字幕显示图像的标题属性（填充）。
		show_infobar	布尔	真	切换显示的图像吧台的标志。
		infobar_opacity	浮动	1.0	信息栏默认的不透明度（范围0.0 - 1.0） */
		
		 $('#myGallery_${param.id}_<%=radParam%>').galleryView({
			transition_interval:800,
			transition_speed: 100, 	
			panel_width:"400%", 		
			panel_height: "250%",
			//frame_width: "100%",
		    //frame_height: "100%",
	    	show_captions: true
		   // show_filmstrip_nav: false
		}); 
	});
	 --%>
	
	<%
    Random rad=new Random();
    int radParam=rad.nextInt();
    String id=request.getParameter("id");
    request.setAttribute("id",id);
   %>

<script type="text/javascript">
	$(function(){
		 var id=${param.id};
		 initContent("picList",id,"myGallery_${param.id}_<%=radParam%>");
		 
	});
	
</script>

	<ul id="myGallery_${param.id}_<%=radParam%>"></ul>

</body>
</html>
