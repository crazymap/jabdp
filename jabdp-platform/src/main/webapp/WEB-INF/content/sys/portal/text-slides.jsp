<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

</head>
<body>

<%
    Random rad=new Random();
    int radParam=rad.nextInt();
    String id=request.getParameter("id");
    request.setAttribute("id",id);
%>

<script type="text/javascript">
	$(function(){
		 var id=${param.id};
		 initContent("textSlides",id,"div_textSlides_${param.id}_<%=radParam%>");
		 
	});

</script>
<style>
.text-slide ul{  margin:0;padding:0;}
.text-slide ul li{ float:left; margin-right:45px; line-height:30px;}
.text-slide ul li a{ font:32px Arial, Helvetica, sans-serif; color:red;}
</style>
   <div class="text-slide" id="div_textSlides_${param.id}_<%=radParam%>" ><ul id="div_textSlides_${param.id}_<%=radParam%>_ul" ></ul></div>
</body>
</html>