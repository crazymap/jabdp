<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<%
    Random rad=new Random();
    int radParam=rad.nextInt();
    String id=request.getParameter("id");
    request.setAttribute("id",id);
   %>

<script type="text/javascript">
	$(function(){
		 var id=${param.id};
		 initContent("picList",id,"slides_${param.id}_<%=radParam%>");	 
	});	
</script> 
	<div id="slides_${param.id}_<%=radParam%>" class="slides_personal"></div>
</body>
</html>
