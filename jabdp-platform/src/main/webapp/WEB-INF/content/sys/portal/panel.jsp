<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<% 
    String layoutType=request.getParameter("layoutType");
    String[] widthValue=layoutType.split(";");
    request.setAttribute("layouts", widthValue);
 %>
  <div id="pp${param.id}" style="position:relative">
          <c:forEach items="${layouts}" var="value">
				<div  style="width:${value};"></div>
		  </c:forEach>			
  </div>
  <div id="hidPP${param.id}" style="position:relative;display:none;"> 
		<div style="width:100%;"></div>
  </div> 
   <script type="text/javascript">
	$(function(){
		modifyPanels("${param.id}","${param.desktopId}","${param.layoutVal}");
	});
</script>

