<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.Random"%>
<%@ include file="/common/taglibs.jsp"%>

<%
    Random rad=new Random();
    int radParam=rad.nextInt();
%>
 

<script type="text/javascript">
	$(function(){
		 var id="${param.id}";
		desktopSource(id);
		 
	});
	
	function desktopSource(id) {
		var options = {
					url:'${ctx}/gs/process.action',
					 data : {
		                 "desktopId" :id
		             },
					success:function(data) {
					    var obj=data.msg;
					    var showDisplayNum=obj.displayNum;//条数
					    var showFontNum=obj.fontNum;//字数
					   var options=obj.options;//{'createUser':true,'createTime':true}
					    
					    var jsonData=obj.list;
					    var len=jsonData.length;
					    var content=[];
					    if(showDisplayNum<=len){
					    	len=showDisplayNum;
					    }
					    for(var i=0;i<len;i++){
					    	//创建人
					    	
					    	 var userName=jsonData[i].startUser;
					    	 //var uObj=_userList[jsonData[i].startUser];
                           /*   if(uObj){
                                 userName=uObj.realName;
                             }else{
                                 userNeme=jsonData[i].startUser;
                             } */
                             
                             
                             //标题字数控制
                              var title=jsonData[i].name+"--"+jsonData[i].moduleId;
                             var showTitle="";
                             if(showFontNum<=title.length){
                            	 showTitle=title.substr(0,showFontNum)+"...";
                             }else{
                            	showTitle=title; 
                             } 
                             
                             //是否显示创建者, 创建时间
                             var createUser="";
                             var createTime="";
                             if(options){
                            	 var opt=$.parseJSON(options);
                                 if(opt.createUser){
                                	 createUser="["+userName+"]";
                                 }
                                 if(opt.createTime){
                                	 createTime="("+jsonData[i].createTime+")";
                                 }
                             } else{
                            	 createUser="["+userName+"]";
                            	 createTime="("+jsonData[i].createTime+")";
                             }
                           /*   
                           	 var strArr = [];
                             strArr.push('<li><span>[');
                             strArr.push(jsonData[i].moduleName);
                             strArr.push(']');
                             strArr.push(showTitle+createTime+createUser);
                             strArr.push('</span><a  href="javascript:void(0);" onclick="doSignin(');
                             strArr.push(jsonData[i].id);
                             strArr.push(',"');
                             strArr.push(jsonData[i].name);
                             if(jsonData[i].assignee==null){	                                                         
                             strArr.push('");">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签收并办理</a></li>');                
                             }else if(jsonData[i].assignee!=null){ 
                             strArr.push('");">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;办理</a></li>');
                             }
                             
                             
                             strArr
                             */
                             if(jsonData[i].assignee==null){							
								 var str =
								"<li><span>"
								+"["+jsonData[i].moduleName+"]"
								+showTitle+createTime+createUser
								+"</span><a  href='javascript:void(0);' onclick='doSignin("+jsonData[i].id+",\""+jsonData[i].name+"\");'>&nbsp;&nbsp;&nbsp;签收并办理</a></li>";
                             }
                             else if(jsonData[i].assignee!=null){                       	 
                            	 var str =
     								"<li><span>"
     								+"["+jsonData[i].moduleName+"]"
     								+showTitle+createTime+createUser
     								+"</span><a  href='javascript:void(0);' onclick='doSignin("+jsonData[i].id+",\""+jsonData[i].name+"\");'>&nbsp;&nbsp;&nbsp;办理</a></li>";
							}
	
							 content.push(str);
						 }
				
					   var res=content.join("");
					   $("#ul_id_${param.id}_<%=radParam%>").append(res); 
					}
			};
			fnFormAjaxWithJson(options,true);			 
	}

</script>
 
<div>
  
	<ul id="ul_id_${param.id}_<%=radParam%>" type="square"></ul>
</div>
