<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<script type="text/javascript">

	$(document).ready(function() {
		initMenu();
	});

var setting = {
		check: {
			enable: true
		},
		data: {
			simpleData: {
				enable: true
			}
		}
	};


	//根据一个任务环节角色，显示用户树
 function initMenu(){
		var options = {
				url : '${ctx}/gs/process!getRolesList.action',
				data : {
					"roles":"${param.roles}"
				},
				success : function(data) {
					if (data.msg && data.msg.length>0) {
						$.each(data.msg, function(k,v) {
							v.checked = false;
						});
						$.fn.zTree.init($("#roleIds"), setting, data.msg);
						zTree = $.fn.zTree.getZTreeObj("roleIds");
						zTree.expandAll(true);
						//zTree.checkAllNodes(false);
					}else{
						$("#userSel").hide();
					}
				}
			};
			fnFormAjaxWithJson(options,true);
	} 
 function getIds(){
		var zTree = $.fn.zTree.getZTreeObj("roleIds");
		if(zTree!=null){
			var nodes = zTree.getCheckedNodes(true);
			if(nodes.length){
				var nodeIds = [];
				$.each(nodes, function(k,v) {
					nodeIds.push(v.id);
				});
				$("#userIds").val(nodeIds.join(","));
			} else {
				$.messager.alert('提示信息','请选择一个或多个审批人','info');
				return false;
			}			
		}
		return true;
	}
 //启动流程
 function doStart(){
	 var flag = getIds();
	 if(flag){
		var options = {
				url : '${ctx}/gs/process!start.action',
				success : function(data) {
					if (data.msg) {
						 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.sysmng.process.startSuccess.title"/>',
							'info'); 
						doCancel();
						var funcName = "${param.fname}";
						var callFunc = eval(funcName);
						if(callFunc) {
							callFunc();
						}
					}
				}
			};
			fnAjaxSubmitWithJson('handleForm', options);
	 }
 }

 function doCancel() {
	 $("#processWin").window("close");
	}
 
 </script>
<div class="easyui-layout" fit="true">
 	<div region="center" border="false"
				style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
		<form action="" name="handleForm" id="handleForm" method="post">
			<input type="hidden" id="businessId" name="businessId" value="${param.bid}" />
				<input type="hidden" id="businessName" name="businessName" value="${param.bname}" />
					<table align="center">
						<tbody >
							<tr id="userSel">
							<th><label ><s:text name="system.sysmng.process.customUser.title"/>:</label>
								</th>
								<td >
								<input type="hidden" name="userIds" id="userIds"/>
							<ul  style="text-align: center; width: 230px;height:200px;" id="roleIds" class="ztree"></ul>	
							</td>			
							</tr>
						</tbody>	
					</table>

				</form>
	</div>
	<div region="south" border="false"
				style="text-align: center; height: 30px; line-height: 30px;">
				<a class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="doStart()"> <s:text name="system.sysmng.process.start.title"/></a> 
				<a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0)" onclick="doCancel()"> <s:text name="system.button.cancel.title"/></a>
	</div>
</div>
	
