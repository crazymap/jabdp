<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><s:text name="system.index.title" /></title>
<%@ include file="/common/meta-min.jsp"%>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/common.js"></script>

<script type="text/javascript">
	
</script>
</head>  
<body class="easyui-layout" fit="true">
	<div region="center" title="">
		<img src="${ctx}/sys/process/deploy!getProcessPicture.action?deploymentId=${param.deploymentId}" style="position:absolute; left:0px; top:0px;"/>
	</div>
</body>
</html>