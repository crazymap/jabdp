<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="/common/meta.jsp"%>
	<link type="text/css" href="${ctx}/js/jquery-ui/blue/jquery-ui-1.8.23.custom.css" rel="stylesheet"/>
	<script type="text/javascript" src="${ctx}/js/jquery-ui/scripts/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/jquery-ui/scripts/jquery-ui-1.8.23.custom.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*var _userList=findAllUser();
			var _userSelList = [];
			$.each(_userList, function(k, v) {
				var obj = {};
				obj["label"]= v.realName + "(" + v.loginName + ")[" + v.orgName + "]";
				if("${param.taskId}"){
					obj["value"]= v.loginName;
				}else{
					obj["value"]= v.id;
				}				
				_userSelList.push(obj);
			});*/
			//初始化用户集合
			/*$("#userSel").autocomplete({
				source: _userSelList,
				minLength: 1,
				select: function( event, ui ) {
					$("#userSel").val( ui.item.label);
					$("#userId").val( ui.item.value );
					return false;
				}
			});*/
			
			$( "#userSel" ).autocomplete({
			      source: function( request, response ) {
			        $.ajax({
			          url: "${ctx}/sys/account/user!queryUserListMobile.action",
			          dataType: "json",
			          type : "post",
			          data: {
			              filter_LIKES_loginName_OR_realName_OR_nickname_OR_employeeId : request.term,
			              filter_EQS_status : "1",
			        	  page:1,
			        	  rows:10
			          },
			          success: function( data ) {
			            response( $.map( data.msg, function( item ) {
			              	item["label"] = item.nickName + "【" + item.orgName + "】";
			              	if("${param.taskId}"){
			              		item["value"] = item.loginName;
			              	} else {
			              		item["value"] = item.id;
			              	}
			              	return item;
			            }));
			          }
			        });
			      },
			      minLength: 1,
			      select: function( event, ui ) {
			    	  $("#userSel").val( ui.item.label);
					  $("#userId").val( ui.item.value );
					  return false;
			      }
			 });
		});

		var tid ="${param.taskId}";
		//设置任务转派
		function doDelegate(){
			var uId = $('#userId').val();
			if(uId) {
				var options = {
						url : '${ctx}/gs/process!delegateUser.action',
						data : {
							"userId":uId,
							"taskId":tid
						},
						success : function(data) {
							if (data.msg) {
								 alert('任务转派成功！');
								 window.parent.$("#pass_win").window("close");
								 window.parent.doClose();
							}				
						}
					};
					fnFormAjaxWithJson(options,true);
			} else {
				alert('请输入转派人员');
			}
		} 
		function update(){
			window.parent.updateCreateUser();
		}
		function getUserId(){
			var uId = $('#userId').val();
			return uId;
		}
	</script>
	</head>
<body class="easyui-layout" >
		<div region="center"  style="overflow:hidden;padding:20px 50px;">
			<input id="userSel" style="width:82%;" placeholder="输入登录名/账户名/员工号/姓名查询" />
			<input type="hidden" name="userId" id="userId" class="easyui-validatebox" required="true"/> 
				<c:if test="${taskId != null}">
			<button  type="button" onclick="doDelegate();"><s:text name="system.button.submit.title"/></button>
				</c:if>
				<c:if test="${taskId == null}">
				<a class="easyui-linkbutton"iconCls="icon-ok" href="javascript:void(0);"onclick="update();">确定 </a>
		</c:if>
		</div>		
</body>
</html>