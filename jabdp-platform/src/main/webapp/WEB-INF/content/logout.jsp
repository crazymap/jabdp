<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><s:text name="system.login.title"/></title>
	<%@ include file="/common/meta-min.jsp" %>
	<script>
		$(document).ready(function() {
			$("#logoutForm").submit();
		});
	</script>
</head>
<body>
		<div>正在注销中...请稍等</div>
		<div style="display:none">
			<form id="logoutForm" action="${ctx}/j_spring_security_logout" method="post"></form>
		</div>
</body>
</html>

