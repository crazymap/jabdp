<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><s:text name="system.index.license.info.title"/></title>
	<%@ include file="/common/meta-min.jsp" %>
</head>
<body>
	<div>
		${licCheckInfo}
	</div>
	<div><a href="${ctx}/index!reCheckAuth.action">更新授权</a></div>
</body>
</html>
