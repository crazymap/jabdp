<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>退出系统</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="抗癌 健康"/>
    <meta name="description" content="抗癌健康网"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="memreg.css" rel="stylesheet" />
</head>
<body>
		<div>正在注销中...请稍等</div>
		<div style="display:none">
			<form id="logoutForm" action="${ctx}/j_spring_security_logout" method="post" >
				<input type="hidden" name="spring-security-redirect" value="/login.jsp"></input>
			</form>
		</div>
		<script src="${ctx}/js/jquery/jquery-1.7.2.min.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			$("#logoutForm").submit();
		});
		</script>
</body>
</html>

