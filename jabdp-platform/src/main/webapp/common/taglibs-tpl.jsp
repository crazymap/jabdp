<%@page import="org.apache.commons.lang3.time.DateFormatUtils"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:if test="${empty LOCALE}">
	<c:set var="LOCALE" scope="session" value="zh_CN"></c:set>
</c:if>
<c:if test="${empty THEME_STYLE}">
	<c:set var="THEME_STYLE" scope="session" value="themes/default"></c:set>
</c:if>
<c:if test="${empty THEME_COLOR}">
	<c:set var="THEME_COLOR" scope="session" value="blue"></c:set>
</c:if>
<c:if test="${empty THEME_VERSION}">
	<c:set var="THEME_VERSION" scope="session" value="bootstrap"></c:set>
</c:if>
<c:set var="locale" value="${LOCALE}"/>
<c:set var="themeStyle" value="${THEME_STYLE}"/>
<c:set var="themeColor" value="${THEME_COLOR}"/>
<c:set var="imgPath" value="${ctx}/${themeStyle}/${themeColor}/images"/>
<c:set var="cssPath" value="${ctx}/${themeStyle}/${themeColor}/css"/>
<c:set var="loadUrl" value="${imgPath}/loading.gif"/>
<c:set var="today" value="<%=DateFormatUtils.format(new java.util.Date(),\"yyyy-MM-dd\")%>" />
<c:set var="jsVersion" value="JS_VERSION" />