/**
 * jQuery Form Validator
 * ------------------------------------------
 *
 * Spanish language package
 *
 * @website http://formvalidator.net/
 * @license MIT
 */
!(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define(["jquery"], function (a0) {
      return (factory(a0));
    });
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require("jquery"));
  } else {
    factory(jQuery);
  }
}(this, function (jQuery) { 
(function($, window) {

  'use strict';

  $.formUtils.LANG = {
		  		errorTitle:"表单提交失败！",
		        requiredField:"这是必填字段",
		        requiredFields:"你没有回答所有必填字段",
		        badTime:"你没有给出正确的时间",
		        badEmail:"你没有给出正确的电子邮件地址",
		        badTelephone:"你没有给出正确的电话号码",
		        badSecurityAnswer:"你没有给出正确答案的安全问题",
		        badDate:"你没有给出正确的日期",
		        lengthBadStart:"输入值必须在",
		        lengthBadEnd:"字符",
		        lengthTooLongStart:"输入值长于",
		        lengthTooShortStart:"输入值短于",
		        notConfirm:"输入值无法确认",
		        badDomain:"域名不正确",
		        badUrl:"输入值不正确的URL",
		        badCustomVal:"输入值不正确",
		  	    andSpaces:"和空格",
		        badInt:"输入的值格式不正确，必须为整数",
		        badSecurityNumber:"你的社会保险号码不正确",
		        badUKVatAnswer:"英国增值税号不正确",
		        badUKNin:"不正确的英国NIN",
		        badUKUtr:"不正确的英国编号",
		        badStrength:"密码不够强",
		        badNumberOfSelectedOptionsStart:"你必须至少选择",
		        badNumberOfSelectedOptionsEnd:"答案",
		        badAlphaNumeric:"输入值只能包含字母数字字符",
		        badAlphaNumericExtra:"和",
		        wrongFileSize:"您尝试上传的文件太大（最多％s）",
		        wrongFileType:"只允许类型为％s的文件",
		        groupCheckedRangeStart:"请选择",
		        groupCheckedTooFewStart:"请至少选择",
		        groupCheckedTooManyStart:"请选择最大",
		        groupCheckedEnd:"项目",
		        badCreditCard:"信用卡号不正确",
		        badCVV:"CVV号码不正确",
		        wrongFileDim:"图像尺寸不正确",
		        imageTooTall:"图像不能高于",
		        imageTooWide:"图像不能宽于",
		        imageTooSmall:"图像太小",
		        min:"最小值",
		        max:"最大值",
		        imageRatioNotAccepted:"图像比例不被接受",
		        badBrazilTelephoneAnswer:"输入的电话号码无效",
		        badBrazilCEPAnswer:"输入的CEP无效",
		        badBrazilCPFAnswer:"CPF输入无效",
		        badPlPesel:"输入的PESEL无效",
		        badPlNip:"输入的NIP无效",
		        badPlRegon:"输入的REGON无效",
		        badreCaptcha:"请确认你不是机器人",
		  		passwordComplexityStart:'密码必须至少包含',
		         passwordComplexitySeparator:',',
		         passwordComplexityUppercaseInfo:'大写字母',
		         passwordComplexityLowercaseInfo:'小写字母',
		         passwordComplexitySpecialCharsInfo: '特殊字符（S）',
		         passwordComplexityNumericCharsInfo: '数字字符（多个）',
		         passwordComplexityEnd:'.'
    };

})(jQuery, window);

}));
