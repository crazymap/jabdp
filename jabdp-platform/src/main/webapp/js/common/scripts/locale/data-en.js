$.jwpf = {
	system : {
		alertinfo : {
			errorTitle : "Error",
			errorInfo : "Sorroy,Something is wrong!",
			titlet : "Prompt",
			info : "Please Wait...",
			titleInfo : "Title Info",
			file : "File",
			uploadFailed : "Upload Failed:",
			tryAgain : "Please try again later！",
			serverException : "Server Exception，Please try again later！",
			failedInfo : "Upload Failed，Error Info",
			set : "Please Select",
			addType : "--Add Type--",
			confirmTitle:"Confirm information",
			operDialog:"Operation dialog box",
			expTitle:"Exceptional Tip:",
			loadDataError:"Loading data is abnormal:",
			getDataError:"Get data exception:",
			currPage:"Current page:",
			selPage:"Selected:"
		},
		attach : {
			attachList : "Attach List"
		},
		process : {
			processStartWin : "ProcessStart Window",
			startSuccess : "Start Process Successful!"
		},
		button : {
			expand : "ExpandAll",
			contract : "ContractAll",
			deleteImage:"Delete Picture",
			add :"Add",
			edit:"Edit",
			view:"View",
			save:"Save",
			saveNew:"Save+New",
			close:"Close",
			quickAddEdit:"Quick Add Edit",
			quickSelect:"Quick Select",
			ok:"Ok",
			cancel:"Cancel"
		},
		confirm:{
			selectOnlyOneRow:"Please select and select only one data!",
			sureDeleteSelRow:"Are you sure you want to delete the selected data?",
			selectRow:"Please select data first!",
			nodata:"No data",
			exportAllData:"You have no choice of data, are you sure you want to export all the data?",
			selectInsertRow:"Please select the location of the inserted row"
		},
		module : {
			status : {
				"00":"init",
				"10":"draft",
				"20":"approving",
				"30":"finish",
				"31":"approval",
				"32":"disapproval",
				"35":"passed",
				"40":"invalid"
			},
			statusSub : {
				"10":"enabled",
				"40":"disabled"
			},
			statusList:[
						{"id":'00,10',"text":'draft'},
						{"id":'20',"text":'approving'},
						{"id":'31,35',"text":'approval'},
						{"id":'32',"text":'disapproval'},
						{"id":'40',"text":'invalid'}          
					],
			reviseLog:"Revision log"
		},
		notice : {
			style:{
				"01": "general notice",
				"02": "business reminder",
				"03": "to do",
				"04": "business confirmation",
				"05": "notice of reminders",
				"06": "business for"
			}
		}
	}
};