//子模块菜单
myLib.NS("desktop.childMenu");
myLib.desktop.childMenu = {
	childMenuHtml:function(node, page) {
		//ctx服务器根目录
		var ctx = $("body").data("ctx");
		if(node.childs.length == 0) {
			var str = "<div _id='" + node.id + "' _page='" + page + "' _url='" + ctx + node.resourceUrl + "' _name='" + node.name
			+ "' id='menu_" + node.id + "' iconCls='imgIcon " + node.iconSkin +"'>"
			+ "<span>" + node.name + "</span>"
			+ "</div>";
			return str;
		}
		else {
			var menudata = node.childs;
			var width = menudata[0].name.replace(/[^\x00-\xff]/g, '__').length;
			for(var l = 1; l < menudata.length; l++) {
				var x = menudata[l].name.replace(/[^\x00-\xff]/g, '__').length;
				if(width < x) {
					width = x;
				}
			}
			width = width * 7 + 40;//7是菜单的字体大小
			var height = "";
			if(menudata.length > 10) {
				height = ';height:225px';
			}
			var str = "<div iconCls='imgIcon "+ node.iconSkin +"'>"
			+ "<span>" + node.name + "</span>"
			+ "<div style='width:" + width + "px" + height + "' class='easyui-panel'>";
			for(var i = 0; i<node.childs.length; i++) {
				str = str + myLib.desktop.childMenu.childMenuHtml(node.childs[i], page);
			}
			str = str + "</div></div>";
			return str;
		}
	},
	init:function(data) {
		var _this = this;
		//将菜单动态添加到body的末尾
		for(var i = 0; i < data.length; i++) {
			var adata = data[i].childs;
			for(var j = 0; j < adata.length; j++) {
				//判断该模块是否有下级模块，若有则生成相应菜单
				if(adata[j].childs.length) {
					//计算生成的菜单需要的宽高
					var menudata = adata[j].childs;
					var width = menudata[0].name.replace(/[^\x00-\xff]/g, '__').length;
					for(var l = 1; l < menudata.length; l++) {
						var x = menudata[l].name.replace(/[^\x00-\xff]/g, '__').length;
						if(width < x) {
							width = x;
						}
					}
					width = width * 7 + 40;//7是菜单的字体大小
					var height = "";
					if(menudata.length > 10) {
						height = ';height:225px';
					}
					var menustr = "<div id='menu_" + adata[j].id + "' class='easyui-menu easyui-panel' style='width:" + width + "px" + height + "'>";
					for(var l = 0; l < menudata.length; l++) {
						menustr = menustr + _this.childMenuHtml(menudata[l], i);
					}
					menustr = menustr + "</div>";
					$("body").append(menustr);
				}
			}
		}
		//添加点击事件
		$("div[_page]").click(function(event) {
			var option = {
				"WindowTitle":$(this).attr("_name"),
				"iframSrc":$(this).attr("_url"),
				"WindowsId":$(this).attr("_id"),
				"WindowAnimation":'none',
				"imgclass":$(this).attr('iconCls'),
				"pageindex":$(this).attr('_page')
			};
			myLib.desktop.win.newWin(option);
		});
	}
};
