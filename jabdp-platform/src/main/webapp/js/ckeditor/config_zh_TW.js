﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	config.language = 'zh';
	config.uiColor = '#D2E0F2';
	
	config.width = "auto";
    	config.height = 'auto'; // 高度

    // 换行方式  
        config.enterMode = CKEDITOR.ENTER_BR;  
      
        // 当输入：shift+Enter是插入的标签  
        config.shiftEnterMode = CKEDITOR.ENTER_BR;//   
        //图片处理  
        config.pasteFromWordRemoveStyles = true;  
        config.filebrowserImageUploadUrl =__ctx+ "/sys/ckeditor/ckeditor-upload!ckUploadImage.action";  
          
        // 去掉ckeditor“保存”按钮  
        config.removePlugins = 'save';  
		// 增加中文字体
		config.font_names = '宋体/SimSun;新宋体/NSimSun;仿宋/FangSong;楷体/KaiTi;仿宋_GB2312/FangSong_GB2312;'+  
        '楷体_GB2312/KaiTi_GB2312;黑体/SimHei;华文细黑/STXihei;华文楷体/STKaiti;华文宋体/STSong;华文中宋/STZhongsong;'+  
        '华文仿宋/STFangsong;华文彩云/STCaiyun;华文琥珀/STHupo;华文隶书/STLiti;华文行楷/STXingkai;华文新魏/STXinwei;'+  
        '方正舒体/FZShuTi;方正姚体/FZYaoti;细明体/MingLiU;新细明体/PMingLiU;微软雅黑/Microsoft YaHei;微软正黑/Microsoft JhengHei;'+  
        'Arial Black/Arial Black;'+ config.font_names;  
};
