<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-css.jsp" %>
	<style>
	.select2-container--open{
	     z-index: 99999 !important;
	}
	</style>
	<#-- <link href="${r"${ctx}"}/js/easyui/${r"${themeColor}"}/panel.css" rel="stylesheet" type="text/css"/>-->
<#import "common/field.ftl" as fieldftl>  
<#assign entityName=root.entityName />  
<#assign formList=root.dataSource.formList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>
<#if formList??>
        <#list formList as form>
            <#if form.isMaster>
</head>            
<body>
<@compress single_line=true>
 	<div class="easyui-layout" fit="true">       
          <#-- <div region="west" title="<s:text name='system.search.title'/>" collapsed="true" 
                 split="true" style="width:215px;padding:0px;"
                 iconCls="icon-search" tools="#pl_tt" border="false">
		           <@fieldftl.initFormTree form=form/>
			       <form action="${r"${ctx}"}/gs/gs-mng!queryDictList.action?entityName=${entityName}"
	                      name="queryForm" id="queryForm">
	                    <div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
		                       <@fieldftl.getQueryView form=form/>	
	                    </div>
	                    <div style="text-align:center;padding:8px 8px;">
						             <button type="button" id="bt_query" class="button_small">
									     <s:text name="system.search.button.title"/>
									 </button>&nbsp;&nbsp;
									 <button type="button" id="bt_reset" class="button_small">
									     <s:text name="system.search.reset.title"/>
									 </button>
						   </div> 
	                </form>           
      
            </div>-->
            <@fieldftl.initFormTree form=form/>
            <div region="center" title="" border="false">
                <table id="queryList" border="false"></table>
            </div>
    </div>
    <div class="container-fluid advance-query-form" style="margin-top:10px;display:none;">
		<div class="row form-group">
			<div class="col-sm-12">
				<div class="input-group input-group-sm">
				  <span class="input-group-addon" id="sizing-addon3">自定义查询条件</span>
				  <select id="queryFieldSel"></select>
				</div>
			</div>
		</div>
		<form id="queryForm" onsubmit="return false;">
		</form>
	</div>
    <div style="display:none;">     
    		<div id="queryListToolBar">
             	<@fieldftl.getQueryViewV2 formList=formList />
             	<div class="datagrid-toolbar" style="border-bottom-width:0;">
             		<security:authorize url="/gs/gs-mng!add.action?entityName=${entityName}">
             		<a id="bt_add" href="#" class="easyui-linkbutton" plain="true" icon="icon-add" onclick="doAdd()"><s:text name="system.button.add.title"/>(I)</a>
             		</security:authorize>
             		<security:authorize url="/gs/gs-mng!delete.action?entityName=${entityName}">
             		<a id="bt_del" href="#" class="easyui-linkbutton" plain="true" icon="icon-remove" onclick="doDelete()"><s:text name="system.button.delete.title"/>(D)</a>
             		</security:authorize>
             		<security:authorize url="/gs/gs-mng!view.action?entityName=${entityName}">
             		<a id="bt_view" href="#" class="easyui-linkbutton" plain="true" icon="icon-search" onclick="doView()"><s:text name="system.button.view.title"/>(E)</a>
             		</security:authorize>
             		<security:authorize url="/gs/gs-mng!edit.action?entityName=${entityName}">
             		<a id="bt_copy" href="#" class="easyui-linkbutton" plain="true" icon="icon-copy" onclick="doCopyTbar()"><s:text name="system.button.copy.title"/>(G)</a>
             		</security:authorize>
             		<security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
             		<a id="bt_accessory" href="#" class="easyui-linkbutton" plain="true" icon="icon-attach" onclick="doAccessory()"><s:text name="system.button.accessory.title"/>(Y)</a>
             		</security:authorize>
                     	<security:authorize url="/gs/process!startApprove.action?entityName=${entityName}">
	                        <a id="bt_selfApprove" href="#" class="easyui-linkbutton" plain="true" icon="icon-flowStart" onclick="doSelfProcess()"><s:text name="system.button.selfApprove.title"/></a>
	             		</security:authorize>
	             		<security:authorize url="/gs/process!cancelApprove.action?entityName=${entityName}">
	             			<a id="bt_cancelSelfApprove" href="#" class="easyui-linkbutton" plain="true" icon="icon-user" onclick="doCancelSelfProcess()"><s:text name="system.button.cancelSelfApprove.title"/></a>
	             		</security:authorize>
	             	<#if form.enableImport>
                        <security:authorize url="/gs/gs-mng!importModuleExcel.action?entityName=${entityName}"> 
                        	<a id="bt_excelImport" href="#" class="easyui-linkbutton" plain="true" icon="icon-excelImport" onclick='doOpenExcelImportWin("${entityName}")'><s:text name="system.button.importExcel.title"/></a>
	             		</security:authorize>
	             	</#if>
                    <#if form.enableExport>
                        <security:authorize url="/gs/gs-mng!exportModuleExcel.action?entityName=${entityName}">
                        	<a id="bt_excelExport" href="#" class="easyui-linkbutton" plain="true" icon="icon-excelExport" onclick='doExportExcel("${entityName}", getExportParams("queryList"), jwpf.getTableVal("queryList",true,"id").join(","))'><s:text name="system.button.exportExcel.title"/></a>
	             		</security:authorize>
	             	</#if>
	             	<@fieldftl.initDatagridToolBarButtonV2 form=form />
             	</div>
             </div>   
             <div id="mm" class="easyui-menu" style="width:120px;">
				<security:authorize
					url="/gs/gs-mng!add.action?entityName=${entityName}">
					<div onclick="doAdd()" iconCls="icon-add"><s:text name="system.button.add.title"/>(I)</div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!delete.action?entityName=${entityName}">
					<div onclick="doDelete()" iconCls="icon-remove"><s:text name="system.button.delete.title"/>(D)</div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!view.action?entityName=${entityName}">
					<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/>(E)</div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!edit.action?entityName=${entityName}">
					<div onclick="doCopy()" iconCls="icon-copy"><s:text name="system.button.copy.title"/>(G)</div>
				</security:authorize>
			</div>
			<div id="pl_tt">
				<a href="#" id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>" style="display:none;"></a>
				<a href="#" id="a_exp_clp" class="accordion-expand" title="<s:text name="system.button.expand.title"/>"></a>
			</div>
			<div id="detail_win" closed="true" modal="true"
				title="<s:text name="${i18nKey!titleName}"/>"
				iconCls="icon-save"
				style="width: 600px; height: 400px; padding: 0px; background: #fafafa;">
				<div class="easyui-layout" fit="true">
					<div region="center" border="false"
						style="padding: 2px; background: #fff; border: 1px solid #ccc;">
						<form action="" name="viewForm" id="viewForm" method="post">
							<input type="hidden" id="id" name="id"/>
							<input type="hidden" id="atmId" name="atmId" />
                        	<input type="hidden" id="flowInsId" name="flowInsId" /> 
                        	<input type="hidden" id="status" name="status" value="10" />
                        	<div style="display:none;">
                      			<#list form.fieldList as field>
                      				<#if !field.editProperties.visible>
                      					<@fieldftl.getFiledView field=field form=form/>
                      				</#if>
                      			</#list>
                      		</div>
							<table border="0" cellpadding="0" cellspacing="0" class="module_form">
								<tbody>
									<#assign colCount=1>
												                       	<#assign rowCount=0>
																		<#assign fields=form.fieldList>
																		<#assign columns=form.cols!>
																		<#assign column=columns>
																		<#assign rows=newWritableNumberArray(fields?size)><#--每一行被占的列数-->
														               <#list fields as field>
														                <#if field.editProperties.visible>
																			    <#-- 一行首列 -->
																			    <#if colCount == 1>
																			    	<tr>
																			    	<#assign column=column-rows[rowCount]>
																			    </#if>
																			    <#assign oldColumn=column>
																			    <#assign column=column-field.editProperties.cols>
											                                    <#if column gte 0>
											                                    	 <#assign rowspan=field.editProperties.rows>
											                                    	 <#assign colspan=field.editProperties.cols*2-1>
											                                         <#-- 判断行是不是满了-->
											                                        <#if field.key?? && field.key!="">
												                                         <th rowspan="${rowspan}">
												                                         	<#if field.caption?? && field.caption!="">
											                                         			<label for="${field.key}"><s:text name="${field.i18nKey!field.caption}"/>:</label>
											                                                <#else>
											                                                	&nbsp;
											                                                </#if> 
												                                         </th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">
												                                                   <@fieldftl.getFiledViewV2 field=field form=form/>
												                                         </td>
											                                        <#else>
											                                        	 <th rowspan="${rowspan}">&nbsp;</th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
												                                     </#if>
												                                         <#if rowspan gt 1>
													                                         <#assign rowStart = rowCount+1>
													                                         <#assign rowEnd = rowCount+rowspan-1>
													                                         <#list rowStart..rowEnd as i>
													                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
													                                         </#list>
													                                      </#if>
											                                      	 <#if column==0>
											                                      	 	<#-- 一行中最后一列 -->
											                                            </tr>
											                                            <#assign rowCount=rowCount+1>
											                                            <#assign colCount=1>
											                                            <#assign column=columns>
											                                         <#else>
											                                         	<#assign colCount=colCount + 1>
											                                         </#if>
											                                     <#else>
												                                     	<th>&nbsp;</th>
														                                <td colspan="${oldColumn*2-1}">&nbsp;</td>
														                                </tr>
														                                <#assign rowCount=rowCount+1>
											                                            <#assign column=columns>
											                                            <#assign column=column-field.editProperties.cols-rows[rowCount]>
														                                <tr>
														                                <#if column gte 0>
														                                 <#assign rowspan=field.editProperties.rows>
												                                    	 <#assign colspan=field.editProperties.cols*2-1>
												                                         <#-- 判断行是不是满了-->
												                                       <#if field.key?? && field.key!="">  
												                                         <th rowspan="${rowspan}">
												                                         	<#if field.caption?? && field.caption!="">	
											                                         			<label for="${field.key}"><s:text name="${field.i18nKey!field.caption}"/>:</label>
											                                                <#else>
											                                                	&nbsp;
											                                                </#if> 
												                                         </th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">
												                                                   <@fieldftl.getFiledViewV2 field=field form=form/>
												                                         </td>
												                                       <#else>
												                                       	 <th rowspan="${rowspan}">&nbsp;</th>
												                                         <td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
												                                       </#if>
												                                         <#if rowspan gt 1>
													                                         <#assign rowStart = rowCount+1>
													                                         <#assign rowEnd = rowCount+rowspan-1>
													                                         <#list rowStart..rowEnd as i>
													                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
													                                         </#list>
													                                     </#if>
														                                 <#if column==0>
												                                      	 	<#-- 一行中最后一列 -->
												                                            </tr>
												                                               <#assign rowCount=rowCount+1>
													                                            <#assign colCount=1>
													                                            <#assign column=columns>
												                                         </#if>
												                                      </#if>
		   																	 </#if>
		   																  </#if>
										                                </#list>
										                                <#if column != columns>
										                                	<th>&nbsp;</th>
											                                <td colspan="${column*2-1}">&nbsp;</td>
											                                </tr>
										                                </#if>
								</tbody>
							</table>
						</form>
					</div>
					<div data-options="region:'south',border:false" style="text-align:right;padding:2px 2px;">
						<a id="stSave" class="easyui-linkbutton" iconCls="icon-ok"
							href="javascript:void(0)" onclick="doSaveObj();"><s:text
								name="system.button.save.title" /> </a> 
						<a id="stClose" class="easyui-linkbutton"
							iconCls="icon-cancel" href="javascript:void(0)" onclick="doCloseWindow();"><s:text
								name="system.button.cancel.title" /> </a>
					</div>
				</div>
			</div>
		</div>
</@compress>		
			<%@ include file="/common/meta-js.jsp" %>
			<#-- <script type="text/javascript" src="${r"${ctx}"}/js/easyui-1.4/easyui.xpPanel.js"></script>-->
			<script type="text/javascript">
            var _userList={};
            var operMethod="view";
			
			function doSelfProcess(id) {
				var ids = [];
				if(id) {
                	ids.push(id);
                } else {
                	var rows = $('#queryList').datagrid('getChecked');
	                for (var i = 0; i < rows.length; i++) {
	                   	if(rows[i].status != "10" && rows[i].status != "31") {
	                    	$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.selfApproveWarn.title"/>', 'info');
	                    	return;
	                    }
	                    ids.push(rows[i].id);
	                }
                }
                if(ids.length) {
	                $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.selfApproveSure"/>',
						function(r) {
			           		if(r) {
			                 	jwpf.doStartOrCancelApprove("${entityName}", ids, "startApprove", function() {doAfterSelfProcess();});
			                } 
			        	}
			        );
                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
                }
			}
			
			function doAfterSelfProcess() {
				doQuery();
			}
	
			function doCancelSelfProcess(id) {
				var ids = [];
				if(id) {
					ids.push(id);
				} else {
	                var rows = $('#queryList').datagrid('getChecked');
	                for (var i = 0; i < rows.length; i++) {
	                   	if(rows[i].status != "35" && rows[i].status != "31") {
	                    	$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.cancelSelfApproveWarn.title"/>', 'info');
	                    	return;
	                    }
	                    ids.push(rows[i].id);
	                }
                }
				if(ids.length) {
	                $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.cancelSelfApproveSure"/>',
						function(r) {
			           		if(r) {
			                 	jwpf.doStartOrCancelApprove("${entityName}", ids, "cancelApprove", function() {doAfterSelfProcess();});
			                } 
			        	}
			        );
                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
                }
			}
			
			//作废
			function doInvalidData(id) {
				$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.invalid.title"/>',
					function(r) {
		           		if(r) {
		                 	jwpf.updateField("${entityName}", "EQS_status", id, "40", doAfterSelfProcess);
		                } 
		        	}
		        );
			}
			
			function operFormatter(value, rowData, rowIndex){
					var strArr = [];
					<security:authorize url="/gs/gs-mng!view.action?entityName=${entityName}">
					strArr.push('<a href="javascript:void(0);" onclick="doDblView(');
					strArr.push(rowData.id);
					strArr.push(')"><s:text name="system.button.view.title"/></a>');
					</security:authorize>
					if(rowData.status == "10") {
						<security:authorize url="/gs/gs-mng!modify.action?entityName=${entityName}">
						strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doEdit(');
						strArr.push(rowData.id);
						strArr.push(')"><s:text name="system.button.modify.title"/></a>');
						</security:authorize>
						<security:authorize url="/gs/gs-mng!delete.action?entityName=${entityName}">
						strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doDelete(');
						strArr.push(rowData.id);
						strArr.push(')"><s:text name="system.button.delete.title"/></a>');
						</security:authorize>
						<security:authorize url="/gs/process!startApprove.action?entityName=${entityName}">
						strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doSelfProcess(');
						strArr.push(rowData.id);
						strArr.push(')"><s:text name="system.button.selfApprove.title"/></a>');
						</security:authorize>
					} else if(rowData.status == "35" || rowData.status == "31") {
						<security:authorize url="/gs/process!cancelApprove.action?entityName=${entityName}">
						strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doCancelSelfProcess(');
						strArr.push(rowData.id);
						strArr.push(')"><s:text name="system.button.cancelSelfApprove.title"/></a>');
						</security:authorize>
						<security:authorize url="/gs/process!invalidProcess.action?entityName=${entityName}">
						strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doInvalidData(');
						strArr.push(rowData.id);
						strArr.push(')"><s:text name="system.button.invalid.title"/></a>');
						</security:authorize>
					}
					return strArr.join("");
				}
			
            function getOption() {
                return {
                	editing:false,
                    width : 'auto',
                    height : 'auto',
                    nowrap : false,
                    striped : true,
                    autoRowHeight: false,
                    fit : true,
                    url :'${r"${ctx}"}/gs/gs-mng!queryDictList.action?entityName=${entityName}',
                    sortName : <#if ((form.sortName)!"") != "">'${form.sortName}'<#else>'id'</#if>,
                    sortOrder : <#if ((form.sortOrder)!"") != "">'${form.sortOrder}'<#else>'desc'</#if>,
                    pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
					pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
                    frozenColumns : [
                        [
                            {
                                field : 'ck',
                                checkbox : true
                            }, 
                            {
								field : "atmId",
								title : "<s:text name="system.button.accessory.title"/>",
								width : 35,
								formatter:function(value, rowData, rowIndex) {
									if(value) {
										var astr = [];
										astr.push('<a class="easyui-linkbutton l-btn l-btn-plain" href="javascript:void(0);"');
										<security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
										astr.push(' onclick="doAccessory(');
										astr.push(rowData["id"]);
										astr.push(',');
										astr.push(value);
										astr.push(',\'');
										astr.push(rowData["status"]);
										astr.push('\')"');
										</security:authorize>
										astr.push('>');
										astr.push('<span class="l-btn-left"><span class="l-btn-text"><span class="l-btn-empty icon-attach">&nbsp;</span></span></span></a>');
										return astr.join("");
									} else {
										return "<span></span>";
									}
								}
                            }
                            <#if !((form.isHideStatus)!false) >
                            ,
                            {
                            	field : "status",
								title : "<s:text name="system.module.status.title"/>",
								width : 60,
								formatter:function(value, rowData, rowIndex) {
									<#--if(value) {
										var val = $.jwpf.system.module.status[value];
										return (val?val:"");
									} else {
										return "";
									}-->
									return rowData["status_caption"];
								}
                            }
                            </#if>
                            <#assign fields=form.fieldList/>
                            <#if fields??&&fields?size gt 0>
                                <#list fields as field>
	                                    <#if ((field.queryProperties.showInGrid)!false) && ((field.queryProperties.frozenColumn)!false)> 
	                                        <@fieldftl.initMainQuery field=field prefix="dg_" form=form appendCommaFirst=true/>
	                                    </#if>
                                </#list>
                            </#if>
                        ]
                    ],
                    columns : [
                        [
                            <#if fields??&&fields?size gt 0>
                                <#list fields as field>
	                                    <#if ((field.queryProperties.showInGrid)!false) && !((field.queryProperties.frozenColumn)!false)> 
	                                        <@fieldftl.initMainQuery field=field prefix="dg_" form=form/>
	                                    </#if>
                                </#list>
                            </#if>      
                                        <#assign isShowComma = false />
                            			<#if form.isShowCreateUser >   
                                         {
	                                            field : 'createUser',
	                                            title : '<s:text name="system.module.createUser.title"/>',
	                                            width : 120,
	                                            sortable : true,
												isFilterData:true,
	                                            formatter:function(value, rowData, rowIndex){
	                                                    <#--var uObj=_userList[value];
	                                                    if(uObj){
	                                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
															return uObj.realName + "[" + uObj.loginName + "]" + nickName;
	                                                    }else{
	                                                        return value;
	                                                    }-->
	                                                    return rowData["createUser_caption"];
	                                            }
                                         }<#assign isShowComma = true /></#if>
                                         <#if form.isShowCreateTime ><#if isShowComma>,</#if> 
                                         {
	                                            field : 'createTime',
	                                            title : '<s:text name="创建时间"/>',
	                                            width : 100,
												sortable : true,
												align : 'center'
                                         }<#assign isShowComma = true /></#if>
                                         <#if form.isShowLastUpdateUser ><#if isShowComma>,</#if> 
                                         {
	                                            field : 'lastUpdateUser',
	                                            title : '<s:text name="最后修改者"/>',
	                                            width : 120,
	                                            sortable : true,
												isFilterData:true,
	                                            formatter:function(value, rowData, rowIndex){
	                                                   return rowData["lastUpdateUser_caption"];
	                                            }
                                         }<#assign isShowComma = true /></#if>
                                         <#if form.isShowLastUpdateTime ><#if isShowComma>,</#if>
                                         {
	                                            field : 'lastUpdateTime',
	                                            title : '<s:text name="最后修改时间"/>',
	                                            width : 100,
												sortable : true,
												align : 'center'
                                         }<#assign isShowComma = true /></#if>
                                         <#if isShowComma>,</#if>
                                        {
											field : 'oper',
											title : '<s:text name="system.button.oper.title"/>',
											width : 250,
											align : 'center',
											formatter : operFormatter
										}
                        ]
                    ],
                    onDblClickRow:function(rowIndex, rowData){
                    	<@fieldftl.initDatagridOnDblClickRow form=form />
                    	doDblView(rowData.id);
                    	<#-- $('#queryList').datagrid('unselectRow',rowIndex);-->
                    },
                    
    				onRowContextMenu  : function(e, rowIndex, rowData){
    					e.preventDefault();
    					$('#mm').data("id", rowData.id);
    					$('#mm').menu('show', {
    						left: e.pageX,
    						top: e.pageY
    					});
    				},
    				onLoadError:function() {
    					$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="数据加载异常"/>', 'info');
    				},
                    pagination : true,
                    rownumbers : true,
                    toolbar :"#queryListToolBar"
                    <#-- toolbar :[
                    	'-'
						<security:authorize url="/gs/gs-mng!add.action?entityName=${entityName}">
                        ,{
                            id : 'bt_add',
                            text : '<s:text name="system.button.add.title"/>(I)',
                            iconCls : 'icon-add',
                            handler : function() {
                                doAdd();
                            }
                        }</security:authorize><security:authorize url="/gs/gs-mng!delete.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_del',
                            text : '<s:text name="system.button.delete.title"/>(D)',
                            iconCls : 'icon-remove',
                            handler : function() {
                                doDelete();
                            }                           
                        }</security:authorize> <security:authorize url="/gs/gs-mng!view.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_view',
                            text : '<s:text name="system.button.view.title"/>(E)',
                            iconCls : "icon-search",
                            handler : function() {
                                doView();
                            }
                        }</security:authorize><security:authorize url="/gs/gs-mng!edit.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_copy',
                            text : '<s:text name="system.button.copy.title"/>(G)',
                            iconCls : "icon-copy",
                            handler : function() {
                            	doCopyTbar();
                            }
                        }</security:authorize>
                        <security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_accessory',
                            text : '<s:text name="system.button.accessory.title"/>(Y)',
                            iconCls : "icon-attach",
                            handler : function() {
                                doAccessory();
                            }
                        }</security:authorize>
                        <security:authorize url="/gs/process!startApprove.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_selfApprove',
                            text : '<s:text name="system.button.selfApprove.title"/>',
                            iconCls : "icon-flowStart",
                            handler : function() {
                               doSelfProcess();
                            }
                        }</security:authorize>
                        <security:authorize url="/gs/process!cancelApprove.action?entityName=${entityName}">
                        ,{
                        	id : 'bt_cancelSelfApprove',
                            text : '<s:text name="system.button.cancelSelfApprove.title"/>',
                            iconCls : "icon-user",
                            handler : function() {
                               doCancelSelfProcess();
                            }
                        }</security:authorize>
                         <#if form.enableImport>
                        <security:authorize url="/gs/gs-mng!add.action?entityName=${entityName}">  
                        ,
                        {
                        	id : 'bt_excelImport',
                            text : '<s:text name="system.button.importExcel.title"/>',
                            iconCls : "icon-excelImport",
                            handler : function() {
                               doOpenExcelImportWin("${entityName}");
                            }
                        }
                        </security:authorize>
                        </#if>
                        <#if form.enableExport>
                        <security:authorize url="/gs/gs-mng!view.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_excelExport',
                            text : '<s:text name="system.button.exportExcel.title"/>',
                            iconCls : "icon-excelExport",
                            handler : function() {
                               var rows = $('#queryList').datagrid('getChecked');
                               var ids = [];
                               if(rows && rows.length) {
                               	  $.each(rows, function(i, v) {
                               	  	ids.push(v.id);
                               	  });
                               }
                               doExportExcel("${entityName}", getParams(), ids.join(","));
                            }
                        }
                        </security:authorize>
                        </#if>
                    ]-->
                    <@fieldftl.initDatagridParam form=form />
                };
            }
            
            function getParams() {
				var param = $("#queryForm").serializeArrayToParamStr();
				var sort = $("#queryList").datagrid("options")["sortName"];
				var order = $("#queryList").datagrid("options")["sortOrder"];
				var strParam = "sort=" + sort + "&order=" + order;
				if(param) {
					strParam += "&" + param;
				}
				return strParam;
			}
					 
			   function formatterImage(value, rowData, rowIndex){
			           if(value){
			        	   var path="${r"${systemParam.virtualPicPath}"}"+value;
			               return  "<div><img onclick='$(body).gzoomExpand(\" "+path+" \")' src='"+path+"'  id='contractImgSrc' width='20px' height='20px' /></div>"; 
			           }
				 }
						            
				<#-- 
                <#assign fields=form.fieldList/>
                <#if fields??&&fields?size gt 0>
                    <#list fields as field>
                        <#if (field.queryProperties.showInGrid)!false>
                           <@fieldftl.initControlsData field=field type="datagrid" prefix="dg_" entityName=form.entityName/>
                        </#if>
                    </#list>
                </#if>-->
                
            function comboboxInit(){
			         	<#assign fields=form.fieldList!>
		                <#if fields??&&fields?size gt 0>
		                    <#list fields as field>
			                    <@fieldftl.initControlsData field=field type="query" prefix="" entityName=form.entityName isV2=true/>
		             		</#list>
		          		</#if>
      		 }
      		 
      		 function initFormDefaultValue() {
	    		<#list formList as form>
	            	<#if form.isMaster>
	                	<@fieldftl.initFormFieldDefaultValue form=form />
	            	</#if>    
	    		</#list>
	    	}
		
		//设置焦点
		function focusFirstElement() {
			if(operMethod != "view") {
	  			jwpf.focusFirstInputOnForm("viewForm");
	  		}
		}	
    
         //右键复制一条记录 
         function doCopy(){
             var id = $("#mm").data("id");
             if(!id){
            	 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
                 return ;
             }else{
            	 doCopyData(id);
             }
             
          }
           //工具条复制一条记录
           function doCopyTbar(){
        	     var id=0;
             	 var rows = $('#queryList').datagrid('getChecked');
             	 if(rows.length!=1){
             		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
             	     return ;
             	 } else{
             		 id=rows[0].id;
             		 doCopyData(id);
             	 }
           }
           
           //删除一条记录
            function doDelete(id) {
                var ids = [];
                if(id) {   
                	ids.push(id);
                } else {
                    var rows = $('#queryList').datagrid('getChecked');
                    for (var i = 0; i < rows.length; i++) {
                    	if(rows[i].status != "10") {
                    		$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.delApproveData"/>', 'info');
                    		return;
                    	}
                        ids.push(rows[i].id);
                    }
                } 
                if (ids != null && ids.length > 0) {
                    $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
                        if (r) {
                            var options = {
                                url : '${r"${ctx}"}/gs/gs-mng!delete.action?entityName=${entityName}',
                                data : {
                                    "ids" : ids
                                },
                                success : function(data) {
                                    if (data.msg) {
                                        doQuery();
                                    }
                                },
                                traditional:true
                            };
                            fnFormAjaxWithJson(options);
                        }
                    })

                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
                }
            }
            
            var _ad_query_opt_ = {"data":<@fieldftl.getQueryViewJson formList=formList/>,"isInit":false};
            function doAdvanceQuery() {
            	openFormWinV2({"message":$(".advance-query-form"),"isIframe":false,"isModal":false,
            		"okTitle":"<s:text name="system.search.button.title"/>","cancelTitle":"<s:text name="system.search.reset.title"/>"
            		,onAfterSure:function(win) {
            			doQuery();
            		}, onAfterCancel:function(win) {
            			doResetQuery(true);
            		}});
            	if(!_ad_query_opt_["isInit"]) {
            		$ES.initAdvanceQuery(_ad_query_opt_);
            		_ad_query_opt_["isInit"] = true;
            	}
            }
            
            //查询
            function doQuery() {
            	var param = $("#simpleQueryForm").serializeArrayToParam();
                $.extend(true, param, $("#queryForm").serializeArrayToParam());<#-- 高级查询 -->
                $('#queryList').datagrid('clearSelections');
                $("#queryList").edatagrid("load", param);
            }
            function doResetQuery(isResetAdQuery) {
            	!isResetAdQuery && $("#simpleQueryForm")[0] && $("#simpleQueryForm")[0].reset();
            	$ES.doResetAdvanceQuery();
            	doQuery();
            }
            //保存
			function doSaveObj() {
				   var isValidate= $ES.validateForm("#viewForm"); <#--$("#viewForm").form("validate"); -->
				   if(isValidate){
					   var jsons = formToJSON('viewForm');
						if(operMethod == "edit" || operMethod == "add"){
							jsons["id"]=null;
							jsons["atmId"]=null;
							jsons["flowInsId"]=null;
						}
						var jsonSavedata = JSON.stringify(jsons);
						var options = {
							url : '${r"${ctx}"}/gs/gs-mng!save.action?entityName=${entityName}',
							async : false,
							data : {
								"jsonSaveData" : jsonSavedata
							},
							success : function(data) {
								<#-- doModify(data.msg, doAfterSave); -->
								doInitForm(data, doAfterSave);
							}
						};
						fnFormAjaxWithJson(options);
				   }
				}
				//保存后继续新增
				function doAfterSave() {
					doRefreshDataGrid();
					operMethod="add";
					formClear();
					initFormDefaultValue();
					focusFirstElement();
				}
				
		function doPrepareForm(jsonData) {
			<#list formList as form>
			     <#if form.isMaster>
			     	<@fieldftl.prepareForm form=form />
			     </#if>
			</#list>
		}			
			
		function clearAddField() {
			$("#id").val("");
			$("#atmId").val("");
			$("#flowInsId").val("");
			$("#status").val("10");
		}	
				
		function doInitForm(data, callFunc) {
			if (data.msg) {
                  var jsonData = data.msg;
                  doPrepareForm(jsonData);
                  $('#viewForm').form('load',jsonData);
                  if(operMethod == "edit") {
                  	clearAddField();
                  }
            }
            if(callFunc) {
            	callFunc();
            }
		}			
		
		function formClear() {
			$('#viewForm').form('clear');
			$("#status").val("10");
			$(".jquery_ckeditor").each(function() {
				try {
					$(this).ckeditorGet().setData("");
				} catch(e) {
					$(this).ckeditorGet().setData("");
				}
			});
			$("img.previewImageCls").attr("src", "");
		}	
				
			   //页面数据初始化
		   function doModify(id, callFunc) {
		       	var options = {
		             url : '${r"${ctx}"}/gs/gs-mng!queryEntity.action?entityName=${entityName}',
		             data : {
		                 "id" : id
		             },
		             success : function(data) {
		                 doInitForm(data, callFunc);
		             }
		        };
		        fnFormAjaxWithJson(options);
		   }	
          
           //新增
            function doAdd() {
                operMethod = "add";
                var title = "<s:text name="system.button.add.title"/><s:text name="${i18nKey!titleName}"/>";
            	$("#detail_win").window("setTitle", title);
            	$("#detail_win").window("open");
            }
            //查看
            function doView() {
            	 var id=0;
            	 var rows = $('#queryList').datagrid('getChecked');
            	  if(rows.length!=1){
            		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
            	     return ;
            	 } else{
            		 id=rows[0].id;
            	 }
            	 doDblView(id); 
            }
            //修改
            function doEdit(id) {
            	operMethod = "modify";
            	$("#detail_win").data("id", id);
            	var title = "<s:text name="system.button.modify.title"/><s:text name="${i18nKey!titleName}"/>";
            	$("#detail_win").window("setTitle", title);
            	$("#detail_win").window("open");
            }
            
           //查看tab
            function doDblView(id) {
            	operMethod = "view";
            	$("#detail_win").data("id", id);
            	var title = "<s:text name="system.button.view.title"/><s:text name="${i18nKey!titleName}"/>";
            	$("#detail_win").window("setTitle", title);
            	$("#detail_win").window("open");
            }
            
            //复制记录
           function doCopyData(id) {
           		operMethod = "edit";
           		$("#detail_win").data("id", id);
           		var title = "<s:text name="system.button.copy.title"/><s:text name="${i18nKey!titleName}"/>";
            	$("#detail_win").window("setTitle", title);
            	$("#detail_win").window("open");
           }
            
            //右键查看
            function doCmView() {
            	var id = $("#mm").data("id");
            	doDblView(id);
            }
            
           //刷新列表
            function doRefreshDataGrid() {
    			$("#queryList").edatagrid("load");
    		}
    		//关闭窗口
    		function doCloseWindow() {
    			$("#detail_win").window("close");
    		}
    		
    		function initQueryData() {
				<#assign fields=form.fieldList!>
                <#if fields??&&fields?size gt 0>
                    <#list fields as field>
	                        <#if  ((field.queryProperties.showInSearch)!false)>
		                         <@fieldftl.initControlsData field=field type="listQuery" prefix="query_" entityName=form.entityName/>
	                        </#if>
	                        <#if (field.queryProperties.showInTreeSearch)!false>
	                        	<@fieldftl.initTreeQuery field=field prefix="dg_" entityName=form.entityName/>
	                        </#if>
                    </#list>
                </#if>
                <#-- 系统状态初始化参数 -->
			    query_statusJson = $ES.getStatusList();
    		}
    		
    		function doEnable() {
			     	$("div.file_upload_button").show();
					$("button:not(.validInView),input.Idate:not(.validInView)").removeAttr("disabled");
					$("a.btn:not(.validInView)").removeAttr("disabled").css("pointer-events","auto");
					$("input.form-control:not(.read-only), textarea:not(.read-only)").removeAttr("readonly");
					$("select.combobox").removeProp("disabled");
					$("input.easyui-dialoguewindow").dialoguewindow("enable");
					$("input.easyui-processbarSpinner").processbarSpinner('enable');
					$(".jquery_ckeditor").each(function() {
						try {
							$(this).ckeditorGet().setReadOnly(false);
						} catch(e) {
							$(this).ckeditorGet().setReadOnly(false);
						}
					});
					$("a.bt-extra").linkbutton('enable');
					jwpf.enableLinkButton($("a.hyperlink").not(".validInView"));	
					$("#stSave").show();
			}	
    		
    		//将页面上的控件置为不可编辑状态
			function doDisabled() {
			    $("div.file_upload_button").hide();
				$("button:not(.validInView), input.Idate:not(.validInView)").attr("disabled", "disabled");
				$("a.btn:not(.validInView)").attr("disabled", "disabled").css("pointer-events", "none");
				$("input.form-control:not(.read-only), textarea:not(.read-only)").attr("readonly","readonly");
				$("select.combobox").prop("disabled", true);
				$("input.easyui-dialoguewindow").dialoguewindow("disable");
				$("input.easyui-processbarSpinner").processbarSpinner('disable');
				$(".jquery_ckeditor").each(function() {
					try {
						$(this).ckeditorGet().setReadOnly(true);
					} catch(e) {
						$(this).ckeditorGet().setReadOnly(true);
					}
				});
				$("a.bt-extra").linkbutton('disable');
				jwpf.disableLinkButton($("a.hyperlink").not(".validInView"));
				$("#stSave").hide();
			}
			
			//添加附件
            function doAccessory(id, attachId, status) {
				 if(!id) {
	           	     var rows = $('#queryList').datagrid('getChecked');
		           	 if(rows.length!=1){
		           		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
		           	     return ;
		           	 } else{
		           		attachId=rows[0].atmId;
		           		id=rows[0].id;
		           		status=rows[0].status;
		           	 }
	           	 }
	           	var opts ={"attachId":attachId,
           				 "id":id,
           				 "key":"",
           				 "entityName":"${entityName}",
           				 "dgId":"queryList",
           				 "flag":"mainQuery",
           				 "readOnly" : ""
           				};
	           	 if(status == "10") {
	           		opts.readOnly =false;
	           	 	doAttach(opts);
	           	 } else {
	           		opts.readOnly =true;
	           	 	doAttach(opts);
	           	 }
            }

			//更新附件ID flag: mainQuery-主实体查询，subEdit-子实体编辑，mainEdit-主实体编辑
            function doUpdateAttach(opts) {
            	var options = {
            			url : '${r"${ctx}"}/gs/gs-mng!updateAttach.action',
                        data : {
                        	"entityName":opts.entityName,
                            "id" : opts.id,
                            "attachId": opts.attachId,
                            "field" : "atmId"
                        },
    					async: false,
    					success : function(data) {
    						if(data.msg) {
        						if(opts.flag == "mainQuery") {
        							$("#" + opts.dgId).datagrid("reload");
            					} else if(opts.flag == "subEdit"){
            						$("#" + opts.dgId).edatagrid("reload");
                				}
    						}
    					}
    			};
    			fnFormAjaxWithJson(options);
            }
			
			<@fieldftl.initFormFieldOnListEvent form=form /> <#-- 初始化列表按钮事件 -->
			
			    function __doListButtonClick(fieldVal, fieldKey, tableKey, rowIndex, callFunc) {
					if(callFunc) {
						callFunc(fieldVal, fieldKey, tableKey, rowIndex);
					}
				}
				
				function __getListButton(fieldVal, fieldKey, tableKey, rowIndex, title) {
					var astr = [];
					astr.push('<button type="button" class="button_big"');
					astr.push(' onclick="__doListButtonClick(\'');
					astr.push(fieldVal);
					astr.push('\',\'');
					astr.push(fieldKey);
					astr.push('\',\'');
					astr.push("queryList");
					astr.push('\',\'');
					astr.push(rowIndex);
					astr.push('\',');
					astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
					astr.push(')"');
					astr.push('>');
					astr.push(title);
					astr.push('</button>');
					return astr.join("");
				}
				
				function __getListHyperLink(fieldVal, fieldKey, tableKey, rowIndex, title) {
					var astr = [];
					astr.push('<a href="javascript:void(0);" style="text-decoration: underline;"');
					astr.push(' onclick="__doListButtonClick(\'');
					astr.push(fieldVal);
					astr.push('\',\'');
					astr.push(fieldKey);
					astr.push('\',\'');
					astr.push("queryList");
					astr.push('\',\'');
					astr.push(rowIndex);
					astr.push('\',');
					astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
					astr.push(')"');
					astr.push('>');
					astr.push(title);
					astr.push('</a>');
					return astr.join("");
				}
    		
    		function initDetailWindow() {
    			$("#detail_win").window({
    				onOpen:function() {
    					$(this).window("move", {
							top:($(window).height()-400)*0.5,
							left:($(window).width()-600)*0.5
						});
    					var id = $("#detail_win").data("id");
    					switch(operMethod) {
    						case "add"://新增
    							initFormDefaultValue();
    							focusFirstElement();
    							break;
    						case "view"://查看
    							doModify(id, doDisabled);
    							break;
    						case "modify"://修改
    							doModify(id, focusFirstElement);
    							break;
    						case "edit"://复制
    							doModify(id, focusFirstElement);
    							break;
    						default:
    							doModify(id, doDisabled);
    					}
    				},
    				onClose:function() {
    					doEnable();
    					formClear();
    				}
    			});
    		}

            $(document).ready(function() {
               //_userList=findAllUser();
               //回车查询
                 doQuseryAction("queryForm");
                $("#queryList").edatagrid(getOption());
                $("#bt_query").click(doQuery);
                $("#bt_reset").click(function() {
                    //$("#queryForm")[0].reset();
                    $("#queryForm").form("clear");
                    collapseAll();
                    doQuery();
                });
                initQueryData();
                comboboxInit();
                initDetailWindow();
                jwpf.bindKeydownOnForm("viewForm");
                jwpf.bindHotKeysOnListPage();
                $ES.initFormValidate();
            });

            </script>		
</body>
            </#if>
      </#list>
</#if>
</html>