<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%
SystemParam systemParam = SpringContextHolder.getBean("systemParam");
request.setAttribute("systemParam", systemParam);
%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-css.jsp" %>
<#-- <link href="${r"${ctx}"}/js/easyui/${r"${themeColor}"}/panel.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="${r"${ctx}"}/js/easyui/gray/easyui-module.css" colorTitle="gray"/>	
<link rel="stylesheet" type="text/css" href="${r"${ctx}"}/js/easyui/blue/easyui-module.css" colorTitle="blue"/>-->
<#import "common/field.ftl" as fieldftl> 
<#assign entityName=root.entityName />  
<#assign formList=root.dataSource.formList/>
<#assign tabsList=root.dataSource.tabsList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>
<style type="text/css">
<@fieldftl.initFormCssOrJavascript formList=formList eventKey="addCssStyle"/>
</style>
</head>
<body class="easyui-layout" fit="true" id="layout_body">
<#list formList as form>
	      <#if form.isMaster>
	       <#if form.isListType>
	       <c:if test="${r"${param.notShowQuery!='1'}"}">
     		<div region="west" title="<s:text name='system.search.title'/>" border="false"
                 split="true" style="width:215px;padding:0px;"
                 iconCls="icon-search" tools="#pl_tt" >
                		<@fieldftl.initFormTree form=form/>
			            <form action="${r"${ctx}"}/gs/gs-mng!queryFormList.action?entityName=${entityName}"
	                      name="queryForm" id="queryForm">
						   <input type="hidden" name="filter_EQL_id" value="${r"${param.id}"}"/>
						   <div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
		                     	<@fieldftl.getQueryView form=form isForm=true />
	                       </div>
	                       <div style="text-align:center;padding:8px 8px;">
						             <button type="button" id="bt_query" class="button_small">
									     <s:text name="system.search.button.title"/>
									 </button>&nbsp;&nbsp;
									 <button type="button" id="bt_reset" class="button_small">
									     <s:text name="system.search.reset.title"/>
									 </button>
						   </div>
	                </form>
            </div>
           </c:if> 
		</#if> 
    </#if>    
</#list>

<#list formList as form>
	      <#if form.isMaster && !form.isListType >
	       <#if ((form.referEntityTableName)!"")!="">
<c:choose>
	<c:when test="${r"${param.isEdit=='10'}"}"> <#-- 只读查看隐藏按钮 -->
	</c:when>	
	<c:otherwise>
<div region="north" style="overflow:hidden;" border="false" id="toolbar_layout">
           <div class="datagrid-toolbar">
           		<c:if test="${r"${param.isEdit!='2'}"}"> <#-- 主表只读，不可修改 -->
           		<a id="tedit" href="javascript:void(0);" onclick="doSetFormToEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-edit"><s:text name="system.button.modify.title"/></a>
				<a id="tcancel" href="javascript:void(0);" onclick="doCancelEdit()" class="easyui-linkbutton" plain="true" iconCls="icon-undo"><s:text name="system.button.cancel.title"/></a>
                <a id="tsave" href="javascript:void(0);" onclick="doSaveObj()" class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/></a>
		 		</c:if>
		 		<a id="tattach" href="javascript:void(0);" onclick="doAccessory('atmId')" class="easyui-linkbutton" plain="true" iconCls="icon-attach"><s:text name="system.button.accessory.title"/></a>
		 		<@fieldftl.initButtonOnViewPage entityName=entityName moduleKey=moduleKey formList=formList isDisplayReport=false isReportOnly=false />
		 </div>
</div>
	</c:otherwise>
</c:choose>  
		 <#else>
		 	<@fieldftl.initButtonOnViewPage entityName=entityName moduleKey=moduleKey formList=formList isDisplayReport=false isReportOnly=false />
		</#if> 
    </#if>    
</#list>				

<#list formList as form>
	      <#if form.isMaster && !form.isListType>
					<div region="center" border="false"
						style="+position:relative;overflow:auto;" class="module-bg">
						<form action="" name="viewForm" id="viewForm" method="post">
							<input type="hidden" id="id" name="id"/>
 							<input type="hidden" id="atmId" name="atmId" />
 							<input type="hidden" id="status" name="status" value="10" />
                        	 <#list formList as form>
                      		<#if form.isMaster>
                      			<div style="display:none;">
                      			<#list form.fieldList as field>
                      				<#if !field.editProperties.visible>
                      					<@fieldftl.getFiledView field=field form=form/>
                      				</#if>
                      			</#list>
                      			</div>
                      		</#if>
                      </#list>  
                            <#--分tab页面 -->    
                            <#list tabsList as tabs>
                               <#assign tabList= tabs.tabList>
                               <#if tabList?size gt 0>
	                               <#if tabs_index gt 0>
	                               <div style="height:10px;"></div>
	                               </#if>
                               <div id="form_tabs_${tabs.rows}" class="easyui-tabs">
                               <#list tabList as tab >
                                     <#list formList as form>
	                                        <#if form.key==tab.form>
		                                         <#if form.isMaster && !form.isListType><#--主表分tab页面-->
									                 <div title="<s:text name="${tab.i18nKey!tab.caption}"/>" style="padding:0;overflow:auto;">
									                <#if tab.layout == "absolute"><#-- 绝对布局 -->
		                                         	  <div class="module_div" style="height:${(tab.height)!200}px">
		                                         	  	<#assign fields=form.fieldList>
		                                         	  	<#list fields as field>
														   <#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>
														   	<#if field.tabRows==tab.rows && field.tabCols==tab.cols>
															   	<#if field.key?? && field.key!="">
															   	    <#if ((field.editProperties.visibleCondition)!"")!="">
														          		<s:if test='#session.USER.${field.editProperties.visibleCondition}'>
														      		</#if>   
															   		<div style="position:absolute;left:${((field.editProperties.left)!10)?c}px;top:${((field.editProperties.top)!10)?c}px;">
																   		<span class="module_th">
																   		<#if field.caption?? && field.caption!="">	
													                        <label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
													                    <#else>
													                        &nbsp;
													                    </#if>
													                    </span>
													                    <div class="module_td">
												                    	<@fieldftl.getFiledView field=field form=form/>
												                    	</div>
												                    </div>
												                      <#if ((field.editProperties.visibleCondition)!"")!="">
														          		</s:if>
														      		</#if>   
												                </#if>
												             </#if>
														   </#if>
														</#list>
		                                         	  </div>
		                                         	<#elseif tab.layout == "excel"><#-- excel布局 -->
		                                         	  <div style="padding-top:${tab.content.paddingTop}px;padding-bottom:${tab.content.paddingBottom}px;padding-left:${tab.content.paddingLeft}px;padding-right:${tab.content.paddingRight}px">
		                                         	  	<#assign fields=form.fieldList>
		                                         	  	<#assign html=tab.content.html>
		                                         	  	<#list fields as field>
		                                         	  		<#if field.key?? && field.key!="" && !(field.isCaption?? && field.isCaption)>
		                                         	  			<#assign control><@fieldftl.getFiledView field=field form=form/></#assign>
		                                         	  			<#assign editType = field.editProperties.editType/>
		                                         	  			<#assign myfieldkey = field.key/>
		                                         	  			<#assign html = freeMarkerExcel(html,control,editType,myfieldkey)/>
		                                         	  		</#if>
		                                         	  	</#list>
		                                         	  	${html}
		                                         	  </div>
		                                         	<#else><#-- 表格布局 -->
									                  <table border="0" cellpadding="0" cellspacing="0"  class="module_form">
												                       	<#assign colCount=1>
												                       	<#assign rowCount=0>
																		<#assign fields=form.fieldList>
																		<#assign columns=tab.tableCols!(form.cols!3)>
																		<#assign column=columns>
																		<#assign rows=newWritableNumberArray(fields?size)><#--每一行被占的列数-->
														               <#list fields as field>
														                <#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>														                 
											                                 <#if field.tabRows==tab.rows && field.tabCols==tab.cols>
																			    <#-- 一行首列 -->
																			    <#if colCount == 1>
																			    	<tr>
																			    	<#assign column=column-rows[rowCount]>
																			    </#if>
																			    <#assign oldColumn=column>
																			    <#assign column=column-field.editProperties.cols>
											                                    <#if column gte 0>
											                                    	 <#assign rowspan=field.editProperties.rows>
											                                    	 <#assign colspan=field.editProperties.cols*2-1>
											                                         <#-- 判断行是不是满了-->
											                                       <#if field.key?? && field.key!="">
											                                         <#if field.editProperties.editType != 'Label' && field.editProperties.editType != 'BrowserBox'>
											                                         <th rowspan="${rowspan}">
											                                            <#if ((field.editProperties.visibleCondition)!"")!="">
														          							<s:if test='#session.USER.${field.editProperties.visibleCondition}'>
														      							</#if>   
											                                         		<#if field.caption?? && field.caption!="">	
											                                         			<label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
											                                                <#else>
											                                                	&nbsp;
											                                                </#if> 
											                                           
														          						  <#if ((field.editProperties.visibleCondition)!"")!="">
														          							</s:if>
														      							</#if>   
											                                         </th>
											                                         <td rowspan="${rowspan}" colspan="${colspan}">
											                                      <#if ((field.editProperties.visibleCondition)!"")!="">
														          							<s:if test='#session.USER.${field.editProperties.visibleCondition}'>
														      							</#if>  
											                                                   <@fieldftl.getFiledView field=field form=form/>
											                                     <#if ((field.editProperties.visibleCondition)!"")!="">
														          							</s:if>
														      							</#if>    
											                                         </td>
											                                         <#else>
											                                         <td rowspan="${rowspan}" colspan="${colspan+1}">
													                                      <#if ((field.editProperties.visibleCondition)!"")!="">
																          							<s:if test='#session.USER.${field.editProperties.visibleCondition}'>
																      							</#if>  
													                                      <@fieldftl.getFiledView field=field form=form/>
													                                      <#if ((field.editProperties.visibleCondition)!"")!="">
																          							</s:if>
																      							</#if>    
											                                         </td>
											                                         </#if>
											                                       <#else>
											                                       	 <th rowspan="${rowspan}">&nbsp;</th>
											                                         <td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
											                                       </#if>
												                                         <#if rowspan gt 1>
													                                         <#assign rowStart = rowCount+1>
													                                         <#assign rowEnd = rowCount+rowspan-1>
													                                         <#list rowStart..rowEnd as i>
													                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
													                                         </#list>
													                                      </#if>
											                                      	 <#if column==0>
											                                      	 	<#-- 一行中最后一列 -->
											                                            </tr>
											                                            <#assign rowCount=rowCount+1>
											                                            <#assign colCount=1>
											                                            <#assign column=columns>
											                                         <#else>
											                                         	<#assign colCount=colCount + 1>
											                                         </#if>
											                                     <#else>
												                                     	<th>&nbsp;</th>
														                                <td colspan="${oldColumn*2-1}">&nbsp;</td>
														                                </tr>
														                                <#assign rowCount=rowCount+1>
											                                            <#assign column=columns>
											                                            <#assign column=column-field.editProperties.cols-rows[rowCount]>
														                                <tr>
														                                <#if column gte 0>
														                                 <#assign rowspan=field.editProperties.rows>
												                                    	 <#assign colspan=field.editProperties.cols*2-1>
												                                         <#-- 判断行是不是满了-->
												                                       <#if field.key?? && field.key!="">
												                                       	 <#if field.editProperties.editType != 'Label' && field.editProperties.editType != 'BrowserBox'>
												                                         <th rowspan="${rowspan}">
													                                       <#if ((field.editProperties.visibleCondition)!"")!="">
															          							<s:if test='#session.USER.${field.editProperties.visibleCondition}'>
															      							</#if>  
													                                         	<#if field.caption?? && field.caption!="">	
												                                         			<label for="${field.key}"><#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/>:</label>
												                                                <#else>
												                                                	&nbsp;
												                                                </#if> 
												                                        <#if ((field.editProperties.visibleCondition)!"")!="">
															          							</s:if>
															      							</#if>  
													                                      </th>
													                                      <td rowspan="${rowspan}" colspan="${colspan}">
													                                     <#if ((field.editProperties.visibleCondition)!"")!="">
															          							<s:if test='#session.USER.${field.editProperties.visibleCondition}'>
															      							</#if>  
													                                                   <@fieldftl.getFiledView field=field form=form/>
													                                      <#if ((field.editProperties.visibleCondition)!"")!="">
															          							</s:if>
															      							</#if>  
													                                      </td>
													                                     <#else>
													                                      <td rowspan="${rowspan}" colspan="${colspan+1}">
													                                     	<#if ((field.editProperties.visibleCondition)!"")!="">
															          							<s:if test='#session.USER.${field.editProperties.visibleCondition}'>
															      							</#if>  
													                                        <@fieldftl.getFiledView field=field form=form/>
													                                        <#if ((field.editProperties.visibleCondition)!"")!="">
															          							</s:if>
															      							</#if>  
													                                      </td>
													                                      </#if>
												                                        <#else>
												                                        	<th rowspan="${rowspan}">&nbsp;</th>
												                                         	<td rowspan="${rowspan}" colspan="${colspan}">&nbsp;</td>
												                                        </#if>
												                                         <#if rowspan gt 1>
													                                         <#assign rowStart = rowCount+1>
													                                         <#assign rowEnd = rowCount+rowspan-1>
													                                         <#list rowStart..rowEnd as i>
													                                         	<@modifyArrayDirective seq=rows index=i value=rows[i]+colspan />
													                                         </#list>
													                                     </#if>
														                                 <#if column==0>
												                                      	 	<#-- 一行中最后一列 -->
												                                            </tr>
												                                               <#assign rowCount=rowCount+1>
													                                            <#assign colCount=1>
													                                            <#assign column=columns>
												                                         </#if>
												                                      </#if>
		   																	 </#if>
		   																  </#if>
		   																
		   																 </#if>
										                                </#list>
										                                <#if column != columns>
										                                	<th>&nbsp;</th>
											                                <td colspan="${column*2-1}">&nbsp;</td>
											                                </tr>
										                                </#if>
		                                               </table>
		                                               </#if>
		                                             </div>
		                                        <#else>
		                                        <#-- 子表  -->
		                                        <#if ((form.visibleCondition)!"")!="">
														<s:if test='#session.USER.${form.visibleCondition}'>
													</#if>        
		                                               <div id="dv_tab_${form.key}" title="<s:text name="${tab.i18nKey!tab.caption}"/>" closable="false">
									                        <table id="${form.key}" >
									                        </table>
									                   </div>
									                   <#if ((form.visibleCondition)!"")!="">
														</s:if >
														</#if> 
		                                        </#if>
		                                   </#if>
		                              </#list>
		                        </#list>
		                 </div>	
		                 </#if>
		           	</#list> 		       

					</form>
				</div>
				<#elseif form.isMaster && form.isListType>	
				<div region="center" border="false" style="overflow:auto;" >
					<div class="easyui-layout" fit="true">
				    	<@fieldftl.initFormTabQuery form=form isInitTab=true/>
						<div region="center" border="false">
						<table id="${form.key}" border="false" fit="true"></table>
						</div>
						<#if form.enablePageSelect ><#-- 启用分页选择模式 -->
	            		<div region="south" border="false" style="height:200px;" split="true" >
	            			<table id="pageSelectList" border="false" fit="true"></table>
	            		</div>
	            		</#if>
					</div>
				<#-- 
				<#list tabsList as tabs>
                    <#assign tabList= tabs.tabList>
                        <#if tabList?size gt 0>
	                        <#if tabs_index gt 0>
	                            <div style="height:10px;"></div>
	                        </#if>                 
                               <#list tabList as tab >
                                     <#list formList as form>
	                                        <#if form.key==tab.form>		                                              
									                        <table id="${form.key}" fit="true">
									                        </table>									                   
									         </#if>
									  </#list>
								</#list>
								
						</#if>
				</#list>-->
		                 </div>	
				 </#if>
		     </#list> 
         	<div id="mm" class="easyui-menu" style="width:140px;">
				<div id="stInsert" onclick="doInsert()"><s:text name="system.button.insert.title"/></div>
				<div id="stCopy" onclick="doCopy()"><s:text name="system.button.copy.title"/></div>
				<div id="stCopyAfter" onclick="doCopy(true)"><s:text name="复制到下一行"/></div>
				<div id="stMoveTo" onclick="doMoveTo()"><s:text name="移动至第几行之前"/></div>
				<div id="stMoveUp" onclick="doMoveUp()"><s:text name="system.button.moveUp.title"/></div>
				<div id="stMoveDown" onclick="doMoveDown()"><s:text name="system.button.moveDown.title"/></div>
				<div id="stDelete" onclick="doDelete()"><s:text name="system.button.delete.title"/></div>
			</div>
			<div id="mm_treeGrid" class="easyui-menu" style="width:120px;">
				<div id="stInsert_tree" onclick="doTreeInsert()"><s:text name="system.button.insert.title"/></div>
				<div id="stMoveUp_tree" onclick="doTreeMoveUp()"><s:text name="system.button.moveUp.title"/></div>
				<div id="stMoveDown_tree" onclick="doTreeMoveDown()"><s:text name="system.button.moveDown.title"/></div>
				<div id="stDelete_tree" onclick="doTreeDelete()"><s:text name="system.button.delete.title"/></div>
			</div>
			<div id="psl_mm" class="easyui-menu" style="width:120px;">
				<div iconCls="icon-remove" onclick="doDeleteOnPageSelectList()"><s:text name="移除所选数据"/></div>
			</div>
			<div id="psl_mm_treeGrid" class="easyui-menu" style="width:120px;">
				<div iconCls="icon-remove" onclick="doTreeDeleteOnPageSelectList()"><s:text name="移除"/></div>
			</div>
			<@fieldftl.initButtonMenuOnViewPage formList=formList />
		<div style="display:none;">	
		<form method="post" name="hidFrm" id="hidFrm" >
			<textarea style="display:none;" name="hid_queryParams" id="hid_queryParams">${r"${param.queryParams}"}</textarea><#-- 查询参数 -->
			<textarea style="display:none;" name="hid_addParams" id="hid_addParams">${r"${param.addParams}"}</textarea><#-- 新增参数 -->
		</form>
		</div>
			<div id="pl_tt">
				<a href="#" id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>" style="display:none;"></a>
				<a href="#" id="a_exp_clp" class="accordion-expand" title="<s:text name="system.button.expand.title"/>"></a>
			</div>
	 <%@ include file="/common/meta-js.jsp" %>		
	 <script type="text/javascript" src="${r"${ctx}"}/js/easyui-1.4/easyui.xpPanel.js"></script>			
	 <script type="text/javascript">  
   //定义一个全局变量保存操作状态
    var operMethod="${r"${param.operMethod}"}" || "view";
    var _loadSubCount_ = 0;
	var _subTabCount_ = 0;
	var _wait_ = null;
    
	<#list formList as form><#-- 用于将隐藏子表数量去掉 -->
		<#if !form.isMaster>
			<#if form.visible>
	_subTabCount_ ++ ;
			</#if>
		</#if>
	</#list>

       <#list formList as form>
	      <#if form.isMaster>
	      	<#if form.extension?? && form.extension.editPage??>
	      		<#assign eventList=form.extension.editPage.eventList />
	      		<@fieldftl.initFormEvent eventList=eventList/>
	      	</#if>          
	      </#if>    
		</#list>
	<#list formList as form>
	      <#if form.isMaster && form.isListType>	
	    function doResizeDataGrid() {
            $("#${form.key}").datagrid("resize");
            $("#${form.key}").datagrid("fixColumnSize","ck");
        }  
		//获取所有数据
		function _getAllFilterData_() {
			var list = $("#${form.key}").datagrid("getRows");
			list = $.extend(true, [], list);
			return list;
		}
		//获取所有选中的数据
		function _getSelectFilterData_() {
			var list = $("#${form.key}").datagrid("getChecked");
			list = $.extend(true, [], list);
			return list;
		}
		//获取所有未选中的数据
		function _getUnSelectFilterData_() {
			var allList = _getAllFilterData_();
			var selList = _getSelectFilterData_();
			var unSelList = [];
			if(allList && allList.length) {
				if(selList && selList.length) {
					for(var i=0,len=selList.length;i<len;i++) {
						var selObj = selList[i];
						for(var j=0,jLen=allList.length;j<jLen;j++) {
							var allObj = allList[j];
							if(JSON.compObj(allObj, selObj)) {
								allList.splice(j, 1);
								break;
							}
						}
					}
				} 
				unSelList = allList;
			}
			return unSelList;
		}
		   </#if>    
		</#list>
		
	//初始化公式代码及汇总统计
	function initExpression() {
	   <#list formList as form>
		         <#assign fields=form.fieldList!>
	                <#if fields??&&fields?size gt 0>
	                    <#list fields as field>
	                    	<@fieldftl.initFieldExpression field=field formKey=form.key isMaster=(form.isMaster && !form.isListType) />
	                    </#list>
	               </#if>
	        <#if (form.isMaster && form.isListType) || !form.isMaster>    
	            <@fieldftl.initStatistics form=form isInit="true"/>
				<#-- <@fieldftl.initStatistics form=form isInit="false"/> -->
			</#if>	
	   </#list>
	}
	function initEvent() {
		<#list formList as form>
			<#if form.isMaster>
		         <#assign fields=form.fieldList!>
	                <#if fields??&&fields?size gt 0>
	                    <#list fields as field>
	                    	<@fieldftl.initFieldEvent field=field/>
	                    </#list>
	               </#if>
	        </#if>       
	   </#list>                     
	}                 

       	function comboboxInit(){
			   <#list formList as form>
			     <#if form.isMaster>
			         <#assign fields=form.fieldList!>
		                <#if fields??&&fields?size gt 0>
		                  <#list fields as field>
			                <@fieldftl.initControlsData field=field type="query" prefix="" entityName=form.entityName formKey=form.key moduleTitle=i18nKey!titleName />
		             	  </#list>
		          		</#if>
		          	<@fieldftl.initDynamicProp form = form />
		        </#if>
		     </#list>
       }
       	<#--子表初始化相关combo -->
	function initCombo(){
		   <#list formList as form>
		     <#if !form.isMaster>
		         <#assign fields=form.fieldList!>
	                <#if fields??&&fields?size gt 0>
	                    <#list fields as field>
		                  <@fieldftl.initControlsData field=field type="subQuery" prefix=("sub_"+form.key+"_") entityName=form.entityName formKey=form.key />
	             		</#list>
	          		</#if>
	        </#if>        
	     </#list>
  
  }
   function initRichTextBox() {
	  	var config = {
					/*toolbar:
					[
						['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
						['UIColor', 'Preview']
					],
					customConfig : 'config_en.js'*/
		};
		$('.jquery_ckeditor').ckeditor(config);
	  }
	   function initSubGridHotKey(){
     <#--list formList as form>
	            <#if !form.isMaster>
	                    $('#dv_tab_${form.key} input').live('keydown', 'return',function (evt){ evt.preventDefault();$("#${form.key}").edatagrid('addRow'); return false; });
	            </#if>    
	    </#list-->
      }
//将控件设置成可编辑状态
	    function doSetFormToEdit(){
			hideTool();
    	  	$("#tsave").show();
    	  	$('#tcancel').show();
	    	operMethod="modify";
	    	doEnable();
           	autoRow();
            //focusFirstElement();
	    }

	function doSetFormFieldReadOnly() {
		<#list formList as form>
	       <#if form.isMaster>
	       	<@fieldftl.setFormFieldReadOnly form=form />	      
	       </#if>    
	    </#list>
	}
	
	function doEnable() {
	    $("div.file_upload_button").show();
		//$("input[type!=hidden],textarea,select,button").removeAttr("disabled");
		$("button,input.Idate").removeAttr("disabled");
		$("textarea").removeAttr("readonly");
		$("input.combo-f,select.combo-f").combo('readonly',false);
		$("input.easyui-numberbox").numberbox('readonly',false);
		$("input.easyui-textbox").numberbox('readonly',false);
		$("input.easyui-processbarSpinner").processbarSpinner('enable');
		//$("input.textbox-f").textbox('enable');
		//$('input.Idate').addClass("Wdate");
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(false);
			} catch(e) {
				$(this).ckeditorGet().setReadOnly(false);
			}
		});
		//hide start process
		$('#tprocess').hide();
		$("a.bt-extra").linkbutton('enable');
		jwpf.enableLinkButton($("a.hyperlink").not(".validInView"));
		$("input[type=text].validInView").removeAttr("readonly");
		<#--设置该只读的字段只读-->
		doSetFormFieldReadOnly();
		<#list formList as form>
	            <#if !form.isMaster && !((form.isVirtual)!false)>
	                   $("#${form.key}").edatagrid('enableEditing');
	            </#if>    
	    </#list>
	}
	
	//将页面上的控件置为不可编辑状态
	function doDisabled() {
	    $("div.file_upload_button").hide();
		//$("input[type!=hidden],textarea,select,button").attr("disabled", "disabled");
		$("button,input.Idate").attr("disabled", "disabled");
		$("textarea").attr("readonly","readonly");
		$("input.combo-f,select.combo-f").combo('readonly',true);
		$("input.easyui-numberbox").numberbox('readonly',true);
		$("input.easyui-textbox").numberbox('readonly',true);
		$("input.easyui-processbarSpinner").processbarSpinner('disable');
		//$("input.textbox-f").textbox('disable');
		//$('input.Idate').removeClass("Wdate");
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setReadOnly(true);
			} catch(e) {
				$(this).ckeditorGet().setReadOnly(true);
			}
		});
		$("a.bt-extra").linkbutton('disable');
		jwpf.disableLinkButton($("a.hyperlink").not(".validInView"));
		$("button.validInView").removeAttr("disabled");<#-- 查看状态下可操作开启 -->
		$("input[type=text].validInView").removeAttr("disabled").attr("readonly","readonly");
		<#list formList as form>
	            <#if !form.isMaster && !((form.isVirtual)!false)>
	                   $("#${form.key}").edatagrid('disableEditing');
	            </#if>    
	    </#list>
	}
	
	function doDisableSubDatagrid(tableKey) {
		if(operMethod=="view") {
			var subTabId = "#dv_tab_" + tableKey;
			$("input[type!=hidden]:not(:checkbox),textarea,select,button", $(subTabId)).attr("disabled", "disabled");
			$("select.pagination-page-list").removeAttr("disabled");
			$("input:checkbox", $(subTabId)).removeAttr("disabled");
			$("button.validInView", $(subTabId)).removeAttr("disabled");<#-- 查看状态下可操作开启 -->
			jwpf.disableLinkButton($("a.hyperlink", $(subTabId)).not(".validInView"));
		}
	}

	    //初始化一个可编辑行
       function autoRow(){
        <#list formList as form>
	            <#if !form.isMaster  && !((form.showTools)!false) >
	                $("#${form.key}").edatagrid('addRow',false);
	            </#if>    
	    </#list>
      }
      	//设置焦点
		function focusFirstElement() {
			if(operMethod != "view") {
	  			jwpf.focusFirstInputOnFormTabs("form_tabs_1");
	  		}
		}	
 	//隐藏工具条
      function hideTool(){
    	  $("div.datagrid-toolbar a", "#toolbar_layout").hide();
      }
	  
	  function showSave(){
    	  $("#tsave").show();
      }
	  
        function hideSave(){
    	  $('#tedit').show();
    	  $('#tattach').show();
      }

   <#list formList as form>
	     <#if form.isMaster>
	     	 <#if ((form.referEntityTableName)!"")!="">
	  //页面数据初始化
   function doModify(id, callFunc, notShowProgress) {
       	var options = {
             url : '${r"${ctx}"}/gs/gs-mng!queryEntity.action?entityName=${form.referEntityTableName}',
             data : {
                 "id" : id
             },
             success : function(data) {
                 doInitForm(data, callFunc);
             }
        };
        fnFormAjaxWithJson(options, notShowProgress || false);  
           }
              	</#if> 
    	</#if>    
  </#list>

   
   	function doPrepareForm(jsonData) {
			<#list formList as form>
			     <#if form.isMaster>
			     	<@fieldftl.prepareForm form=form />
			     <#else>
			     	<@fieldftl.prepareSubForm form=form />
			     </#if>
			</#list>
		}
   	function doInitForm(data, callFunc) {
			if (data.msg) {
                  var jsonData = data.msg;
                  doPrepareForm(jsonData);
                  $('#viewForm').form('load',jsonData);
                  if(window.onAfterLoadData) {
                  	window.onAfterLoadData(jsonData);
                  }
                  <@fieldftl.initDynamicShowSubTable formList=formList isAdd=false />
                  if(operMethod == "view") {
					<#-- initAutoIdFieldValue(true);查看时，去除自动编号提示文本 -->
                  	//setViewTitle();
                  }
                  if(operMethod == "edit") {
                  	clearAddField();
                  }
            }
            if(callFunc) {
            	callFunc(data.msg);
            }
		}
		function clearAddField() {
			$("#id").val("");
			$("#atmId").val("");
			$("#flowInsId").val("");
			$("#status").val("10");
			clearAutoIdFieldValue();
		}	
			//添加附件
    function doAccessory(key, stat) {	
     <#list formList as form>
	     <#if form.isMaster>
	     	 <#if ((form.referEntityTableName)!"")!="">
		var attachId=$("#"+key).val();
		var id=$("#id").val();
		var opts ={"attachId":attachId,
   				 "id":id,
   				 "key":key,
   				 "entityName":"${form.referEntityTableName}",
   				 "dgId":"queryList",
   				 "flag":"mainEdit",
   				 "readOnly" : ""
   		};
		setAttachOptReadonly(opts, stat);
       	doAttach(opts);
       	             </#if> 
		      </#if>    
			</#list>

    }
    
        //列表添加附件
    function doAccessoryOnList(fieldVal, fieldKey, tableKey, rowIndex, entityName, stat) {
		var opts ={"attachId":fieldVal,
   				 "id":rowIndex,
   				 "key":fieldKey,
   				 "entityName":entityName||"",
   				 "dgId":tableKey,
   				 "flag":"subQuery",
   				 "readOnly" : false
   		};
		setAttachOptReadonly(opts, stat);
		doAttach(opts);
    }
    
    function setAttachOptReadonly(opts, stat) {
    		<c:choose>
			<c:when test="${r"${param.isEdit=='1'}"}">
				opts.readOnly = false;
			</c:when>
			<c:when test="${r"${param.isEdit=='2'}"}">
				opts.readOnly = true;
			</c:when>
			<c:otherwise>
				var status = stat || $("#status").val();
				if(status) {
					if(status == "10") {
						opts.readOnly = false; 	 	
			    	} else {
			    		opts.readOnly = true;
			    	}
				}
		   	</c:otherwise>
		    </c:choose>
    }
    
     //更新附件ID flag: mainQuery-主实体查询，mainEdit-主实体编辑，subQuery-子实体查询，subEdit-子实体编辑
    function doUpdateAttach(opts) {
        if(opts.flag == "subQuery") {
        	var rowIndex = parseInt(opts.id);
        	var tableKey = opts.dgId;
        	var row = getObject(rowIndex, tableKey);
        	if(row) {
        		if(operMethod == "view") {<#-- 查看状态下上传附件 -->
					if(row[opts.key] != opts.attachId) {
						var options = {
			    			url : '${r"${ctx}"}/gs/gs-mng!updateAttach.action',
			                data : {
			                	"entityName":opts.entityName,
			                    "id" : row["id"],
			                    "attachId": opts.attachId,
			                    "field" : opts.key
			                },
							async: false,
							success : function(data) {
								if(data.msg) {
									$("#" + tableKey).edatagrid("load");//刷新子表
								}
							}
						};
						fnFormAjaxWithJson(options);
					}
				} else {
        			row[opts.key] = opts.attachId;
        			$("#"+tableKey).edatagrid("refreshRow", rowIndex);
        		}
        	}
        } else {
        	if(opts.id) { <#-- 非新增操作时 -->
		        var options = {
		    			url : '${r"${ctx}"}/gs/gs-mng!updateAttach.action',
		                data : {
		                	"entityName":opts.entityName,
		                    "id" : opts.id,
		                    "attachId": opts.attachId,
		                    "field" : opts.key
		                },
						async: false,
						success : function(data) {
							if(data.msg) {
								$("#"+opts.key).val(opts.attachId);
								if(opts.key=="atmId"){		
		        					doRefreshModuleData();
		        				}
							}
						}
				};
				fnFormAjaxWithJson(options);
			} else {
				$("#"+opts.key).val(opts.attachId);
			}
        }
    }
    
    function doRefreshModuleData() {
		if (top.refreshTabData) {
			top.refreshTabData("<s:text name="${i18nKey!titleName}"/>");
		}
	}
    	//取消修改
	function doCancelEdit() {
		hideTool();
		hideSave();
		var id = jwpf.getId();
		formClear();
		operMethod="view";
		doModify(id, doDisabled);
	}
		function formClear() {
		$('#viewForm').form('clear');
		$(".jquery_ckeditor").each(function() {
			try {
				$(this).ckeditorGet().setData("");
			} catch(e) {
				$(this).ckeditorGet().setData("");
			}
		});
		$("img.previewImageCls").attr("src", "");
	}
	function cSaveObj(childTableIndex) {
		var updateRows = $('#' + childTableIndex).edatagrid('saveRow');
		if(updateRows) {
			var deleteRows = $('#' + childTableIndex).edatagrid('getChanges',
				'deleted');
			return getChangeRows(updateRows, deleteRows);
		} else {
			return false;
		}
	}
		function cSaveObjTree(childTableIndex) {
		var updateRows = $('#' + childTableIndex).edatagrid('saveRow');
		//var updateRows = $('#' + childTableIndex).edatagrid('getAllNodes');
		if(updateRows) {
			var deleteRows = $('#' + childTableIndex).edatagrid('getDeleteNodes');
			return getChangeRows(updateRows, deleteRows);
		} else {
			return false;
		}
	}
	   function getChangeRows(updateRows, deleteRows) {
   		var changesRows = {
			inserted : [],
			updated : [],
			deleted : []
		};
		if (operMethod == "edit" || operMethod == "add") {
			if (updateRows && updateRows.length > 0) {
				//复制时删除id
				for ( var k = 0; k < updateRows.length; k++) {
					//if (!jwpf.isEmptyRow(updateRows[k])) {
						updateRows[k].id = null;
						changesRows.updated.push(updateRows[k]);
					//}
				}
			}

		} else {
			if (updateRows && updateRows.length > 0) {
				for ( var k = 0; k < updateRows.length; k++) {
					//if (!jwpf.isEmptyRow(updateRows[k])) {
						changesRows.updated.push(updateRows[k]);
					//}
				}
			}
			if (deleteRows && deleteRows.length > 0) {
				for ( var j = 0; j < deleteRows.length; j++) {
					changesRows.deleted.push(deleteRows[j]);
				}
			}
		}
		return changesRows;
   }
   	<#list formList as form>
	    <#if form.isMaster>
	      <#if ((form.referEntityTableName)!"")!="">
	<#-- 生成自动编号参数对象 -->
	function buildAutoIdParam() {
		var params = [];
		var val = "";
		<#list formList as form>
	      <#if form.isMaster>
	      	 <#assign fields=form.fieldList/>
                   <#if fields??&&fields?size gt 0>
                         <#list fields as field>
                              <#assign autoIdRule = (field.editProperties.autoIdRule)!""/>
                              <#if autoIdRule != "">
                                    	try {
                                    		<c:choose>
											<c:when test="${r"${param.isChange=='1'}"}"><#-- 变更修改 -->
											if(operMethod == "edit") {
												val = _autoIdMap_["${field.key}"] + "-";
												params.push({"key":"${field.key}","busVal":val,"bitNum":"3","excludeDate":"1"});		
											} else {
												val = ${autoIdRule};
	                                    		if($.isPlainObject(val)) {
	                                    			params.push({"key":"${field.key}","busVal":val["busVal"],"bitNum":val["bitNum"],"dateFmt":val["dateFmt"],"excludeDate":val["excludeDate"],"dateFirst":val["dateFirst"]});
	                                    		} else {
	                                    			params.push({"key":"${field.key}","busVal":val});
	                                    		}
											}
											</c:when>
											<c:otherwise><#-- 其他情况 -->
											val = ${autoIdRule};
                                    		if($.isPlainObject(val)) {
                                    			params.push({"key":"${field.key}","busVal":val["busVal"],"bitNum":val["bitNum"],"dateFmt":val["dateFmt"],"excludeDate":val["excludeDate"],"dateFirst":val["dateFirst"]});
                                    		} else {
                                    			params.push({"key":"${field.key}","busVal":val});
                                    		}
											</c:otherwise>
											</c:choose>
                                    	} catch(e) {alert("为${field.caption}(${field.key})属性配置自动编号规则时出现异常：" + e);}
                              </#if>
                         </#list>
                  </#if> 
	      </#if>    
		</#list>
		return params;
	}
	
	<#-- 自动编号框提示 
	function initAutoIdFieldValue(isHide) {
		<#list formList as form>
	      <#if form.isMaster>
	      	 <#assign fields=form.fieldList/>
                   <#if fields??&&fields?size gt 0>
                         <#list fields as field>
                              <#assign autoIdRule = (field.editProperties.autoIdRule)!""/>
                              <#if autoIdRule != "">
         $("#${field.key}").span_tip({text:"(<s:text name="system.autoGen.title"/>)", hide: isHide});
                              </#if>
                         </#list>
                  </#if> 
	      </#if>    
		</#list>
	}-->
	<#-- 清除自动编号值 -->
	function clearAutoIdFieldValue() {
		<#list formList as form>
		      <#if form.isMaster>
		      	 <#assign fields=form.fieldList/>
	                   <#if fields??&&fields?size gt 0>
	                         <#list fields as field>
	                              <#assign autoIdRule = (field.editProperties.autoIdRule)!""/>
	                              <#if autoIdRule != "">
	    jwpf.setFormVal("${field.key}", "");
	                              </#if>
	                         </#list>
	                  </#if> 
		      </#if>    
		</#list>
	}
	
	function doSaveData(sd) {
		   var sendData = $.extend({"businessMessage":[]}, sd);
			//key,busVal
		   sendData["autoIdRule"] = buildAutoIdParam();
	   
		   var jsons = formToJSON('viewForm');
		   <#list formList as form>
	            <#if !form.isMaster>
	       if($("#${form.key}").length) {
	            	<#if form.listType == "TreeGrid">
	           var rows = cSaveObjTree("${form.key}");
	            	<#else>
	           var rows = cSaveObj("${form.key}");
	            	</#if>
	           if(rows === false) {return;}<#-- 校验子表必输项 -->
	           var sub = "${form.referEntityTableName}";
	           var lcKey = [];
	           var subKey = sub.split(".")[1];
	           var first = subKey.charAt(0).toLowerCase();
	           var rest = subKey.substring(1,subKey.length);
	           lcKey.push(first);
	           lcKey.push(rest);
	           lcKey.push("s");
	           var key = lcKey.join("");
	           jsons[key] = rows; 	
	       }     
	            </#if>    
	       </#list>
			
			var jsonSendData = JSON.stringify(sendData);
			var jsonSavedata = JSON.stringify(jsons);

			var options = {
				url : '${r"${ctx}"}/gs/gs-mng!save.action?entityName=${form.referEntityTableName}',
				async : false,
				data : {
					"jsonSaveData" : jsonSavedata,
					"jsonSenddata": jsonSendData
				},
	
				success : function(data) {
						operMethod="view";
						<#-- 保存后操作函数判断，用于快速添加数据等操作 -->
						<c:if test="${r"${not empty param.afterSaveFunc}"}">
						if(window.onAfterSave) {
							<#-- jsons["id"] = data.msg;-->
							window.onAfterSave(data.msg);
						}
						var asFunc = eval("parent.${r"${param.afterSaveFunc}"}");
						if(asFunc) {
							asFunc(data.msg.id);
						}
						</c:if>
						formClear();
	                  	$("#id").val(data.msg.id);<#-- 用于加载子表 -->
	                  	doInitForm(data, doAfterSave);
					    <#-- doModify(data.msg, doAfterSave); -->
				}
			};

			fnFormAjaxWithJson(options);
	}
						</#if>  
			</#if>    
	  </#list>
	function doSaveObj() {
	   var isValidate= $("#viewform").form("validate");
	   if(isValidate){
	   	   var sendData = {"businessMessage":[]};
	   	   var bsFlag = true;
		   if(window.onBeforeSave) {
				bsFlag = window.onBeforeSave(sendData);
		   }
		   if(bsFlag === false) {
		   		return false;
		   }
		   doSaveData(sendData);
	   }
	}
	function doAfterSave(data) {
		doRefreshModuleData();
		doDisabled();
		hideTool();
		hideSave();
		if(window.onAfterSave) {
			window.onAfterSave(data);
		}
	}
		<#list formList as form>
        <#if !form.isMaster || form.isListType>
             <#assign methodName='get'+'${form.key}'>
             function ${methodName}(columnListMap){
             	<#assign fields=form.fieldList/>
             	var frozenFields = [[ <#-- 冻结列 -->
	                 <#if fields??&&fields?size gt 0>
         			    <#assign layerend = 0>
         			    <#assign nohasfield = 1>
                        <#list 1..fields?size as layer>
                        	<#assign i=0>
                        	<#assign layerend = 1>
                            <#list fields as field>
                            	<#if field.key?? && field.key != "">
                            	<#if (!field.editProperties.layer?? && layer = 1) || (field.editProperties.layer?? && field.editProperties.layer = layer)>
                                <#if field.queryProperties.showInGrid && ((field.queryProperties.frozenColumn)!false)>
                                  <#if layer gt 1 && i = 0>,[</#if>
                            	  <#assign layerend = 0>
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>     
								  <#if i gt 0>,</#if>
                                	<@fieldftl.initSubQuery field=field prefix=("sub_"+form.key+"_") form=form />                                  
                                  <#assign i=i+1>	
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>    
                                </#if>
                                </#if>
                                </#if>
                            </#list>
                            <#if layerend = 0 && i gt 0>]<#assign nohasfield = 0></#if><#if layerend = 1><#break></#if>
                        </#list>
                        <#if nohasfield = 1>]</#if>
                    </#if>
	                ];
               handleFilterFields(columnListMap, frozenFields, "${form.key}");  
             	var fields = [[
         			<#if fields??&&fields?size gt 0>
         			    <#assign layerend = 0>
         			    <#assign nohasfield = 1>
                        <#list 1..fields?size as layer>
                        	<#assign i=0>
                        	<#assign layerend = 1>
                            <#list fields as field>
                            	<#if field.key?? && field.key != "">
                            	<#if (!field.editProperties.layer?? && layer = 1) || (field.editProperties.layer?? && field.editProperties.layer = layer)>
                                <#if field.queryProperties.showInGrid && !((field.queryProperties.frozenColumn)!false)>
                                  <#if layer gt 1 && i = 0>,[</#if>
                            	  <#assign layerend = 0>
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>     
								  <#if i gt 0>,</#if>
                                	<@fieldftl.initSubQuery field=field prefix=("sub_"+form.key+"_") form=form />                                  
                                  <#assign i=i+1>	
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>    
                                </#if>
                                </#if>
                                </#if>
                            </#list>
                            <#if layerend = 0 && i gt 0>]<#assign nohasfield = 0></#if><#if layerend = 1><#break></#if>
                        </#list>
                        <#if nohasfield = 1>]</#if>
                    </#if>
                ];
               handleFilterFields(columnListMap, fields, "${form.key}");  
		       return {"frozenFields":frozenFields,"fields":fields};
		    }
		    <@fieldftl.initFormFieldDefaultValue form=form isListType=true />
		    <@fieldftl.initFormFieldOnListEvent form=form />
        </#if>
    </#list>
    
    function handleFilterFields(columnListMap, fields, tableKey) {
    			if(columnListMap && columnListMap[tableKey]) {
               		var clms = columnListMap[tableKey];
               		for(var i=0,len=fields.length; i<len; i++) {
               			for(var j=0; j<fields[i].length; j++) {
               				var fld = fields[i][j];
	               			if(fld.field && $.inArray(fld.field, clms) == -1) {
	               				fld["hidden"] = true;
	               			}
               			}
               		}
               }      
    }

    function getSubUrl(entityName, id) {
		return id?("${r"${ctx}"}/gs/gs-mng!querySubList.action?entityName=" + entityName
				+ "&id=" + id):null;
	}
	
	function getFormUrl(entityName, id) {
		return ("${r"${ctx}"}/gs/gs-mng!queryFormList.action?entityName=" + entityName
				+ "&id=" + id);
	}
	
	 function initFormDefaultValue() {
	    	<#list formList as form>
	            <#if form.isMaster>
	                <@fieldftl.initFormFieldDefaultValue form=form />
	            </#if>    
	    	</#list>
	    }
		
	<#-- 初始化新增操作，供外部链接使用 -->  
	function _initAddParam_() {
		var hidParam = $("#hid_addParams").val();
		if(hidParam) {
			try {
				var paramObj = JSON.parse(hidParam);
				if(paramObj) {
					$.each(paramObj, function(k, v) {
						jwpf.setFormVal(k, v);
					});
				}
			}catch(e) {alert("初始化新增参数时出现异常：" + e);}
		}
		if(window.onAfterAddInit) {
			window.onAfterAddInit();
		}
	}
	    
		function _initTimer_() {
		if(_wait_) {
			clearInterval(_wait_);
		}
		
	    _wait_ = setInterval(function(){    
                    if(_loadSubCount_ >= _subTabCount_){    
                        clearInterval(_wait_);    
						setTimeout(initExpression, 0);
						if(operMethod=="add") {
							initFormDefaultValue();
							_initAddParam_();<#-- 初始化新增参数 -->
							autoRow();
							//$("#viewform").form("validate");
							focusFirstElement();
						} else if(operMethod=="edit" || operMethod=="modify"){
							autoRow();
						}
						<#-- if(operMethod=="add" || operMethod=="edit") {
							initAutoIdFieldValue();
						}新增或复制 -->
                    }
       },300);
	}
	<#list formList as form>
	     <#if form.isMaster>
	     	 <#if !form.isListType && formList?size gt 1>
	     	 
	function initEdatagrid(entityName,id, options, columns) {
		var fcs1 =  [{
				field : 'ck',
				checkbox : true,
				rowspan : 1,<#-- 新版easyui不能大于field数组length -->
				colspan : 1
		}];
		var fcs = [];
		for(var i = 0; i < columns.frozenFields.length; i++){
			if(i===0) {
				fcs.push(fcs1.concat(columns.frozenFields[0]));
			} else {
				fcs.push(columns.frozenFields[i]);
			}
		}
		var opt = {
			iconCls : 'icon-edit',
			editing:false,//取消单击编辑
			//height:330,
			nowrap : false,
			striped : true,
			//singleSelect:true,
			//idField:'itemid',
			url : null,
			remoteSort : false,
			sortName : "sortOrderNo",
			rownumbers : true,
			frozenColumns : fcs,
			columns : columns.fields,
			onRowContextMenu : function(e, rowIndex, rowData) {
				//查看状态下没有右键功能
                if(operMethod!="view"){
                	e.preventDefault();
    				$('#mm').data("rowIndex", rowIndex);
    				$('#mm').data('ChildTableIndex', id);
    				$('#mm').menu('show', {
    					left : e.pageX,
    					top : e.pageY - 8
    				});
                }
				
			},
			//双击查看一条记录详细信息
			onDblClickRow : function(rowIndex, rowData) {
				 /*if(operMethod!="view"){
					openDialog(entityName,rowIndex, id);
					$('#' + id).edatagrid('endEdit', rowIndex);
				 }*/
			},
			onLoadError:function() {
    			$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="数据加载异常"/>', 'info');
    		}
		};
		var opts = $.extend(true, {}, opt, options);
		$('#' + id).edatagrid(opts);
	}
	
	function initTreeEdatagrid(entityName,id, options, columns) {
		var fcs1 =  [{
				field : 'ck',
				checkbox : true,
				rowspan : 1,<#-- 新版easyui不能大于field数组length -->
				colspan : 1
		}];
		var fcs = [];
		for(var i = 0; i < columns.frozenFields.length; i++){
			if(i===0) {
				fcs.push(fcs1.concat(columns.frozenFields[0]));
			} else {
				fcs.push(columns.frozenFields[i]);
			}
		}
		var opt = {
			//height:330,
			nowrap : false,
			striped : true,
			tree:true,
			lines:true,
			isShowLevel:true,
			iconCls : 'icon-edit',
			idField:'autoCode',
			url : null,
			remoteSort : false,
			sortName : "sortOrderNo",
			rownumbers : true,
			frozenColumns : fcs,
			columns : columns.fields,
			onContextMenu : function(e, rowData) {
				//查看状态下没有右键功能
                if(operMethod!="view"){
                	e.preventDefault();
    				$('#mm_treeGrid').data("rowIndex", rowData);
    				$('#mm_treeGrid').data('ChildTableIndex', id);
    				$('#mm_treeGrid').menu('show', {
    					left : e.pageX,
    					top : e.pageY - 8
    				});
                }
				
			},
			//双击查看一条记录详细信息
			onDblClickRow : function(rowData) {
				 /*if(operMethod!="view"){
					openDialog(entityName,rowIndex, id);
					$('#' + id).edatagrid('endEdit', rowData);
				 }*/
			},
			onLoadError:function() {
    			$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="数据加载异常"/>', 'info');
    		}
		};
		var opts = $.extend(true, {}, opt, options);
		$('#' + id).edatagrid(opts);
	}
	<#-- //子表最新复制方法
	function doCopy() {
		var rowIndex = $('#mm').data('rowIndex');
		var ChildTableIndex = $('#mm').data('ChildTableIndex');
		var rows = $("#" + ChildTableIndex).edatagrid("getRows");
		var newRow = $.extend({}, rows[rowIndex]);
		newRow.id = null;
		$("#" + ChildTableIndex).edatagrid('addRow', newRow);
		jwpf.doFormExpression(ChildTableIndex);
	}-->
	
	//子表最新复制方法
	function doCopy(isCopyAfter) {
		var rowIndex = $('#mm').data('rowIndex');
		var ChildTableIndex = $('#mm').data('ChildTableIndex');
		var rows = $("#" + ChildTableIndex).edatagrid("saveRow");<#--$("#" + ChildTableIndex).edatagrid("getRows"); -->
		var newRow = $.extend({}, rows[rowIndex]);
		newRow.id = null;
		if(isCopyAfter) {
			var newRowIndex = parseInt(rowIndex)+1;
			$("#" + ChildTableIndex).edatagrid('insertRow', {
				index:newRowIndex, row:newRow
			});
			$('#' + ChildTableIndex).datagrid('selectRow', newRowIndex);
		} else {
			$("#" + ChildTableIndex).edatagrid('addRow', newRow);
		}
		jwpf.doFormExpression(ChildTableIndex);
	}
	
	//移动至哪一行
	function doMoveTo() {
		var ChildTableIndex = $('#mm').data('ChildTableIndex');
		var table_grid = $("#" + ChildTableIndex);
		var rows = table_grid.edatagrid("saveRow");
		var selRows = table_grid.edatagrid("getChecked");
		if(selRows.length == 0) {
			alert("请先选中要移动的记录，支持多行记录同时移动！");
			return false;
		}
		$.messager.prompt('提示信息', '请输入要移动至哪一行之前的行号', function(r){
			if(r && $.isNumeric(r)){
				var newRowIndex = parseInt(r);
				//var rowIndex = parseInt($('#mm').data('rowIndex'));
				if (newRowIndex > 0 && newRowIndex <= rows.length) {
					newRowIndex = newRowIndex - 1;
					var c = 0;
					$('#' + ChildTableIndex).datagrid('uncheckAll');
					$.each(selRows, function(j, row) {
						var rowIndex = rows.indexOf(row);
						if(newRowIndex > rowIndex) {
							for(var i=rowIndex-c; i<newRowIndex-1; i++) {
			    					var currentRow = $.extend({"id":""}, rows[i]);
				    				var lastRow = $.extend({"id":""}, rows[i+1]);
				    				table_grid.edatagrid('updateRow', {
				    					index : i,
				    					row : lastRow
				    				});
				    				table_grid.edatagrid('updateRow', {
				    					index : i+1,
				    					row : currentRow
				    				});
			    			}
			    			$('#' + ChildTableIndex).datagrid('checkRow', newRowIndex-1-c);
			    			c++;
						} else if(newRowIndex < rowIndex) {
							for(var i=rowIndex-c; i>newRowIndex; i--) {
			    					var currentRow = $.extend({"id":""}, rows[i-1]);
				    				var lastRow = $.extend({"id":""}, rows[i]);
				    				table_grid.edatagrid('updateRow', {
				    					index : i-1,
				    					row : lastRow
				    				});
				    				table_grid.edatagrid('updateRow', {
				    					index : i,
				    					row : currentRow
				    				});
			    			}
			    			$('#' + ChildTableIndex).datagrid('checkRow', newRowIndex);
			    			newRowIndex++;
						}
					});
				} else {
					alert("请输入有效行号！");
				}
			} else {
				alert("请输入有效行号！");
			}
		});
	}
	
	//子表中插入一条记录
	function doInsert() {
		var rowIndex = $("#mm").data("rowIndex");
		var ChildTableIndex = $('#mm').data('ChildTableIndex');
		$("#" + ChildTableIndex).edatagrid('insertNewRow', parseInt(rowIndex));
	}
	//插入一个子节点
	function doTreeInsert() {
		var rowData = $("#mm_treeGrid").data("rowIndex");
		var ChildTableIndex = $('#mm_treeGrid').data('ChildTableIndex');
		$("#" + ChildTableIndex).edatagrid('insertNewRow', rowData);
	}

	//上移
	function doMoveUp() {
		var rowIndex = parseInt($('#mm').data('rowIndex'));
		if (rowIndex > 0) {
			var ChildTableIndex = $('#mm').data('ChildTableIndex');
			//var rows = $("#" + ChildTableIndex).edatagrid("getRows");
			var rows = $("#" + ChildTableIndex).edatagrid("saveRow");
			var currentRow = $.extend({"id":""}, rows[rowIndex]);
			var lastRow = $.extend({"id":""}, rows[rowIndex - 1]);
			$('#' + ChildTableIndex).edatagrid('updateRow', {
				index : rowIndex - 1,
				row : currentRow
			});
			$('#' + ChildTableIndex).edatagrid('updateRow', {
				index : rowIndex,
				row : lastRow
			});
		}
		$('#' + ChildTableIndex).datagrid('unselectRow', parseInt(rowIndex));
		$('#' + ChildTableIndex).datagrid('selectRow', parseInt(rowIndex - 1));
	}
	
	function doTreeMoveUp() {
		var rowIndex = $('#mm_treeGrid').data('rowIndex');
		var ChildTableIndex = $('#mm_treeGrid').data('ChildTableIndex');
		var currentCode = rowIndex.autoCode;
		var pCode = rowIndex.autoParentCode;
		var arr = [];
		if(pCode){
			var p =$('#' + ChildTableIndex).treegrid('getParent',currentCode);
			arr = p.children;
		}else{
			arr = $('#' + ChildTableIndex).treegrid('getRoots');
		}
		var len = arr.length;
		for(var i=0;i<len;i++){
			if(arr[i].autoCode==currentCode){
				if(i>0){
					var a = $.extend({},arr[i-1]);
					var b = $.extend({},arr[i]);
					var c1 = a.autoCode;
					var c2 = b.autoCode;
					setTreeNodeCode(b, c1);
					setTreeNodeCode(a, c2);
					$('#' + ChildTableIndex).edatagrid("update",  {
						id: c1,
						row : b,
						update : false
					});
		 			$('#' + ChildTableIndex).edatagrid("update",  {
						id: c2,
						row : a,
						update : false
					});  
					break;
				}
			}
		}  	
	}
	
	function doSetDataGridParam(tableKey) {
		$('#mm').data('ChildTableIndex', tableKey);
		var rows = $('#'+tableKey).edatagrid('getChecked');
		if(rows && rows.length) {
			var index = $('#'+tableKey).edatagrid('getRowIndex', rows[rows.length-1]);
			$('#mm').data('rowIndex', index);
			return true;
		} else {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.recordInfo"/>',
							'info');
			return false;
		}
	}
	
	function doSetTreeGridParam(tableKey) {
		$('#mm_treeGrid').data('ChildTableIndex', tableKey);
		var rows = $('#'+tableKey).edatagrid('getChecked');
		if(rows && rows.length) {
			$('#mm_treeGrid').data('rowIndex', rows[rows.length-1]);
			return true;
		} else {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.recordInfo"/>',
							'info');
			return false;
		}
	}
	
	function setTreeNodeCode(node, code) {
		node.autoCode = code;
		var clds = node.children;
		if(clds && clds.length) {
			for(var i=0,len=clds.length;i<len;i++) {
				clds[i].autoParentCode = code;
				clds[i]._parentId = code;
			}
		}
	}

	//下移
	function doMoveDown() {
		var rowIndex = parseInt($('#mm').data('rowIndex'));
		var ChildTableIndex = $('#mm').data('ChildTableIndex');
		var rows = $("#" + ChildTableIndex).edatagrid("saveRow");
		//var rows = $("#" + ChildTableIndex).edatagrid("getRows");
		if (rowIndex < rows.length - 1) {
			var currentRow = $.extend({"id":""}, rows[rowIndex]);
			var nextRow = $.extend({"id":""}, rows[rowIndex + 1]);
			$('#' + ChildTableIndex).edatagrid('updateRow', {
				index : rowIndex + 1,
				row : currentRow
			});
			$('#' + ChildTableIndex).edatagrid('updateRow', {
				index : rowIndex,
				row : nextRow
			});
		}
		$('#' + ChildTableIndex).datagrid('unselectRow', parseInt(rowIndex));
		$('#' + ChildTableIndex).datagrid('selectRow', parseInt(rowIndex + 1));

	}
	
	function doTreeMoveDown() {
		var rowIndex = $('#mm_treeGrid').data('rowIndex');
		var ChildTableIndex = $('#mm_treeGrid').data('ChildTableIndex');
		var currentCode = rowIndex.autoCode;
		var pCode = rowIndex.autoParentCode;
		var arr = [];
		if(pCode){
			var p =$('#' + ChildTableIndex).treegrid('getParent',currentCode);
			arr = p.children;
		}else{
			arr = $('#' + ChildTableIndex).treegrid('getRoots');
		}
		var len = arr.length;
		for(var i=0;i<len;i++){
			if(arr[i].autoCode==currentCode){
				if(i<len-1){
					var a = $.extend({},arr[i]);
					var b = $.extend({},arr[i+1]);
					var c1 = a.autoCode;
					var c2 = b.autoCode;
					setTreeNodeCode(b, c1);
					setTreeNodeCode(a, c2);
					$('#' + ChildTableIndex).edatagrid("update",  {
						id: c1,
						row : b,
						update : false
					});
		 			$('#' + ChildTableIndex).edatagrid("update",  {
						'id': c2,
						'row' : a,
						update : false
					});  
					break;
				}
			}
		}  		
	}

	//右键批量删除
	function doDelete() {
     	var ChildTableIndex = $('#mm').data('ChildTableIndex');
     	$("#" + ChildTableIndex).edatagrid("saveRow");
		var rows = $('#'+ChildTableIndex).edatagrid('getChecked');
		if (rows.length < 1) {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.question"/>',
							'info');
		} else {
			$.messager.confirm(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.info"/>',
					function(r) {
					if(r){
						$("#"+ChildTableIndex).edatagrid("deleteRows", rows);
						jwpf.doFormExpression(ChildTableIndex);				
					}
			});
		}
	}
	
	//右键批量删除
	function doTreeDelete() {
			$.messager.confirm(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.info"/>',
					function(r) {
					if(r){
						var rowData = $("#mm_treeGrid").data("rowIndex");
						var ChildTableIndex = $('#mm_treeGrid').data('ChildTableIndex');
						$("#" + ChildTableIndex).edatagrid('removeNodes', rowData);
					}
			});
	}
	              </#if> 
    	</#if>    
  </#list>
	function initFormEdatagrid(entityName,id, options, columns, isReadOnly) {
	    var fcs1 =  [{
				field : 'ck',
				checkbox : true,
				rowspan : 1,<#-- 新版easyui不能大于field数组length -->
				colspan : 1
		}];
		var fcs = [];
		for(var i = 0; i < columns.frozenFields.length; i++){
			if(i===0) {
				fcs.push(fcs1.concat(columns.frozenFields[0]));
			} else {
				fcs.push(columns.frozenFields[i]);
			}
		}
		var isEditing = true;
		if(isReadOnly) {
			if(columns.frozenFields && columns.frozenFields.length) {
				for(var i in columns.frozenFields[0]) {
		    		delete columns.frozenFields[0][i]["editor"];
		    	}
			}
			if(columns.fields && columns.fields.length) {
				for(var i in columns.fields[0]) {
		    		delete columns.fields[0][i]["editor"];
		    	}
			}
			isEditing = false;
	    }
		var opt = {
			editing:isEditing,
			width : 'auto',
			height : 'auto',
			nowrap : false,
			striped : true,
			rownumbers : true,
			pagination : true,
			rownumbers : true,
			frozenColumns : fcs,
			remoteSort: true,
			columns : columns.fields,
			fit : true,
			onLoadError:function() {
    			$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="数据加载异常"/>', 'info');
    		}
		};
		var opts = $.extend(true, {}, opt, options);
		$('#' + id).edatagrid(opts);
	}
	
	<#-- 
	function initAddSub(tableList, columnListMap) { 
   <#list formList as form>
        <#if !form.isMaster>
        	<#if form.visible>
        	<@fieldftl.initCheckFormAuth form=form isEnd=false/>
        	if(!tableList || $.inArray("${form.key}", tableList)>= 0) {
             <#assign methodName='get'+'${form.key}'>
             <#if form.listType=="TreeGrid">
             	initTreeEdatagrid("${form.referEntityTableName}","${form.key}", {
					treeField:"${form.treeField}",
					iconCls : 'icon-edit',
					<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
					<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
					defaultRow : ${methodName}DefaultRow()
					,onDblClickRow:function(rowIndex, rowData){
	                    <@fieldftl.initDatagridOnDblClickRow form=form />
	                }
					<@fieldftl.initDatagridParam form=form />
					<@fieldftl.initDatagridToolBar form=form />
				}, ${methodName}(columnListMap));
             <#else>
                initEdatagrid("${form.referEntityTableName}","${form.key}", {
					iconCls : 'icon-edit',
					editing:true,
					<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
					<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
					defaultRow : ${methodName}DefaultRow()
					,onDblClickRow:function(rowIndex, rowData){
	                    <@fieldftl.initDatagridOnDblClickRow form=form />
	                }
					<@fieldftl.initDatagridParam form=form />
					<@fieldftl.initDatagridToolBar form=form />
				}, ${methodName}(columnListMap));
             </#if>
				var dataObj${form.key} = {
					'total' : 0,
					'rows' : []
				};
				$("#${form.key}").edatagrid("loadData", dataObj${form.key});
				$("#${form.key}").edatagrid("clearColumnFilter");
				_loadSubCount_++;
			}
			<@fieldftl.initCheckFormAuth form=form isEnd=true/>
			</#if>	
		</#if>
   </#list>
	  } -->
	
	function initSub(id, tableList, columnListMap) {
	<#list formList as form>
        <#if !form.isMaster >
        	<#if form.visible>
			if(!id){
				id="${r"${param.id}"}";
			}
        	if((!tableList || $.inArray("${form.key}", tableList)>= 0)<#if (form.dynamicShow)!false> && columnListMap</#if>) {
         	<#assign methodName='get'+'${form.key}'>
         	<#if form.listType=="TreeGrid">
         		<#if ((form.visibleCondition)!"")!="">
					<s:if test='#session.USER.${form.visibleCondition}'>
				</#if> 
         	initTreeEdatagrid("${form.referEntityTableName}","${form.key}", {
         		"editing":true,
         		"treeField":"${form.treeField}",
				"url" : getSubUrl("${form.referEntityTableName}", id),
				"remoteSort" : (id?true:false),
				<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
				<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
				defaultRow : ${methodName}DefaultRow(),
				onLoadSuccess :function(data){
					if(id) {
						_loadSubCount_++;
						doDisableSubDatagrid("${form.key}");
						<@fieldftl.initDatagridOnLoadSuccess form=form />
					}
				}
				,onDblClickRow:function(rowIndex, rowData){
                    <@fieldftl.initDatagridOnDblClickRow form=form />
                }
				<@fieldftl.initDatagridParam form=form />
				<@fieldftl.initDatagridToolBar form=form />
			}, ${methodName}(columnListMap));
			  <#if ((form.visibleCondition)!"")!="">
					</s:if>
				</#if> 
         	<#else>
         	 	<#if ((form.visibleCondition)!"")!="">
					<s:if test='#session.USER.${form.visibleCondition}'>
				</#if> 
         	initEdatagrid("${form.referEntityTableName}","${form.key}", {
         		"editing":true,
				"url" : getSubUrl("${form.referEntityTableName}", id),
				"remoteSort" :(id?true:false),
				<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
				<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
				defaultRow : ${methodName}DefaultRow(),
				onLoadSuccess :function(data){
					if(id) {
						_loadSubCount_++;
						doDisableSubDatagrid("${form.key}");
						<@fieldftl.initDatagridOnLoadSuccess form=form />
					}
				}
				,onDblClickRow:function(rowIndex, rowData){
                    <@fieldftl.initDatagridOnDblClickRow form=form />
                }
				<@fieldftl.initDatagridParam form=form />
				<@fieldftl.initDatagridToolBar form=form />
			}, ${methodName}(columnListMap));
			    	<#if ((form.visibleCondition)!"")!="">
					</s:if>
					</#if>
			</#if> 
				if(!id) {
					var dataObj${form.key} = {
						'total' : 0,
						'rows' : []
					};
					$("#${form.key}").edatagrid("loadData", dataObj${form.key});
					$("#${form.key}").edatagrid("clearColumnFilter");
					_loadSubCount_++;
				}
			}	
         	</#if>
        </#if>
    </#list>
	}
	function initForm(id, qp, autoNotQuery) {
		if(!id){
			id="${r"${param.id}"}";
		}
	<#list formList as form>
        <#if form.isMaster && form.isListType>
         <#if form.visible>
         	<#assign methodName='get'+'${form.key}'>
         	<#if form.listType=="TreeGrid">
         	initTreeEdatagrid("${form.entityName}","${form.key}", {
         		"treeField":"${form.treeField}",
				"url" :(autoNotQuery!=="true")?getFormUrl("${form.entityName}", id):null,
				"remoteSort" : true,
				"queryParams":qp,
				pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
				pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
				<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
				<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
				defaultRow : ${methodName}DefaultRow(),
				onLoadSuccess :function(data){
					_loadSubCount_++;
					<@fieldftl.initStatistics form=form isInit="true" tableKey=form.key />
					<@fieldftl.initDatagridOnLoadSuccess form=form />
				}
				,onDblClickRow:function(rowIndex, rowData){
                    <@fieldftl.initDatagridOnDblClickRow form=form />
                }
				<@fieldftl.initDatagridParam form=form />
				<@fieldftl.initFormDatagridToolBar form=form isUserForm=true />
			}, ${methodName}());
         	<#else>
         	initFormEdatagrid("${form.entityName}","${form.key}", {
				"url" :(autoNotQuery!=="true")?getFormUrl("${form.entityName}", id):null,
				"remoteSort" : true,
				"queryParams":qp,
				pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
				pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
				<#if ((form.sortName)!"") != "">sortName:'${form.sortName}',</#if>
				<#if ((form.sortOrder)!"") != "">sortOrder:'${form.sortOrder}',</#if>
				defaultRow : ${methodName}DefaultRow(),
				onLoadSuccess :function(data){
					_loadSubCount_++;
					<@fieldftl.initStatistics form=form isInit="true" tableKey=form.key />
					<@fieldftl.initDatagridOnLoadSuccess form=form />
				}
				,onDblClickRow:function(rowIndex, rowData){
                    <@fieldftl.initDatagridOnDblClickRow form=form />
                }
				<@fieldftl.initDatagridParam form=form />
				<@fieldftl.initFormDatagridToolBar form=form isUserForm=true />
			}, ${methodName}(), ${(form.readOnly)?string});
			</#if> 
         	</#if>
        </#if>
    </#list>
	}
		
	<#-- 初始化分页选择模式列表 -->
	function initPageSelectList() {
	<#list formList as form>
        <#if form.isMaster && form.isListType>
         <#if form.visible && form.enablePageSelect > <#-- 分页选择模式 -->
         	<#assign methodName='get'+'${form.key}'>
         	<#if form.listType=="TreeGrid">
         	initTreeEdatagrid("${form.entityName}","pageSelectList", {
         		"treeField":"${form.treeField}",
				"url" :null,
				onContextMenu : function(e, rowData) {
	                	e.preventDefault();
	    				$('#psl_mm_treeGrid').data("rowIndex", rowData);
	    				$('#psl_mm_treeGrid').data('ChildTableIndex', "pageSelectList");
	    				$('#psl_mm_treeGrid').menu('show', {
	    					left : e.pageX,
	    					top : e.pageY - 8
	    				});
				}
				,onDblClickRow:function(rowIndex, rowData){
                    <@fieldftl.initDatagridOnDblClickRow form=form />
                }
				<@fieldftl.initDatagridParam form=form />
			}, ${methodName}());
         	<#else>
         	initFormEdatagrid("${form.entityName}","pageSelectList", {
				"url" : null,
				onRowContextMenu : function(e, rowIndex, rowData) {
	                	e.preventDefault();
	    				$('#psl_mm').data("rowIndex", rowIndex);
	    				$('#psl_mm').data('ChildTableIndex', "pageSelectList");
	    				$('#psl_mm').menu('show', {
	    					left : e.pageX,
	    					top : e.pageY - 8
	    				});
				},
				onDblClickRow:function(rowIndex, rowData){
                    <@fieldftl.initDatagridOnDblClickRow form=form />
                }
				<@fieldftl.initDatagridParam form=form />
			}, ${methodName}(), true);
			</#if> 
         	</#if>
        </#if>
    </#list>	
	}	
	
		//右键批量删除
	function doDeleteOnPageSelectList() {
     	var ChildTableIndex = $('#psl_mm').data('ChildTableIndex');
		var rows = $('#'+ChildTableIndex).edatagrid('getChecked');
		if (rows.length < 1) {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.operRecord"/>',
							'info');
		} else {
			$("#"+ChildTableIndex).edatagrid("deleteRows", rows);
		}
	}
	
	//右键批量删除
	function doTreeDeleteOnPageSelectList() {
			var rowData = $("#psl_mm_treeGrid").data("rowIndex");
			var ChildTableIndex = $('#psl_mm_treeGrid').data('ChildTableIndex');
			$("#" + ChildTableIndex).edatagrid('removeNodes', rowData);
	}
			
	   //查询
      function doQuery(paramObj, isInit) {
                var param = $("#queryForm").serializeArrayToParam();
                if(paramObj) {
                	$.extend(true, param, paramObj);
                }
                if($("#ul_tab_query").length) {
                	var obj = $("#ul_tab_query").data("queryParam");
                	$.extend(true, param, obj);
                }
				if($("#_commonTypeTree_div_").length) {
                	var obj = $("#_commonTypeTree_div_").data("queryParam");
                	$.extend(true, param, obj);
                }
                var qps = getFormQueryParams();
                var qp = $.extend(true, {}, qps, param);
                var autoNotQuery = "${r"${param.autoNotQuery}"}";
                if(isInit) { <#-- 初始化列表 -->
                	initForm(null, qp, autoNotQuery);
                } else {
                	<#list formList as form>
	     			<#if form.isMaster>
	     			$("#${form.key}").datagrid('clearSelections');
	     			if(autoNotQuery === "true") {
						$("#${form.key}").datagrid('options').url = getFormUrl("${form.entityName}","");
					}
                	$("#${form.key}").edatagrid("load", qp);
                	<#break/>
                	</#if>
                	</#list>
                }
        }
        
        <@fieldftl.initButtonEvent formList=formList />
        
        $(document).ready(function() {	
          	$.ajaxSetup({
			  cache:false
			});
		<#list formList as form>
	     <#if form.isMaster>
	         <#if form.isListType> <#-- 列表显示 -->
                	//initForm();
                	initQueryData();
                	<@fieldftl.initFormTabQuery form=form isInitTab=false jsonPrefix=("sub_"+form.key) />
                	$("#bt_query").click(function() {doQuery();});
                	$("#bt_reset").click(function() {$("#queryForm").form("clear");collapseAll();doQuery();}); 
                	//回车查询
                	doQuseryAction("queryForm");
                    //doQuery(null,true);
                    if($("#_commonTypeTree_div_").length == 0 || !$("#_commonTypeTree_div_").attr("issys")) { <#-- 无系统字典树查询时自动查询，有时，则由字典树触发查询 -->
                		doQuery(null, true);
                	}
                    <c:if test="${r"${param.notShowQuery!='1'}"}">
					setTimeout(function() {$("#layout_body").layout("collapse", "west");},400);
					</c:if>
					
					<#if form.enablePageSelect ><#-- 启用分页选择模式 -->
					initPageSelectList();
					</#if>
					_initTimer_();
	     	 <#else>
	     	     comboboxInit();
	    	     initCombo();
	    	     initRichTextBox();
		     	 <#if ((form.referEntityTableName)!"")!="">	<#-- 关联表单显示 -->
					hideTool();		  		
	      			if(operMethod == "add") {
	                    	//新增
	                    	<@fieldftl.initDynamicShowSubTable formList=formList isAdd=true />
	                        //autoRow();
	                        doSetFormFieldReadOnly();
	                        showSave();
	                } else if(operMethod == "view"){
	                       //查看
	 					   doModify("${r"${param.id}"}", doDisabled, true);
	 					   hideSave();
	                }
	                initSubGridHotKey();
					initEvent();
	                _initTimer_();
	                jwpf.bindKeydownOnForm("viewForm");
	                jwpf.bindHotKeysOnViewPage();
	             <#else> <#-- 自定义表单显示 -->
	             <@fieldftl.initDynamicShowSubTable formList=formList isAdd=true />
				 doSetFormFieldReadOnly();
				 initEvent();
				 initFormDefaultValue();
				 _initAddParam_();<#-- 初始化新增参数 -->
				 jwpf.bindKeydownOnForm("viewForm");
				 </#if>
			</#if>	  
    	</#if>    
  </#list>
            });
            function initQueryData() {
         <#list formList as form>
	     <#if form.isMaster>
				<#assign fields=form.fieldList!>
                <#if fields??&&fields?size gt 0>
                    <#list fields as field>
	                        <#if (field.queryProperties.showInSearch)!false>
		                         <@fieldftl.initControlsData field=field type="query" prefix="query_" entityName=form.entityName/>
	                        </#if>
	                        <#if (field.queryProperties.showInTreeSearch)!false>
	                        	<@fieldftl.initTreeQuery field=field prefix="" entityName=form.entityName dgName=form.key/>
	                        </#if>
	                        <#if (field.queryProperties.showInGrid)!false>
	                           <@fieldftl.initControlsData field=field type="subQuery" prefix=("sub_"+form.key+"_") entityName=form.entityName formKey=form.key />
	                        </#if>
                    </#list>
                </#if>
          </#if>
          </#list>
			}

	function __doListButtonClick(fieldVal, fieldKey, tableKey, rowIndex, callFunc) {
		if(callFunc) {
			callFunc(fieldVal, fieldKey, tableKey, rowIndex);
		}
	}
	
	function __getListButton(fieldVal, fieldKey, tableKey, rowIndex, title, validInView) {
		var astr = [];
		astr.push('<button type="button" class="button_big ' + ((validInView)?"validInView":"") + '"');
		astr.push(' onclick="__doListButtonClick(\'');
		astr.push(fieldVal);
		astr.push('\',\'');
		astr.push(fieldKey);
		astr.push('\',\'');
		astr.push(tableKey);
		astr.push('\',\'');
		astr.push(rowIndex);
		astr.push('\',');
		astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
		astr.push(')"');
		astr.push('>');
		astr.push(title);
		astr.push('</button>');
		return astr.join("");
	}
	
	function __getListHyperLink(fieldVal, fieldKey, tableKey, rowIndex, title, validInView) {
					var astr = [];
					astr.push('<a href="javascript:void(0);" style="text-decoration: underline;"');
					astr.push(' class="hyperlink ' + ((validInView)?"validInView":"") + '"');
					astr.push(' onclick="__doListButtonClick(\'');
					astr.push(fieldVal);
					astr.push('\',\'');
					astr.push(fieldKey);
					astr.push('\',\'');
					astr.push(tableKey);
					astr.push('\',\'');
					astr.push(rowIndex);
					astr.push('\',');
					astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
					astr.push(')"');
					astr.push('>');
					astr.push(title);
					astr.push('</a>');
					return astr.join("");
	}
	
	function __doListChange(obj, fieldVal, fieldKey, tableKey) {
		if(fieldVal) {
	 			try {
	 				var ftd = $(obj).closest("td[field]");
	 				if(ftd) {
	 					var ftr = ftd.parent();
						var rowIndex = ftr.attr("datagrid-row-index");
						if(rowIndex != undefined && rowIndex != null) {
							var callFunc = eval('onListChangeFor_'+tableKey+'_'+fieldKey);
							if(callFunc) {
								callFunc(fieldVal, fieldKey, tableKey, parseInt(rowIndex));
							}
						}
	 				}
		 		} catch(ex) {
		 			alert(ex);
		 		}
	 	}
	}
	function getFormQueryParams() {
		var param =$("#hid_queryParams").val();
		var qp = $.parseJSON(param);
		if(qp) {
			for(var k in qp) {
				if(qp[k] === "$curUserId$") {
					qp[k] = $curUserId$;
				} else if(qp[k] === "$curOrgCode$%") {
					qp[k] = $curOrgCode$ + "%";
				} else if(qp[k] === "$curOrgId$") {
					qp[k] = $curOrgId$;
				}
			} 
		}
		return qp;
	}
	
	        <#-- 初始化tab查询，自定义表单特殊处理 -->
			function initTabQuery(tabDataJson, dataParam, fpName) {
			    var tabData = tabDataJson.data;
				if(tabData && tabData.length && dataParam) {
					var dtObj = {};
					dtObj[dataParam.key] = "";
					dtObj[dataParam.caption] = "全部";
					var arr = [dtObj];
					tabData = arr.concat(tabData);
					var ulHtmlArr = [];
					for(var i=0,len=tabData.length;i<len;i++) {
						var key = tabData[i][dataParam.key];
						var caption = tabData[i][dataParam.caption];
						if(i > 0) {
							ulHtmlArr.push("<li>");
						} else { <#-- 缓存tab参数 -->
							ulHtmlArr.push("<li class='tabs-selected'>");
							var paramObj = {};
							if(key !== "") paramObj[fpName] = key;
							$("#ul_tab_query").data("queryParam", paramObj);
						}
						ulHtmlArr.push('<a href="javascript:void(0)" class="tabs-inner" keyVal="');
						ulHtmlArr.push(key);
						ulHtmlArr.push('"><span class="tabs-title">');
						ulHtmlArr.push(caption);
						ulHtmlArr.push('</span></a>');
						ulHtmlArr.push("</li>");
					}
					$("#ul_tab_query").html(ulHtmlArr.join(""));
					$("#ul_tab_query li a").click(function(event) {
						$("#ul_tab_query li").removeClass("tabs-selected");
						$(this).parent().addClass("tabs-selected");
						var keyVal = $(this).attr("keyVal");
						var paramObj = {};
						if(keyVal !== "") paramObj[fpName] = keyVal;
						$("#ul_tab_query").data("queryParam", paramObj);
						doQuery(paramObj);
					});
				}
			}
			
	$(function() {
		if($("ul.tabs li").length==1) { <#-- 如果只有一个tab页，则将其tab标题隐藏 -->
			$("div.tabs-header").hide();
		}
	});		
			
	<@fieldftl.initFormCssOrJavascript formList=formList eventKey="addJavaScript"/>		
</script>	
	<@fieldftl.initFormCssOrJavascript formList=formList eventKey="referJsOrCss"/>	
</body>
</html>