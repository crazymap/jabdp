<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<#import "common/field.ftl" as fieldftl>   
<#assign entityName=root.entityName /> 
<#assign formList=root.dataSource.formList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>

<#-- 关联表表 -->
<#assign realEntityName = entityName />
<#if root.moduleProperties.relevanceModule??>
<#assign relevanceModule=root.moduleProperties.relevanceModule/>
<#list formList as form>
<#if form.isMaster>
	<#assign realEntityName = form.entityName />
	<#break/>
</#if>
</#list>
<#else>
<#assign relevanceModule=""/>
</#if>
<#assign isShowInQueryList=false/><#--标识是否存在列表页面显示的tabs-->
<#assign flowList= root.flowList />