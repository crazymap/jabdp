<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${r"${ctx}"}/js/easyui/${r"${themeColor}"}/panel.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="${r"${ctx}"}/js/easyui/scripts/easyui.xpPanel.js"></script>  
<#import "common/field.ftl" as fieldftl>   
<#assign entityName=root.entityName /> 
<#assign formList=root.dataSource.formList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>
<#assign flowList= root.flowList />
<#if formList??>
        <#list formList as form>
            <#if form.isMaster>
            <style type="text/css"><#-- 初始化样式 -->
			<@fieldftl.initQueryPageFormCssOrJavascript formList=formList eventKey="addCssStyle"/>
			</style>
            <script type="text/javascript">
            var _userList={};

			function getQueryParams() {
				var param = $("#queryForm").serializeArrayToParam();
				param["sort"] = $("#queryList").datagrid("options")["sortName"];
				param["order"] = $("#queryList").datagrid("options")["sortOrder"];
				var strParam = JSON.stringify(param);
				var params = [];
				params.push("&queryParams=");
				params.push(strParam);
				if($("#_commonTypeTree_div_").length) {
					var ap = $("#_commonTypeTree_div_").data("addParam");
					if(ap) {
						params.push("&addParams=");
						params.push(JSON.stringify(ap));
					}
				}
				return params.join("");
			}
			
            function getOption() {
                return {
                	url:'${r"${ctx}"}/gs/gs-mng!queryList.action?entityName=${entityName}',
                    width : 'auto',
                    height : 'auto',
                    nowrap : false,
                    striped : true,
                    fit : true,                
                    sortName : <#if ((form.sortName)!"") != "">'${form.sortName}'<#else>'id'</#if>,
                    sortOrder : <#if ((form.sortOrder)!"") != "">'${form.sortOrder}'<#else>'desc'</#if>,
					pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
					pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
                    frozenColumns : [
                        [
                            {
                                field : 'ck',
                                checkbox : true
                            },{
								field : "atmId",
								title : "<s:text name="system.button.accessory.title"/>",
								width : 35,
								formatter:function(value, rowData, rowIndex) {
									if(value) {
										var astr = [];
										astr.push('<a class="easyui-linkbutton l-btn l-btn-plain" href="javascript:void(0);"');
										<security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
										astr.push(' onclick="doAccessory(');
										astr.push(rowData["id"]);
										astr.push(',');
										astr.push(value);
										astr.push(',\'');
										astr.push(rowData["status"]);
										astr.push('\')"');
										</security:authorize>
										astr.push('>');
										astr.push('<span class="l-btn-left"><span class="l-btn-text"><span class="l-btn-empty icon-attach">&nbsp;</span></span></span></a>');
										return astr.join("");
									} else {
										return "<span></span>";
									}
								}
                            }
                            <#if !((form.isHideStatus)!false) >
                            ,{
                            	field : "status",
								title : "<s:text name="system.module.status.title"/>",
								width : 60,
								sortable : true,
								isFilterData:true,
								formatter:function(value, rowData, rowIndex) {
									if(value) {
										var val = $.jwpf.system.module.status[value];
										return (val?val:"");
									} else {
										return "";
									}
								}
                            }
                            </#if>
                            <#assign fields=form.fieldList/>
                            <#list fields as field> <#-- 列表显示且作为冻结列 -->
	                                <#if ((field.queryProperties.showInGrid)!false) && (field.queryProperties.frozenColumn)!false>
	                                	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>  
	                                    <@fieldftl.initMainQuery field=field prefix="dg_" form=form appendCommaFirst=true/>
	                                    <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
	                                </#if>
                            </#list>
                        ]
                    ],
                    columns : [
                        [
                            <#if fields??&&fields?size gt 0>
                                <#list fields as field>
	                                <#if ((field.queryProperties.showInGrid)!false) && !((field.queryProperties.frozenColumn)!false)>
	                                	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>  
	                                    <@fieldftl.initMainQuery field=field prefix="dg_" form=form/>
	                                    <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
	                                </#if>
                                </#list>
                            </#if>         <#--{
	                                            field : 'createUser',
	                                            title : '<s:text name="system.module.createUser.title"/>',
	                                            width : 120,
	                                            sortable : true,
												isFilterData:true,
	                                            formatter:function(value, rowData, rowIndex){
	                                                    var uObj=_userList[value];
	                                                    if(uObj){
	                                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
															return uObj.realName + "[" + uObj.loginName + "]" + nickName;
	                                                    }else{
	                                                        return value;
	                                                    }
	                                            }
                                         },-->{
	                                            field : 'createTime',
	                                            title : '<s:text name="创建时间"/>',
	                                            width : 100,
												sortable : true,
												align : 'center'
												,formatter: function(value,row,index){
													return fnFormatDate(value,'yyyy-MM-dd');
												}
                                         },
                                         {
	                                            field : 'cancelReason',
	                                            title : '<s:text name="system.module.invalidReason.title"/>',
	                                            width : 120
                                         }
                        ]
                    ],
                    onDblClickRow:function(rowIndex, rowData){
                    	<@fieldftl.initDatagridOnDblClickRow form=form />
                    	doDblView(rowData.id);
                    	//$('#queryList').datagrid('unselectRow',rowIndex);
                    },
                    
    				onRowContextMenu  : function(e, rowIndex, rowData){
    					e.preventDefault();
    					$('#mm').data("id", rowData.id);
    					$('#mm').data("attachId", rowData.atmId);
    					$('#mm').data("status", rowData.status);
    					$('#mm').menu('show', {
    						left: e.pageX,
    						top: e.pageY
    					});
    				},
    				onLoadError:function() {
    					$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="数据加载异常"/>', 'info');
    				},
                    pagination : true,
                    rownumbers : true,
                    toolbar :[
                        '-'
						<security:authorize url="/gs/gs-mng!add.action?entityName=${entityName}">       
                        ,{
                            id : 'bt_add',
                            text : '<s:text name="system.button.add.title"/>(I)',
                            iconCls : 'icon-add',
                            handler : function() {
                                doAdd();
                            }
                        }</security:authorize><security:authorize url="/gs/gs-mng!delete.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_del',
                            text : '<s:text name="system.button.delete.title"/>(D)',
                            iconCls : 'icon-remove',
                            handler : function() {
                                doDelete();
                            }                           
                        }</security:authorize><security:authorize url="/gs/gs-mng!view.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_view',
                            text : '<s:text name="system.button.view.title"/>(E)',
                            iconCls : "icon-search",
                            handler : function() {
                                doView();
                            }
                        }</security:authorize><security:authorize url="/gs/gs-mng!add.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_copy',
                            text : '<s:text name="system.button.copy.title"/>(G)',
                            iconCls : "icon-copy",
                            handler : function() {
                            	doCopyTbar();
                            }
                        }</security:authorize><security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_accessory',
                            text : '<s:text name="system.button.accessory.title"/>(Y)',
                            iconCls : "icon-attach",
                            handler : function() {
                                doAccessory();
                            }
                        }</security:authorize>
                        <#-- <security:authorize url="/sys/report/report.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_report',
                            text : '<s:text name="system.button.report.title"/>(R)',
                            iconCls : "icon-report",
                            handler : function() {
                                doViewReport('${moduleKey}');
                            }
                        }</security:authorize>-->
                        <#if flowList??&&flowList?size gt 0>
                        <#else>
                        <security:authorize url="/gs/process!startApprove.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_selfApprove',
                            text : '<s:text name="system.button.selfApprove.title"/>',
                            iconCls : "icon-flowStart",
                            handler : function() {
                               doSelfProcess();
                            }
                        }</security:authorize><security:authorize url="/gs/process!cancelApprove.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_cancelSelfApprove',
                            text : '<s:text name="system.button.cancelSelfApprove.title"/>',
                            iconCls : "icon-user",
                            handler : function() {
                               doCancelSelfProcess();
                            }
                        }</security:authorize>
                        </#if>
                        <#if form.enableImport><#-- 导入功能权限控制同新增功能 -->
                        <security:authorize url="/gs/gs-mng!add.action?entityName=${entityName}"> 
                        ,
                        {
                        	id : 'bt_excelImport',
                            text : '<s:text name="system.button.importExcel.title"/>',
                            iconCls : "icon-excelImport",
                            handler : function() {
                               doOpenExcelImportWin("${entityName}");
                            }
                        }</security:authorize>
                        </#if>
                        <#if form.enableExport><#-- 导出功能权限控制同查看功能 -->
                        <security:authorize url="/gs/gs-mng!view.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_excelExport',
                            text : '<s:text name="system.button.exportExcel.title"/>',
                            iconCls : "icon-excelExport",
                            handler : function() {
                               var rows = $('#queryList').datagrid('getSelections');
                               var ids = [];
                               if(rows && rows.length) {
                               	  $.each(rows, function(i, v) {
                               	  	ids.push(v.id);
                               	  });
                               }
                               doExportExcel("${entityName}", getExportParams("queryList"), ids.join(","));
                            }
                        }</security:authorize>
                        </#if>
                        <@fieldftl.initDatagridToolBarButton form=form />
                    ]
                    <@fieldftl.initDatagridParam form=form />
                };
            }
            
		    function doSelfProcess() {
		    	if(window.onBeforeStartProcess) {
		           var flag = window.onBeforeStartProcess();
		           if(flag === false) {
		           	   return;
		           }
		        }
		    	var ids = [];
                var rows = $('#queryList').datagrid('getSelections');
                for (var i = 0; i < rows.length; i++) {
                   	if(rows[i].status != "10") {
                    	$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.selfApproveWarn.title"/>', 'info');
                    	return;
                    }
                    ids.push(rows[i].id);
                }
                if(ids.length) {
	                $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.selfApproveSure"/>',
						function(r) {
			           		if(r) {
			                 	jwpf.doStartOrCancelApprove("${entityName}", ids, "startApprove", function() {
			                 		if(window.onAfterStartProcess) {
									   window.onAfterStartProcess();
									}
									doRefreshDataGrid();
			                 	});
			                } 
			        	}
			        );
                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
                }
			}
			
			function doCancelSelfProcess() {
				var ids = [];
                var rows = $('#queryList').datagrid('getSelections');
                for (var i = 0; i < rows.length; i++) {
                   	if(rows[i].status != "35") {
                    	$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.cancelSelfApproveWarn.title"/>', 'info');
                    	return;
                    }
                    ids.push(rows[i].id);
                }
				if(ids.length) {
	                $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.cancelSelfApproveSure"/>',
						function(r) {
			           		if(r) {
			                 	jwpf.doStartOrCancelApprove("${entityName}", ids, "cancelApprove", function() {
			                 		if(window.onAfterCancelProcess) {
									   window.onAfterCancelProcess();
									}
									doRefreshDataGrid();
			                 	});
			                } 
			        	}
			        );
                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
                }
			}
					 
			   function formatterImage(value, rowData, rowIndex){
			           if(value){
			        	   var path="${r"${systemParam.virtualPicPath}"}"+value;
			               return  "<div><img onclick='$(body).gzoomExpand(\" "+path+" \")' src='"+path+"'  id='contractImgSrc' width='20px' height='20px' /></div>"; 
			           }
				 }
						            
				
                <#assign fields=form.fieldList/>
                <#if fields??&&fields?size gt 0>
                    <#list fields as field>
                        <#if (field.queryProperties.showInGrid)!false>
                           <@fieldftl.initControlsData field=field type="datagrid" prefix="dg_" entityName=form.entityName/>
                        </#if>
                    </#list>
                </#if>
                
                <@fieldftl.initFormFieldOnListEvent form=form /> <#-- 初始化列表按钮事件 -->
                
                <@fieldftl.initFormQueryPageEvent form=form /> <#-- 初始化查询页面事件 -->
               
            //右键复制一条记录 
            function doCopy(){
             var id = $("#mm").data("id");
             if(id==null){
            	 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
                  return ;
             }else{
            	  top.addTab('<s:text name="system.button.copy.title"/><s:text name="${i18nKey!titleName}"/>-'+id, '${r"${ctx}"}/gs/gs-mng!edit.action?entityName=${entityName}&id=' + id+ getQueryParams());
             }
             
          }
           //工具条复制一条记录
           function doCopyTbar(){
        	   var id=0;
             	 var rows = $('#queryList').datagrid('getSelections');
             	 if(rows.length!=1){
             		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
             	     return ;
             	 } else{
             		 id=rows[0].id;
             		 top.addTab('<s:text name="system.button.copy.title"/><s:text name="${i18nKey!titleName}"/>-'+id, '${r"${ctx}"}/gs/gs-mng!edit.action?entityName=${entityName}&id=' + id+ getQueryParams());
             	 }
           }
           //删除一条记录
            function doDelete() {
                var ids = [];
              <#--if (id == null) {-->  
                    var rows = $('#queryList').datagrid('getSelections');
                    for (var i = 0; i < rows.length; i++) {
                    	if(rows[i].status != "10") {
                    		$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.delApproveData"/>', 'info');
                    		return;
                    	}
                        ids.push(rows[i].id);
                    }
               <#--  } else {
                    ids.push(id);
                }--> 

                if (ids != null && ids.length > 0) {
                    if(window.onBeforeDelete) {
			           var flag = window.onBeforeDelete();
			           if(flag === false) {
			           	   return;
			           }
			        }
                    $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
                        if (r) {
                            var options = {
                                url : '${r"${ctx}"}/gs/gs-mng!delete.action?entityName=${entityName}',
                                data : {
                                    "ids" : ids
                                },
                                success : function(data) {
                                    if (data.msg) {
                                        doQuery();
                                    }
                                },
                                traditional:true
                            };
                            fnFormAjaxWithJson(options);
                        }
                    })

                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
                }
            }
            //查询
            function doQuery(paramObj, isInit) {
                var param = $("#queryForm").serializeArrayToParam();
                if(paramObj) {
                	$.extend(true, param, paramObj);
                }
                if($("#ul_tab_query").length) {
                	var obj = $("#ul_tab_query").data("queryParam");
                	$.extend(true, param, obj);
                }
                if($("#_commonTypeTree_div_").length) {
                	var obj = $("#_commonTypeTree_div_").data("queryParam");
                	$.extend(true, param, obj);
                }
                var qps = getFormQueryParams();<#-- 加入初始化查询条件 -->
                $.extend(true, param, qps);
                if(isInit) { <#-- 初始化列表 -->
                	var opt = getOption();
                	opt["queryParams"] = param;
                	$("#queryList").edatagrid(opt);
                } else {
                	$('#queryList').datagrid('clearSelections');
                	$("#queryList").edatagrid("load", param);
                }
            }

          
           //新增
            function doAdd() {
                top.addTab('<s:text name="system.button.add.title"/><s:text name="${i18nKey!titleName}"/>', '${r"${ctx}"}/gs/gs-mng!add.action?entityName=${entityName}'+ getQueryParams());
            }
            
            function doView() {
            	 var id=0;
            	 var rows = $('#queryList').datagrid('getSelections');
            	  if(rows.length!=1){
            		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
            	     return ;
            	 } else{
            		 id=rows[0].id;
            	 }
            	 doDblView(id); 
            }
           //查看tab
            function doDblView(id) {
            	top.addTab('<s:text name="system.button.view.title"/><s:text name="${i18nKey!titleName}"/>-'+id, '${r"${ctx}"}/gs/gs-mng!view.action?entityName=${entityName}&id=' + id+ getQueryParams());
            }
            
            //右键查看
            function doCmView() {
            	var id = $("#mm").data("id");
            	doDblView(id);
            }
            
           //刷新列表
            function doRefreshDataGrid() {
    			$("#queryList").edatagrid("load");
    		}
    		
			//添加附件
            function doAccessory(id, attachId, status) {
				 if(!id) {
	           	     var rows = $('#queryList').datagrid('getSelections');
		           	 if(rows.length!=1){
		           		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
		           	     return ;
		           	 } else{
		           		attachId=rows[0].atmId;
		           		id=rows[0].id;
		           		status=rows[0].status;
		           	 }
	           	 }
	           	var opts ={"attachId":attachId,
           				 "id":id,
           				 "key":"",
           				 "entityName":"${entityName}",
           				 "dgId":"queryList",
           				 "flag":"mainQuery",
           				 "readOnly" : false
           				};
	           	 if(status == "10") {
	           		opts.readOnly =false;
	           	 	//doAttach(opts);
	           	 } else {
	           		opts.readOnly =true;
	           	 	//doAttach(opts);
	           	 }
	           	 doAttach(opts);
            }

			//更新附件ID flag: mainQuery-主实体查询，subEdit-子实体编辑，mainEdit-主实体编辑
            function doUpdateAttach(opts) {
            	var options = {
            			url : '${r"${ctx}"}/gs/gs-mng!updateAttach.action',
                        data : {
                        	"entityName":opts.entityName,
                            "id" : opts.id,
                            "attachId": opts.attachId,
                            "field" : "atmId"
                        },
    					async: false,
    					success : function(data) {
    						if(data.msg) {
        						if(opts.flag == "mainQuery") {
        							$("#" + opts.dgId).datagrid("reload");
            					} else if(opts.flag == "subEdit"){
            						$("#" + opts.dgId).edatagrid("reload");
                				}
    						}
    					}
    			};
    			fnFormAjaxWithJson(options);
            }
            
            
            function doViewReport(tptName,id,type) {
            	 if(!id) {
            	 	 var rows = $('#queryList').datagrid('getSelections');
            	 	 if(rows.length!=1){
		           		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
		           	     return ;
		           	 } else{
		           		id=rows[0].id;
		           	 }
            	 }
            	 tptName = tptName.toLowerCase();
				 top.addTab("<s:text name="${i18nKey!titleName}"/>-<s:text name="system.button.report.title"/>-" + tptName + "-" + id,'${r"${ctx}"}/sys/report/report.action?rptName=' + tptName + "&id=" + id + "&type=" + (type?type:""));
			}
			
			function initQueryData() {
				<#assign fields=form.fieldList!>
                <#if fields??&&fields?size gt 0>
                    <#list fields as field>
	                        <#if (field.queryProperties.showInSearch)!false>
		                         <@fieldftl.initControlsData field=field type="query" prefix="query_" entityName=form.entityName/>
	                        </#if>
	                        <#if (field.queryProperties.showInTreeSearch)!false>
	                        	<@fieldftl.initTreeQuery field=field prefix="dg_" entityName=form.entityName/>
	                        </#if>
                    </#list>
                </#if>
			}
			
			<#-- 初始化tab查询 -->
			function initTabQuery(tabData, dataParam, fpName) {
				if(tabData && tabData.length && dataParam) {
					var dtObj = {};
					dtObj[dataParam.key] = "";
					dtObj[dataParam.caption] = "全部";
					var arr = [dtObj];
					tabData = arr.concat(tabData);
					var ulHtmlArr = [];
					for(var i=0,len=tabData.length;i<len;i++) {
						var key = tabData[i][dataParam.key];
						var caption = tabData[i][dataParam.caption];
						if(i > 0) {
							ulHtmlArr.push("<li>");
						} else { <#-- 缓存tab参数 -->
							ulHtmlArr.push("<li class='tabs-selected'>");
							var paramObj = {};
							paramObj[fpName] = key;
							$("#ul_tab_query").data("queryParam", paramObj);
						}
						ulHtmlArr.push('<a href="javascript:void(0)" class="tabs-inner" keyVal="');
						ulHtmlArr.push(key);
						ulHtmlArr.push('"><span class="tabs-title">');
						ulHtmlArr.push(caption);
						ulHtmlArr.push('</span></a>');
						ulHtmlArr.push("</li>");
					}
					$("#ul_tab_query").html(ulHtmlArr.join(""));
					$("#ul_tab_query li a").click(function(event) {
						$("#ul_tab_query li").removeClass("tabs-selected");
						$(this).parent().addClass("tabs-selected");
						var keyVal = $(this).attr("keyVal");
						var paramObj = {};
						paramObj[fpName] = keyVal;
						$("#ul_tab_query").data("queryParam", paramObj);
						doQuery(paramObj);
					});
				}
			}
			
				function __doListButtonClick(fieldVal, fieldKey, tableKey, rowIndex, callFunc) {
					if(callFunc) {
						callFunc(fieldVal, fieldKey, tableKey, rowIndex);
					}
				}
				
				function __getListButton(fieldVal, fieldKey, tableKey, rowIndex, title) {
					var astr = [];
					astr.push('<button type="button" class="button_big"');
					astr.push(' onclick="__doListButtonClick(\'');
					astr.push(fieldVal);
					astr.push('\',\'');
					astr.push(fieldKey);
					astr.push('\',\'');
					astr.push("queryList");
					astr.push('\',\'');
					astr.push(rowIndex);
					astr.push('\',');
					astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
					astr.push(')"');
					astr.push('>');
					astr.push(title);
					astr.push('</button>');
					return astr.join("");
				}
				
				function __getListHyperLink(fieldVal, fieldKey, tableKey, rowIndex, title) {
					var astr = [];
					astr.push('<a href="javascript:void(0);" style="text-decoration: underline;"');
					astr.push(' onclick="__doListButtonClick(\'');
					astr.push(fieldVal);
					astr.push('\',\'');
					astr.push(fieldKey);
					astr.push('\',\'');
					astr.push("queryList");
					astr.push('\',\'');
					astr.push(rowIndex);
					astr.push('\',');
					astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
					astr.push(')"');
					astr.push('>');
					astr.push(title);
					astr.push('</a>');
					return astr.join("");
				}
			
			<s:if test='#session.USER.isSuperAdmin=="1"'>
			function updateCreateUser() {
				 var rows = $('#queryList').datagrid('getSelections');
				 doUpdateCreateUser(rows,"${entityName}",doRefreshDataGrid);
			}
			
			function doOpenModifyCreateUserWin() {
				var rows = $('#queryList').datagrid('getSelections');
				if(rows && rows.length) {
					doOpenUserWin('${r"${ctx}"}/gs/process!passOn.action','<s:text name="修改所有者"/>');
				} else {
					$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
				}
			}
			</s:if>
			
	function getFormQueryParams() {
		var param =$("#hid_queryParams").val();
		var qp = $.parseJSON(param);
		if(qp) {
			for(var k in qp) {
				if(qp[k] === "$curUserId$") {
					qp[k] = $curUserId$;
				} else if(qp[k] === "$curOrgCode$%") {
					qp[k] = $curOrgCode$ + "%";
				}
			} 
		}
		return qp;
	}

            $(document).ready(function() {
               _userList=findAllUser();
			    initQueryData();
			    
			    <@fieldftl.initFormTabQuery form=form isInitTab=false/>
			    
                $("#bt_query").click(function() {doQuery();});
                $("#bt_reset").click(function() {
                    //$("#queryForm")[0].reset();
                    $("#queryForm").form("clear");
                    doQuery();
                });
                //回车查询
                doQuseryAction("queryForm");
                if($("#_commonTypeTree_div_").length == 0 || !$("#_commonTypeTree_div_").attr("issys")) { <#-- 无系统字典树查询时自动查询，有时，则由字典树触发查询 -->
                	doQuery(null, true);
                }
                jwpf.bindHotKeysOnListPage();
             	<c:if test="${r"${param.isHideQc=='1'}"}">
				$("#layout_body").layout("collapse", "west");
				</c:if>
            });

            </script>
</head>            
         <body class="easyui-layout" fit="true" id="layout_body">
          <div region="west" title="<s:text name='system.search.title'/>" border="false"
                 split="true" style="width:215px;padding:0px;" 
                 iconCls="icon-search" tools="#pl_tt" >
                		<@fieldftl.initFormTree form=form/>
			            <form action="${r"${ctx}"}/gs/gs-mng!queryList.action?entityName=${entityName}"
	                      name="queryForm" id="queryForm">
	                       <div class="xpstyle-panel" id="queryFormId">
		                     	<@fieldftl.getQueryView form=form/>
		                     	<div style="text-align:center;padding:8px 8px;">
						             <button type="button" id="bt_query" class="button_small">
									     <s:text name="system.search.button.title"/>
									 </button>&nbsp;&nbsp;
									 <button type="button" id="bt_reset" class="button_small">
									     <s:text name="system.search.reset.title"/>
									 </button>
								</div> 
	                       </div>
	                </form>           
      
            </div>
            <div region="center" title="" border="false">
                <div class="easyui-layout" fit="true">
            		<#-- <div region="north" border="false" style="text-align:right;height:28px;overflow:hidden;">
            			<ul class="tabs">
            				<li class="">
          						<a href="javascript:void(0)" class="tabs-inner"><span class="tabs-title">总控制台</span><span class="tabs-icon"></span></a>
          					</li>
          					<li class="tabs-selected">
          						<a href="javascript:void(0)" class="tabs-inner"><span class="tabs-title">销售订单</span><span class="tabs-icon"></span></a>
          					</li>
          				</ul>
            		</div> -->
            		<@fieldftl.initFormTabQuery form=form isInitTab=true/>
            		<div region="center" border="false">
            			<table id="queryList" border="false" fit="true"></table>
            		</div>
            	</div>
            </div>
             <div id="mm" class="easyui-menu" style="width:120px;">
				<security:authorize
					url="/gs/gs-mng!add.action?entityName=${entityName}">
					<div onclick="doAdd()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!delete.action?entityName=${entityName}">
					<div onclick="doDelete()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!view.action?entityName=${entityName}">
					<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!add.action?entityName=${entityName}">
					<div onclick="doCopy()" iconCls="icon-copy"><s:text name="system.button.copy.title"/></div>
				</security:authorize>
				<#-- <security:authorize
					url="/sys/report/report.action?entityName=${entityName}">
						<div onclick="doViewReport('${moduleKey}',$('#mm').data('id'))" iconCls = "icon-report"><s:text name="system.button.report.title"/></div>
				</security:authorize> --> 
				<security:authorize
					url="/sys/attach/attach.action?entityName=${entityName}">
						<div onclick="doAccessory($('#mm').data('id'),$('#mm').data('attachId'),$('#mm').data('status'))" iconCls = "icon-attach"><s:text name="system.button.accessory.title"/></div>
					 </security:authorize>
				<s:if test='#session.USER.isSuperAdmin=="1"'>
					<div onclick="doOpenModifyCreateUserWin()" iconCls="icon-user"><s:text name="修改所有者"/></div>
				</s:if>
			</div>
			<div id="pl_tt">
				<a href="#" id="a_exp_clp" class="icon-expand" title="<s:text name="system.button.expand.title"/>"></a>
				<a href="#" id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>" style="display:none;"></a>
			</div>
			<form method="post" name="hidFrm" id="hidFrm" >
				<textarea style="display:none;" name="hid_queryParams" id="hid_queryParams">${r"${param.queryParams}"}</textarea><#-- 查询参数 -->
			</form>
			<script type="text/javascript">
			    <#-- 初始化JS脚本 -->
    			<@fieldftl.initQueryPageFormCssOrJavascript formList=formList eventKey="addJavaScript"/>
			</script>
			<@fieldftl.initQueryPageFormCssOrJavascript formList=formList eventKey="referJsOrCss"/>
            </body>
            </#if>
      </#list>
</#if>
</html>