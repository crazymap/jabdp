<#--view页面封装的函数-->
<#macro getFiledView field form>
		<#assign editType = field.editProperties.editType/>
        <#if editType=='TextBox'>
        		<#assign dataType = field.dataProperties.dataType />
		        <#if dataType=='dtLong' || dataType=='dtInteger'>
		             <input id="${field.key}"  name="${field.key}" class="easyui-numberbox <@getClass field=field/>"  ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")}
		              <@getValidType field=field form=form/> groupSeparator="${(field.editProperties.dtFormat)!""}" <@getStyleWidHei field=field/>  <@initCheckMasterFieldReadOnlyAuth field=field form=form />
		              <@getDefaultValue field=field/> />
	           <#elseif dataType=='dtDouble'>
		             <input id="${field.key}" name="${field.key}" class="easyui-numberbox <@getClass field=field/>"  precision="${(field.dataProperties.scale)!0}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />
		              <@getValidType field=field form=form/> ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} groupSeparator="${(field.editProperties.dtFormat)!""}"
		              <@getDefaultValue field=field/> />
		       <#elseif dataType=='dtText'>
		        	 <textarea class="easyui-validatebox  <@getClass field=field/>" id="${field.key}"  name="${field.key}" <@getStyleWidHei field=field/> <@initCheckMasterFieldReadOnlyAuth field=field form=form />
		        	  <@getValidType field=field form=form/> ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")}><@getDefaultValue field=field editType="textarea"/></textarea>
		       <#else>
		        	<input type="text" class="easyui-textbox <#if (field.editProperties.validInView)!false>validInView</#if> <@getClass field=field/>" name="${field.key}" id="${field.key}" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} 
		        	  <@getValidType field=field form=form/> <@getStyleWidHei field=field/>  <@initCheckMasterFieldReadOnlyAuth field=field form=form />
		        	 <@getDefaultValue field=field/> <#if ((field.editProperties.autoIdRule)!"")!= "">prompt="(<s:text name="system.autoGen.title"/>)"</#if>/>
	           </#if>
	     <#elseif editType=='PasswordBox'>
	      	    <input type="password" id="${field.key}" name="${field.key}" class="easyui-textbox  <@getClass field=field/>" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} 
	      	    	  <@getValidType field=field form=form/> <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />
	      	   	      <@getDefaultValue field=field/> />
		 <#elseif editType=='DateBox'>
		        <input class="Idate Wdate <@getClass field=field/>" onfocus="WdatePicker({dateFmt:'<#if (field.editProperties.dtFormat)!="">${field.editProperties.dtFormat}<#else>yyyy-MM-dd</#if>',onpicked:window.__eventObj_${field.key}_onpicked<#if field.editProperties.minDate?? && (field.editProperties.minDate)!="">,minDate:'${field.editProperties.minDate}'</#if><#if field.editProperties.maxDate?? && (field.editProperties.maxDate)!="">,maxDate:'${field.editProperties.maxDate}'</#if>});" type="text" name="${field.key}" 
		                   id="${field.key}" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> />
		 <#elseif editType=='ComboBox'>
		         <input class="easyui-combobox  <@getClass field=field/>"  name="${field.key}" id="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />
		         	<@getValidType field=field form=form/>  ${((field.editProperties.multiple)!false)?string("multiple=\"true\"","")} ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")}/> 
		 <#-- 新增带搜索框的下拉控件 -->
		 <#elseif editType=='ComboBoxSearch'>
			<input class="easyui-comboboxsearch <@getClass field=field/>" name="${field.key}" id="${field.key}" <@getStyleWidHei field=field/>
			 <@initCheckMasterFieldReadOnlyAuth field=field form=form/> <@getValidType field=field form=form/>
			 ${((field.editProperties.multiple)!false)?string("multiple=\"true\"","")}
			 ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")}/>
		 <#elseif editType=='ImageBox'>
		   		<input id="${field.key}" class="<@getClass field=field/>" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />/>
	     <#elseif editType=='RichTextBox'>
		             <#if field.editProperties.dtFormat=='html'>
		                     <textarea class="jquery_ckeditor <@getClass field=field/>" id="${field.key}" name="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ><@getDefaultValue field=field editType="textarea"/></textarea>
		             <#else>
		                     <textarea class="<@getClass field=field/>" name="${field.key}" id="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ><@getDefaultValue field=field editType="textarea"/></textarea>
		            </#if>
		  <#elseif editType=='ComboTree'>
          <select  class="easyui-combotree  <@getClass field=field/>"  <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />
                  name="${field.key}" id="${field.key}" ${((field.editProperties.multiple)!false)?string("multiple=\"true\"","")} cascadeCheck="false" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")}></select>
          <#elseif editType=='DictTree'>
          <select  class="easyui-combotree  <@getClass field=field/>"  <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />
                  name="${field.key}" id="${field.key}" ${((field.editProperties.multiple)!false)?string("multiple=\"true\"","")} cascadeCheck="false" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")}></select>        
          <#elseif editType=='RadioBox'>
            <input name="${field.key}" id="${field.key}" class="easyui-newradiobox  <@getClass field=field/>" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />/>
          <#elseif editType=='CheckBox'>
            <input name="${field.key}" id="${field.key}" class="easyui-newcheckbox  <@getClass field=field/>" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />/>
          <#elseif editType=='ComboRadioBox'>
            <input name="${field.key}" id="${field.key}" class="easyui-comboradiobox  <@getClass field=field/>" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} <@getStyleWidHei field=field /> <@initCheckMasterFieldReadOnlyAuth field=field form=form />/>
          <#elseif editType=='ComboCheckBox'>
            <input name="${field.key}" id="${field.key}" class="easyui-combocheckbox  <@getClass field=field/>" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} <@getStyleWidHei field=field /> <@initCheckMasterFieldReadOnlyAuth field=field form=form />/>  
          <#elseif editType=='Button'>
             <button class="button_big <#if (field.editProperties.validInView)!false>validInView</#if>  <@getClass field=field/>" type="button" id="bt_${field.key}" style="text-align:center;"  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ><#if (field.editProperties.defaultValue)?? && (field.editProperties.defaultValue)!="">${field.editProperties.defaultValue}<#else><s:text name="${field.i18nKey!field.caption}"/></#if></button>
             <input type="hidden" name="${field.key}" id="${field.key}" />
          <#elseif editType=='HyperLink'>
             <a id="hl_${field.key}" class="hyperlink <#if (field.editProperties.validInView)!false>validInView</#if>  <@getClass field=field/>" style="text-decoration: underline;" href="javascript:void(0);" target="_blank" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ><#if (field.editProperties.defaultValue)?? && (field.editProperties.defaultValue)!="">${field.editProperties.defaultValue}<#else><s:text name="${field.i18nKey!field.caption}"/></#if></a>
			 <input type="hidden" name="${field.key}" id="${field.key}" class="hyperlink" />
		  <#elseif editType=='BrowserBox'>
               <iframe name="${field.key}" id="${field.key}" src="<#-- ${(field.editProperties.defaultValue)!""}-->" style="width:<#if (field.editProperties.width)??>${(field.editProperties.width)?c}px<#else>100%</#if>;height:<#if (field.editProperties.height)??>${(field.editProperties.height)?c}px<#else>100%</#if>;" scrolling="auto" frameborder="0"></iframe>
          <#elseif editType=='DataGrid'>
          <#-- 加grid_widget表示主表的datagrid控件，避免设置子表焦点时出错 -->
               <table grid_widget class="easyui-datagrid  <@getClass field=field/>" id="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ></table>
          <#elseif editType=='TreeGrid'>
               <table grid_widget class="easyui-treegrid  <@getClass field=field/>" id="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ></table> 
          <#elseif editType=='Tree'>
               <ul class="easyui-tree  <@getClass field=field/>" id="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ></ul>
          <#elseif editType=='zTree'>
               <ul class="ztree  <@getClass field=field/>" id="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ></ul>
          <#elseif editType=='Div'>
                <div id="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> style="padding: 2px;"></div>    
          <#elseif editType=='Label'>
         	   <label name="${field.key}" id="${field.key}" title="${field.caption}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> >${(field.editProperties.defaultValue)!""}</label>
          <#elseif editType=='ComboGrid'>
         	   <select ${((field.editProperties.multiple)!false)?string("multiple=\"true\"","")} class="easyui-combogrid  <@getClass field=field/>" name="${field.key}" 
         	   id="${field.key}" <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form /> ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")}></select>
          <#elseif editType=='DialogueWindow'>
         	   <input id="${field.key}" name="${field.key}" <@getStyleWidHei field=field /> <@initCheckMasterFieldReadOnlyAuth field=field form=form />/>
	      <#elseif editType=='ProgressBar'>
          	   <input id="${field.key}" name="${field.key}" class="easyui-processbarSpinner  <@getClass field=field/>" ${((field.dataProperties.notNull)!false)?string("required=\"true\"","")} <@getStyleWidHei field=field />  <@initCheckMasterFieldReadOnlyAuth field=field form=form />/>
          </#if>
          <#if field.editProperties.quickAdd> <#-- 快速添加 -->
          <a href="javascript:void(0);" onclick="quickAddFor_${field.key}()" class="easyui-linkbutton bt-extra" plain="true" iconCls="icon-quickAdd" title="<s:text name="快速新增或修改"/>"></a>
          </#if>
          <@initFieldSuffix field=field isInList=false />
</#macro>
<#-- 定义控件长度宽度 -->
<#macro getStyleWidHei field>
 <#assign editType = field.editProperties.editType/>
 		<#assign dataType='S'>
		<#if field.dataProperties.dataType=='dtInteger'>
		       <#assign dataType='I'>
		<#elseif field.dataProperties.dataType=='dtLong'>
		        <#assign dataType='L'>
		<#elseif field.dataProperties.dataType=='dtDouble'>
		        <#assign dataType='C'>
		<#elseif field.dataProperties.dataType=='dtDateTime'>
				<#assign dataType='D'>
		</#if>
<#-- 新增带搜索框的下拉控件 -->
<#assign isCombo = (editType=="ComboBox" || editType=="ComboBoxSearch" || editType=="ComboTree" || editType=="ComboRadioBox" || editType=="ComboCheckBox" || editType=="DictTree" || editType=="ComboGrid")/>
jwDataType="${dataType}" jwTitle="<s:text name="${field.i18nKey!field.caption}"/>" style="<#if (field.editProperties.width)??>width:${(field.editProperties.width)?c}px;<#else><#if isCombo>width:152px;</#if></#if><#if (field.editProperties.height)??>height:${(field.editProperties.height)?c}px;</#if>text-align:<#if field.dataProperties.dataType=="dtDouble">right<#else>${(field.editProperties.align)!"left"}</#if>;"
</#macro>
<#-- 定义控件class -->
<#macro getClass field><#if field.enableRevise>jwpf-revise</#if></#macro>
<#-- 定义控件默认值 -->
<#macro getDefaultValue field editType="">
<#-- <#if editType=="textarea"><#if (field.editProperties.defaultValue)!="">${field.editProperties.defaultValue}</#if><#else><#if (field.editProperties.defaultValue)!="">value="${field.editProperties.defaultValue}"</#if></#if>-->
</#macro>
<#-- 定义控件校验规则 -->
<#macro getValidType field form>
	<#assign validType = "">
	<#assign uniqueType = "">
	<#assign dataType = "S">
		 <#if field.dataProperties.dataType=='dtLong'>
		 	<#assign dataType = "L">
		 <#elseif field.dataProperties.dataType=='dtInteger'>
		 	<#assign dataType = "I">
		 <#elseif field.dataProperties.dataType=='dtDouble'>
		 	<#assign dataType = "C">
		 <#elseif field.dataProperties.dataType=='dtText'>
		 	<#assign dataType = "S">
		 </#if>
	<#if field.editProperties.editType=='TextBox'>
		 <#assign validType = "length[1," + ((field.dataProperties.size)!30)?c + "]">
		 <#if form.isMaster && dataType != "" && (field.dataProperties.unique!false)>
		 	<#assign uniqueType = "unique['" + r"${ctx}" + "/gs/gs-mng!checkUnique.action?entityName=" + form.entityName + "','filter_EQ" + dataType + "_" + field.key + "']">
		 </#if>	 
	<#elseif field.editProperties.editType=='PasswordBox'>
		 <#assign validType = "length[1," + ((field.dataProperties.size)!30)?c + "]">
	<#-- 新增带搜索框的下拉控件 -->
	<#elseif field.editProperties.editType=='ComboBox' || field.editProperties.editType=='ComboBoxSearch'>
		 <#if form.isMaster && dataType != "" && (field.dataProperties.unique!false)>
		 	<#assign uniqueType = "unique['" + r"${ctx}" + "/gs/gs-mng!checkUnique.action?entityName=" + form.entityName + "','filter_EQ" + dataType + "_" + field.key + "','" + field.key +"']">
		 </#if>
	</#if>
 	<#if validType != "">
 		<#if uniqueType != "">
 		 <#-- validType="multiple['${validType}','${uniqueType}]" -->
 		 validType="${uniqueType}"
 		<#else>
 		 validType="${validType}"
 		</#if>
 	<#else>
 		<#if uniqueType != "">
 		 validType="${uniqueType}"
 		</#if>
 	</#if>
</#macro>
<#-- 初始化字段事件  -->
<#macro initFieldEvent field>
	<#if field.key?? && field.key!="">
	 <#assign editProp=field.editProperties!>
	 <#if editProp.eventList?? && editProp.eventList?size gt 0>
	 	var __eventObj_${field.key} = {};
	 	<#list editProp.eventList as event>
	 	<#if event.key!="onStyler" && event.key!="onListClick" && event.key!="onpicked" 
	 		&& event.key != "onFormatter" && event.key != "onAfterRevise">
	 	__eventObj_${field.key}["${event.key}"] = function(${event.params}) {
	 		try {
	 			${event.content}
	 		} catch(ex) {
	 			alert("执行${field.caption}(${field.key})属性事件${event.key}出现异常:" + ex);
	 		}
	 	};
	 	<#elseif event.key=="onpicked">
	 	__eventObj_${field.key}_${event.key} = function(${event.params}) {
	 		try {
	 			${event.content}
	 		} catch(ex) {
	 			alert("执行${field.caption}(${field.key})属性事件${event.key}出现异常:" + ex);
	 		}
	 	};
	 	</#if>
	 	</#list>
		<#assign editType=editProp.editType!"">
		jwpf.bindEvent("${field.key}", __eventObj_${field.key}, "${editType}", "${field.dataProperties.dataType}");
      </#if>    
    </#if>      
</#macro>
<#-- 初始化列表字段按钮事件  -->
<#macro initFormFieldOnListEvent form>
	<#assign fields=form.fieldList/>
    <#if fields??&&fields?size gt 0>
    <#list fields as field>
	<#if field.key?? && field.key!="">
	 <#assign editProp=field.editProperties!>
	 <#if editProp.eventList?? && editProp.eventList?size gt 0>
	 	<#list editProp.eventList as event>
	 	<#if event.key=="onListClick">
	 	function onListClickFor_${form.key}_${field.key}(${event.params}) {
	 		try {
				${event.content}
	 		} catch(ex) {
	 			alert("执行${field.caption}(${field.key})属性事件onListClick出现异常:" + ex);
	 		}
	 	};
	 	<#elseif event.key=="onListChange">
	 	function onListChangeFor_${form.key}_${field.key}(${event.params}) {
	 		try {
	 			${event.content}
	 		} catch(ex) {
	 			alert("执行${field.caption}(${field.key})属性事件onListChange出现异常:" + ex);
	 		}
	 	};
	 	</#if>
	 	</#list>
      </#if>    
    </#if>      
    </#list>
   	</#if>
</#macro>
<#-- 初始化字段修订事件  -->
<#macro initFieldReviseEvent form formKey=form.key>
	<#assign fields=form.fieldList/>
    <#if fields??&&fields?size gt 0>
    <#list fields as field>
	<#if field.key?? && field.key!="">
	 <#assign editProp=field.editProperties!>
	 <#if editProp.eventList?? && editProp.eventList?size gt 0>
	 	<#list editProp.eventList as event>
	 	<#if event.key=="onAfterRevise"><#-- 修订后事件 -->
	 	function onAfterReviseFor_${formKey}_${field.key}(${event.params}) {
	 		try {
				${event.content}
	 		} catch(ex) {
	 			alert("执行${field.caption}(${field.key})属性事件${event.key}出现异常:" + ex);
	 		}
	 	};
	 	</#if>
	 	</#list>
      </#if>    
    </#if>      
    </#list>
   	</#if>
</#macro>
<#-- 初始化按钮事件  -->
<#macro initButtonEvent formList>
	<#list formList as form>
									      <#if form.isMaster>
									      	<#if form.extension?? && form.extension.editPage??>
									      		<#assign buttonList=form.extension.editPage.buttonList />
									      		<#if buttonList?? && buttonList?size gt 0>
									      			<#list buttonList as button>
									      				<#if button.eventList?? && button.eventList?size gt 0>
									      					<#list button.eventList as event>
									      						<#if event.key == "onclick">
									      							function ${button.key}_onclick() {
									      								try {
																 			${event.content}
																 		} catch(ex) {
																 			alert("执行${button.caption}(${button.key})属性事件${event.key}出现异常:" + ex);
																 		}
									      							}
									      						</#if>
									      					</#list>
									      				</#if>
									      					<#if button.buttonList?? && button.buttonList?size gt 0 >
									      						<#list button.buttonList as bt>
									      							<#if bt.eventList?? && bt.eventList?size gt 0>
												      					<#list bt.eventList as event>
												      						<#if event.key == "onclick">
												      							function ${bt.key}_onclick() {
												      								try {
																			 			${event.content}
																			 		} catch(ex) {
																			 			alert("执行${bt.caption}(${bt.key})属性事件${event.key}出现异常:" + ex);
																			 		}
												      							}
												      						</#if>
												      					</#list>
												      				</#if>
									      						</#list>
									      					</#if>
									      			</#list>
									      		</#if>
									      	</#if>          
									      </#if>    
	</#list>     
</#macro>
<#-- 初始化表单事件  -->
<#macro initFormEvent eventList>
	 <#if eventList?? && eventList?size gt 0>
	 	<#list eventList as event>
	 	<#if event.key != "onRowStyler" && event.key != "addCssStyle" 
	 		&& event.key != "addJavaScript" && event.key != "referJsOrCss"
	 		&& event.key != "setQueryParam" && event.key != "showDetailView">
	 	function ${event.key}(${event.params}) {
	 		try {
	 			${event.content}
	 		} catch(ex) {
	 			alert("执行主表单事件${event.key}出现异常:" + ex);
	 		}
	 	};
	 	</#if>
	 	</#list>
      </#if>  
</#macro>
<#-- 初始化字段表达式 -->
<#macro initFieldExpression field formKey isMaster=true >
	<#if field.key?? && field.key!="">
	 	<#assign editProp=field.editProperties!>
	    	<#if (editProp.expCode)?? && (editProp.expCode)!="">
	    		<#if isMaster>
	    			jwpf.changeTableFormValue("${field.key}", "${editProp.expCode?js_string}");
	    		<#else>
	    			jwpf.changeTableFormValue("${formKey}.${field.key}", "${editProp.expCode?js_string}");
	    		</#if>
	    	</#if>
	    </#if>   
</#macro>
<#-- 查询条件封装  isForm:是否自定义表单-->
<#macro getQueryView form isForm=false >
<#assign formTitle="">
<#if !form.isMaster>
<#assign formTitle='<s:text name="${form.i18nKey!form.caption}"/>--'>
</#if>
<#assign fields=form.fieldList!>
<#if fields??&&fields?size gt 0>
<#list fields as field>
<#--判断控件比较类型-->
<#if field.key?? && field.key!="">
	<#assign dataType='S'>
	<#assign searchType='LIKE'> 
	<#if field.dataProperties.dataType=='dtInteger'>
	       <#assign dataType='I'>
	       <#assign searchType='EQ'>
	<#elseif field.dataProperties.dataType=='dtLong'>
	        <#assign dataType='L'>
	        <#assign searchType='EQ'>
	<#elseif field.dataProperties.dataType=='dtDouble'>
	        <#assign dataType='C'>
	        <#assign searchType='EQ'>
	<#elseif field.dataProperties.dataType=='dtDateTime'>
			<#assign dataType='D'>
	</#if>
	<#--判断控件比较类型结束--> 
	<#--循环查询列表-->  
	<#if (field.queryProperties.showInSearch)!false>
			<#assign fieldKey = field.key>
			<#assign fieldKeyId = field.key>
			<#if !form.isMaster>
				<#assign fieldKey = form.entityName + "|||" + fieldKey>
				<#assign fieldKeyId = form.key + "_" + fieldKeyId>
			</#if>
			<#if (field.editProperties.editType=='DateBox' || field.dataProperties.dataType=='dtDateTime')>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input type="text" name="filter_GE${dataType}_${fieldKey}"
								   id="query_${fieldKeyId}Start" readonly="readonly"
								   class="Idate Wdate" onClick="WdatePicker({dateFmt:<#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">'${field.editProperties.dtFormat}'<#else>'yyyy-MM-dd'</#if>,onpicked:function(dp) {if(!$dp.$('query_${fieldKeyId}End').value)$dp.$('query_${fieldKeyId}End').value=this.value;}})" style="width:160px;" />
							<br/>--<br/><input type="text" name="filter_LE${dataType}_${fieldKey}"
								   id="query_${fieldKeyId}End"
								   readonly="readonly" class="Idate Wdate"
								   onClick="WdatePicker({dateFmt:<#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">'${field.editProperties.dtFormat}'<#else>'yyyy-MM-dd'</#if>})" style="width:160px;" />
					</div>
			<#elseif field.editProperties.editType=='RichTextBox'>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<#if field.editProperties.dtFormat=='html'>
									<textarea class="jquery_ckeditor" id="query_${fieldKeyId}" name="filter_LIKES_${fieldKey}" ></textarea>
							<#else>
									<textarea name="filter_LIKES_${fieldKey}" id="query_${fieldKeyId}" ></textarea>
							</#if>
					</div> 
			<#-- 新增带搜索框的下拉控件 -->
			<#elseif field.editProperties.editType=='ComboBox' || field.editProperties.editType=='ComboBoxSearch'>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input name="filter_IN${dataType}_${fieldKey!}"
								   id="query_${fieldKeyId}" style="width:150px;" class="easyui-comboboxsearch" multiple="true" />
					</div>
			<#elseif field.editProperties.editType=='ComboTree'>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input  class="easyui-combotree" style="width:150px;"
								    name="filter_IN${dataType}_${fieldKey!}" id="query_${fieldKeyId}" multiple="true" />
					</div>
			<#elseif field.editProperties.editType=='ComboGrid'>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input class="easyui-combogrid" name="filter_IN${dataType}_${fieldKey!}"
								   id="query_${fieldKeyId}" style="width:150px;" multiple="true" />
					</div>
			<#elseif field.editProperties.editType=='CheckBox'>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input class="easyui-newcheckbox" name="filter_IN${dataType}_${fieldKey!}"
								   id="query_${fieldKeyId}" style="width:150px;" multiple="true" />
					</div>
			<#elseif field.editProperties.editType=='RadioBox'>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input class="easyui-newradiobox" name="filter_EQ${dataType}_${fieldKey!}"
								   id="query_${fieldKeyId}" style="width:150px;" />
					</div>
			<#elseif field.editProperties.editType=='ComboCheckBox'>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input class="easyui-combocheckbox" name="filter_IN${dataType}_${fieldKey!}"
								   id="query_${fieldKeyId}" style="width:150px;" multiple="true" />
					</div>
			<#elseif field.editProperties.editType=='ComboRadioBox'>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input class="easyui-comboradiobox" name="filter_EQ${dataType}_${fieldKey!}"
								   id="query_${fieldKeyId}" style="width:150px;" />
					</div>
			<#elseif (field.editProperties.editType=='TextBox') && (dataType=='L' || dataType=='I' || dataType=='C')>		
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input class="easyui-numberbox" name="filter_GE${dataType}_${fieldKey}" <#if dataType == "C">precision="${(field.dataProperties.scale)!0}"</#if>
								   id="query_${fieldKeyId}Start" groupSeparator="${(field.editProperties.dtFormat)!""}" />
							<br/>--<br/><input class="easyui-numberbox" name="filter_LE${dataType}_${fieldKey}" <#if dataType == "C">precision="${(field.dataProperties.scale)!0}"</#if>
								   id="query_${fieldKeyId}End" groupSeparator="${(field.editProperties.dtFormat)!""}" />
					</div>		
			<#--结束-->
			<#else>
					<div class="easyui-panel xpstyle" title="${formTitle}<s:text name="${field.i18nKey!field.caption}"/>" collapsible="true" collapsed="true">
							<input type="text" class="Itext" name="filter_${searchType}${dataType}_${fieldKey!}"
								   id="query_${fieldKeyId!}" />
					</div>
			</#if>
	</#if>
<#--循环结束-->      
</#if>
</#list>
</#if>
<#if !isForm && form.isMaster>
<div class="easyui-panel xpstyle" title="<s:text name="system.module.status.title"/>" collapsible="true" collapsed="true">
	<input type="checkbox" name="filter_INS_status" value="10"/><label><s:text name="system.module.status.10.title"/></label><br/>
	<input type="checkbox" name="filter_INS_status" value="20"/><label><s:text name="system.module.status.20.title"/></label><br/>
	<input type="checkbox" name="filter_INS_status" value="30"/><label><s:text name="system.module.status.30.title"/></label><br/>
	<input type="checkbox" name="filter_INS_status" value="31"/><label><s:text name="system.module.status.31.title"/></label><br/>
	<input type="checkbox" name="filter_INS_status" value="32"/><label><s:text name="system.module.status.32.title"/></label><br/>
	<input type="checkbox" name="filter_INS_status" value="35"/><label><s:text name="system.module.status.35.title"/></label><br/>
	<input type="checkbox" name="filter_INS_status" value="40"/><label><s:text name="system.module.status.40.title"/></label>
</div>
<div class="easyui-panel xpstyle" title="<s:text name="system.module.createUser.title"/>" collapsible="true" collapsed="true">
	<input type="text" name="filter_USERS_createUser" id="query_createUser" />
</div>		
</#if>                              
</#macro>
<#-- 初始化主表查询条件，isForm:是否自定义表单，控件显示位于主表查询显示列表的上方  -->
<#macro getMainQueryView form isForm=false >
<#assign fields=form.fieldList!>
<#assign index=0><#--给查询条件计数，给查询布局用的-->
<#if fields??&&fields?size gt 0>
<#list fields as field>
<#if field.key?? && field.key!="">
	<#--判断控件比较类型-->
	<#assign dataType='S'>
	<#assign searchType='LIKE'> 
	<#if field.dataProperties.dataType=='dtInteger'>
       <#assign dataType='I'>
       <#assign searchType='EQ'>
	<#elseif field.dataProperties.dataType=='dtLong'>
        <#assign dataType='L'>
        <#assign searchType='EQ'>
	<#elseif field.dataProperties.dataType=='dtDouble'>
        <#assign dataType='C'>
        <#assign searchType='EQ'>
	<#elseif field.dataProperties.dataType=='dtDateTime'>
		<#assign dataType='D'>
	</#if>
	<#--判断控件比较类型结束-->
	<#--循环查询列表-->
	<#if (field.queryProperties.showInSearch)!false>
		<#--查询条件计数加，如果有两个表单控件的加2，如果只有一个表单控件的加1-->
		<#if field.editProperties.editType=='DateBox' || field.dataProperties.dataType=='dtDateTime' || (field.editProperties.editType=='TextBox' && (dataType=='L' || dataType=='I' || dataType=='C'))>
			<#if index%5==0>
				<#assign index=index+1>
			<#else>
				<#assign index=index+2>
			</#if>
		<#else>
			<#assign index=index+1>
		</#if>
		<#--每行五个单元格，超过5个换行-->
		<#if index%5==1>
			<div class="nav-block J_commonBlock type-text"><table><tr><#--查询列表行元素-->
		</#if>
		<td><#--查询列表单元格-->
		<#if (field.editProperties.editType=='DateBox' || field.dataProperties.dataType=='dtDateTime')>
			<#if index%5==1><#assign index=index+1></#if><#--补上行第一个单元格，因换行少算的站位格-->
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input type="text" name="filter_GE${dataType}_${field.key}"
					   id="query_${field.key}Start" readonly="readonly"
					   class="Idate Wdate" onClick="WdatePicker({dateFmt:<#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">'${field.editProperties.dtFormat}'<#else>'yyyy-MM-dd'</#if>})" style="width:160px;" />
				&nbsp;-&nbsp;
				<input type="text" name="filter_LE${dataType}_${field.key}"
					   id="query_${field.key}End"
					   readonly="readonly" class="Idate Wdate"
					   onClick="WdatePicker({dateFmt:<#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">'${field.editProperties.dtFormat}'<#else>'yyyy-MM-dd'</#if>})" style="width:160px;" />
			</div>
		<#--文本域太高，不适合新查询布局
		<#elseif field.editProperties.editType=='RichTextBox'>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/>&nbsp;:&nbsp;</span>
				<#if field.editProperties.dtFormat=='html'>
						<textarea class="jquery_ckeditor" id="query_${field.key}" name="filter_LIKES_${field.key}" ></textarea>
				<#else>
						<textarea name="filter_LIKES_${field.key}" id="query_${field.key}" ></textarea>
				</#if>
			</div> 
		-->
		<#-- 新增带搜索框的下拉控件 -->
		<#elseif field.editProperties.editType=='ComboBox' || field.editProperties.editType=='ComboBoxSearch'>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input name="filter_IN${dataType}_${field.key!}"
					   id="query_${field.key}" style="width:150px;" class="easyui-comboboxsearch" multiple="true" />
			</div>
		<#elseif field.editProperties.editType=='ComboTree'>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input  class="easyui-combotree" style="width:150px;"
					    name="filter_IN${dataType}_${field.key!}" id="query_${field.key}" multiple="true" />
			</div>
		<#elseif field.editProperties.editType=='ComboGrid'>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input class="easyui-combogrid" name="filter_IN${dataType}_${field.key!}"
					   id="query_${field.key}" style="width:150px;" multiple="true" />
			</div>
		<#elseif field.editProperties.editType=='CheckBox'>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input class="easyui-newcheckbox" name="filter_IN${dataType}_${field.key!}"
					   id="query_${field.key}" style="width:150px;" multiple="true" />
			</div>
		<#elseif field.editProperties.editType=='RadioBox'>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input class="easyui-newradiobox" name="filter_EQ${dataType}_${field.key!}"
					   id="query_${field.key}" style="width:150px;" />
			</div>
		<#elseif field.editProperties.editType=='ComboCheckBox'>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input class="easyui-combocheckbox" name="filter_IN${dataType}_${field.key!}"
					   id="query_${field.key}" style="width:150px;" multiple="true" />
			</div>
		<#elseif field.editProperties.editType=='ComboRadioBox'>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input class="easyui-comboradiobox" name="filter_EQ${dataType}_${field.key!}"
					   id="query_${field.key}" style="width:150px;" />
			</div>
		<#elseif (field.editProperties.editType=='TextBox') && (dataType=='L' || dataType=='I' || dataType=='C')>		
			<#if index%5==1><#assign index=index+1></#if><#--补上行第一个单元格，因换行少算的站位格-->
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input class="easyui-numberbox" name="filter_GE${dataType}_${field.key}" <#if dataType == "C">precision="${(field.dataProperties.scale)!0}"</#if>
					   id="query_${field.key}Start" groupSeparator="${(field.editProperties.dtFormat)!""}" />
				&nbsp;-&nbsp;
				<input class="easyui-numberbox" name="filter_LE${dataType}_${field.key}" <#if dataType == "C">precision="${(field.dataProperties.scale)!0}"</#if>
					   id="query_${field.key}End" groupSeparator="${(field.editProperties.dtFormat)!""}" />
			</div>		
		<#else>
			<div class="cell">
				<span><s:text name="${field.i18nKey!field.caption}"/></span>&nbsp;:&nbsp;
				<input type="text" class="Itext" name="filter_${searchType}${dataType}_${field.key!}"
					   id="query_${field.key!}" />
			</div>
		</#if>
		</td><#--查询列表单元格结束-->
		<#--每行五个单元格，超过5个换行-->
		<#if index%5==0>
		</tr></table></div>
		</#if>
	</#if>
	<#--循环结束-->
</#if>
</#list>
</#if>
<#if !isForm>
	<#if index%5==0>
		<#assign index=index+1>
	<#else>
		<#assign index=index+2>
	</#if>
	<#--每行五个单元格，超过5个换行-->
	<#if index%5==1>
		<div class="nav-block J_commonBlock type-text"><table><tr><#--查询列表行元素-->
	</#if>
	<td>
		<div class="cell">
			<span><s:text name="system.module.status.title"/></span>&nbsp;:&nbsp;
			<input type="checkbox" name="filter_INS_status" value="10"/><span><s:text name="system.module.status.10.title"/></span>
			<input type="checkbox" name="filter_INS_status" value="20"/><label><s:text name="system.module.status.20.title"/></label>
			<input type="checkbox" name="filter_INS_status" value="30"/><label><s:text name="system.module.status.30.title"/></label>
			<input type="checkbox" name="filter_INS_status" value="31"/><label><s:text name="system.module.status.31.title"/></label>
			<input type="checkbox" name="filter_INS_status" value="32"/><label><s:text name="system.module.status.32.title"/></label>
			<input type="checkbox" name="filter_INS_status" value="35"/><label><s:text name="system.module.status.35.title"/></label>
			<input type="checkbox" name="filter_INS_status" value="40"/><label><s:text name="system.module.status.40.title"/></label>
		</div>
	</td>
	<#assign index=index+1><#--查询条件计数加一-->
	<#--每行五个单元格，超过5个换行-->
	<#if index%5==1>
		</tr></table></div><div class="nav-block J_commonBlock type-text"><table><tr><#--查询列表行元素-->
	</#if>
	<td>
		<div class="cell">
			<span><s:text name="system.module.createUser.title"/></span>&nbsp;:&nbsp;
			<input type="text" name="filter_USERS_createUser" id="query_createUser" />
		</div>
	</td>
	<#assign index=index+1><#--查询条件计数加一-->
	<#--每行五个单元格，超过5个换行-->
	<#if index%5==1>
		</tr></table></div><div class="nav-block J_commonBlock type-text"><table align="center"><tr><#--查询列表行元素-->
	</#if>
	<td>
		<div class="cell" <#if index%5==1>style="border-left:0px;"</#if>>
	        <button type="button" id="bt_query" class="button_small">
			    <s:text name="system.search.button.title"/>
			</button>&nbsp;&nbsp;
			<button type="button" id="bt_reset" class="button_small">
			    <s:text name="system.search.reset.title"/>
			</button>
		</div> 
	</td>
	<#if index%5==0>
		</tr></table></div>
	</#if>
</#if>
<#if index%5!=0>
</tr></table></div>
</#if>
</#macro>

<#-- 初始化控件数据方法封装  -->
<#macro initControlsData field type="query" prefix="" entityName="" formKey="" moduleTitle="">
							<#if field.key?? && field.key!="">
								<#assign fieldKey = prefix + field.key/>
								<#assign editType = field.editProperties.editType/>
								<#if field.editProperties.formula?? && field.editProperties.formula.type??>
					            <#if field.editProperties.formula.type==1> <#-- 固定值 -->
					               <#assign itemList=field.editProperties.formula.itemList/>
				                     ${fieldKey}Json = [];
				                     <#if itemList??&&itemList?size gt 0>
							         ${fieldKey}Json =
							         [<#list itemList as item>
							          {"key":'${item.key}',"caption":'${item.caption}'} <#if item_has_next> ,</#if>
						             </#list> ];
						             ${fieldKey}Obj = {"key":"key","caption":"caption"};
						             ${fieldKey}JsonObj={<#list itemList as item>"${item.key}":'${item.caption}'<#if item_has_next> ,</#if></#list>};
						             <#if type=="datagrid">
		                                function get${fieldKey}Value(value, rowData, rowIndex) {
		                                    var tmpCap = ${fieldKey}JsonObj[value];
		                                    if(!tmpCap) {
		                                    	tmpCap = "";
		                                    }
		                                    return tmpCap;
		                                }
						             <#elseif type=="subQuery">
						             		<#if editType=="ComboBox" || editType=="ComboBoxSearch">
											  ${fieldKey}Json=transComboboxData(${fieldKey}Json,${fieldKey}Obj);
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											<#elseif editType=="ComboTree">
											  ${fieldKey}Json=transCombotreeData(${fieldKey}Json,${fieldKey}Obj,${((field.editProperties.autoCollapse)!false)?string});
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											<#elseif editType=="ComboGrid">
											  ${fieldKey}Json=transCombogridData(${fieldKey}Json,${fieldKey}Obj);
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											<#elseif editType=="CheckBox" || editType=="ComboCheckBox">
											  ${fieldKey}Json=transCheckboxData(${fieldKey}Json,${fieldKey}Obj);
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											<#elseif editType=="RadioBox" || editType=="ComboRadioBox">
											  ${fieldKey}Json=transRadioboxData(${fieldKey}Json,${fieldKey}Obj);
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											</#if>  
						             <#else>
						             	<#if editType=="ComboBox">
						             	  selectinit(${fieldKey}Json, "${fieldKey}", ${fieldKey}Obj);
						             	<#elseif editType=="ComboBoxSearch">
						             	  comboboxsearchinit(${fieldKey}Json, "${fieldKey}", ${fieldKey}Obj);
						             	<#elseif editType=="ComboCheckBox">
										  comboCheckInit(${fieldKey}Json, "${fieldKey}", ${fieldKey}Obj);
										<#elseif editType=="ComboRadioBox">
										  comboRadioInit(${fieldKey}Json, "${fieldKey}", ${fieldKey}Obj);
										<#elseif editType=="CheckBox">
										  newCheckBoxInit(${fieldKey}Json, "${fieldKey}", ${fieldKey}Obj);
										<#elseif editType=="RadioBox">
										  newRadioBoxInit(${fieldKey}Json, "${fieldKey}", ${fieldKey}Obj);  
										</#if>
						             </#if>
			                         </#if>
					             <#elseif field.editProperties.formula.type==2 || field.editProperties.formula.type==5>
					             	 <#assign sql="" />
					             	 <#if field.editProperties.formula.type==2><#--sql数据源-->
						             	 <#assign refSqlKey = (field.editProperties.formula.refKey)!""/>
						             	 <#if refSqlKey != "">
						             		<#assign sqlList = field.editProperties.sqlList/>
						             		<#if sqlList??&&sqlList?size gt 0>
						             			<#list sqlList as sq>
						             				<#if sq.key == refSqlKey>
						             				<#assign sql=sq />
						             				<#break/>
						             				</#if>
						             			</#list>		
						             		</#if>
						             	 </#if>	
						             <#elseif field.editProperties.formula.type==5><#--模块数据源-->
						             	<#assign sql=field.editProperties.formula.sql />
						             </#if>
					             	 ${fieldKey}JsonParam = {};
					             	 ${fieldKey}JsonParam["entityName"] = "${entityName}.${field.key}.${sql.key}";
					             	 <#if field.editProperties.formula.type==5>
									 ${fieldKey}JsonParam["style"] = "module";
									 </#if>
									 <#if (field.editProperties.autoComplete)!false ><#-- 自动完成 -->
									 ${fieldKey}JsonParam["rows"] = 10;
									 </#if>
									 ${fieldKey}Obj = {};
									 <#assign items=(sql.setValueItemList)! />
									 <#list items as item>
									 	 <#if editType=="ComboGrid">
									 	 ${fieldKey}Obj["${item.key}"] = {"column":"${item.column}","caption":"${item.caption}"};
										 <#else>
										 ${fieldKey}Obj["${item.key}"] = "${item.column}";
										 </#if>
									 </#list>
									 <#if type=="datagrid">
									 		${fieldKey}JsonParam["appendFlag"] = "false";
		                                    ${fieldKey}Json = getJsonObjByParam(${fieldKey}JsonParam);
		                                    function get${fieldKey}Value(value, rowData, rowIndex) {
		                                    	<#if editType=="ComboTree">
		                                    	return getColumnValueByIdText(${fieldKey}Obj, ${fieldKey}Json, value);
		                                    	<#else>
		                                    	return getColumnValueByKeyCaption(${fieldKey}Obj, ${fieldKey}Json, value);
		                                    	</#if>
		                                    }
		                                    <#if field.editProperties.quickAdd> <#-- 快速添加数据，刷新数据 -->
		                                    doRefreshDataGridFieldDataFor_${field.key} = function() {
										 		${fieldKey}Json = getJsonObjByParam(${fieldKey}JsonParam);
										 	};
		                                    </#if>
									 <#elseif type=="subQuery">
										 	<#if editType=="ComboBox" || editType=="ComboBoxSearch">
										 	  <#-- 
										 	  <#if (sql.sqlWhereItemList)?? && 
											    (sql.sqlWhereItemList[0])??>
											    <#assign selIdKey=sql.sqlWhereItemList[0].key/>
											    ${fieldKey}Json=transComboboxData([],${fieldKey}Obj);
											    $("#${selIdKey}").combobox({onChange:function(newValue,oldValue){
											    	var select1value = newValue+"";
											       <#assign dataType='S'>
											       <#if field.dataProperties.dataType=='dtInteger'>
											           <#assign dataType='I'>
											       <#elseif field.dataProperties.dataType=='dtLong'>
											            <#assign dataType='L'>
											        </#if>
											     	if(select1value && select1value.length>0) {
											          ${fieldKey}JsonParam["filter_EQ${dataType}_${selIdKey}"] = select1value;
											          ${fieldKey}Json=transComboboxData(getJsonObjByParam(${fieldKey}JsonParam),${fieldKey}Obj);
											        } else {
											          ${fieldKey}Json=transComboboxData([],${fieldKey}Obj);
											        }
											        jwpf.updateTableComboFieldDataOption("${formKey}", "${field.key}", ${fieldKey}Json.data);
											    }});                        
										      <#else>
											   ${fieldKey}Json=transComboboxData(getJsonObjByParam(${fieldKey}JsonParam),${fieldKey}Obj);
											  </#if>-->
											  ${fieldKey}Json=transComboboxData(getJsonObjByParam(${fieldKey}JsonParam),${fieldKey}Obj);
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											<#elseif editType=="ComboTree">
											  ${fieldKey}Json=transCombotreeData(getJsonObjByParam(${fieldKey}JsonParam),${fieldKey}Obj,${((field.editProperties.autoCollapse)!false)?string});
											  ${fieldKey}JsonFt = ${fieldKey}Json; 
											<#elseif editType=="ComboGrid">
											  ${fieldKey}Json=transCombogridData(getJsonObjByParam(${fieldKey}JsonParam),${fieldKey}Obj);
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											<#elseif editType=="CheckBox" || editType=="ComboCheckBox">
											  ${fieldKey}Json=transCheckboxData(getJsonObjByParam(${fieldKey}JsonParam),${fieldKey}Obj);
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											<#elseif editType=="RadioBox" || editType=="ComboRadioBox">
											  ${fieldKey}Json=transRadioboxData(getJsonObjByParam(${fieldKey}JsonParam),${fieldKey}Obj);
											  ${fieldKey}JsonFt = ${fieldKey}Json;
											</#if>  
									 <#else>
										  <#assign initFunc = ""/>
										  <#if editType=="ComboBox">
										    <#if !((field.editProperties.autoComplete)!false) >
											     <#if (sql.sqlWhereItemList)?? && 
												    (sql.sqlWhereItemList[0])??>
												    selectinit(null, "${fieldKey}", null);
												    <#assign selIdKey=sql.sqlWhereItemList[0].key/>
												    $("#${prefix}${selIdKey}").combobox({onChange:function(newValue,oldValue){
												    	var select1value = newValue+"";
												       <#assign dataType='S'>
												       <#if field.dataProperties.dataType=='dtInteger'>
												           <#assign dataType='I'>
												       <#elseif field.dataProperties.dataType=='dtLong'>
												            <#assign dataType='L'>
												        </#if>
												     	if(select1value && select1value.length>0) {
												          ${fieldKey}JsonParam["filter_EQ${dataType}_${selIdKey}"] = select1value;
												          initCommonData("${fieldKey}", ${fieldKey}JsonParam, ${fieldKey}Obj,selectinit, null, false);
												        } else {
												          selectinit(null, "${fieldKey}", null);
												        }
												     }});                        
											     <#else>
											       <#assign initFunc = "selectinit"/>
												  </#if>
											<#else> <#-- 初始化自动完成控件 -->
												autoCompleteSelectInit("${fieldKey}", ${fieldKey}JsonParam);
											</#if>
										<#elseif editType=="ComboBoxSearch">
										    <#if !((field.editProperties.autoComplete)!false) >
											     <#if (sql.sqlWhereItemList)?? && 
												    (sql.sqlWhereItemList[0])??>
												    comboboxsearchinit(null, "${fieldKey}", null);
												    <#assign selIdKey=sql.sqlWhereItemList[0].key/>
												    $("#${prefix}${selIdKey}").comboboxsearchinit({onChange:function(newValue,oldValue){
												    	var select1value = newValue+"";
												       <#assign dataType='S'>
												       <#if field.dataProperties.dataType=='dtInteger'>
												           <#assign dataType='I'>
												       <#elseif field.dataProperties.dataType=='dtLong'>
												            <#assign dataType='L'>
												        </#if>
												     	if(select1value && select1value.length>0) {
												          ${fieldKey}JsonParam["filter_EQ${dataType}_${selIdKey}"] = select1value;
												          initCommonData("${fieldKey}", ${fieldKey}JsonParam, ${fieldKey}Obj,selectinit, null, false);
												        } else {
												          comboboxsearchinit(null, "${fieldKey}", null);
												        }
												     }});                        
											     <#else>
											       <#assign initFunc = "selectinit"/>
												  </#if>
											<#else> <#-- 初始化自动完成控件 -->
												autoCompleteComboBoxSearchInit("${fieldKey}", ${fieldKey}JsonParam);
											</#if>
										<#elseif editType=="ComboTree">
										  <#assign initFunc = "comnbotreeInit"/>
										  <#if (field.editProperties.autoCollapse)!false >
										   <#assign initFunc = "comnbotreeInitWithCollapse"/>
										  </#if> 
										<#elseif editType=="ComboGrid">
										  <#assign initFunc = "combogridInit"/>
										<#elseif editType=="ComboCheckBox">
										  <#assign initFunc = "comboCheckInit"/>
										<#elseif editType=="ComboRadioBox">
										  <#assign initFunc = "comboRadioInit"/>
										<#elseif editType=="CheckBox">
										  <#assign initFunc = "newCheckBoxInit"/>
										<#elseif editType=="RadioBox">
										  <#assign initFunc = "newRadioBoxInit"/>
										<#elseif editType=="TextBox">
											  <#assign dataType = field.dataProperties.dataType />
											  <#if dataType=="dtLong" || dataType=="dtDouble">
											    <#assign initFunc = "numberBoxValueInit"/>
											  <#else>
											    <#assign initFunc = "textBoxValueInit"/>
											  </#if>
										<#elseif editType=="zTree">
										  <#assign initFunc = "zTreeInit"/>
										  <#if (field.editProperties.autoCollapse)!false >
										   <#assign initFunc = "zTreeInitWithCollapse"/>
										  </#if> 
										<#elseif editType=="DataGrid">
										 	<#assign initFunc = "dataGridInit"/>	
										 </#if>
										 	 
										 <#if initFunc != "">
										  	initCommonData("${fieldKey}", ${fieldKey}JsonParam, ${fieldKey}Obj, ${initFunc},null,false);
										 	<#if field.editProperties.quickAdd>
										 		<#if prefix == ""> <#-- 编辑页面快速添加数据 -->
										 	quickAddFor_${field.key} = function() {<#-- 打开编辑页面函数 -->
												var idVal = jwpf.getFormVal("${field.key}");
												var qUrl = "${r"${ctx}"}/gs/gs-mng!add.action?entityName=${sql.entityName}&afterSaveFunc=doAfterQuickAddFor_${field.key}&isEdit=3";
												var qTitle = "<s:text name="新增数据"/>";
												if(idVal) { <#-- 修改 -->
													qUrl = "${r"${ctx}"}/gs/gs-mng!view.action?entityName=${sql.entityName}&afterSaveFunc=doAfterQuickAddFor_${field.key}&isEdit=1&id=" + idVal;
													qTitle = "<s:text name="修改数据"/>";
												}
										 		doOpenModuleOperWin(qUrl,qTitle,800,600,true);
										 	};
										 	doAfterQuickAddFor_${field.key} = function(id) {<#-- 保存后调用函数 -->
										 		var subCallFunc = function() {
										 			jwpf.setFormVal("${field.key}", id);
										 		};
										 		initCommonData("${fieldKey}", ${fieldKey}JsonParam, ${fieldKey}Obj, ${initFunc}, subCallFunc);
										 		doCloseModuleOperWin();
										 		if(top.getTabDataWin) {
										 			var listWin = top.getTabDataWin("<s:text name="${moduleTitle}"/>");
										 			if(listWin) {
										 				if(listWin.doRefreshQueryFieldDataFor_${field.key}) {
										 					listWin.doRefreshQueryFieldDataFor_${field.key}();
										 				}
										 				if(listWin.doRefreshDataGridFieldDataFor_${field.key}) {
										 					listWin.doRefreshDataGridFieldDataFor_${field.key}();
										 				}
										 			}
										 		}
										 	};
										 		<#else><#--列表页面快速数据-->
										 	doRefreshQueryFieldDataFor_${field.key} = function() {
										 		initCommonData("${fieldKey}", ${fieldKey}JsonParam, ${fieldKey}Obj, ${initFunc});
										 	};	
										 		</#if>
										 	</#if>
										 </#if>
									 </#if>
					       </#if>
					       </#if>
					       <#if editType=="ImageBox">
		                 		<#if type=="query">
		                 		$("#${fieldKey}").imagebox({
		                 			vPath:"${r"${systemParam.virtualPicPath}"}"
		                 		});
					         	</#if>
					       <#-- <#elseif editType=="DateBox">
					       		var curDateVal = "${(field.editProperties.defaultValue)!''}" || new Date().format("yyyy-MM-dd HH:mm:ss");
					        	var dateVal = fnFormatDate(curDateVal, <#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">'${field.editProperties.dtFormat}'<#else>'yyyy-MM-dd'</#if>);
						   		$("#${field.key}").val(dateVal);-->
						   <#elseif editType=='ProgressBar'>
						   		$("#${fieldKey}").processbarSpinner();
						   <#elseif editType=="DictTree">
						   		${fieldKey}Obj = {"id":"id","pid":"pid","text":"name"};
						   		${fieldKey}JsonParam = {"type":"${entityName}.${field.key}","url":"${r"${ctx}"}/sys/common/common-type!queryList.action"};
						   		<#if ((field.editProperties.dtFormat)!"")!=""><#-- 系统分类树 -->
						   		${fieldKey}JsonParam["type"] = "${field.editProperties.dtFormat}";
						   		${fieldKey}JsonParam["isSys"] = "1";
						   		</#if>
						   		<#if type=="datagrid">
						   		${fieldKey}Json = getJsonObjByParam(${fieldKey}JsonParam);
		                        function get${fieldKey}Value(value, rowData, rowIndex) {
		                            return getColumnValueByIdText(${fieldKey}Obj, ${fieldKey}Json, value);
		                        }
						   		<#elseif type=="subQuery">
								
						   		<#else>
						   		          <#assign initFunc = "comnbotreeInit"/>
						   				  <#if (field.editProperties.autoCollapse)!false >
										   <#assign initFunc = "comnbotreeInitWithCollapse"/>
										  </#if> 
						   		initCommonData("${fieldKey}", ${fieldKey}JsonParam, ${fieldKey}Obj,${initFunc},null,false);
						   		</#if>
						   <#elseif editType=='DialogueWindow' && field.editProperties.visible>
						     <#if type=="query">
						   		$("#${fieldKey}").dialoguewindow({
		                 			title:"${field.caption}",
		                 			entityName:${fieldKey}JsonParam.entityName,
		                 			style:${fieldKey}JsonParam.style
		                 			<#if field.editProperties.width??>
		                 			,width:${field.editProperties.width}
		                 			</#if>
		                 			<#if field.editProperties.height??>
		                 			,heigth:${field.editProperties.height}
		                 			</#if>
		                 		});
		                 	 </#if>
						   </#if>
			            </#if>
</#macro>
<#-- 初始化子表datagrid字段属性方法 -->
<#macro initSubQuery field form prefix="">
					<#if field.key?? && field.key!="">
								<#assign formKey = form.key />
								<#assign fieldKey = prefix + field.key/>
								<#assign editType = field.editProperties.editType/>
								<#assign dataType='S'>
								<#if field.dataProperties.dataType=='dtInteger'>
								       <#assign dataType='I'>
								<#elseif field.dataProperties.dataType=='dtLong'>
								        <#assign dataType='L'>
								<#elseif field.dataProperties.dataType=='dtDouble'>
								        <#assign dataType='C'>
								<#elseif field.dataProperties.dataType=='dtDateTime'>
										<#assign dataType='D'>
								</#if>
								{
											<#-- 新增列属性，根据值相同合并单元格 -->
											<#if (field.queryProperties.mergeable)!false>mergeable : true,</#if>
								            <#if !field.isCaption>field : '${field.key}',</#if>
                                            <#if field.editProperties.rowspan??>rowspan : ${field.editProperties.rowspan},</#if>
                                            <#if field.editProperties.colspan??>colspan : ${field.editProperties.colspan},</#if>
                                            <#if (field.dataProperties.notNull)!false >
                                            required: true,
                                            </#if>
                                            <#if field.enableRevise>jwRevise:true,</#if><#-- 启用修订 -->
                                            jwDataType:"${dataType}",<#-- 字段类型 -->
                                            title : '<#if (field.dataProperties.notNull)!false ><span style="color:red;">*</span></#if><s:text name="${field.i18nKey!field.caption}"/><@initFieldSuffix field=field isInList=true />',
                                            width : ${field.queryProperties.width!200},
                                            sortable : <#if (field.queryProperties.sortable)!false >true<#else>false</#if>,
                                            <#if (field.queryProperties.filterable)!false >isFilterData:true,</#if> <#-- 过滤文本 -->
                                            align : '<#if field.dataProperties.dataType=="dtDouble">right<#else>${(field.queryProperties.align)!"left"}</#if>'
                                            <#if (editType=="DateBox" || field.dataProperties.dataType == "dtDateTime")>
                                             ,formatter: function(value,row,index){
												return fnFormatDate(value,<#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">'${field.editProperties.dtFormat}'<#else>'yyyy-MM-dd'</#if>);
											 }
											<#elseif editType=="RichTextBox">
											 <#if field.editProperties.dtFormat!='html'>
                                             ,formatter: function(value,row,index){
												return fnFormatText(value);
											 }
											 </#if>
											<#elseif editType=="Button">
											 ,formatter: function(value,row,index){
											 	if(!row._isFooter_) {
											 		if(!$.isNumeric(index)) {
											 			index = row["autoCode"];
											 		}
													return __getListButton(value, "${field.key}", "${formKey}", index, 
													"<#if (field.editProperties.defaultValue)?? && (field.editProperties.defaultValue)!="">${field.editProperties.defaultValue}<#else><s:text name="${field.i18nKey!field.caption}"/></#if>",
													${(field.editProperties.validInView)?string});
											 	}
											 }
											<#elseif editType=="HyperLink">
												 ,formatter: function(value,row,index){
												 	if(!row._isFooter_) {
												 		if(!$.isNumeric(index)) {
												 			index = row["autoCode"];
												 		}
														var dfValue = "${(field.editProperties.defaultValue)!""}";
														if(!dfValue) {
															if(value) {
																dfValue = value;
															} else {
																dfValue = "<s:text name="${field.i18nKey!field.caption}"/>";
															}
														}
														return __getListHyperLink(value, "${field.key}", "${formKey}", index, dfValue,${(field.editProperties.validInView)?string});
												 	}
												 } 	
											<#elseif editType=="ImageBox">
											<%-- 格式化datagrid单元格，如果单元格value不为空添加图片
											        图片宽高度为设计器编辑属性的宽高，因为datagrid单元格存在默认的padding：4px样式，宽度自动减去8像素
											        添加双击事件，调用imgperview插件的gzoomExpand（图片路径）方法展示图片 --%>
											,formatter: function formatterImage(value, rowData, rowIndex){
												       if(value) {
												    	   //var path="/jwpf/upload/gs-attach/"+value;
												    	   var path="${r"${systemParam.virtualPicPath}"}"+value;
												           return "<div ondblclick='$(body).gzoomExpand(\""+path+"\")'><img src='"+path+"' width='${(field.editProperties.width!72)-8}px' height='${field.editProperties.height!64}px' /></div>"; 
												       } else {
												           //var path="${r"${imgPath}"}/undefine.png";
												           var path="#";
												           return "<div ondblclick='$(body).gzoomExpand(\""+path+"\")'><img src='"+path+"' width='${(field.editProperties.width!72)-8}px' height='${field.editProperties.height!64}px' /></div>";
												       }
												  }
											<#else>
												<#if field.editProperties.formula?? && field.editProperties.formula.type?? 
													&& field.editProperties.formula.type != 4 && !((field.editProperties.autoComplete)!false)>
													<#if editType!="DialogueWindow">
		                                                ,formatter:function(value,row,index){
		                                                <#if editType=="ComboBox" || editType=="ComboBoxSearch" || editType=="CheckBox" || editType=="RadioBox" || editType=="ComboCheckBox" || editType=="ComboRadioBox">
		                                                 return getColumnValue(${fieldKey}JsonFt.obj.key, ${fieldKey}JsonFt.obj.caption, ${fieldKey}JsonFt.data, value);
		                                                <#elseif editType=="ComboGrid">
		                                                 return getColumnValue("key", "caption", ${fieldKey}JsonFt.data.rows, value);
		                                                 <#elseif editType=="ComboTree">
		                                                  return getColumnValue("id", "text", ${fieldKey}JsonFt.treeData, value);
		                                                </#if>
		                                                }
		                                            </#if>
		                                         </#if>	
                                            </#if>
                                            <#if field.dataProperties.dataType=='dtDouble'>
	                                        ,formatter: function(value,row,index) {
	                                        	if($.isNumeric(value)) {
	                                        		return accounting.formatNumber(value, ${(field.dataProperties.scale)!0}, "");
	                                        	}
                                           	}                   		
	                                        </#if>
                                            <#assign eventList = field.editProperties.eventList/>
                                            <#if eventList?? && eventList?size gt 0>
                                            	<#list eventList as event>
                                            		<#if event.key == "onStyler">
                                            			,styler: function(${event.params}) {
                                            				${event.content}
                                            			}
                                            		<#elseif event.key == "onFormatter">
                                            			,formatter: function(${event.params}) {
                                            				${event.content}
                                            			}	
                                            		</#if>
                                            	</#list>
                                            </#if>                		
                                            <#if editType!="Button" && editType!="HyperLink">
                                            ,editor : {
                                            				<#if editType=="DialogueWindow">
	                                                       		 type : 'dialoguewindow',
	                                                             options : {
	                                                                 title : '${field.caption}',
		                 											 entityName : ${fieldKey}JsonParam.entityName,
		                 											 style : ${fieldKey}JsonParam.style
		                 											 <#if field.queryProperties.width??>
		                 											 ,width : ${field.queryProperties.width}
		                 											 </#if>
		                 											 <@initFieldOnListEvent field=field formKey=formKey />
	                                                             }
	                                                        <#elseif editType=="DateBox">
	                                                       		 type : 'my97',
	                                                             options:{
	                                                                 <@initCheckFieldReadOnlyAuth field=field form=form isReadOnly=true/>
	                                                                 dateFmt:<#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">"${field.editProperties.dtFormat}"<#else>"yyyy-MM-dd"</#if>  
	                                                                 <#if field.editProperties.minDate?? && field.editProperties.minDate != "">,minDate:"${field.editProperties.minDate}"</#if>
	                                                                 <#if field.editProperties.maxDate?? && field.editProperties.maxDate != "">,maxDate:"${field.editProperties.maxDate}"</#if>
	                                                                 <@initFieldOnListEvent field=field formKey=formKey />                                                      	    
	                                                             }
                                                             <#-- 编辑器类型ImageBox是自定义的编辑器类型，定义在
                                                             		 easyui.extend.js扩展datagrid编辑器，增加imagebox支持
                                                             		 options参数依次为：图片宽度，高度，上传图片存放根路径-->
                                                             <#elseif editType=="ImageBox">
                                                           	 	type:'ImageBox',
                                                           	 	options:{
                                                           	 		width:${(field.editProperties.width!72)-8},
                                                           	 		height:${field.editProperties.height!64},
												    	   			vPath:'${r"${systemParam.virtualPicPath}"}'
                                                           	 		//vPath:"/jwpf/upload/gs-attach/"
                                                           	 	}
	                                                        <#elseif editType=="ComboBox">
	                                                           	 type:'combobox',
	                                                        <#elseif editType=="ComboBoxSearch">
	                                                           	 type:'comboboxsearch',
	                                                        <#elseif editType=="ComboGrid">
	                                                       		 type:'combogrid',
	                                                        <#elseif editType=="ComboTree">
	                                                             type:'combotree',
	                                                        <#elseif editType=="ComboCheckBox">
	                                                             type:'combocheck',
	                                                        <#elseif editType=="ComboRadioBox">
	                                                             type:'comboradio',
	                                                        <#elseif editType=="CheckBox">
	                                                             type:'newcheckbox',
	                                                        <#elseif editType=="RadioBox">
	                                                             type:'newradiobox',     
	                                                        <#elseif editType=='RichTextBox'>
	                                                             type:'textarea',
	                                                             options:{
		                                                             <@initCheckFieldReadOnlyAuth field=field form=form isReadOnly=true/>
			                                                         "isUsed":true
			                                                         <@getValidTypeInDatagrid field=field form=form/>
	                                                             }
	                                                        <#else>
	                                                           <#if field.dataProperties.dataType=="dtLong" || field.dataProperties.dataType=="dtInteger"
	                                                           		|| field.dataProperties.dataType=='dtDouble'>
	                                                                type : 'numberbox',
	                                                                options:{
	                                                                	<#if field.dataProperties.dataType=='dtDouble'>
	                                                                	precision:"${(field.dataProperties.scale)!0}",
	                                                                	</#if>
	                                                                	<@initCheckFieldReadOnlyAuth field=field form=form isReadOnly=true/>
	                                                                	groupSeparator:"${(field.editProperties.dtFormat)!""}"
	                                                                	<@initFieldOnListEvent field=field formKey=formKey />
	                                                                	<@getValidTypeInDatagrid field=field form=form/>
	                                                                }
	                                                           <#else>
	                                                                type : 'textbox',
		                                                            options:{
		                                                            	<@initCheckFieldReadOnlyAuth field=field form=form isReadOnly=true/>
		                                                             	"isUsed":true
		                                                             	<@initFieldOnListEvent field=field formKey=formKey />
		                                                             	<@getValidTypeInDatagrid field=field form=form/>
		                                                            }
	                                                           </#if>
	                                                        </#if>
                                                        <#if editType=="ComboBox" || editType=="ComboBoxSearch" || editType=="ComboTree"
                                                       || editType=="CheckBox" || editType=="RadioBox"
                                                       || editType=="ComboCheckBox" || editType=="ComboRadioBox">
                                                         options:{
                                                                  <#if !((field.editProperties.autoComplete)!false) ><#-- 自动完成 -->
                                                                  valueField:${fieldKey}Json.obj.key,
			                            					      textField:${fieldKey}Json.obj.caption,
			                            					      </#if>
			                            					      <@initCheckFieldReadOnlyAuth field=field form=form isReadOnly=true/>
	                                                              <#if (field.editProperties.multiple)!false>
	                                                              multiple:true,
	                                                              </#if>
	                                                              <#if (field.editProperties.autoComplete)!false ><#-- 自动完成 -->
	                                                                mode:"remote",
																	hasDownArrow:false,
																	autoComplete:true,
																	url:"${r"${ctx}"}/gs/gs-mng!queryAjaxResult.action",
																	valueField:"key",
																	textField:"caption",
																	onBeforeLoad:function(p) {
																		$.extend(p, ${fieldKey}JsonParam);
																	}
	                                                              <#else>data:${fieldKey}Json.data</#if>
			                            					      <#if editType=="ComboTree">,lines:true</#if>
			                            					      <@initFieldOnListEvent field=field formKey=formKey />
                            							}
                            							<#elseif editType=="ComboGrid">
                            							 options:{
                                                                    panelWidth:400,
																    idField:'key',
																    textField:'caption',
			                                                        <@initCheckFieldReadOnlyAuth field=field form=form isReadOnly=true/>			                                                      
		                                                            <#if (field.editProperties.multiple)!false>
		                                                            multiple:true,
		                                                            </#if>
																    frozenColumns:[${fieldKey}Json.fColumns],
																    columns:[${fieldKey}Json.obj],
			                            					        data:${fieldKey}Json.data
			                            					        <@initFieldOnListEvent field=field formKey=formKey />
                            							}
                            							</#if>
													}
													</#if>
                                        }
                    </#if>                    
</#macro>
<#-- 初始化主表datagrid查询formatter方法 -->
<#macro initMainQuery field prefix="" form=null appendCommaFirst=false>
						<#if field.key?? && field.key!="">	
							<#assign fieldKey = prefix + field.key/>
							<#assign editType = field.editProperties.editType/>
								<#if appendCommaFirst>,</#if>			
											{
												<#if !field.isCaption>field : '${field.key}',</#if>
	                                            <#if field.editProperties.rowspan??>rowspan : ${field.editProperties.rowspan},</#if>
	                                            <#if field.editProperties.colspan??>colspan : ${field.editProperties.colspan},</#if>
	                                            title : '<s:text name="${field.i18nKey!field.caption}"/><@initFieldSuffix field=field isInList=true />',
	                                            width : ${field.queryProperties.width!200},
	                                            sortable : <#if (field.queryProperties.sortable)!false >true<#else>false</#if>,
	                                            <#if (field.queryProperties.filterable)!false >isFilterData:true,</#if> <#-- 过滤文本 -->
	                                            align : '<#if field.dataProperties.dataType=="dtDouble">right<#else>${(field.queryProperties.align)!"left"}</#if>'
	                                            <#if field.editProperties.formula?? && field.editProperties.formula.type?? && field.editProperties.formula.type != 4 && !((field.editProperties.autoComplete)!false)>
	                                              ,
	                                                formatter:get${fieldKey}Value
	                                            </#if>
	                                            <#if editType=='DictTree'>
	                                            ,
	                                                formatter:get${fieldKey}Value
	                                            <#elseif editType=='ImageBox'>
	                                            ,
	                                                formatter:formatterImage
	                                            <#elseif (editType=='DateBox' || field.dataProperties.dataType == "dtDateTime")>
	                                            ,
	                                                formatter: function(value,row,index){
														return fnFormatDate(value, <#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">'${field.editProperties.dtFormat}'<#else>'yyyy-MM-dd'</#if>);
													}
	                                            <#elseif editType=="RichTextBox">
	                                             <#if field.editProperties.dtFormat!='html'>
	                                             ,formatter: function(value,row,index){
														return fnFormatText(value);
												 } 
												 </#if>
												<#elseif editType=="Button">
												 ,formatter: function(value,row,index){
												 	if(!row._isFooter_) {
												 		if(!$.isNumeric(index)) {
												 			index = row["autoCode"];
												 		}
														return __getListButton(value, "${field.key}", "${form.key}", index, "<#if (field.editProperties.defaultValue)?? && (field.editProperties.defaultValue)!="">${field.editProperties.defaultValue}<#else><s:text name="${field.i18nKey!field.caption}"/></#if>");
												 	}
												 }
												<#elseif editType=="HyperLink">
												 ,formatter: function(value,row,index){
												 	if(!row._isFooter_) {
												 		if(!$.isNumeric(index)) {
												 			index = row["autoCode"];
												 		}
														var dfValue = "${(field.editProperties.defaultValue)!""}";
														if(!dfValue) {
															if(value) {
																dfValue = value;
															} else {
																dfValue = "<s:text name="${field.i18nKey!field.caption}"/>";
															}
														}
														return __getListHyperLink(value, "${field.key}", "${form.key}", index, dfValue);
												 	}
												 }
												</#if> 
												<#if field.dataProperties.dataType=='dtDouble'>
		                                        ,formatter: function(value,row,index) {
		                                        	if($.isNumeric(value)) {
		                                        		return accounting.formatNumber(value, ${(field.dataProperties.scale)!0}, "");
		                                        	}
	                                           	}                   		
		                                        </#if>
	                                            <#assign eventList = field.editProperties.eventList/>
	                                            <#if eventList?? && eventList?size gt 0>
	                                            	<#list eventList as event>
	                                            		<#if event.key == "onStyler">
	                                            		,styler: function(${event.params}) {
	                                            			${event.content}
	                                            		}
	                                            		<#elseif event.key == "onFormatter">
                                            			,formatter: function(${event.params}) {
                                            				${event.content}
                                            			}		
	                                            		</#if>
	                                            	</#list>
	                                            </#if> 
	                                        }<#if !appendCommaFirst>,</#if>
	                    </#if>                    
</#macro>
<#-- 初始化树形查询  -->
<#macro initTreeQuery field prefix="" entityName="" dgName="queryList">
						<#if field.key?? && field.key!="">
								<#assign fieldKey = prefix + field.key/>
																$("#queryFormId").hide();
																 $("#a_exp_clp").hide();
																	$("#a_switch_query").toggle(function() {
																		$("#queryFormId").show();
																		$("#a_exp_clp").show();
																		$("#_commonTypeTree_div_").hide();
																	}, function() {
																		$("#queryFormId").hide();
																		$("#a_exp_clp").hide();
																		$("#_commonTypeTree_div_").show();
																	});
																  $("#a_switch_query").show();
																  <#if field.editProperties.editType=="DictTree">
																  	<#assign dataType = "S">
																 	<#if field.dataProperties.dataType=='dtLong'>
																 	<#assign dataType = "L">
																    </#if>
																  doTreeQuery_${field.key} = function(ids, isInit) {
																  		var param = {"filter_IN${dataType}_${field.key}":ids.join(",")};
																  		$("#_commonTypeTree_div_").data("queryParam", param);
																  		var addParam = {"${field.key}":ids[0]};
																  		$("#_commonTypeTree_div_").data("addParam", addParam);											                
															            doQuery(null, isInit);
																  };
																  doTreeAfterSave_${field.key} = function(obj) {
																  	${fieldKey}Json = getJsonObjByParam(${fieldKey}JsonParam);
																  };
																  <#elseif field.editProperties.editType=="ComboTree">
																  	<@initControlsData field=field type="datagrid" prefix="tree_" entityName=entityName/>
																  	<#assign treeFieldKey = "tree_" + field.key />
																  	<#assign dataType = "S">
																 	<#if field.dataProperties.dataType=='dtLong'>
																 	<#assign dataType = "L">
																   	</#if>
																   	<#assign initFunc = "treeInit"/>
																   	<#if (field.editProperties.autoCollapse)!false >
																	   <#assign initFunc = "treeInitWithCollapse"/>
																	</#if> 
													                initCommonData("tree_${field.key}", ${treeFieldKey}JsonParam, ${treeFieldKey}Obj, ${initFunc}, doTreeQuery_${field.key});
													                function doTreeQuery_${field.key}(id){
																		var param = {"filter_IN${dataType}_${field.key}":id};
																		var ids = id.split(",");
																		var addParam = {"${field.key}":ids[0]};
																  		$("#_commonTypeTree_div_").data("addParam", addParam);															                
															            doQuery(param);
															        }
															      </#if>   
	                    </#if>                    
</#macro>
<#-- 初始化树形查询  -->
<#macro initFormTree form>
				<#assign fields=form.fieldList!>
                <#if fields??&&fields?size gt 0>
                    <#list fields as field>
	                        <#if (field.queryProperties.showInTreeSearch)!false>
	                        	<#if field.editProperties.editType=="DictTree">
	                        	<#if ((field.editProperties.dtFormat)!"")!=""> <#-- 系统分类树 -->
						   		<div id="_commonTypeTree_div_" issys="1"><div class="easyui-panel" href="${r"${ctx}"}/sys/common/dict-tree.action?type=${field.editProperties.dtFormat}&isSys=1&fname=doTreeQuery_${field.key}&isReadOnly=true&autoCollapse=${(field.editProperties.autoCollapse)?string}" border="0"></div></div>
						   		<#else>
						   		<div id="_commonTypeTree_div_"><div class="easyui-panel" href="${r"${ctx}"}/sys/common/dict-tree.action?type=${form.entityName}.${field.key}&fname=doTreeQuery_${field.key}&fsname=doTreeAfterSave_${field.key}&autoCollapse=${(field.editProperties.autoCollapse)?string}" border="0"></div></div>
						   		</#if>
	                        	<#elseif field.editProperties.editType=="ComboTree">
		                        <div id="_commonTypeTree_div_"><ul class="easyui-tree" id="tree_${field.key}" ></ul></div>
		                        </#if>
	                        </#if>
                    </#list>
                </#if>            
</#macro>
<#-- 是否存在树形查询  -->
<#macro encounterFormTree form>
	<#assign fields=form.fieldList!>
    <#if fields??&&fields?size gt 0>
        <#list fields as field>
            <#if (field.queryProperties.showInTreeSearch)!false>
            	<#if field.editProperties.editType=="DictTree"><#-- 系统分类树 -->
            	true
            	<#elseif field.editProperties.editType=="ComboTree">
                true
                </#if>
            </#if>
        </#list>
    </#if>
	false
</#macro>
<#-- 主表form初始化方法  -->
<#macro prepareForm form>
			         <#assign fields=form.fieldList!>
		             <#if fields??&&fields?size gt 0>
		                <#list fields as field>
		                  <#if field.key?? && field.key!="">
		                  	<#assign editType = field.editProperties.editType/>
		                    <#if editType=="ImageBox">
		                        $("#${field.key}").imagebox("setValue", jsonData["${field.key}"]);
					        <#elseif (editType=="DateBox" || field.dataProperties.dataType == "dtDateTime")>
					        	jsonData["${field.key}"] = fnFormatDate(jsonData["${field.key}"], <#if field.editProperties.dtFormat?? && field.editProperties.dtFormat != "">'${field.editProperties.dtFormat}'<#else>'yyyy-MM-dd'</#if>);
					        <#elseif editType=="ProgressBar">
		                        $("#${field.key}").processbarSpinner("setValue", jsonData["${field.key}"]);
		                    <#elseif editType=="HyperLink">
								jwpf.setFormVal("${field.key}", jsonData["${field.key}"]);    
					        <#elseif editType=="ComboBox">
					        	<#if field.editProperties.formula?? && field.editProperties.formula.type??>
									<#if field.editProperties.formula.type==2>
					             	 <#assign refSqlKey = (field.editProperties.formula.refKey)!""/>
					             	 <#if refSqlKey != "">
					             		<#assign sqlList = field.editProperties.sqlList/>
					             		<#if sqlList??&&sqlList?size gt 0>
					             			<#list sqlList as sql>
					             				<#if sql.key == refSqlKey>					        	
					        						<#if (sql.sqlWhereItemList)?? && 
														    (sql.sqlWhereItemList[0])??>
									        			<#assign selIdKey=sql.sqlWhereItemList[0].key />
									        			$("#${field.key}").data("selVal", jsonData["${field.key}"]);
									        			$("#${selIdKey}").combobox("setValue", jsonData["${selIdKey}"]);
									        		</#if>
									        		<#break/>
					        					</#if>
					        				</#list>
					        			</#if>
					        		 </#if>
					        		</#if>
					        	</#if>
					        	<#if field.dynamicProp??>
	 								<#assign dynamicProp = field.dynamicProp />
	 								<#if ((dynamicProp.dynamicTable)!false) || ((dynamicProp.dynamicColumn)!false) >
	 								$("#${field.key}").combobox("setValue", jsonData["${field.key}"]);
	 								</#if>
	 							</#if>
							<#elseif editType=="ComboBoxSearch">
					        	<#if field.editProperties.formula?? && field.editProperties.formula.type??>
									<#if field.editProperties.formula.type==2>
					             	 <#assign refSqlKey = (field.editProperties.formula.refKey)!""/>
					             	 <#if refSqlKey != "">
					             		<#assign sqlList = field.editProperties.sqlList/>
					             		<#if sqlList??&&sqlList?size gt 0>
					             			<#list sqlList as sql>
					             				<#if sql.key == refSqlKey>					        	
					        						<#if (sql.sqlWhereItemList)?? && 
														    (sql.sqlWhereItemList[0])??>
									        			<#assign selIdKey=sql.sqlWhereItemList[0].key />
									        			$("#${field.key}").data("selVal", jsonData["${field.key}"]);
									        			$("#${selIdKey}").comboboxsearch("setValue", jsonData["${selIdKey}"]);
									        		</#if>
									        		<#break/>
					        					</#if>
					        				</#list>
					        			</#if>
					        		 </#if>
					        		</#if>
					        	</#if>
					        	<#if field.dynamicProp??>
	 								<#assign dynamicProp = field.dynamicProp />
	 								<#if ((dynamicProp.dynamicTable)!false) || ((dynamicProp.dynamicColumn)!false) >
	 								$("#${field.key}").comboboxsearch("setValue", jsonData["${field.key}"]);
	 								</#if>
	 							</#if>
							</#if>	
						  </#if>	
		                </#list>
		             </#if>           
</#macro>
<#-- 子表form初始化方法  -->
<#macro prepareSubForm form>
	<#-- 
			         <#assign fields=form.fieldList!>
		             <#if fields??&&fields?size gt 0>
		                <#list fields as field>
		                  <#if field.key?? && field.key!="">
		                  	<#assign editType = field.editProperties.editType/>
					        <#if editType=="ComboBox">
					        	<#if field.editProperties.formula?? && field.editProperties.formula.type??>
									<#if field.editProperties.formula.type==2>
					             	 <#assign refSqlKey = (field.editProperties.formula.refKey)!""/>
					             	 <#if refSqlKey != "">
					             		<#assign sqlList = field.editProperties.sqlList/>
					             		<#if sqlList??&&sqlList?size gt 0>
					             			<#list sqlList as sql>
					             				<#if sql.key == refSqlKey>					        	
					        						<#if (sql.sqlWhereItemList)?? && 
														    (sql.sqlWhereItemList[0])??>
									        			<#assign selIdKey=sql.sqlWhereItemList[0].key />
									        			$("#${selIdKey}").combobox("setValue", jsonData["${selIdKey}"]);
									        		</#if>
									        		<#break/>
					        					</#if>
					        				</#list>
					        			</#if>
					        		 </#if>
					        		</#if>
					        	</#if>
							</#if>	
						  </#if>	
		                </#list>
		             </#if>     
	-->	                   
</#macro>
<#-- 初始化汇总统计事件  -->
<#macro initStatistics form isInit="true" tableKey=form.key >
	<#if form.extension??>
		<#if form.extension.queryPage??>
			<#assign stsList=form.extension.queryPage.stsList />
			<#if stsList?? && stsList?size gt 0>
				var rules = [];
				<#list stsList as sts>
				var rule = {"key":"${sts.key}","caption":"${sts.caption}","type":"${sts.order}","displayKey":"${sts.column}","dataType":"dtLong","value":"${sts.value}"};
				<#assign fields=form.fieldList!>
		        <#if fields??&&fields?size gt 0>
		           <#list fields as field>
		           		<#if field.key == sts.key>
		        rule["dataType"] = "${field.dataProperties.dataType}";
		           		  <#break/>
		           		</#if>
		           </#list>
		        </#if>
		        rules.push(rule);
				</#list>
				jwpf.setTableTotalVal("${tableKey}", rules, ${isInit});					      			
			</#if>
		</#if>          
	</#if>  
</#macro>
<#-- 初始化datagrid初始化参数  -->
<#macro initDatagridParam form>
	<#if form.extension??>
		<#if form.extension.queryPage??>
			<#assign eventList=form.extension.queryPage.eventList />
			<#if eventList?? && eventList?size gt 0>
                <#list eventList as event>
                     <#if event.key == "onRowStyler">
                     ,rowStyler: function(${event.params}){
                        try {
                        	${event.content}
                        } catch(e) {
                        	alert("执行表单${form.caption}(${form.key})事件${event.key}出现异常:" + e);
                        }
                     }
                     <#elseif event.key == "setQueryParam"> <#-- 设置查询参数 -->
                     ,queryParams : ${event.content}
                     <#elseif event.key == "showDetailView"> <#-- 设置详情视图 -->
                     ,view: detailview,
                     detailFormatter:function(index,row){
                        return '<div class="ddv" style="padding:5px 0"></div>';
                     },
                     onExpandRow: function(index,row){
                        var container = $(this).datagrid('getRowDetail',index).find('div.ddv');
                        <#-- var vUrl = (${form.detailViewUrl}) || "";
                        var str = '<iframe scrolling="auto" frameborder="0" src="' + vUrl + '" style="width:100%;height:100%;overflow:hidden;"></iframe>';
                        container.html(str);-->
                        try {
                        	${event.content}
                        } catch(e) {
                        	alert("设置表单${form.caption}(${form.key})详情视图出现异常:" + e);
                        }
                        $('#queryList').datagrid('fixDetailRowHeight',index);
                     }
                     <#elseif event.key != "onLoadSuccess" && event.key != "onBeforeStartProcess" 
                     		&& event.key != "onAfterStartProcess" && event.key != "onBeforeCancelProcess" && event.key != "onAfterCancelProcess"
                     		&& event.key != "onBeforeQuery" && event.key != "onAfterInitEdatagrid">
                     ,${event.key}:function(${event.params}) {
                     	try {
                        	${event.content}
                        } catch(e) {
                        	alert("执行表单${form.caption}(${form.key})事件${event.key}出现异常:" + e);
                        }
                     }
                     </#if>
                </#list>
            </#if>
            <#assign stsList=form.extension.queryPage.stsList />
			<#if stsList?? && stsList?size gt 0>
            		,showFooter:true
            </#if>
		</#if>          
	</#if>
	<#if form.height?? && form.height gt 0>
	,height:${form.height}
	</#if>
</#macro>
<#-- 初始化datagrid-onLoadSuccess事件  -->
<#macro initDatagridOnLoadSuccess form>
	<#if form.extension??>
		<#if form.extension.queryPage??>
			<#assign eventList=form.extension.queryPage.eventList />
			<#if eventList?? && eventList?size gt 0>
                <#list eventList as event>
                     <#if event.key == "onLoadSuccess">
                     	try {
                        	${event.content}
                        } catch(e) {
                        	alert("执行表单${form.caption}(${form.key})事件${event.key}出现异常:" + e);
                        }
                     	<#break/>
                     </#if>
                </#list>
            </#if>
		</#if>          
	</#if>
</#macro>
<#-- 初始化主表查询页面事件  -->
<#macro initFormQueryPageEvent form>
	<#if form.extension??>
		<#if form.extension.queryPage??>
			<#assign eventList=form.extension.queryPage.eventList />
			<#if eventList?? && eventList?size gt 0>
                <#list eventList as event>
                     <#if event.key == "onBeforeStartProcess" || event.key == "onAfterStartProcess" 
                     		|| event.key == "onBeforeCancelProcess" || event.key == "onAfterCancelProcess" || event.key == "onBeforeDelete"
                     		|| event.key == "onBeforeQuery">
                     	function ${event.key}(${event.params}) {
					 		try {
					 			${event.content}
					 		} catch(ex) {
					 			alert("执行事件${event.key}出现异常:" + ex);
					 		}
					 	};
                     </#if>
                </#list>
            </#if>
		</#if>          
	</#if>
</#macro>
<#-- 初始化子表编辑控件事件 -->
<#macro initFieldOnListEvent field formKey >
	<#if field.key?? && field.key!="">
	 <#assign editProp=field.editProperties!>
	 <#if editProp.eventList?? && editProp.eventList?size gt 0>
	 	<#list editProp.eventList as event>
	 	<#if event.key=="onListChange">
	 	,onChange:function(newVal, oldVal) {
	 		if(oldVal!=undefined && oldVal!=null && newVal != oldVal) {
	 			__doListChange(this, newVal, "${field.key}", "${formKey}");
	 		}
	 	}
	 	<#elseif event.key=="onListSelect">
	 	,onSelect:function(record) {
	 			var tableKey = "${formKey}";
		 		var fieldKey = "${field.key}";
		 		var fieldObj = $(this);
		 		var ftd = fieldObj.closest("td[field]");
		 		if(ftd) {
		 			var ftr = ftd.parent();
					var rowIndex = ftr.attr("datagrid-row-index") || ftr.attr("node-id");
					if(rowIndex != undefined && rowIndex != null) {
						try {
			                ${event.content}
			            } catch(e) {
			                alert("执行表单${formKey}属性${field.caption}(${field.key})事件${event.key}出现异常:" + e);
			            }
					}
				}
	 	}
	 	<#elseif event.key=="onListEnter">
	 	,onkeydown:function(event) { <#-- 回车事件 -->
	 		if(event.keyCode == 13) {
		 		var tableKey = "${formKey}";
		 		var fieldKey = "${field.key}";
		 		var fieldObj = $(event.target);
		 		var fieldVal = fieldObj.val();
		 		var ftd = fieldObj.closest("td[field]");
		 		if(ftd) {
		 			var ftr = ftd.parent();
					var rowIndex = ftr.attr("datagrid-row-index") || ftr.attr("node-id");
					if(rowIndex != undefined && rowIndex != null) {
						try {
			                ${event.content}
			            } catch(e) {
			                alert("执行表单${formKey}属性${field.caption}(${field.key})事件${event.key}出现异常:" + e);
			            }
					}
				}
			}
		}
	 	</#if>
	 	</#list>
      </#if>    
    </#if>	
</#macro>
<#-- 初始化主表、子表默认值  -->
<#macro initFormFieldDefaultValue form>
	<#if form.isMaster>
		<#assign fields=form.fieldList/>
	    <#if fields??&&fields?size gt 0>
	    <#list fields as field>
		<#if field.key?? && field.key!="">
		 <#assign editProp = field.editProperties!>
		 <#assign editType = editProp.editType/>
		 <#if editType != "Button" && editType != "HyperLink" && editType != "BrowserBox" && editType != "Label">
			 <#if (editProp.defaultValue)!="">
			 try {
			 	jwpf.setFormVal("${field.key}", ${editProp.defaultValue});
			 } catch(e) {alert("${field.caption}(${field.key})属性设置默认值出错："+e);}
			 </#if>
		 </#if>
	    </#if>      
	    </#list>
	   	</#if>
	<#else>
		function get${form.key}DefaultRow() {
			var defRow = {};
		<#assign fields=form.fieldList/>
	    <#if fields??&&fields?size gt 0>
	    <#list fields as field>
		<#if field.key?? && field.key!="">
		 <#assign editProp=field.editProperties!>
		 <#assign editType = editProp.editType/>
		 <#if editType != "Button" && editType != "HyperLink" && editType != "BrowserBox" && editType != "Label">
			 <#if (editProp.defaultValue)!="">
			 try {
				defRow["${field.key}"] = ${editProp.defaultValue};
			 } catch(e) {alert("${field.caption}(${field.key})属性设置默认值出错："+e);}
			 </#if>
		 </#if>
	    </#if>      
	    </#list>
	   	</#if>
	   		return defRow;
	   	}
	</#if>
</#macro>
<#-- 初始化主表iframe控件默认值  -->
<#macro initFormIframeDefaultValue form>
	<#if form.isMaster>
		<#assign fields=form.fieldList/>
	    <#if fields??&&fields?size gt 0>
	    <#list fields as field>
		<#if field.key?? && field.key!="">
		 <#assign editProp = field.editProperties!>
		 <#assign editType = editProp.editType/>
		 <#if editType == "BrowserBox">
			 <#if (editProp.defaultValue)!="">
			 try {
			 	var dfVal = ${editProp.defaultValue};
			 	$("#${field.key}").attr("src", dfVal);
			 } catch(e) {alert("${field.caption}(${field.key})属性设置默认值出错："+e);}
			 </#if>
		 </#if>
	    </#if>      
	    </#list>
	   	</#if>
	</#if>
</#macro>

<#-- 初始化datagrid工具条  -->
<#macro initDatagridToolBar form>
	<#if (form.showTools)!false >
		,toolbar:[     
                        {
                            id : 'bt_${form.key}_add',
                            text : '<s:text name="system.button.add.title"/>',
                            iconCls : 'icon-add',
                            handler : function() {
                                $("#${form.key}").edatagrid('addRow',get${form.key}DefaultRow());
                            }
                        },
                        {
                            id : 'bt_${form.key}_insert',
                            text : '<s:text name="system.button.insert.title"/>',
                            iconCls : 'icon-excelExport',
                            handler : function() {
                                <#if form.listType=="TreeGrid">
                                if(doSetTreeGridParam("${form.key}")) doTreeInsert();
                                <#else>
                                if(doSetDataGridParam("${form.key}")) doInsert();
                                </#if>
                            }
                        },
                        <#if form.listType=="TreeGrid">
                        <#else>
                        {
                            id : 'bt_${form.key}_copy',
                            text : '<s:text name="system.button.copy.title"/>',
                            iconCls : "icon-copy",
                            handler : function() {
                                if(doSetDataGridParam("${form.key}")) doCopy();
                            }
                        },
                        </#if>
                        {
                            id : 'bt_${form.key}_delete',
                            text : '<s:text name="system.button.delete.title"/>',
                            iconCls : 'icon-remove',
                            handler : function() {
                            	$('#mm').data('ChildTableIndex', "${form.key}");
                                <#if form.listType=="TreeGrid">
                                if(doSetTreeGridParam("${form.key}")) doTreeDelete();
                                <#else> 
                                if(doSetDataGridParam("${form.key}")) doDelete();
                                </#if>
                            }                           
                        }
                        <@initDatagridToolBarButton form=form />
                    ]
	</#if>
</#macro>

<#-- 检查是否存在需按值合并的列  -->
<#macro initMergeable form>
	<#assign fields=form.fieldList/>
    <#if fields??&&fields?size gt 0>
	    <#list fields as field>
			<#if field.queryProperties.mergeable?? && field.queryProperties.mergeable>
,mergeable:true
		    	<#break>
		    </#if>
	    </#list>
   	</#if>
</#macro>

<#-- 初始化自定义按钮 -->
<#macro initDatagridToolBarButton form>
<#if form.extension?? && form.extension.queryPage??>
									      		<#assign buttonList=form.extension.queryPage.buttonList />
									      		<#if buttonList?? && buttonList?size gt 0>
									      			<#list buttonList as button>
						<#if (button.enableAuthCheck)!false >
						<security:authorize url="buttonInQueryPage_${form.entityName}_${button.key}">
						</#if> 			      			
						,
                        {
                            id : '${button.key}',
							text : '${button.caption}',
                            iconCls : "${button.iconCls} enableInView", <#-- 默认在编辑和查看模式下都有效 -->
                            handler : function() {
                               <#if button.eventList?? && button.eventList?size gt 0>
									      					<#list button.eventList as event>
									      						<#if event.key == "onclick">
									      								try {
																 			${event.content}
																 		} catch(ex) {
																 			alert("执行${button.caption}(${button.key})属性事件${event.key}出现异常:" + ex);
																 		}
																 <#break/>
									      						</#if>
									      					</#list>
									      				</#if>
                            }
                        }		
                        <#if (button.enableAuthCheck)!false >
						</security:authorize>
						</#if> 	      				
									      			</#list>
									      		</#if>
						</#if>
</#macro>
<#-- 动态显示表设置  -->
<#macro initDynamicShowSubTable formList isAdd=true isDoAdd=false >
<#list formList as form>
	<#if form.isMaster>
		<#if !((form.dynamicShow)!false)>
			<#if isAdd>
			<#if isDoAdd>
			_loadSubCount_ = 0;
        	_initTimer_();
        	</#if>
			initAddSub();
			<#else>
			initSub(jsonData["id"]);
			</#if>
		</#if>
		<#break/>
	</#if>
</#list>
</#macro>

<#-- 动态显示属性设置 -->
<#macro initDynamicProp form >
	<#assign fields=form.fieldList/>
    <#if fields??&&fields?size gt 0>
    <#list fields as field>
	<#if field.key?? && field.key!="">
		<#if field.dynamicProp??>
	 	<#assign dynamicProp = field.dynamicProp />
	 	<#if dynamicProp.dynamicTable || dynamicProp.dynamicColumn>
	 		$("#${field.key}").combobox({onChange:function(newValue,oldValue){
	 			if(newValue) {
	 				var newVal = String(newValue);
	 				var tableList = null, columnListMap = {}, data=null;
				<#assign dataType='S'>
				<#if field.dataProperties.dataType=='dtInteger'>
					<#assign dataType='I'>
				<#elseif field.dataProperties.dataType=='dtLong'>
					<#assign dataType='L'>
				</#if>
		 		<#if dynamicProp.dynamicTable><#-- 动态显示子表 -->
		 			<#assign dynamicTableItem = (dynamicProp.dynamicTableItem) />
		 			<#if dynamicTableItem?? && dynamicTableItem.key??>
		 			data = jwpf.getFilterData("${dynamicTableItem.key}", {"filter_EQ${dataType}_${field.key}":newVal});
		 			if(data && data["list"]) {
		 				tableList = [];
		 				$.each(data["list"], function(i, v) {
		 					tableList.push(v["key"]);
		 				});
		 				_subTabCount_ = tableList.length;
		 				initDynamicTab(data["list"]);
		 			}
		 			</#if>
		 		</#if>
		 		<#if dynamicProp.dynamicColumn><#-- 动态显示子表字段 -->
		 			<#assign dynamicColumnItems = (dynamicProp.dynamicColumnItems) />
		 			<#if dynamicColumnItems?? && dynamicColumnItems?size gt 0>
		 				<#list dynamicColumnItems as dci>
		 					<#if dci.key?? && dci.value??>
		 			data = jwpf.getFilterData("${dci.key}", {"filter_EQ${dataType}_${field.key}":newVal});
		 			if(data && data["list"]) {
		 				var arr = [];
		 				$.each(data["list"], function(i, v) {
		 					arr.push(v["key"]);
		 				});
		 				columnListMap["${dci.value}"] = arr;
		 			}			
		 					</#if>
		 				</#list>
		 			</#if>
		 		</#if>
		 			_loadSubCount_ = 0;
                  	_initTimer_();
		 			if(operMethod == "add") {
		 				initAddSub(tableList, columnListMap);
		 			} else {
		 				initSub(jwpf.getId(), tableList, columnListMap);
		 			}
		 		}
	 		}}); 
	 	</#if>
	 	</#if>
    </#if>      
    </#list>
   	</#if>
</#macro>

<#-- 检查字段可见权限设置 -->
<#macro initCheckFieldAuth field form isEnd=false>
<#if isEnd>
	<#if ((field.editProperties.visibleCondition)!"")!="">
		</s:if>
	</#if>
	<#if ((form.enableFieldAuthCheck)!false) && ((field.enableFieldAuthCheck)!false)><#-- 字段权限校验 -->
		</security:authorize>
	</#if>
<#else>
	<#if ((form.enableFieldAuthCheck)!false) && ((field.enableFieldAuthCheck)!false)><#-- 字段权限校验 -->
		<security:authorize url="field_${form.entityName}.${field.key}.visible">
	</#if>
	<#if ((field.editProperties.visibleCondition)!"")!="">
		<s:if test='#session.USER.${field.editProperties.visibleCondition}'>
	</#if>
</#if>
</#macro>

<#-- 检查表单可见权限设置 -->
<#macro initCheckFormAuth form isEnd=false>
<#if isEnd>
	<#if ((form.visibleCondition)!"")!="">
		</s:if>
	</#if>
	<#if (form.enableFieldAuthCheck)!false><#-- 字段权限校验 -->
		</security:authorize>
	</#if>
<#else>
	<#if (form.enableFieldAuthCheck)!false><#-- 字段权限校验 -->
		<security:authorize url="form_${form.entityName}">
	</#if>
	<#if ((form.visibleCondition)!"")!="">
		<s:if test='#session.USER.${form.visibleCondition}'>
	</#if>
</#if>
</#macro>

<#-- 检查子表字段只读权限设置 -->
<#macro initCheckFieldReadOnlyAuth field form isReadOnly=true>
<#if (field.editProperties.readOnly)!false>
	<#if isReadOnly>
	readonly:true,	
	<#else>
	disabled:true,	
	</#if>
<#else>
	<#if ((field.enableFieldAuthCheck)!false)><#-- 字段权限校验 -->
		<security:authorize url="field_${form.entityName}.${field.key}.readonly">
		<#if isReadOnly>
		readonly:true,	
		<#else>
		disabled:true,	
		</#if>
		</security:authorize>
	</#if>
	<#if field.editProperties.editType=="ComboTree">editable:true,</#if>
</#if>
</#macro>

<#-- 检查主表字段只读权限设置 -->
<#macro initCheckMasterFieldReadOnlyAuth field form>
<#--
<#assign editType = field.editProperties.editType />
<#assign dataType = field.dataProperties.dataType />
<#if (field.editProperties.readOnly)!false>
	<#if editType=="ComboBox" || editType=="ComboTree" 
		|| editType=="CheckBox" || editType=="RadioBox" || editType=="ComboCheckBox" 
		|| editType=="ComboRadioBox" || editType=="ComboGrid"
		|| (editType=="TextBox" && (dataType=="dtLong" || dataType=="dtInteger" || dataType=='dtDouble'))>
	disabled="true"
	<#elseif editType=="DateBox">
	onfocus="return false;"
	<#else>
    readonly="true"
	</#if>
<#else>
	<#if ((field.enableFieldAuthCheck)!false)>
		<security:authorize url="field_${form.entityName}.${field.key}.readonly">
		<#if editType=="ComboBox" || editType=="ComboTree" 
			|| editType=="CheckBox" || editType=="RadioBox" || editType=="ComboCheckBox" 
			|| editType=="ComboRadioBox" || editType=="ComboGrid"
			|| (editType=="TextBox" && (dataType=="dtLong" || dataType=="dtInteger" || dataType=='dtDouble'))>
		disabled="true"
		<#elseif editType=="DateBox">
		onfocus="return false;"
		<#else>
	    readonly="true"
		</#if>
		</security:authorize>
	</#if>
</#if>
-->
</#macro>
<#-- 设置表单只读字段只读 -->
<#macro setFormFieldReadOnly form>
		<#assign fields=form.fieldList/>
	    <#if fields??&&fields?size gt 0>
	    <#list fields as field>
			<#if field.key?? && field.key!="">
			 	<#if (field.editProperties.readOnly)!false>
			 		jwpf.setFormFieldReadonly("${field.key}", true);
			 	<#else>
			 		<#if ((field.enableFieldAuthCheck)!false)>
					<security:authorize url="field_${form.entityName}.${field.key}.readonly">
					jwpf.setFormFieldReadonly("${field.key}", true);
					</security:authorize>
					</#if>
			 	</#if>
		    </#if>      
	    </#list>
	   	</#if>      
</#macro>

<#-- 初始化编辑页面主表CSS或者JS脚本代码 -->
<#macro initFormCssOrJavascript formList eventKey="addJavaScript">
<#list formList as form>
	      <#if form.isMaster>
	      	<#if form.extension?? && form.extension.editPage??>
	      		<#assign eventList=form.extension.editPage.eventList />
	      			 <#if eventList?? && eventList?size gt 0>
	 					<#list eventList as event>
	 						<#if event.key == eventKey> 
	 							${event.content}
	 							<#break/>
	 						</#if>
	 					</#list>
      				</#if>  
	      		</#if>          
	     </#if>    
</#list>
</#macro>

<#-- 初始化查询页面主表CSS或者JS脚本代码 -->
<#macro initQueryPageFormCssOrJavascript formList eventKey="addJavaScript">
<#list formList as form>
	      <#if form.isMaster>
	      	<#if form.extension?? && form.extension.queryPage??>
	      		<#assign eventList=form.extension.queryPage.eventList />
	      			 <#if eventList?? && eventList?size gt 0>
	 					<#list eventList as event>
	 						<#if event.key == eventKey> 
	 							${event.content}
	 							<#break/>
	 						</#if>
	 					</#list>
      				</#if>  
	      		</#if>          
	     </#if>    
</#list>
</#macro>

<#-- 初始化字段后缀 -->
<#macro initFieldSuffix field isInList=true ><#if ((field.editProperties.fieldSuffix)!"")!=""><#if isInList >(${field.editProperties.fieldSuffix})<#else><span>${field.editProperties.fieldSuffix}</span></#if></#if></#macro>

<#-- 初始化主表Tab查询  -->
<#macro initFormTabQuery form isInitTab=true jsonPrefix="dg">
				<#if (form.showStatusInTab)!false><#-- 系统状态tab页显示 -->
					<#if isInitTab>
					<div region="north" border="false" style="text-align:right;height:28px;overflow:hidden;">
						<ul id="ul_tab_query" class="tabs"></ul>
					</div>
					<#else>
					var statusArr = [{"key":"00,10","caption":"草稿"},{"key":"20","caption":"审批中"},{"key":"30,31,32,35,40","caption":"已审批"}];
					initTabQuery(statusArr, {"key":"key","caption":"caption"}, "filter_INS_status");
					</#if>
				<#else><#-- 其他字段tab页查询 -->
					<#assign fields=form.fieldList!>
	                <#if fields??&&fields?size gt 0>
	                    <#list fields as field>
	                      <#if field.key?? && field.key!="">
		                   <#if (field.queryProperties.showInTabSearch)!false>
		            <#if isInitTab>
					<div region="north" border="false" style="text-align:right;height:28px;overflow:hidden;">
						<ul id="ul_tab_query" class="tabs"></ul>
					</div>
					<#else>
						<#assign dataType='S'>
						<#if field.dataProperties.dataType=='dtInteger'>
						<#assign dataType='I'>
						<#elseif field.dataProperties.dataType=='dtLong'>
						<#assign dataType='L'>
						</#if>
					initTabQuery(${jsonPrefix}_${field.key}Json, ${jsonPrefix}_${field.key}Obj, "filter_IN${dataType}_${field.key}");
					</#if>              	
		                   <#break/>
		                   </#if>
		                  </#if>
	                    </#list>
	                </#if>     
				</#if>   
</#macro>

<#-- 初始化主表页面datagrid工具条  -->
<#macro initFormDatagridToolBar form isUserForm = false >
	<#assign isAppendToolBar=false><#-- 是否添加工具条 -->
	<#if form.extension?? && form.extension.queryPage??>
						<#assign buttonList=form.extension.queryPage.buttonList />
						<#if buttonList?? && buttonList?size gt 0>
						<#assign isAppendToolBar=true>
						,toolbar:['-'
						<#list buttonList as button>
						,
                        {
                            text : '${button.caption}',
                            iconCls : "${button.iconCls} enableInView",
                            handler : function() {
                               <#if button.eventList?? && button.eventList?size gt 0>
									      					<#list button.eventList as event>
									      						<#if event.key == "onclick">
									      								try {
																 			${event.content}
																 		} catch(ex) {
																 			alert("执行${button.caption}(${button.key})属性事件${event.key}出现异常:" + ex);
																 		}
																 <#break/>
									      						</#if>
									      					</#list>
									      				</#if>
                            }
                        }			      				
						</#list>
						</#if>
	</#if>
	<#if form.enableExport>
    	<#if !isAppendToolBar>
    	,toolbar:['-'
    	</#if>
    	,
         {
                        	id : 'bt_excelExport',
                            text : '<s:text name="system.button.exportExcel.title"/>',
                            iconCls : "icon-excelExport",
                            handler : function() {
                               doExportExcel("${form.entityName}", getExportParams("${form.key}"), ""<#if isUserForm>,true</#if>);
                            }
         }
        <#if !isAppendToolBar>
    	]
    	</#if>
    </#if>
    <#if isAppendToolBar>
    	]
    </#if>
</#macro>
<#-- 初始化datagrid-onDblClickRow事件  -->
<#macro initDatagridOnDblClickRow form>
	<#if form.extension??>
		<#if form.extension.queryPage??>
			<#assign eventList=form.extension.queryPage.eventList />
			<#if eventList?? && eventList?size gt 0>
                <#list eventList as event>
                     <#if event.key == "onDblClickRow">
                     	try {
                        	${event.content}
                        } catch(e) {
                        	alert("执行列表${form.caption}(${form.key})事件${event.key}出现异常:" + e);
                        }
                     	<#break/>
                     </#if>
                </#list>
            </#if>
		</#if>          
	</#if>
</#macro>
<#-- 初始化查看页面自定义按钮 isReportOnly--只有报表按钮有效,isDisplayReport--是否显示报表按钮 -->
<#macro initButtonOnViewPage entityName moduleKey formList isDisplayReport=true isReportOnly=false >
									<#list formList as form>
									      <#if form.isMaster>
									      	<#if form.extension?? && form.extension.editPage??>
									      		<#assign buttonList=form.extension.editPage.buttonList />
									      		<#if buttonList?? && buttonList?size gt 0>
									      			<#list buttonList as button>
									      				<#if isDisplayReport && button.key == "bt_report">
									      				<security:authorize url="/sys/report/report.action?entityName=${entityName}">
								                            <a id="bt_report" href="javascript:void(0);" onclick="doViewReport('${moduleKey}',null,window.${button.key}_onclick)" plain="true" iconCls="icon-report"
								                         <#if button.buttonType == "menu">
									      					class="easyui-menubutton" menu="#mm_${button.key}"
									      				 <#else>	
									      				    class="easyui-linkbutton"
									      				 </#if>
								                            ><s:text name="system.button.report.title"/>(R)</a>
								                        </security:authorize>
									      				<#elseif !isReportOnly && button.key != "bt_process">
									      					<#if (button.enableAuthCheck)!false >
									      					<security:authorize url="buttonInViewPage_${form.entityName}_${button.key}">
									      					</#if> 
									      					 <a id="bt_${button.key}" href="javascript:void(0);" onclick="${button.key}_onclick()" plain="true" iconCls="${button.iconCls}"
									                         <#if button.buttonType == "menu">
										      					class="easyui-menubutton userAddBt" menu="#mm_${button.key}"
										      				 <#else>	
										      				    class="easyui-linkbutton userAddBt"
										      				 </#if>
									                            >${button.caption}</a>
									                        <#if (button.enableAuthCheck)!false >
									                        </security:authorize>  
									                        </#if>  
									      				</#if>
									      			</#list>
									      		</#if>
									      	</#if>          
									      </#if>    
									</#list>
</#macro>
<#-- 子表字段唯一性检查 -->
<#macro getValidTypeInDatagrid field form>
	<#assign uniqueType = "">
	<#if field.editProperties.editType=='TextBox'>
		 <#assign dataType = "S">
		 <#if field.dataProperties.dataType=='dtLong'>
		 	<#assign dataType = "L">
		 <#elseif field.dataProperties.dataType=='dtInteger'>
		 	<#assign dataType = "I">
		 <#elseif field.dataProperties.dataType=='dtDouble'>
		 	<#assign dataType = "N">
		 <#elseif field.dataProperties.dataType=='dtText'>
		 	<#assign dataType = "S">
		 </#if>
		 <#if (field.dataProperties.unique!false)>
		    <#assign tmp = ""/>
		    <#if field.editProperties.uniqueFilterMaster><#assign tmp = ",'masterId'"/></#if>
		 	<#assign uniqueType = "uniqueSub['" + r"${ctx}" + "/gs/gs-mng!checkUnique.action?entityName=" + form.entityName + "','filter_EQ" + dataType + "_" + field.key + "','" + form.key + "','id'" + tmp + "]">
		 </#if>
	</#if>
 	<#if uniqueType != "">
 		 ,validType:"${uniqueType}"
 	</#if>
</#macro>
<#-- 初始化查看页面下拉菜单按钮 -->
<#macro initButtonMenuOnViewPage formList>
									<#list formList as form>
									      <#if form.isMaster>
									      	<#if form.extension?? && form.extension.editPage??>
									      		<#assign buttonList=form.extension.editPage.buttonList />
									      		<#if buttonList?? && buttonList?size gt 0>
									      			<#list buttonList as button>
									      				<#if button.buttonType == "menu">
									      				<#if (button.enableAuthCheck)!false >
									      				<security:authorize url="buttonInViewPage_${form.entityName}_${button.key}">
									      				</#if>
									      				<div id="mm_${button.key}" style="width:150px;">
									      					<#if button.buttonList?? && button.buttonList?size gt 0 >
									      						<#list button.buttonList as bt>
									      						    <#if (bt.enableAuthCheck)!false >
												      				<security:authorize url="buttonInViewPage_${form.entityName}_${bt.key}">
												      				</#if>
									      							<div iconCls="${bt.iconCls}" onclick="${bt.key}_onclick()">${bt.caption}</div>
									      							<#if (bt.enableAuthCheck)!false >
												      				</security:authorize>
												      				</#if>
									      						</#list>
									      					</#if>
									      				</div>
									      				<#if (button.enableAuthCheck)!false >
									      				</security:authorize>
									      				</#if>
									      				</#if>
									      			</#list>
									      		</#if>
									      	</#if>          
									      </#if>    
									</#list>
</#macro>
<#-- 初始化datagrid详情展开参数  -->
<#macro initDatagridDetailViewParam form>
	<#if ((form.detailViewUrl)!"") != "">
					,view: detailview,
                    detailFormatter:function(index,row){
                        return '<div class="ddv" style="padding:5px 0"></div>';
                    },
                    onExpandRow: function(index,row){
                        var ddv = $(this).datagrid('getRowDetail',index).find('div.ddv');
                        var vUrl = (${form.detailViewUrl}) || "";
                        var str = '<iframe scrolling="auto" frameborder="0" src="' + vUrl + '" style="width:100%;height:100%;overflow:hidden;"></iframe>';
                        ddv.html(str);
                        $('#queryList').datagrid('fixDetailRowHeight',index);
                    }		
	</#if>
</#macro>